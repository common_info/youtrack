#!/usr/bin/python
# -*- coding: cp1251 -*-
import weakref

from numpy.core import datetime64
from yt_requests import MultipleConfig, User, Issue, IssueWorkItem, multiple_config
from xl_table import _StyleList


class _GlobalProperties:
    is_exists = False

    def __init__(self, version: str = "1.1.1"):
        if not _GlobalProperties.is_exists:
            self.version: str = version
            self.multi_config: MultipleConfig = multiple_config()
            self.styles = _StyleList
            _GlobalProperties.is_exists = True
        else:
            print("_GlobalProperties already exists.")

    def __repr__(self):
        return f"{self.version}"

    def __bool__(self):
        return True

    def child_call(self):
        return self




class _InternalProperties(_GlobalProperties):
    def __init__(self, login: str, version: str = "1.1.1"):
        _GlobalProperties.__init__(self, version)
        self.timestamp = datetime64("now", "s")
        self.login = login

    @property
    def parent(self) -> _GlobalProperties:
        return super().child_call()

    def __repr__(self):
        return f"<_InternalProperties(version={self.timestamp.item()}, user={self.login})>"

    def __str__(self):
        return f"version = {self.version}, timestamp = {self.timestamp.item().isoformat()}, user = {str(self.login)}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"������ {self.version}, �����: {self.timestamp.item().isoformat()}"
        if format_spec == "internal":
            return f"_InternalProperties: version {self.version}, timestamp: {self.timestamp.item().isoformat()}"

    # def parent(self):
    #     return weakref.ref(global_properties)()

    # def __getattribute__(self, item):
    #     return object.__getattribute__(self.)


def main():
    global_properties = _GlobalProperties()

    # print(weakref.ref(global_properties)().version)
    # print(_InternalProperties.__dict__)
    #
    print(id(global_properties))
    # print(global_properties.is_exists)
    internal = _InternalProperties("test")
    print(internal.__dict__)
    # print(internal.__get__(global_properties, _GlobalProperties))
    # print(internal.is_exists)
    # print(internal.version)
    print(id(internal.parent))


if __name__ == "__main__":
    main()
