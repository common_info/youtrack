#!/usr/bin/python
# -*- coding: cp1251 -*-
from datetime import datetime
from pathlib import Path
from typing import Union
from numpy.core import datetime64, arange, ndarray
from yt_requests import Issue, IssueWorkItem, User, MultipleConfig, multiple_config
from const_values import today, DICT_LOGINS_USERS, DICT_START_RENAME_FILES, DICT_END_RENAME_FILES, first_day, \
    current_year
from xl_table import ExcelProp, get_excel_prop
from join_user_xl import JoinUserXL
from xl_style import _StyleList


def rename_file(path_dir: Path, name_replace: str, name_sub: str):
    """
    Rename the file since the surname is not the login in the YouTrack.

    :param path_dir: the path to the directory with files
    :type path_dir: Path
    :param str name_replace: the file name to replace
    :param str name_sub: the file name to substitute
    :return: None.
    """
    path_file: Path = path_dir.joinpath(name_replace)
    if path_file.exists() and path_file.is_file():
        path_file.rename(path_file.with_name(name_sub))
        print(f"Файл {name_replace} переименован в {name_sub}.")
    else:
        print(f"Объект {name_replace} не переименован, поскольку не существует или не является файлом.")
    return


def sort_work_items(
        dict_work_items: dict[str, list[IssueWorkItem]],
        start_day: datetime64,
        end_day: datetime64) -> dict[datetime64, list[IssueWorkItem]]:
    """
    Sort the issue work items by the work date.

    :param dict_work_items: the dictionary of the issue work items
    :type dict_work_items: dict[str, list[IssueWorkItem]]
    :param datetime64 start_day: the start day
    :param datetime64 end_day: the end day
    :return: the sorted dictionary.
    :rtype: dict[datetime64, list[IssueWorkItem]]
    """
    days: ndarray = arange(start_day, end_day, dtype='datetime64[D]')
    all_work_items: list[IssueWorkItem] = [value for values in dict_work_items.values() for value in values]
    sorted_work_items: dict[datetime64, list[IssueWorkItem]] = dict()
    for day in days:
        if not any(work_item.date == day for work_item in all_work_items):
            continue
        else:
            sorted_work_items[day] = []
            for work_item in all_work_items:
                if work_item.date == day:
                    sorted_work_items[day].append(work_item)
    return sorted_work_items


def print_sorted_work_items(dict_sorted: dict[datetime64, list[IssueWorkItem]]) -> tuple[str]:
    """
    Output the issue work items sorted by the work date.

    :param dict_sorted: the dictionary of the sorted issue work items
    :type dict_sorted: dict[datetime64, list[IssueWorkItem]]
    :return: the lines to output.
    :rtype: tuple[str]
    """
    lines: list[str] = []
    for key, values in dict_sorted.items():
        lines.append(f"Дата: {key.item().isoformat()}")
        lines.extend([format(work_item, "date") for work_item in values])
    return *lines,


def delete_files(path_dir: Path, names: list[Union[str, Path]]):
    # TODO
    if not path_dir.is_dir():
        print(f"Путь {path_dir} указывает не на директорию.")
        return
    for name in names:
        path_file: Path = path_dir.joinpath(name)
        try:
            path_file.unlink(missing_ok=False)
        except FileNotFoundError:
            print(f"Файл {path_file} не найден.")
        else:
            print(f"Файл {path_file} удален.")
        finally:
            continue


def main():
    datetime_start = datetime.now()
    # specify the multiple config
    multiconfig: MultipleConfig = multiple_config()
    path_dir: Path = multiconfig.path_dir
    # rename the files
    for old_file_name, new_file_name in DICT_START_RENAME_FILES.items():
        rename_file(path_dir, old_file_name, new_file_name)
    list_users: list[User] = multiconfig.all_users()
    styles: _StyleList = _StyleList()
    for user in list_users:
        user_timer_start = datetime.now()
        lines: list[str] = []
        path: Path = multiconfig.path_table(user.login)
        path_new_file: Path = path.with_name(f"generated_{path.name}")
        # specify the Excel file with the report
        excel_prop: ExcelProp = get_excel_prop(path, styles)
        # specify the JoinUserXL instance
        join_user_xl: JoinUserXL = JoinUserXL(user, excel_prop)
        # get the full information from the YouTrack and work with the IssueWorkItem instances
        print("----------")
        lines.append("----------")
        print(f"Пользователь: {DICT_LOGINS_USERS[user.login]}")
        lines.append(f"Пользователь: {DICT_LOGINS_USERS[user.login]}")
        user.pre_processing()
        # print the information from the YouTrack
        print("Задачи:")
        lines.append("Задачи:")
        issue: Issue
        for issue in user.dict_issue.values():
            print(format(issue, "output"))
            lines.append(format(issue, "output"))
        print("----------")
        lines.append("----------")
        print("Затраченное время по задачам:")
        lines.append("Затраченное время по задачам:")
        issue_work_item: IssueWorkItem
        for issue_item_name, issue_items in user.dict_issue_work_item.items():
            print(f"Задача: {issue_item_name}")
            lines.append(f"Задача: {issue_item_name}")
            for issue_work_item in issue_items:
                print(format(issue_work_item, "short"))
                lines.append(format(issue_work_item, "short"))
        # print the issue work item information sorted by the work date
        print("----------")
        lines.append("----------")
        print("Затраченное время по датам:")
        lines.append("Затраченное время по датам:")
        # sort the issue work items
        sorted_work_items = sort_work_items(user.dict_issue_work_item, first_day(), today())
        for item in print_sorted_work_items(sorted_work_items):
            print(item)
        lines.extend(print_sorted_work_items(sorted_work_items))
        # print the current issues
        print("----------")
        lines.append("----------")
        print("Не закрытые задачи:")
        lines.append("Не закрытые задачи:")
        for issue in user.current_issues():
            print(format(issue, "output"))
            lines.append(format(issue, "output"))
        # print the issues with the deadline date expired
        print("----------")
        lines.append("----------")
        print("Задачи с истекшим крайним сроком (дедлайном):")
        lines.append("Задачи с истекшим крайним сроком (дедлайном):")
        for issue in user.expired_issues():
            print(format(issue, "output"))
            lines.append(format(issue, "output"))
        # delete the empty rows and join the rows with the same issue name
        excel_prop.pre_processing()
        # add new issues to the table
        join_user_xl.add_all_issues()
        print("Задачи добавлены.")
        # update the TableRow instances since the rows are changed
        excel_prop.update_table_cells()
        # modify the issues in the table
        join_user_xl.modify_all_issues()
        excel_prop.update_table_cells()
        print("Параметры задач (название, родительская задача, дедлайн) обновлены.")
        # update the TableRow instances since the rows are changed
        excel_prop.update_table_cells()
        # update the issue state in the table
        join_user_xl.update_states()
        print("Статусы задач обновлены.")
        excel_prop.update_table_cells()
        # add the IssueWorkItems to the table
        join_user_xl.add_new_work_items()
        print("Затраченное время добавлено.")
        # add cell styles, formulae, and hyperlinks
        excel_prop.post_processing()
        print("Пост-обработка завершена.")
        # save the changes, create the new file, and close it
        excel_prop.close(path_new_file)

        print("Работа завершена. Путь до файла:")
        print(f"{path.resolve()}")

        txt_file = path.with_name(f"data_{user.login}.txt")
        final_data = "\n".join(lines)
        with open(txt_file, "w+") as file:
            file.write(final_data)

        user_timer_end = datetime.now()
        user_time = user_timer_end - user_timer_start
        print(f"{user.login}: {str(user_time)}")
    # rename the file
    for old_file_name, new_file_name in DICT_END_RENAME_FILES.items():
        rename_file(path_dir, old_file_name, new_file_name)

    # TODO
    files_delete = list(filter(lambda x: x.stem.startswith(f"{current_year}_report_"), multiconfig.path_dir.iterdir()))
    delete_files(path_dir, files_delete)

    # TODO
    files_rename = list(filter(lambda x: x.stem.startswith("generated_"), multiconfig.path_dir.iterdir()))
    for file in files_rename:
        new_file_name: str = file.stem.replace("generated_", "")
        rename_file(path_dir, file.stem, new_file_name)

    datetime_end = datetime.now()
    work_time = datetime_end - datetime_start
    print(f"Время работы: {str(work_time)}")
    input("Нажмите любую клавишу, чтобы закрыть окно...")


if __name__ == "__main__":
    main()
