from functools import cached_property
from pathlib import Path
import json
import requests
from requests import Response
from decimal import Decimal, ROUND_CEILING, ROUND_FLOOR
from json.decoder import JSONDecodeError
from typing import Union, Optional, Iterable, NamedTuple
from numpy.core import datetime64, isnat, divide
from numpy.core.fromnumeric import sum as np_sum
from requests.exceptions import RequestException, HTTPError, ConnectionError, InvalidURL, InvalidHeader
from collections import Counter
from const_values import convert_long_date, today, STATES_NOT_COMPLETED, STATES_NOT_COMPLETED_JOIN, \
    STATES_COMPLETED_JOIN, HEADERS_REQUEST, period, RequestURL, RequestFields, ASSIGNEES_PROJECT, \
    CONVERSION_TABLE, ASSIGNEES_EXTENDED, ResponseType


__doc__ = """
Functions:
    request(url, params) --- combine and send the GET request; if improper, print the error;\n
    convert_date_iso(input_date) --- convert different date formats to the ISO standard;\n
    parse_response_work_item(response_item) --- get the parameters from the response item;\n
    parse_response_issue(response_item) --- parse the response in the dict format to get the issue;\n
    get_current_states(issues) --- get the current states of the issues;\n
"""


def replace_unicode_chars(text: Optional[str]):
    # TODO
    if text is None:
        return
    if any(key in text for key in CONVERSION_TABLE):
        for key, value in CONVERSION_TABLE.items():
            if key in text:
                text = text.replace(key, value)
    return text


class RequestYT(NamedTuple):
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        url --- the webhook to send the API request, str;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET.

    Functions:
         send_request() --- send the request to the YouTrack and get the response in the JSON format,
         list[dict] or None;\n
         parse_response() --- handle the response to represent as the RequestResult objects,
         list[RequestResult].
    """
    fields: list[str]
    query: list[str]
    custom_fields: Optional[list[str]]
    request_type: RequestURL = None
    headers: dict[str, str] = HEADERS_REQUEST
    method: str = "get"
    non_standard_custom_fields: dict[str, str] = None

    @property
    def url(self):
        return self.request_type.value

    def params(self) -> tuple[tuple[str, str], ...]:
        fields_param = ",".join(self.fields)
        query_param = " AND ".join(self.query)
        custom_fields_param: list[tuple[str, str]] = [
            ("customFields", custom_field) for custom_field in self.custom_fields]
        if self.non_standard_custom_fields:
            non_standard_custm_fields = [
                (f"{key}", f"{value}") for key, value in self.non_standard_custom_fields.items()]
            params: list[tuple[str, str]] = [
                ("fields", fields_param),
                ("query", query_param),
                *custom_fields_param,
                *non_standard_custm_fields,
            ]
        else:
            params: list[tuple[str, str]] = [
                ("fields", fields_param),
                ("query", query_param),
                *custom_fields_param,
            ]
        return *params,

    def __repr__(self):
        return f"<RequestYT(params={self.params}, url={self.url}, headers={self.headers}, method={self.method})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYT: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    def send_request(self) -> Optional[list[dict]]:
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            response: Response = requests.request(self.method, url=self.url, params=self.params, headers=self.headers)
        except InvalidURL as e:
            raise InvalidURL(f"{e.errno}, {e.strerror}. Некорректный URL запроса.")
        except InvalidHeader as e:
            raise InvalidHeader(f"{e.errno}, {e.strerror}. Некорректные HTTP-заголовки.")
        except ConnectionError as e:
            raise ConnectionError(f"{e.errno}, {e.strerror}. Попытка соединение не удалась.")
        except HTTPError as e:
            raise HTTPError(f"{e.errno}, {e.strerror}. Базовая ошибка HTTP-запроса.")
        except RequestException as e:
            raise RequestException(f"{e.errno}, {e.strerror}. Базовая ошибка запроса.")
        except OSError as e:
            raise OSError(f"{e.errno}, {e.strerror}. Общая ошибка системы.")
        except JSONDecodeError as e:
            raise JSONDecodeError(f"Не удалось обработать ответ в формате JSON.", f"{e.doc}", e.pos)
        else:
            return response.json()


class ResponseParserSelector:
    def __init__(
            self,
            response_item: list[dict],
            parsed_instance: ResponseType):
        self.response_item = response_item
        self.parsed_instance = parsed_instance.value

    def parse_response(self):
        if self.parsed_instance == ResponseType.ISSUE.value:
            return ResponseParserIssue(self.response_item, self.parsed_instance).parse_response()
        elif self.parsed_instance == ResponseType.WORK_ITEM.value:
            return ResponseParserWorkItem(self.response_item, self.parsed_instance).parse_response()
        else:
            return


class ResponseParserIssue(ResponseParserSelector):
    def parse_response(self):
        final_response = []
        for unit in self.response_item:
            # specify the issue name
            issue: str = unit["idReadable"]
            # specify the parent issue name
            parent_issues = unit["parent"]["issues"]
            parent: Optional[str]
            # check if the parent issue exists
            if len(parent_issues):
                parent: Optional[str] = parent_issues[0]["idReadable"]
            else:
                parent = None
            # specify the issue summary
            summary: str = unit["summary"]
            deadline: datetime64 = datetime64("NaT")
            state: str = ""
            for item in unit["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = convert_long_date(item["value"])
                else:
                    continue
            final_response.append(Issue(issue, state, replace_unicode_chars(summary), parent, deadline))
        return final_response


class ResponseParserWorkItem(ResponseParserSelector):
    def parse_response(self):
        final_response = []
        for unit in self.response_item:
            # define the issue name of the work item
            issue: str = unit['issue']['idReadable']
            # define the date of the work item
            date: datetime64 = convert_long_date(unit['date'])
            # define the spent time of the work item
            spent_time: int = unit['duration']['minutes']
            # convert to the hours
            modified_spent_time: Decimal = Decimal(divide(spent_time, 60)).quantize(Decimal("1.00"), ROUND_CEILING)
            # define the comment
            comment: str = unit['text']
            final_response.append(IssueWorkItem(issue, date, modified_spent_time, replace_unicode_chars(comment)))
        return final_response


class MultipleConfig:
    """
    Specify the config with several logins.

    Params:
        path --- the path to the configuration file;\n
        json_file --- the serialized json file content;\n

    Properties:
        login --- the list of logins;\n
        path_dir --- the path to the directory with files;\n

    Functions:
        full_user_config() --- specify all user configs;\n
        path_table(login) --- get the path to the report;\n
        user_config(login) --- specify the user config;\n
    """

    def __init__(self, path: Union[str, Path], json_file: dict[str, Union[str, list[str]]]):
        self.path = Path(path).resolve()
        self.json_file = json_file

    def __str__(self):
        return f"MultipleConfig: {str(self.path)}"

    def __repr__(self):
        return f"<MultipleConfig(path={str(self.path)})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"MultipleConfig({str(self.path)})"

    @cached_property
    def login(self) -> list[str]:
        """
        Get the logins in the dict.

        :return: the logins.
        :rtype: list[str] or None
        """
        return [login for login in self.json_file["login"]]

    @property
    def path_dir(self) -> Path:
        """
        Get the path to the directory with the files.

        :return: the path to the directory.
        :rtype: Path
        """
        return Path(self.json_file["path_dir"]).resolve()

    def all_users(self):
        """
        Specify the user configs for all logins.

        :return: the list of user configs.
        :rtype: list[User]
        """
        return [User(login) for login in self.login]

    def path_table(self, login: str) -> Path:
        """
        Get the path to the report.

        :param str login: the login to get the report path
        :return: the path to the Excel file.
        :rtype: Path
        """
        return self.path_dir.joinpath(f"2022_report_{login}.xlsx")


def multiple_config() -> Optional[MultipleConfig]:
    """
    Specify the user configurations.

    :return: the user configurations.
    :rtype: list[UserConfig]
    """
    try:
        path = Path.cwd().resolve().joinpath("youtrack.json")
        with open(path, "r") as file:
            json_file = json.load(file)
    except JSONDecodeError as e:
        print(f"JSONDecodeError: {e.pos}, {e.lineno}, {e.msg}. Не удалось обработать файлы JSON.")
        return None
    except FileNotFoundError as e:
        print(f"FileNotFoundError: {e.errno}, {e.strerror}. Файл не найден.")
        return None
    except RuntimeError as e:
        print(f"RuntimeError: {str(e)}. Истекло время работы программы.")
        return None
    except PermissionError as e:
        print(f"PermissionError: {e.errno}, {e.strerror}. Доступ запрещен.")
        return None
    except ValueError as e:
        print(f"ValueError: {str(e)}. Некорректный путь до файла.")
        return None
    except KeyError as e:
        print(f"KeyError: {str(e)}. Некорректная загрузка файла.")
        return None
    except OSError as e:
        print(f"OSError: {e.errno}, {e.strerror}. Общая ошибка системы.")
        return None
    else:
        return MultipleConfig(path, json_file)


class Issue(NamedTuple):
    """
    Define the Issue entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        state --- the issue state, state;\n
        summary -- the issue short description, summary;\n
        parent --- the parent issue name, parent;\n
        deadline --- the issue deadline, deadline;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    state: str
    summary: str
    parent: str = None
    deadline: datetime64 = datetime64("NaT")

    def __str__(self):
        if self.parent is None:
            parent_string = ""
        else:
            parent_string = f", parent = {self.parent}"
        if isnat(self.deadline):
            deadline_string = ""
        else:
            deadline_string = f", deadline = {self.deadline.item().isoformat()}"
        return f"Issue: issue = {self.issue}, state = {self.state}, summary = " \
               f"{self.summary} {parent_string}{deadline_string}"

    def __repr__(self):
        return f"<Issue(issue={self.issue}, state={self.state}, summary={self.summary}, " \
               f"parent={self.parent}, deadline={self.deadline})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            if self.parent is None:
                parent_string = ""
            else:
                parent_string = f", родительская задача: {self.parent}"
            if isnat(self.deadline):
                deadline_string = ""
            else:
                deadline_string = f", дедлайн: {self.deadline.item().isoformat()}"
            return f"Задача: {self.issue}, статус: {self.state}, описание: {self.summary}" \
                   f"{parent_string}{deadline_string};"


class IssueWorkItem(NamedTuple):
    """
    Define the IssueWorkItem entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        date --- the issue work item date, datetime64;\n
        spent_time -- the issue work item recorded time in minutes, int;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    date: datetime64
    spent_time: Decimal
    comment: str = None

    def __str__(self):
        if self.comment:
            comment_string = f", comment = {self.comment}"
        else:
            comment_string = ""
        return f"IssueWorkItem: issue = {self.issue}, date = {self.date.item().isoformat()}, " \
               f"spent time = {self.spent_time}{comment_string}"

    def __repr__(self):
        return f"<IssueWorkItem(issue={self.issue}, date={self.date}, spent_time={self.spent_time}, " \
               f"comment={self.comment})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"Задача: {self.issue}, дата: {self.date.item().isoformat()}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;"
        if format_spec == "short":
            if self.comment:
                comment_string = f" комментарий = {self.comment};"
            else:
                comment_string = ""
            return f"Дата: {self.date.item().isoformat()}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;{comment_string}"
        if format_spec == "date":
            return f"Задача: {self.issue}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;"


class User:
    """
    Define the User entity to send the YouTrack requests.

    Params:
        dict_issue --- the dictionary of Issue instances and issue names;\n
        dict_issue_work_item --- the dictionary of IssueWorkItem instances and issue names;\n
        user_config --- the UserConfig item;\n

    Properties:
        _non_unique --- the dictionary of the non-unique work items;\n
        login --- the user login;\n
        query_assignees --- the query params for assignees_project fields;\n

    Functions:
        _join_work_items() --- combine the non-unique work items;\n
        current_issues() --- get the current non-closed issues sorted by name;\n
        expired_issues() --- get the issues with the expired dealine;\n
        get_completed_issues() --- get the completed issues with no work items;\n
        get_current_issues() --- get the Issue instances with no work items;\n
        get_current_states(issues) --- get the current issue states;\n
        get_issue_work_items() --- get the IssueWorkItem instances;\n
        get_issues() --- get the Issue instances;\n
        issue_names() --- get the issue names;\n
        not_completed_issues() --- get the current non-closed issues;\n
        pre_processing() --- prepare all information from the YouTrack;\n
    """

    def __init__(self, login: str):
        self.login = login
        self.dict_issue: dict[str, Issue] = dict()
        self.dict_issue_work_item: dict[str, list[IssueWorkItem]] = dict()

    def __str__(self):
        return f"User: login = {self.login}"

    def __repr__(self):
        return f"<User(login={self.login})>"

    def __hash__(self):
        return hash(self.login)

    def __eq__(self, other):
        if isinstance(other, User):
            return self.login == other.login
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, User):
            return self.login != other.login
        else:
            return NotImplemented

    def __bool__(self):
        return not bool(self.login)

    @cached_property
    def query_assignees(self) -> str:
        """
        Get the query params for assignees_project fields.

        :return: the string for the request.
        :rtype: str
        """
        query_assignees = " OR ".join([f"{param}: {self.login}" for param in ASSIGNEES_PROJECT])
        return "".join(("(", query_assignees, ")"))

    @cached_property
    def query_assignees_extended(self) -> str:
        # TODO
        query_assignees_extended = " OR ".join([f"{param}: {self.login}" for param in ASSIGNEES_EXTENDED])
        return "".join(("(", query_assignees_extended, ")"))

    @property
    def _non_unique(self) -> dict[str, list[datetime64]]:
        """
        Get the non-unique work items.

        :return: the dictionary of the issue names and dates.
        :rtype: dict[str, list[datetime64]]
        """
        non_unique: dict[str, list[datetime64]] = dict()
        issue_name: str
        for issue_name, work_items in self.dict_issue_work_item.items():
            counter = Counter([work_item.date for work_item in work_items])
            # get non-unique dates
            non_unique_date = [key for key, value in counter.items() if value > 1]
            if non_unique_date:
                non_unique[issue_name] = non_unique_date
        return non_unique

    def _join_work_items(self):
        """Join the non-unique work items."""
        for issue, dates in self._non_unique.items():
            for date in dates:
                work_item: IssueWorkItem
                # cumulative sum
                cum_spent_time = np_sum(
                    [work_item.spent_time for work_item in self.dict_issue_work_item[issue]
                     if work_item.issue == issue and work_item.date == date])
                #  in case the type of cum_spent_time is not Decimal
                if isinstance(cum_spent_time, Decimal):
                    final_spent_time = cum_spent_time
                else:
                    final_spent_time = Decimal(cum_spent_time.item()).normalize()
                # specify the reversed list of item indexes to delete
                indexes = sorted(
                    [index for index, work_item in enumerate(self.dict_issue_work_item[issue])
                     if work_item.date == date], reverse=True)
                # delete useless items
                for index in indexes:
                    del self.dict_issue_work_item[issue][index]
                # add the cumulative sum of the work items to the dict
                comment = "\n".join(
                    [work_item.comment for work_item in self.dict_issue_work_item[issue] if work_item.comment])
                issue_work_item = IssueWorkItem(issue, date, final_spent_time, comment)
                self.dict_issue_work_item[issue].append(issue_work_item)

    def current_issues(self) -> list[Issue]:
        """
        Get the current non-closed issues sorted by name.

        :return: the sorted issues.
        :rtype: list[Issue]
        """
        return sorted(self.not_completed_issues(), key=lambda item: item.issue)

    def expired_issues(self) -> list[Issue]:
        """
        Get the issues with the expired dealine.

        :return: the issues.
        :rtype: list[Issue]
        """
        return sorted(
            [issue for issue in self.not_completed_issues() if not isnat(issue.deadline) and issue.deadline <= today()],
            key=lambda item: item.issue)

    def get_completed_issues(self):
        """Get the completed Issue instances with no IssueWorkItem instances."""
        fields = [
            "idReadable", "summary", "parent(issues(idReadable,reporter(fullName))),reporter(fullName)",
            "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"]
        query = [
            f"State: {STATES_COMPLETED_JOIN}",
            f"updated: {period()}",
            "has: -work",
            f"{self.query_assignees_extended}"]
        custom_fields = ["State", "Дедлайн"]
        request_type = RequestURL.ISSUE
        request = RequestYT(fields, query, custom_fields, request_type)

        # get the response in the JSON format
        parsed_response: ResponseParserSelector = ResponseParserSelector(request.send_request(), ResponseType.ISSUE)
        final_response = parsed_response.parse_response()
        for issue in final_response:
            self.dict_issue[issue.issue] = issue

    def get_current_issues(self):
        """Get the non-closed Issue instances with no IssueWorkItem instances."""
        fields = [
            "idReadable", "summary", "parent(issues(idReadable,reporter(fullName))),reporter(fullName)",
            "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"]
        query = [f"State: {STATES_NOT_COMPLETED_JOIN}", f"{self.query_assignees_extended}"]
        custom_fields = ["State", "Дедлайн"]
        request = RequestYT(fields, query, custom_fields, RequestURL.ISSUE)
        # get the response in the JSON format
        parsed_response: ResponseParserSelector = ResponseParserSelector(request.send_request(), ResponseType.ISSUE)
        final_response = parsed_response.parse_response()
        for issue in final_response:
            self.dict_issue[issue.issue] = issue

    def get_issue_work_items(self):
        """Get the IssueWorkItem instances."""
        # define the parameters of the request
        fields = ["duration(minutes)", "date", "issue(idReadable)", "text"]
        query = [f"work author: {self.login}", f"work date: {period()}"]
        custom_fields = None
        non_standard_custom_fields = {
            "startDate": "2022-01-01",
            "endDate": "2022-08-04",
            "author": self.login
        }
        request = RequestYT(
            fields, query, custom_fields, RequestURL.WORK_ITEM, non_standard_custom_fields=non_standard_custom_fields)

        parsed_response: ResponseParserSelector = ResponseParserSelector(request.send_request(), ResponseType.WORK_ITEM)
        final_response = parsed_response.parse_response()
        for issue_work_item in final_response:
            if issue_work_item.issue not in self.dict_issue_work_item:
                self.dict_issue_work_item[issue_work_item.issue] = []
            self.dict_issue_work_item[issue_work_item.issue].append(issue_work_item)

    def get_issues(self):
        """Get the Issue instances."""
        if not self.dict_issue_work_item:
            return None
        else:
            issue_names = ",".join([issue for issue in self.dict_issue_work_item])
            fields = [
                "idReadable", "summary", "parent(issues(idReadable,reporter(fullName))),reporter(fullName)",
                "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"]
            query = [f"issue ID: {issue_names}"]
            custom_fields = ["State", "Дедлайн"]
            request = RequestYT(fields, query, custom_fields, RequestURL.ISSUE)
            # get the response in the JSON format
            parsed_response: ResponseParserSelector = ResponseParserSelector(request.send_request(), ResponseType.ISSUE)
            final_response = parsed_response.parse_response()
            for issue in final_response:
                self.dict_issue[issue.issue] = issue

    def issue_names(self) -> set[str]:
        """
        Get the issue names.

        :return: the issue names.
        :rtype: set[str]
        """
        return set(self.dict_issue)

    def not_completed_issues(self):
        """
        Get the current non-closed issues.

        :return: the open issues.
        :rtype: list[Issue]
        """
        return [issue for issue in self.dict_issue.values() if issue.state in STATES_NOT_COMPLETED]

    def pre_processing(self):
        """Get all YouTrack information."""
        self.get_issue_work_items()
        self.get_issues()
        self.get_current_issues()
        self.get_completed_issues()
        self._join_work_items()
