# youtrack



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/common_info/youtrack.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/common_info/youtrack/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Информация в файле data_\<login\>.txt

\<login\> -- логин пользователя на YouTrack (User.login).<br>
В файл data_\<login\>.txt записывается следующая информация:
```
----------
Пользователь: <фамилия>
Задачи:
Задача: <задача>, статус: <статус>, описание: <описание>;
Задача: <задача>, статус: <статус>, описание: <описание>, дедлайн: <ГГГГ-ММ-ДД>;
Задача: <задача>, статус: <статус>, описание: <описание>, родительская задача: <задача>;
Задача: <задача>, статус: <статус>, описание: <описание>, родительская задача: <задача>, дедлайн: <ГГГГ-ММ-ДД>;
----------
Затраченное время по задачам:
Задача: <задача>
Дата: <ГГГГ-ММ-ДД>, затраченное время: <#0.00> ч, <#0> мин;
----------
Затраченное время по датам:
Дата: <ГГГГ-ММ-ДД>:
Задача: <задача>, затраченное время: <#0.00> ч, <#0> мин;

```
* секция Общая информация:
  * `<фамилия>` -- фамилия пользователя, указанная на YouTrack (_User.fullName[0]_);
  * `<задача>` -- отображаемый номер задачи на YouTrack (_Issue.idReadable_), состоит из краткого названия проекта (_Issue.project.shortName_) и идентификатора задачи (_Issue.id_);
  * `<статус>` -- статус задачи на YouTrack (_Issue.State_);
  * `<описание>` -- краткое описание задачи на YouTrack, указываемое рядом с номером задачи (_Issue.summary_);
  * `дедлайн` -- крайний срок выполнения задачи, указанный на YouTrack (_Issue.Дедлайн_). [Формат](#format_date): `ГГГГ-ММ-ДД`;<br>
**Примечание.** Может отсутствовать, в этом случае не указывается при перечислении задач.
  * `родительская задача` -- задача, указанная в разделе _"Подзадача"_ на YouTrack (_Issue.parent.idReadable_);<br>
**Примечание.** Может отсутствовать, в этом случае не указывается при перечислении задач.
* секция Время по задачам:
  * `<задача>` -- отображаемый номер задачи на YouTrack (_Issue.idReadable_), состоит из краткого названия проекта (_Issue.project.shortName_) и идентификатора задачи (_Issue.id_);
  * `дата` -- дата, к которому привязана запись о затраченном времени на YouTrack (_IssueWorkItem.date_). [Формат](#format_date): `ГГГГ-ММ-ДД`;
  * `затраченное время` -- количество времени, затраченное на задачу, из записи на YouTrack (_IssueWorkItem.duration_). [Формат](#format_spent_time): 
    * `<#0.00>` для времени в часах;
    * `<#0>` для времени в минутах.
* секция Время по датам:
  * `дата` -- дата, к которому привязана запись о затраченном времени на YouTrack (_IssueWorkItem.date_). [Формат](#format_date): `ГГГГ-ММ-ДД`;
  * `<задача>` -- отображаемый номер задачи на YouTrack (_Issue.idReadable_), состоит из краткого названия проекта (_Issue.project.shortName_) и идентификатора задачи (_Issue.id_);
  * `затраченное время` -- количество времени, затраченное на задачу, из записи на YouTrack (_IssueWorkItem.duration_). [Формат](#format_spent_time): 
    * `<#0.00>` для времени в часах;
    * `<#0>` для времени в минутах.

Пример:
```
----------
Пользователь: Тарасов
Задачи:
Задача: DOC-1, статус: Done, описание: Описание задачи DOC-1;
Задача: DOC_ST-1, статус: New, описание: Описание задачи DOC_ST-1, дедлайн: 2000-01-01;
Задача: ARCH-1, статус: Active, описание: Описание задачи ARCH-1, родительская задача: ARCH-2;
Задача: ARCH_ST-1, статус: Closed, описание: Описание задачи ARCH_ST-1, родительская задача: ARCH_ST-2, дедлайн: 2000-12-31;
----------
Затраченное время по задачам:
Задача: ARCH-1
Дата: 2000-10-12, затраченное время: 8.50 ч, 510 мин;
----------
Затраченное время по датам:
Дата: 2000-10-12:
Задача: ARCH-1, затраченное время: 8.50 ч, 510 мин;
```

### Форматы записи

При записи в файл data_\<login\>.txt используются следющие форматы даты и времени:
* формат записи даты<a name="format_date"></a>: `ГГГГ-ММ-ДД`:
  * ГГГГ -- год, записанный четьрьмя цифрами;
  * ММ -- месяц, записанный двумя цифрами;
  * ДД -- день, записанный двумя цифрами.
* формат записи времени<a name="format_spent_time"></a>:
  * `<#0.00>` -- число с плавающей точкой с двумя знаками после запятой, задает количество времени в часах;
  * `<#0>` -- целое число, задает количество времени в минутах.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Лицензия
Лицензия "MIT License", свободное использование, изменение без уведомления создателя оригинального продукта без ограничений.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
