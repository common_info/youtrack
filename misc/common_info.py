import re
from enum import Enum
from re import Pattern
import re
import logging
from logging import Logger
from typing import Sequence, Union, Iterable

logger: Logger = logging.getLogger(__name__)


IP_PATTERN: Pattern = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")

ZONES: dict[str, list[str, ...]] = {
    "149.255.118.70": ["PCSCF", "ICSCF", "SCSCF", "MKD", "SBC", "SSW4", "TAS"],
    "192.168.110.116": ["SN2"],
    "192.168.110.189": ["Protei_SB", "Protei_EIR"],
    "192.168.110.19": ["SMSFW"],
    "192.168.126.153": ["Protei_CAPL", "Protei_SigtranTester", "Protei_STP", "Protei_CAPL_extSCP"],
    "192.168.126.186": [
        "Protei_IPSMGW", "Protei_SRF", "Protei_SCL", "Protei_SMPP_BS", "Protei_STP", "Protei_SCL_GIS", "Protei_SMSC",
        "Protei_SMSC_2"],
    "192.168.126.3": [
        "GLR_API", "Protei_HSS", "Protei_SC_Lite", "HLR_Load", "Protei_ITG", "protei_srf", "HSS_API", "Protei_SS7FW",
        "protei_mv_radius", "MI_API", "Protei_OTA_SMS", "Protei_STP", "CLI-Server", "Protei_CAPL", "Protei_RDS",
        "Protei_STP2", "Protei_DRA", "Protei_RG", "Protei_UDM", "Protei_GMSC", "SB"],
    "192.168.77.54": ["SS7FW"],
    "192.168.110.234": ["SN2"]
}

contents: list[str] = [
    "CSI", "bash", "cpu_mallinfo", "data types", "Diameter error codes", "Diameter AVP types", "directories",
    "log file description", "requests", "signalling R1.5, R.2", "zones"
]


class CommonInfo:
    def __init__(self, zones: dict[str, list[str, ...]] = None):
        if zones is None:
            zones = {}
        self.zones: dict[str, list[str, ...]] = zones
        for k, v in ZONES:
            self.zones.__setitem__(k, v)

    def __all_values(self):
        return {item for key in self.zones.keys() for item in self.zones.get(key)}

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.zones.keys()
        else:
            return item in self.__all_values()

    def __iter__(self):
        return iter(self.zones.keys())

    def __getitem__(self, key):
        if key in self.zones.keys():
            return self.zones.__getitem__(key)
        else:
            logger.error(f"KeyError, the key {key} is not found", exc_info=True)
            raise KeyError()

    def __setitem__(self, key, value):
        if not re.fullmatch(IP_PATTERN, key):
            logging.error(f"KeyError, the key {key} is not an IP address")
            return
        if key not in self:
            self.zones.__setitem__(key, list())
        if isinstance(value, str):
            self.zones[key].append(value)
            return
        elif isinstance(value, Iterable):
            if all(isinstance(item, str) for item in value):
                self.zones[key].extend([item for item in value])
            else:
                logger.error(f"ValueError, the value {value} is improper")
                return
        else:
            logger.error(f"TypeError, the value {value} must be str or Iterable[str], not {type(value)}")




def main():
    pass


if __name__ == "__main__":
    main()
