import re
from re import Pattern, Match


REPLACE_TABLE = {
    "long": "int"
}

NO_QUOTES = ("int", "bool")


def replace(text: str):
    for key, value in REPLACE_TABLE.items():
        if text == key:
            return value
        else:
            continue
    return text


class Line:
    def __init__(self, text: str):
        self.text = text

    def pattern_match(self, pattern: Pattern):
        match: Match = re.match(pattern, self.text)
        return match

    @property
    def object_type(self):
        pattern: Pattern = re.compile(r"<([a-zA-Z]+)")
        return self.pattern_match(pattern).group(1)

    def pattern_findall(self, pattern: Pattern):
        match: list[Match] = re.findall(pattern, self.text)
        return match

    @property
    def attrs(self):
        pattern: Pattern = re.compile(r"([a-zA-z_]+=\"[a-zA-Z]+\")")
        return self.pattern_findall(pattern)

    @property
    def parsed_attrs(self):
        list_replace: list[tuple[str, str]] = []
        pattern: Pattern = re.compile(r"([a-zA-Z_]+)=\"([a-zA-Z]+)\"")
        for item in self.attrs:
            match: Match = re.match(pattern, str(item))
            list_replace.append((match.group(1), replace(match.group(2))))
        return list_replace

    @property
    def attrs_xml(self):
        lines = []
        for param, value in self.parsed_attrs:
            if value in NO_QUOTES:
                lines.append(f'<{param}>{value}</{param}>')
            else:
                lines.append(f'<{param}>\"{value}\"</{param}>')
        return "\n".join(lines)

    @property
    def first_line(self):
        return f"<{self.object_type}>"

    @property
    def final_line(self):
        return f"</{self.object_type}>"

    def convert(self):
        return "\n".join((self.first_line, self.attrs_xml, self.final_line))


def main():
    user_input = input("Text, please:\n")
    line = Line(user_input)
    print(line.convert())


if __name__ == "__main__":
    main()
