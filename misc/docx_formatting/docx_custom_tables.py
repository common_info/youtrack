from typing import Union
from docx.enum.table import WD_ROW_HEIGHT_RULE
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
# noinspection PyProtectedMember
from docx.section import _Header, _Footer
# noinspection PyProtectedMember
from docx.table import Table, _Column, _Row
# noinspection PyProtectedMember
from lxml.etree import _Element
from docx_custom_header_footer import LiteralDirection
from docx_header_footer_table import _CustomHeaderFooterTable, _CustomCell


class CustomShortHeaderTable(_CustomHeaderFooterTable):
    """
    The short header in the last section. Columns:\n

    | Справ. № | Подпись и дата
    """
    _instance = None
    _row_height: tuple[int, ...] = (3402, 3402)
    _column_width: tuple[int, ...] = (284, 397)
    _count_rows = len(_row_height)
    _count_cols = len(_column_width)
    _table_width = sum(_column_width)

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, **kwargs)
        return cls._instance

    def __init__(self, section_index: int, **kwargs):
        if self.__class__._instance is None:
            header: _Header = super()._parent.sections[section_index].header
            table: Table = header.add_table(self._count_rows, self._count_cols, self._table_width)
            self._parent: _CustomHeaderFooterTable = super().__init__(section_index, "header", table, **kwargs)
            self._header: _Header = header
            self._table: Table = table
            self.direction: LiteralDirection = "vertical"
            self._instance = self
        return

    def __call__(self, *args, **kwargs):
        return self._instance

    def __repr__(self):
        return "<CustomShortHeaderTable()>"

    def __str__(self):
        return "CustomShortHeaderTable"

    def _merge_cells_specified(self, **kwargs):
        return NotImplemented

    def _merge_cells(self):
        return NotImplemented

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self._table.columns, self._column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self._table.rows, self._row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position.items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    @property
    def _header_footer_position(self, **kwargs):
        return super()._header_footer_position(tblpX=438, tblpY=273)

    @property
    def _table_text(self):
        return {
            (1, 0): "Справ. №",
            (0, 0): "Подпись и дата"
        }

    def set_cell_text(self):
        paragraph_style = super()._parent.styles["Table_11_pt_Centered"]
        for cell_coord, text in self._table_text:
            row_idx, col_idx = cell_coord
            custom_cell: _CustomCell = self.custom_cell(row_idx, col_idx)
            custom_cell.set_cell_text(paragraph_style)


class CustomOddVerticalFooterTable(_CustomHeaderFooterTable):
    _instance = None
    _row_height = (1985, 1418, 1418, 1985, 1418)
    _column_width = (284, 397)
    _count_rows = len(_row_height)
    _count_cols = len(_column_width)
    _table_width = sum(_column_width)

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, **kwargs)
        return cls._instance

    def __init__(self, section_index: int, **kwargs):
        if self._instance is None:
            footer: _Footer = super()._parent.sections[section_index].footer
            table: Table = footer.add_table(self._count_rows, self._count_cols, self._table_width)
            self._parent: _CustomHeaderFooterTable = super().__init__(section_index, "header", table, **kwargs)
            self._footer: _Footer = footer
            self._table: Table = table
            self.direction: LiteralDirection = "vertical"
            self._instance = self
        return

    def __call__(self, *args, **kwargs):
        return self.__class__._instance

    def __repr__(self):
        return "<CustomOddFooterTable()>"

    def __str__(self):
        return "CustomOddFooterTable"

    def _merge_cells_specified(self, **kwargs):
        return NotImplemented

    def _merge_cells(self):
        return NotImplemented

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self._table.columns, self._column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self._table.rows, self._row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    @property
    def _header_footer_position(self, **kwargs) -> dict[str, Union[str, int]]:
        return super()._header_footer_position(tblpX=438, tblpY=8257)

    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position.items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    @property
    def _table_text(self):
        return {
            (4, 0): "Подпись и дата",
            (3, 0): "Инв. № дубл.",
            (2, 0): "Взам. инв. №",
            (1, 0): "Подпись и дата",
            (0, 0): "Инв. № подл."
        }

    def set_cell_text(self, *args):
        paragraph_style = super()._parent.styles["Table_11_pt_Centered"]
        for cell_coord, text in self._table_text:
            row_idx, col_idx = cell_coord
            custom_cell: _CustomCell = self.custom_cell(row_idx, col_idx)
            custom_cell.set_cell_text(paragraph_style)


class CustomOddHorizontalFooterTable(_CustomHeaderFooterTable):
    _instance = None
    _row_height: tuple[int, ...] = (284, 284, 284)
    _column_width: tuple[int, ...] = (509, 563, 1273, 838, 649, 6029, 572)
    _count_rows: int = len(_row_height)
    _count_cols: int = len(_column_width)
    _table_width: int = sum(_column_width)
    _merge_cells: list[tuple[tuple[int, int], tuple[int, int]]] = [
        ((1, 0), (2, 0)), ((0, 1), (2, 1))
    ]
    _cells_properties: list[tuple[int, int]] = [(1, 1)]

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, **kwargs)
        return cls._instance

    def __init__(self, section_index: int, **kwargs):
        if self._instance is None:
            header: _Header = super()._parent.sections[section_index].footer
            table: Table = header.add_table(self._count_rows, self._count_cols, self._table_width)
            self._parent = super().__init__(section_index, "header", table, **kwargs)
            self._header = header
            self._table = table
            self._instance = self
            self.direction: LiteralDirection = "horizontal"
        return

    def __call__(self, *args, **kwargs):
        return self._instance

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self._table.columns, self._column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self._table.rows, self._row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position.items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    def __repr__(self):
        return "<CustomOdd_FooterTable()>"

    def __str__(self):
        return "CustomOdd_FooterTable"

    @property
    def _header_footer_position(self, **kwargs):
        return super()._header_footer_position(tblpX=336, tblpY=15707)

    @property
    def _table_text(self):
        return {
            (0, 0): "Стр.",
            (2, 2): "Изм.",
            (2, 3): "Лист",
            (2, 4): "№ докум.",
            (2, 5): "Подп.",
            (2, 6): "Дата"
        }

    def set_cell_text(self, *args):
        paragraph_style = super()._parent.styles["Table_11_pt_Centered"]
        for cell_coord, text in self._table_text:
            row_idx, col_idx = cell_coord
            custom_cell: _CustomCell = self.custom_cell(row_idx, col_idx)
            custom_cell.set_cell_text(paragraph_style)
        return self

    def set_doc_properties(self):
        for cell_coord in self._cells_properties:
            row_idx, col_idx = cell_coord
            cell: _CustomCell = self.custom_cell(row_idx, col_idx)
            cell.add_doc_properties()

    def table_handle(self, **kwargs):
        super().table_handle().set_cell_sizes().set_cell_text()
        self.set_cell_sizes()


class CustomMajorFooter(_CustomHeaderFooterTable):
    def set_cell_text(self, *args):
        pass

    _instance = None
    _row_height: tuple[int, ...] = (794, 454, 284, 284, 284, 284, 284, 284, 284, 284)
    _column_width: tuple[int, ...] = (498, 807, 1342, 998, 674, 818, 2801, 283, 284, 284, 851, 852)
    _count_rows: int = len(_row_height)
    _count_cols: int = len(_column_width)
    _table_width: int = sum(_column_width)
    _merge_cells: list[tuple[tuple[int, int], tuple[int, int]]] = [
        ((0, 0), (1, 4)),
        ((0, 7), (0, 11)), ((1, 7), (1, 11)),
        ((2, 5), (4, 11)),
        ((5, 0), (5, 1)), ((6, 0), (6, 1)), ((7, 0), (7, 1)), ((8, 0), (8, 1)), ((9, 0), (9, 1)),
        ((5, 5), (9, 6)),
        ((5, 7), (5, 9)),
        ((7, 7), (9, 11))
    ]

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, **kwargs)
        return cls._instance

    def __init__(self, section_index: int, **kwargs):
        if self._instance is None:
            header: _Header = super()._parent.sections[section_index].footer
            table: Table = header.add_table(self._count_rows, self._count_cols, self._table_width)
            self._parent = super().__init__(section_index, "header", table, **kwargs)
            self._header = header
            self._table = table
            self._instance = self
        return

    def __call__(self, *args, **kwargs):
        return self._instance

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self._table.columns, self._column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self._table.rows, self._row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position.items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    @property
    def _header_footer_position(self, **kwargs):
        return super()._header_footer_position(tblpX=-283, tblpY=12985)

    @property
    def _table_text(self):
        return {
            (4, 0): "Изм.",
            (4, 1): "Лист",
            (4, 2): "№ докум.",
            (4, 3): "Подп.",
            (4, 4): "Дата",
            (5, 0): "Разраб.",
            (6, 0): "Пров.",
            (7, 0): "М. экспертиза",
            (8, 0): "Н. контр.",
            (9, 0): "Утв.",
            (7, 7): "Лит.",
            (7, 10): "Лист",
            (7, 11): "Листов",
            (8, 10): "2"
        }


class CustomEvenHorizontalFooterTable(_CustomHeaderFooterTable):
    def set_cell_text(self, *args):
        pass

    _instance = None
    _row_height: tuple[int, ...] = (284, 284, 284)
    _column_width: tuple[int, ...] = (509, 563, 1273, 838, 649, 6029, 572)
    _count_rows: int = len(_row_height)
    _count_cols: int = len(_column_width)
    _table_width: int = sum(_column_width)

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, **kwargs)
        return cls._instance

    def __init__(self, section_index: int, **kwargs):
        if self._instance is None:
            header: _Header = super()._parent.sections[section_index].footer
            table: Table = header.add_table(self._count_rows, self._count_cols, self._table_width)
            self._parent = super().__init__(section_index, "header", table, **kwargs)
            self._header = header
            self._table = table
            self._instance = self
        return

    def __call__(self, *args, **kwargs):
        return self._instance

    def __repr__(self):
        return "<CustomEvenHorizontalFooterTable()>"

    def __str__(self):
        return "CustomEvenHorizontalFooterTable"

    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position.items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self._table.columns, self._column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self._table.rows, self._row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    @property
    def _header_footer_position(self, **kwargs):
        return super()._header_footer_position(tblpX=438, tblpY=8257)

    @property
    def _table_text(self):
        return {
            (2, 0): "Изм.",
            (2, 1): "Лист",
            (2, 2): "№ докум.",
            (2, 3): "Подп.",
            (2, 4): "Дата",
            (0, 6): "Стр."
        }
