import logging
from logging import Logger
from typing import Optional, Union, Any
from docx.dml.color import MSO_COLOR_TYPE
from docx.document import Document
from docx.enum.dml import MSO_THEME_COLOR_INDEX
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_LINE_SPACING, WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt
# noinspection PyProtectedMember
from docx.styles.style import _TableStyle, _ParagraphStyle, _CharacterStyle, _NumberingStyle
from docx.styles.styles import Styles

from docx_document import _CustomDocument, _CustomBaseDocument

__all__ = ["_CustomStyleFactory", "DocStyle"]

style_logger: Logger = logging.getLogger(__name__)

DocStyle = Union[_ParagraphStyle, _TableStyle, _CharacterStyle, _NumberingStyle]


class _CustomStyleFactory(_CustomDocument):
    _parent: Document = _CustomBaseDocument().document
    styles: Styles = _parent.styles
    _instance = None

    def __init__(
            self,
            name: str,
            style_type: WD_STYLE_TYPE,
            builtin: bool):
        super().__init__()
        self.name = name
        self.style_type = style_type
        self.builtin = builtin

    def __repr__(self):
        return f"<_CustomStyleFactory(name={self.name}, style_type={self.style_type}, builtin={self.builtin})>"

    def __str__(self):
        return f"_CustomStyleFactory: name = {self.name}, style_type = {self.style_type}, builtin = {self.builtin}"

    @classmethod
    def generate_style(
            cls,
            name: str,
            style_type: WD_STYLE_TYPE,
            builtin: bool):
        style: DocStyle
        if name in cls.styles:
            style = cls.styles[name]
            style.name = name
            style.type = style_type
            style.builtin = builtin
        else:
            style = cls.styles.add_style(name, style_type, builtin)
        return style

    @classmethod
    def inherit(
            cls,
            style_name: str,
            style_type: WD_STYLE_TYPE,
            builtin: bool = False,
            base_style: Optional[str] = None):
        if base_style is None:
            if style_type == WD_STYLE_TYPE.PARAGRAPH:
                base_style = "Paragraph Normal Style"
            elif style_type == WD_STYLE_TYPE.TABLE:
                base_style = "Table Normal Style"
            elif style_type == WD_STYLE_TYPE.CHARACTER:
                base_style = "Character Normal Style"
            style_logger.warning(f"The base style is not specified, so {base_style} is autoselected")

        base: DocStyle
        base = cls.styles[base_style]

        style = cls.generate_style(style_name, style_type, builtin)

        style.base_style = base
        style.quick_style = None
        style.priority = None

        style.font.all_caps = None
        style.font.bold = None
        style.font.complex_script = None
        style.font.cs_bold = None
        style.font.cs_italic = None
        style.font.double_strike = None
        style.font.emboss = None
        style.font.hidden = None
        style.font.highlight_color = None
        style.font.italic = None
        style.font.imprint = None
        style.font.math = None
        style.font.name = None
        style.font.no_proof = None
        style.font.outline = None
        style.font.rtl = None
        style.font.shadow = None
        style.font.size = None
        style.font.small_caps = None
        style.font.snap_to_grid = None
        style.font.spec_vanish = None
        style.font.strike = None
        style.font.subscript = None
        style.font.superscript = None
        style.font.underline = None
        style.font.web_hidden = None

        style.font.color.type = None
        style.font.color.theme_color = None
        style.font.color.rgb = None

        if style_type in (WD_STYLE_TYPE.TABLE, WD_STYLE_TYPE.PARAGRAPH):
            style.paragraph_format.alignment = None
            style.paragraph_format.keep_with_next = None
            style.paragraph_format.keep_together = None
            style.paragraph_format.space_before = None
            style.paragraph_format.space_after = None
            style.paragraph_format.left_indent = None
            style.paragraph_format.right_indent = None
            style.paragraph_format.first_line_indent = None
            style.paragraph_format.line_spacing = None
            style.paragraph_format.line_spacing_rule = None
            style.paragraph_format.page_break_before = None
            style.paragraph_format.widow_control = None

        return style

    @classmethod
    def modify(
            cls,
            style: DocStyle, *,
            style_properties: Optional[dict[str, Any]] = None,
            paragraph_format: Optional[dict[str, Any]] = None,
            font: Optional[dict[str, Any]] = None,
            color: Optional[dict[str, Any]] = None):
        cls._setattributes(style, **style_properties)
        cls._setattributes(style.paragraph_format, **paragraph_format)
        cls._setattributes(style.font, **font)
        cls._setattributes(style.font.color, **color)
        return style

    @staticmethod
    def _setattributes(item: Any, **kwargs):
        if kwargs is not None:
            for k, v in kwargs.items():
                try:
                    setattr(item, k, v)
                except AttributeError:
                    style_logger.error(f"AttributeError, the object {item} does not have an attribute {k}")
                    continue
                except TypeError:
                    style_logger.error(
                        f"TypeError, the attribute {k} of the object {item} having a value of {v} "
                        f"and the {type(v)} type received")
                    continue
                except ValueError:
                    style_logger.error(
                        f"ValueError, the a value {v} cannot be assigned to the attribute {k} "
                        f"of the object {item}")
                    continue
                except OSError:
                    style_logger.exception(f"OSError, some unwaited error occurred", exc_info=True)
                    raise
        return item

    @classmethod
    def paragraph_normal_style(cls) -> _ParagraphStyle:
        paragraph_style: _ParagraphStyle
        paragraph_style = cls.generate_style("Paragraph_Normal_Style", WD_STYLE_TYPE.PARAGRAPH, False)
        paragraph_style.next_paragraph_style = cls._parent.styles["Paragraph_Normal_Style"]
        paragraph_style.base_style = None
        paragraph_style.quick_style = True
        paragraph_style.priority = 1

        paragraph_style.paragraph_format.alignment = None
        paragraph_style.paragraph_format.keep_with_next = None
        paragraph_style.paragraph_format.keep_together = None
        paragraph_style.paragraph_format.space_before = None
        paragraph_style.paragraph_format.space_after = None
        paragraph_style.paragraph_format.left_indent = None
        paragraph_style.paragraph_format.right_indent = None
        paragraph_style.paragraph_format.first_line_indent = None
        paragraph_style.paragraph_format.line_spacing = None
        paragraph_style.paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
        paragraph_style.paragraph_format.page_break_before = None
        paragraph_style.paragraph_format.widow_control = True

        paragraph_style.font.all_caps = None
        paragraph_style.font.bold = None
        paragraph_style.font.complex_script = None
        paragraph_style.font.cs_bold = None
        paragraph_style.font.cs_italic = None
        paragraph_style.font.double_strike = None
        paragraph_style.font.emboss = None
        paragraph_style.font.hidden = True
        paragraph_style.font.highlight_color = None
        paragraph_style.font.italic = None
        paragraph_style.font.imprint = None
        paragraph_style.font.math = None
        paragraph_style.font.name = "Times New Roman"
        paragraph_style.font.no_proof = None
        paragraph_style.font.outline = None
        paragraph_style.font.rtl = None
        paragraph_style.font.shadow = None
        paragraph_style.font.size = Pt(14.0)
        paragraph_style.font.small_caps = None
        paragraph_style.font.snap_to_grid = None
        paragraph_style.font.spec_vanish = None
        paragraph_style.font.strike = None
        paragraph_style.font.subscript = None
        paragraph_style.font.superscript = None
        paragraph_style.font.underline = None
        paragraph_style.font.web_hidden = None

        paragraph_style.font.color.type = MSO_COLOR_TYPE.THEME
        paragraph_style.font.color.theme_color = MSO_THEME_COLOR_INDEX.TEXT_1
        paragraph_style.font.color.rgb = 000000

        return paragraph_style

    @classmethod
    def table_normal_style(cls) -> _TableStyle:
        table_style: _TableStyle
        table_style = cls.generate_style("Table_Normal_Style", WD_STYLE_TYPE.TABLE, False)
        table_style.quick_style = False
        table_style.base_style = None
        table_style.priority = 99
        table_style.next_paragraph_style = table_style

        table_style.paragraph_format.alignment = None
        table_style.paragraph_format.line_spacing = None
        table_style.paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
        table_style.paragraph_format.first_line_indent = None
        table_style.paragraph_format.left_indent = None
        table_style.paragraph_format.right_indent = None
        table_style.paragraph_format.space_before = None
        table_style.paragraph_format.space_after = None
        table_style.paragraph_format.keep_together = None
        table_style.paragraph_format.keep_with_next = None
        table_style.paragraph_format.widow_control = True

        table_style.font.name = "Times New Roman"
        table_style.font.all_caps = None
        table_style.font.bold = None
        table_style.font.complex_script = None
        table_style.font.cs_bold = None
        table_style.font.cs_italic = None
        table_style.font.double_strike = None
        table_style.font.emboss = None
        table_style.font.hidden = True
        table_style.font.highlight_color = None
        table_style.font.italic = None
        table_style.font.imprint = None
        table_style.font.math = None
        table_style.font.no_proof = None
        table_style.font.outline = None
        table_style.font.rtl = None
        table_style.font.shadow = None
        table_style.font.size = None
        table_style.font.small_caps = None
        table_style.font.snap_to_grid = None
        table_style.font.spec_vanish = None
        table_style.font.strike = None
        table_style.font.subscript = None
        table_style.font.superscript = None
        table_style.font.underline = None
        table_style.font.web_hidden = None

        table_style.font.color.type = MSO_COLOR_TYPE.THEME
        table_style.font.color.theme_color = MSO_THEME_COLOR_INDEX.TEXT_1
        table_style.font.color.rgb = 000000

        return table_style

    @classmethod
    def character_normal_style(cls) -> _CharacterStyle:
        character_style: _CharacterStyle
        character_style = cls.generate_style("Character_Normal_Style", WD_STYLE_TYPE.CHARACTER, False)
        character_style.base_style = None
        character_style.quick_style = False
        character_style.priority = 1

        character_style.font.all_caps = None
        character_style.font.bold = None
        character_style.font.complex_script = None
        character_style.font.cs_bold = None
        character_style.font.cs_italic = None
        character_style.font.double_strike = None
        character_style.font.emboss = None
        character_style.font.hidden = True
        character_style.font.highlight_color = None
        character_style.font.italic = None
        character_style.font.imprint = None
        character_style.font.math = None
        character_style.font.name = "Times New Roman"
        character_style.font.no_proof = None
        character_style.font.outline = None
        character_style.font.rtl = None
        character_style.font.shadow = None
        character_style.font.size = 11
        character_style.font.small_caps = None
        character_style.font.snap_to_grid = None
        character_style.font.spec_vanish = None
        character_style.font.strike = None
        character_style.font.subscript = None
        character_style.font.superscript = None
        character_style.font.underline = None
        character_style.font.web_hidden = None

        character_style.font.color.type = MSO_COLOR_TYPE.THEME
        character_style.font.color.theme_color = MSO_THEME_COLOR_INDEX.TEXT_1
        character_style.font.color.rgb = 000000

        return character_style

    @classmethod
    def table_13_pt_centered(cls) -> _ParagraphStyle:
        paragraph_style: _ParagraphStyle
        paragraph_style = cls.inherit("Table_13_pt_Centered", WD_STYLE_TYPE.PARAGRAPH, False, "Paragraph_Normal_Style")
        paragraph_style.next_paragraph_style = paragraph_style
        style_properties = {
            "next_paragraph_style": paragraph_style,
            "priority": 1
        }
        paragraph_format = {
            "alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
            "space_before": Pt(3.0),
            "space_after": Pt(3.0)
        }
        font = {
            "size": Pt(13.0)
        }
        return cls.modify(
            paragraph_style,
            style_properties=style_properties,
            paragraph_format=paragraph_format,
            font=font)

    @classmethod
    def table_13_pt_left(cls) -> _ParagraphStyle:
        paragraph_style: _ParagraphStyle
        paragraph_style = cls.inherit("Table_13_pt_Left", WD_STYLE_TYPE.PARAGRAPH, False, "Table_13_pt_Centered")
        paragraph_style.next_paragraph_style = paragraph_style
        paragraph_format = {
            "alignment": WD_PARAGRAPH_ALIGNMENT.LEFT
        }
        return cls.modify(paragraph_style, paragraph_format=paragraph_format)

    @classmethod
    def table_11_pt_centered(cls) -> _ParagraphStyle:
        paragraph_style: _ParagraphStyle
        paragraph_style = cls.inherit("Table_11_pt_Centered", WD_STYLE_TYPE.PARAGRAPH, False, "Paragraph_Normal_Style")
        paragraph_style.next_paragraph_style = paragraph_style
        paragraph_style.priority = 1
        paragraph_format = {
            "space_before": Pt(4.0),
            "space_after": Pt(4.0),
            "alignment": WD_PARAGRAPH_ALIGNMENT.CENTER
        }
        font = {
            "size": Pt(11.0),
            "cs_bold": True
        }
        return cls.modify(paragraph_style, paragraph_format=paragraph_format, font=font)

    @classmethod
    def table_11_pt_left(cls) -> _ParagraphStyle:
        paragraph_style: _ParagraphStyle
        paragraph_style = cls.inherit("Table_11_pt_Left", WD_STYLE_TYPE.PARAGRAPH, False, "Table_11_pt_Centered")
        paragraph_style.next_paragraph_style = paragraph_style
        paragraph_format = {
            "alignment": WD_PARAGRAPH_ALIGNMENT.LEFT
        }
        return cls.modify(paragraph_style, paragraph_format=paragraph_format)

    @classmethod
    def add_styles(cls):
        cls._parent.styles["Table_Normal_Style"] = cls.table_normal_style()
        cls._parent.styles["Paragraph_Normal_Style"] = cls.paragraph_normal_style()
        cls._parent.styles["Character_Normal_Style"] = cls.table_normal_style()
        cls._parent.styles["Table_11_pt_Centered"] = cls.table_11_pt_centered()
        cls._parent.styles["Table_11_pt_Left"] = cls.table_11_pt_left()
        cls._parent.styles["Table_11_pt_Centered"] = cls.table_11_pt_centered()
        cls._parent.styles["Table_11_pt_Left"] = cls.table_11_pt_left()
