from abc import abstractmethod
from typing import Any, Literal, Generator, Optional, Sequence
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.oxml import CT_Tbl, CT_TblPr, CT_TblGrid, OxmlElement, CT_Tc, CT_TcPr
from docx.oxml.ns import qn
# noinspection PyProtectedMember
from docx.styles.style import _CharacterStyle, _ParagraphStyle
# noinspection PyProtectedMember
from docx.table import Table, _Column, _Row, _Cell
# noinspection PyProtectedMember
from docx.text.paragraph import Paragraph
from docx.text.run import Run
# noinspection PyProtectedMember
from lxml.etree import _Element
from docx_document import _CustomBaseDocument
from docx_custom_header_footer import _CustomHeaderFooter, LiteralDirection
from docx_property import _CustomDocPropertyField
from docx_section import LiteralHF, _CustomSection

__all__ = [
    "_CustomHeaderFooterTable", "_CustomCell", "LiteralPageText"]

LiteralPageText = Literal["page", "text"]
LiteralTextDirection = Literal["btLr", "btRl"]


class _CustomHeaderFooterTable(_CustomHeaderFooter):
    index = 0
    _parent = _CustomBaseDocument()
    _dict_instances: dict[str, Any] = {}

    _cell_margins: dict[str, int] = {
        "top": 0,
        "bottom": 0,
        "left": 57,
        "right": 28
    }

    def __new__(
            cls,
            index: int,
            hdrftr_type: LiteralHF,
            table: Table,
            direction: LiteralDirection = "vertical"):
        super().__new__(cls, index, hdrftr_type)

    def __init__(
            self,
            index: int,
            hdrftr_type: LiteralHF,
            table: Table,
            direction: LiteralDirection = "vertical"):
        self.custom_header_footer: _CustomHeaderFooter = super().__init__(index, hdrftr_type)
        self.hdrftr_type = hdrftr_type
        self._table = table
        self.direction = direction
        self._name = f"custom_section_{self.index}_{self.hdrftr_type}_{self.__class__.index}"
        self.__class__._dict_instances[self._name] = self

        self.__class__.index += 1

    def __call__(self, **kwargs):
        return self.__class__._dict_instances[self._name]

    def __repr__(self):
        return f"<CustomFooterHeaderTable(custom_section={self._custom_section}, hdrftr_type={self.hdrftr_type}, " \
               f"table={self._table.part})>"

    def __str__(self):
        return f"CustomFooterHeaderTable: custom_section = {self._custom_section}, hdrftr_type = {self.hdrftr_type}, " \
               f"table={self._table}"

    def __hash__(self):
        return hash((self.index, self.hdrftr_type))

    def __key(self):
        return self.index, self.hdrftr_type

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @property
    def element(self) -> _Element:
        # noinspection PyProtectedMember
        return self._table._element

    @property
    def tbl(self) -> CT_Tbl:
        # noinspection PyProtectedMember
        return self._table._tbl

    @property
    def tbl_pr(self) -> CT_TblPr:
        return self.tbl.tblPr

    @property
    def tbl_grid(self) -> CT_TblGrid:
        return self.tbl.tblGrid

    @classmethod
    def custom_header_footer_table(cls, index: int, hdrftr_type: LiteralHF, direction: LiteralDirection = "vertical"):
        custom_section: _CustomSection = _CustomSection(index)
        table: Table = custom_section.header_footer(hdrftr_type).add_table(0, 0, 0)
        return cls(index, hdrftr_type, table, direction)

    @property
    def hdrftr_index(self) -> int:
        # noinspection PyProtectedMember
        return self._custom_section.header_footer(self.hdrftr_type)._hdrftr_index

    def set_alignment(self, alignment: WD_PARAGRAPH_ALIGNMENT):
        self._table.alignment = alignment
        return self

    @abstractmethod
    def set_cell_sizes(self):
        return self

    def set_bottom_top_direction(
            self,
            text_direction: LiteralTextDirection = "btLr"):
        if self.direction == "vertical":
            cell: _Cell
            for cell in self.iter_column_cells():
                # noinspection PyProtectedMember
                tc_pr: _Element = cell._tc.get_or_add_tcPr()
                text_dir: _Element = OxmlElement('w:textDirection')
                text_dir.set(qn('w:val'), text_direction)
                tc_pr.append(text_dir)
        return self

    def iter_row_cells(self) -> Generator[_Cell, None, None]:
        row: _Row
        cell: _Cell
        # noinspection PyTypeChecker
        for row in self._table.rows:
            for cell in row.cells:
                yield cell

    def iter_column_cells(self) -> Generator[_Cell, None, None]:
        column: _Column
        cell: _Cell
        # noinspection PyTypeChecker
        for column in self._table.columns:
            for cell in column.cells:
                yield cell

    def _merge_cells_specified(self, coord_from: tuple[int, int], coord_to: tuple[int, int]):
        coord_from_row_idx, coord_from_col_idx = coord_from
        coord_to_row_idx, coord_to_col_idx = coord_to
        cell_from: _Cell = self._table.cell(coord_from_row_idx, coord_from_col_idx)
        cell_to: _Cell = self._table.cell(coord_to_row_idx, coord_to_col_idx)
        return cell_from.merge(cell_to)

    def _merge_cells(self):
        if hasattr(self, "_merge_cells"):
            return [self._merge_cells_specified(coord_from, coord_to)
                    for coord_from, coord_to in getattr(self, "_merge_cells")]
        else:
            return NotImplemented

    @abstractmethod
    def set_table_position(self):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self._header_footer_position().items():
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    @abstractmethod
    @property
    def _table_text(self) -> dict[tuple[int, int], str]:
        pass

    @abstractmethod
    def _header_footer_position(
            self, *,
            leftFromText: Optional[int] = None,
            rightFromText: Optional[int] = None,
            horzAnchor: Optional[LiteralPageText] = "page",
            vertAnchor: Optional[LiteralPageText] = "page",
            tblpX: Optional[int] = 0,
            tblpY: Optional[int] = 0):
        return {
            f"{attr}": attr for attr in filter(
                lambda x: x is not None, (leftFromText, rightFromText, horzAnchor, vertAnchor, tblpX, tblpY)
            )
        }

    @abstractmethod
    def set_cell_text(self, *args):
        pass

    def custom_cell(self, row_idx, col_idx):
        return _CustomCell(self.index, self.hdrftr_type, self._table, row_idx, col_idx)

    def table_handle(
            self,
            alignment: WD_PARAGRAPH_ALIGNMENT = WD_PARAGRAPH_ALIGNMENT.CENTERED,
            text_direction: LiteralTextDirection = "btLr"):
        return (
            self.set_table_position()
                .set_alignment(alignment)
                .set_bottom_top_direction(text_direction)
                .set_cell_sizes()
        )


class _CustomCell(_CustomHeaderFooterTable):
    _instance = None
    _basic_document: _CustomBaseDocument = _CustomBaseDocument()
    _character_style: _CharacterStyle = _basic_document.styles["Character_Normal_Style"]
    _doc_property_style: _ParagraphStyle = _basic_document.styles["Paragraph_Normal_Style"]

    def __new__(cls, row_idx: int, col_idx: int, **kwargs):
        return object.__new__(cls)

    def __init__(
            self,
            index: int,
            hdrftr_type: LiteralHF,
            table: Table,
            row_idx: int,
            col_idx: int,
            properties: Optional[Sequence[str]] = None):
        if properties is None:
            properties = []
        self._parent = super().__init__(index, hdrftr_type, table)
        self.row_idx: int = row_idx
        self.col_idx: int = col_idx
        self.__cell: _Cell = self._table.cell(self.row_idx, self.col_idx)
        self._properties = properties

    def __call__(self):
        return super()._table.cell(self.row_idx, self.col_idx)

    def __repr__(self):
        return f"<_CustomCell(index={self.index}, hdrftr_type={self.hdrftr_type}, table={self._table}, " \
               f"row_idx={self.row_idx}, col_idx={self.col_idx})>"

    def __str__(self):
        return f"CustomCell: row_idx = {self.row_idx}, col_idx = {self.col_idx}"

    @property
    def _table_text(self):
        for coord, text in super()._table_text.items():
            if (self.row_idx, self.col_idx) == coord:
                return text
            else:
                continue
        return

    def set_cell_text(self, paragraph_style: _ParagraphStyle) -> Run:
        paragraph: Paragraph = self.__cell.add_paragraph(style=paragraph_style)
        return paragraph.add_run(self._table_text, self._character_style)

    def set_cell_sizes(self):
        return NotImplemented

    def set_table_position(self):
        return NotImplemented

    @property
    def tc(self) -> CT_Tc:
        # noinspection PyProtectedMember
        return self.__cell._tc

    @property
    def tc_pr(self) -> CT_TcPr:
        return self.tc.tcPr

    @property
    def grid_span(self) -> int:
        return self.tc_pr.grid_span

    @property
    def v_align(self) -> str:
        return self.tc_pr.vAlign_val

    @property
    def v_merge(self) -> str:
        return self.tc_pr.vMerge_val

    def set_cell_margins(self):
        # noinspection PyProtectedMember
        tc_pr: CT_TcPr = self.tc.get_or_add_tcPr()
        tc_mar: _Element = OxmlElement('w:tcMar')

        for k, v in super()._cell_margins:
            cell_margins: _Element = OxmlElement(f"w:{k}")
            cell_margins.set(qn('w:w'), v)
            cell_margins.set(qn('w:type'), 'dxa')
            tc_mar.append(cell_margins)
        tc_pr.append(tc_mar)
        return self

    @property
    def _header_footer_position(self, **kwargs):
        return NotImplemented

    @property
    def tbl(self) -> CT_Tbl:
        return self.tc.tbl

    @property
    def tbl_pr(self) -> CT_TblPr:
        return self.tbl.tblPr

    @property
    def tbl_grid(self) -> CT_TblGrid:
        return self.tbl.tblGrid

    @property
    def _doc_properties(self) -> list[_CustomDocPropertyField]:
        return [super().get_word_doc_property(property_name) for property_name in self._properties]

    def add_doc_properties(self):
        cell_paragraph: Paragraph = self.__cell.add_paragraph(self._doc_property_style)
        for doc_property in self._doc_properties:
            doc_property.insert_field(doc_property.doc_property, doc_property.value, cell_paragraph)
        return self
