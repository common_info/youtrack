from docx.shared import Pt, Length

_table_position: dict[str, dict[str, dict[str, str]]] = {
    "first_page": {
        "footer_1": {
            "horzAnchor": "page",
            "vertAnchor": "page",
            "tblpX": "336",
            "tblpY": "15707"
        },
        "footer_2_1": {
            "leftFromText": "181",
            "rightFromText": "181",
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8313"
        },
        "footer_2_2": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "-1443",
            "tblpY": "15707"
        }
    },
    "second_page": {
        "footer_4": {
            "leftFromText": "181",
            "rightFromText": "181",
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8313"
        },
        "footer_5_1": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "273"
        },
        "footer_5_2": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8262"
        },
        "footer_5_3": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "-283",
            "tblpY": "12985"
        }
    },
    "other_page": {
        "footer_6": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "336",
            "tblpY": "15707"
        },
        "footer_7": {
            "leftFromText": "181",
            "rightFromText": "181",
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8257"
        },
        "header_7": {
            "leftFromText": "181",
            "rightFromText": "181",
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8257"
        }
    }
}

table_style_names: tuple[str] = ("Table_Normal_Style",)

paragraph_style_names: tuple[str, ...] = (
    "Paragraph_Normal_Style", "Table_13_pt_Centered", "Table_13_pt_Left", "Table_11_pt_Centered", "Table_11_pt_Left")

first_page_footer = {
    "section_0": [
        {
            "height": (Pt(99.25), Pt(70.9), Pt(70.9), Pt(99.25), Pt(70.9)),
            "width": (Pt(14.2), Pt(19.85))
        }
    ],
    "section_1": [
        {
            "height": (Pt(170.1), Pt(170.1)),
            "width": (Pt(14.2), Pt(19.85))
        },
        {
            "height": (Pt(99.25), Pt(70.9), Pt(70.9), Pt(99.25), Pt(70.9)),
            "width": (Pt(14.2), Pt(19.85))
        },
        {
            "height": (
                Pt(39.7), Pt(22.7), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2)),
            "width": (
                Pt(24.8), Pt(40.1), Pt(66.7), Pt(49.6), Pt(33.5), Pt(40.65), Pt(139.25), Pt(14.1), Pt(14.15),
                Pt(14.15), Pt(42.3), Pt(42.35))
        }
    ],
    "section_2": [
        {
            "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
            "width": (Pt(25.45), Pt(28.15), Pt(63.65), Pt(41.9), Pt(32.45), Pt(301.45), Pt(28.6))
        }
    ]
}

default_footer: dict[str, dict[str, tuple[Length, ...]]] = {
    "section_0": {
        "height": (Pt(99.25), Pt(70.9), Pt(70.9), Pt(99.25), Pt(70.9)),
        "width": (Pt(14.2), Pt(19.85))
    },
    "section_1": {
        "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
        "width": (Pt(74.85), Pt(74.9), Pt(74.95), Pt(74.95), Pt(74.95), Pt(74.95), Pt(74.95))
    },
    "section_2": {
        "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
        "width": (Pt(28.8), Pt(304.35), Pt(23.55), Pt(28.95), Pt(66.45), Pt(43.45), Pt(28.95))
    }
}

first_page_header: dict[str, dict[str, tuple[Length, ...]]] = {
    "section_0": {},
    "section_1": {},
    "section_2": {
        "height": (Pt(99.25), Pt(70.9), Pt(70.9), Pt(99.25), Pt(70.9)),
        "width": (Pt(14.2), Pt(19.85))
    }
}

even_page_footer: dict[str, dict[str, tuple[Length, ...]]] = {
    "section_0": {
        "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
        "width": (Pt(28.0), Pt(305.5), Pt(25.45), Pt(28.2), Pt(64.2), Pt(42.15), Pt(28.15))
    },
    "section_1": {
        "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
        "width": (Pt(28.0), Pt(305.5), Pt(25.45), Pt(28.2), Pt(64.2), Pt(42.15), Pt(28.15))
    },
    "section_2": {
        "height": (Pt(14.2), Pt(14.2), Pt(14.2)),
        "width": (Pt(28.0), Pt(305.5), Pt(25.45), Pt(28.2), Pt(64.2), Pt(42.15), Pt(28.15))
    }
}

text_header = ("Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.")
text_header_table = ("Справ. №", "Подпись и дата")
text_footer_vert = ("Подпись и дата", "Инв. № дубл.", "Взам.инв. №", "Подпись и дата", "Инв. № подл.")
text_footer_horiz = (
    "Изм.", "Лист", "№ докум." "Подп.", "Дата", "Разраб.", "Пров.", "М. экспертиза", "Н. контр.", "Утв."
)
