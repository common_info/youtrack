from typing import Any, Literal
from zipfile import ZipFile, BadZipFile
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
# noinspection PyProtectedMember
from docx.styles.style import _CharacterStyle
from docx.table import _Cell
from docx.text.paragraph import Paragraph
from docx.text.run import Run
from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import _Element
from docx_document import _CustomBaseDocument


LiteralFldCharType = Literal["begin", "separate", "end"]


class ImproperCustomWordFile(Exception):
    """Improper Word file."""


class ImproperCustomWordFileZipName(Exception):
    """Improper Word zip file name."""


class _CustomZipWordDocument(_CustomBaseDocument):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, path: str):
        if self.__class__._instance is None:
            self._parent: _CustomBaseDocument = super().__init__(path)
            self.path: str = path
            if not self._valid_docx():
                raise ImproperCustomWordFile
        return

    def __repr__(self):
        return f"<_CustomZipWordDocument(path={self.path})>"

    def __str__(self):
        return f"_CustomZipWordDocument: path = {self.path}"

    def __call__(self):
        return self.__class__._instance

    @classmethod
    def from_base_document(cls):
        return cls(_CustomBaseDocument()._path)

    def _valid_docx(self):
        flag = True
        try:
            ZipFile(self.path, "r")
        except BadZipFile as e:
            print(e.__class__.__name__)
            flag = False
        except FileNotFoundError as e:
            print(e.__class__.__name__, e.strerror)
            flag = False
        finally:
            return flag

    @property
    def zip_file(self) -> ZipFile:
        return ZipFile(self.path, "r")

    @property
    def file_names(self) -> list[str]:
        return self.zip_file.namelist()

    def read_file(self, file_name: str) -> bytes:
        if file_name in self.file_names:
            return self.zip_file.read(file_name)
        else:
            raise ImproperCustomWordFileZipName

    def file_readable(self, file_name: str) -> str:
        return self.read_file(file_name).decode("utf-8")

    def read_properties(self) -> str:
        return self.file_readable("docProps/custom.xml")

    def etree_file(self, file_name: str) -> _Element:
        return etree.fromstring(self.read_file(file_name))

    def etree_custom(self) -> _Element:
        return etree.fromstring(self.read_file("docProps/custom.xml"))

    def etree_file_readable(self, file_name: str):
        return self.read_file(file_name).decode("utf-8")

    def write_etree_file(self, file_name: str, file_path: str):
        try:
            with open(file_path, "w+") as file:
                file.write(self.etree_file_readable(file_name))
        except PermissionError as e:
            print(e.__class__.__name__, e.strerror)
        except OSError as e:
            print(e.__class__.__name__, e.strerror)
        else:
            print("The etree is written to the file")

    def descendants(self) -> list[_Element]:
        return [item for item in self.etree_custom().iterdescendants()]

    def properties(self) -> list[_Element]:
        item: _Element
        return [item.get("name") for item in self.descendants()]

    def property_values(self) -> list[Any]:
        value: _Element
        return [value.text for value in self.descendants() if value.text is not None]

    def custom_doc_property_fields(self):
        return [_CustomDocPropertyField(item.get("name"), item.text) for item in self.descendants()]

    def close(self):
        self.zip_file.close()


class _CustomDocPropertyField:
    _parent = _CustomBaseDocument()
    dict_doc_properties = {}

    def __init__(self, doc_property: str, value: str):
        self.doc_property: str = doc_property
        self.value: str = value
        self.dict_doc_properties[self.doc_property] = self

    def __call__(self):
        return self

    def __repr__(self):
        return f"<_CustomDocPropertyField(doc_property={self.doc_property}, value={self.value})>"

    def __str__(self):
        return f"_CustomDocPropertyField: doc_property={self.doc_property}, value={self.value}"

    def __hash__(self):
        return hash((self.doc_property, self.value))

    def __key(self):
        return self.doc_property, self.value

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def character_style(self) -> _CharacterStyle:
        return self._parent.styles["Character_Normal_Style"]

    def add_run_element(self, cell_paragraph: Paragraph, __parent: str, __child: str, __value: str):
        # noinspection PyProtectedMember
        p: _Element = cell_paragraph._p
        run: Run = cell_paragraph.add_run(style=self.character_style())
        # noinspection PyProtectedMember
        r: _Element = run._r
        element: _Element = OxmlElement(__parent)
        element.set(qn(__child), __value)
        r.append(element)
        p.append(r)
        return cell_paragraph

    def add_run_instr(self, cell_paragraph: Paragraph, args: tuple[tuple[bool, str, str], ...]):
        # noinspection PyProtectedMember
        p: _Element = cell_paragraph._p
        for (preserve, k, v) in zip(*args):
            run: Run = cell_paragraph.add_run(style=self.character_style())
            # noinspection PyProtectedMember
            r: _Element = run._r
            if preserve:
                r.set(qn("xml:space"), "preserve")
            r.set(qn(f"w:{k}"), v)
            p.append(r)
        return cell_paragraph

    def insert_field(self, doc_property: str, value: str, cell_paragraph: Paragraph):
        args = (
            (False, "instrText", "DOCPROPERTY"),
            (True, "instrText", ""),
            (False, "instrText", doc_property),
            (True, "instrText", r" \* "),
            (False, "instrText", "MERGEFORMAT"),
        )
        _temp = self.add_run_element(cell_paragraph, "fldChar", "fldCharType", "begin")
        _temp = self.add_run_instr(_temp, args)
        self.add_run_element(_temp, "fldChar", "fldCharType", "separate").text = value
        cell_paragraph = _temp
        del _temp
        return cell_paragraph


class _CustomDocField:
    _parent = _CustomBaseDocument()
    _dict_fields = dict()

    def __init__(
            self,
            field: str,
            value: str):
        super().__init__()
        self._field: str = field.upper()
        self._value = value
        self._dict_fields[self._field] = self

    def __call__(self, field: str):
        return self._dict_fields[field]

    def __hash__(self):
        return hash(self._field)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._field == other._field
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._field != other._field
        else:
            return NotImplemented

    def character_style(self) -> _CharacterStyle:
        return self._parent.styles["Character_Normal_Style"]

    def add_run_element(self, cell_paragraph: Paragraph, __parent: str, __child: str, __value: str):
        # noinspection PyProtectedMember
        p: _Element = cell_paragraph._p
        run: Run = cell_paragraph.add_run(style=self.character_style())
        # noinspection PyProtectedMember
        r: _Element = run._r
        element: _Element = OxmlElement(__parent)
        element.set(qn(__child), __value)
        r.append(element)
        p.append(r)
        return cell_paragraph

    def add_run_instr(self, cell_paragraph: Paragraph, args: tuple[tuple[bool, str, str], ...]):
        # noinspection PyProtectedMember
        p: _Element = cell_paragraph._p
        for (preserve, k, v) in zip(*args):
            run: Run = cell_paragraph.add_run(style=self.character_style())
            # noinspection PyProtectedMember
            r: _Element = run._r
            if preserve:
                r.set(qn("xml:space"), "preserve")
            r.set(qn(f"w:{k}"), v)
            p.append(r)
        return cell_paragraph

    def insert_field(self, doc_property: str, value: str, cell_paragraph: Paragraph):
        _temp = self.add_run_element(cell_paragraph, "fldChar", "fldCharType", "begin")
        _temp = self.add_run_instr(_temp, ((True, "instrText", self._field),))
        _temp = self.add_run_element(_temp, "fldChar", "fldCharType", "separate")
        _temp = _temp.add_run(self._value)
        cell_paragraph = self.add_run_element(_temp, "fldChar", "fldCharType", "end")
        del _temp
        return cell_paragraph

    @staticmethod
    def add_fld_char(cell_paragraph: Paragraph, fld_char_type: LiteralFldCharType):
        p: _Element = cell_paragraph._p
        run: Run = cell_paragraph.add_run()
        r: _Element = run._r
        element: _Element = OxmlElement("w:fldChar")
        element.set(qn("w:fldCharType"), fld_char_type)
        r.append(element)
        p.append(r)
        return cell_paragraph


    def add_instr_text(self, cell_paragraph: Paragraph, __field: str, preserve: bool = True):
        p: _Element = cell_paragraph._p
        run: Run = cell_paragraph.add_run()
        r: _Element = run._r
        element: _Element = OxmlElement("w:instrText")
        if preserve:
            element.set(qn("xml:space"), "preserve")
        element.text = f" {__field} "
        r.append(element)
        p.append(r)
        return self

    def add_text(self, cell_paragraph: Paragraph, __value: str):
        p: _Element = cell_paragraph._p
        run: Run = cell_paragraph.add_run()
        r: _Element = run._r
        r.text = __value
        p.append(r)
        return self

    def combine(self, cell: _Cell):
        paragraph: Paragraph = cell.add_paragraph()
        p: _Element = paragraph._p
        
        run: Run = paragraph.add_run()
        r: _Element = run._r
        element: _Element = OxmlElement("w:fldChar")
        element.set(qn("w:fldCharType"), "begin")
        r.append(element)
        p.append(r)

        run: Run = paragraph.add_run()
        r: _Element = run._r
        element: _Element = OxmlElement("w:instrText")
        element.set(qn("xml:space"), "preserve")
        element.text = f" {self._field} "
        r.append(element)
        p.append(r)
        
        run: Run = paragraph.add_run()
        r: _Element = run._r
        r.text = f"{self._value}"
        p.append(r)
        
        run: Run = paragraph.add_run()
        r: _Element = run._r
        element: _Element = OxmlElement("w:fldChar")
        element.set(qn("w:fldCharType"), "end")
        r.append(element)
        p.append(r)
"""
<w:r>
<w:fldChar w:fldCharType="begin"/>
</w:r>
<w:r>
<w:instrText xml:space="preserve"> DATE </w:instrText>
</w:r>
<w:r>
<w:fldChar w:fldCharType="separate"/>
</w:r>
<w:r>
<w:t>12/31/2005</w:t>
</w:r>
<w:r>
<w:fldChar w:fldCharType="end"/>
</w:r>
"""
