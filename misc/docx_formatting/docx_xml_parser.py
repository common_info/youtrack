from functools import cached_property
from zipfile import ZipFile
from lxml import etree
from lxml.etree import _Element


class DocxXmlParser:
    def __init__(self, path: str):
        self.path = path

    def doc_string(self):
        document: ZipFile = ZipFile(self.path)
        doc_content: bytes = document.read("docProps/custom.xml")
        tree: _Element = etree.fromstring(doc_content)
        string_tree: bytes = etree.tostring(tree, xml_declaration=True, pretty_print=True, encoding="utf-8")
        return string_tree.decode("utf-8")

    @property
    def doc_xml(self) -> _Element:
        document: ZipFile = ZipFile(self.path)
        doc_content: bytes = document.read("docProps/custom.xml")
        return etree.fromstring(doc_content)

    @cached_property
    def ns(self):
        return self.doc_xml.nsmap

    def doc_properties(self):
        elements: list[_Element] = self.doc_xml.findall("property", self.doc_xml.nsmap)
        return [el.attrib.items() for el in elements]

    def doc_properties_names(self):
        print([el.attrib["name"] for el in self.doc_xml.findall("property", self.ns)])


