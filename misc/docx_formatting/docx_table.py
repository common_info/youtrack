import logging
from abc import abstractmethod
from logging import Logger
from typing import Sequence, Generator, Literal, Optional, Union
from docx.enum.table import WD_ROW_HEIGHT_RULE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.oxml import OxmlElement, CT_Tbl, CT_TblPr, CT_TcPr, CT_Tc
# noinspection PyProtectedMember
from docx.table import Table, _Row, _Column, _Cell
from docx.text.paragraph import Paragraph
# noinspection PyProtectedMember
from docx.text.run import Run
# noinspection PyProtectedMember
from lxml.etree import _Element
from docx_document import _CustomBaseDocument, LiteralHeader, LiteralFooter
from docx_page import CustomFirstPage, CustomSecondPage, CustomOtherPage, CustomPage

__all__ = [
    "CustomTable", "CustomCellGroup", "_CustomDocPropertyField", "CustomHeaderFooterTable", "CustomEvenFooterTable",
    "CustomMajorFooterTable"]

table_logger: Logger = logging.getLogger(__name__)

LiteralRowColumn = Literal["row", "column"]


class CustomTable:
    _parent = _CustomBaseDocument()
    _cell_margins = {
        "top": 0,
        "left": 0,
        "bottom": 0,
        "right": 0
    }

    header_footer_position = {
        "header_footer": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8257"
        },
        "even_footer_table": {
            "leftFromText": "181",
            "rightFromText": "181",
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8313"
        },
        "major_footer_table": {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "-283",
            "tblpY": "12985"
        }
    }

    def __init__(
            self,
            custom_page: CustomPage,
            table: Table):
        self._custom_page: CustomPage = custom_page
        self._table: Table = table
        self.header_footer_type: Union[LiteralHeader, LiteralFooter, None] = None

    @abstractmethod
    @property
    def header_footer_index(self):
        return

    @property
    def count_rows(self):
        return len(self.rows())

    @property
    def count_columns(self):
        return len(self.columns())

    @property
    def table_styles(self):
        return self._custom_page.table_styles

    @property
    def paragraph_styles(self):
        return self._custom_page.paragraph_styles

    def set_bottom_top_direction(self, direction: Literal["tbRl", "btLr"] = "btLr"):
        cell: _Cell
        for cell in self.iter_row_cells():
            # noinspection PyProtectedMember
            tc_pr: _Element = cell._tc.get_or_add_tcPr()
            text_direction: _Element = OxmlElement('w:textDirection')
            text_direction.set(qn('w:val'), direction)
            tc_pr.append(text_direction)

    def cell(self, row_idx: int, column_idx: int) -> Optional[_Cell]:
        try:
            cell: _Cell = self._table.cell(row_idx, column_idx)
        except KeyError:
            table_logger.error(f"Indexes are out of bounds: row = {row_idx}, column = {column_idx}")
            return
        except OSError:
            table_logger.error(f"Some error with the cell \"row = {row_idx}, column = {column_idx}\" occurred")
            return
        else:
            return cell

    def rows(self) -> list[_Row]:
        # noinspection PyTypeChecker
        return list(self._table.rows)

    def columns(self) -> list[_Column]:
        # noinspection PyTypeChecker
        return list(self._table.columns)

    def iter_columns(self) -> Generator[_Column, None, None]:
        for column in self.columns():
            yield column

    def iter_rows(self) -> Generator[_Row, None, None]:
        for row in self.rows():
            yield row

    def iter_row_cells(self) -> Generator[_Cell, None, None]:
        row: _Row
        cell: _Cell
        for row in self.iter_rows():
            for cell in row.cells:
                yield cell

    def iter_column_cells(self) -> Generator[_Cell, None, None]:
        column: _Column
        cell: _Cell
        for column in self.iter_columns():
            for cell in column.cells:
                yield cell

    @abstractmethod
    def set_cell_sizes(self, **kwargs):
        pass

    def set_table_position(self, position_name: str):
        tbl_pr: CT_TblPr = self.tbl_pr
        tbl_ppr = OxmlElement("w:tblpPr")
        for k, v in self.header_footer_position[position_name]:
            tbl_ppr.set(qn(f"w:{k}"), v)
            tbl_pr.append(tbl_ppr)

    def set_alignment(self):
        self._table.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT

    def set_style(
            self,
            style_name: str,
            text_lines: Sequence[str],
            flag: LiteralRowColumn = "column"):
        style = self._parent.styles[style_name]
        cell_group = CustomCellGroup(self)
        cell_group.set_cell_group_values(text_lines, style, flag)

    def set_cell_margins(self):
        for cell in self.iter_row_cells():
            cell: _Cell
            # noinspection PyProtectedMember
            tc: CT_Tc = cell._tc
            tc_pr: CT_TcPr = tc.get_or_add_tcPr()
            tc_mar = OxmlElement('w:tcMar')

            for k, v in self.__class__._cell_margins:
                cell_margins = OxmlElement(f"w:{k}")
                cell_margins.set(qn('w:w'), v)
                cell_margins.set(qn('w:type'), 'dxa')
                tc_mar.append(cell_margins)
            tc_pr.append(tc_mar)

    @property
    def tbl(self) -> CT_Tbl:
        # noinspection PyProtectedMember
        return self._table._tbl

    @property
    def tbl_pr(self) -> CT_TblPr:
        return self.tbl.tblPr


class CustomHeaderFooterTable(CustomTable):
    _instance = None

    column_width: tuple[int, ...] = (284, 397)
    row_height: tuple[int, ...] = (1985, 1418, 1418, 1985, 1418)

    cell_text: tuple[str, ...] = ("Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.")

    def __init__(self, table: Table, header_footer_type: Union[LiteralHeader, LiteralFooter]):
        super().__init__(CustomOtherPage(), table)
        self._custom_page: CustomOtherPage = CustomOtherPage()
        self._table: Table = table
        self.header_footer_type: Union[LiteralHeader, LiteralFooter] = header_footer_type
        self.__class__._instance = self

    def __call__(self, *args, **kwargs):
        return self.__class__._instance

    @classmethod
    def header_table(cls, header_footer_type: Union[LiteralHeader, LiteralFooter]):
        table = CustomOtherPage().add_header_footer_table(2, 5, 681, header_footer_type)
        return cls(table, header_footer_type)

    @property
    def header_footer_index(self):
        # noinspection PyProtectedMember
        return self._custom_page.header_footer(self.header_footer_type)._hdrftr_index

    def set_alignment(self):
        self._table.alignment = WD_PARAGRAPH_ALIGNMENT.CENTERED

    def set_cell_sizes(self, **kwargs):
        column: _Column
        for (column, width) in zip(self.iter_columns(), self.__class__.column_width):
            column.width = width
        row: _Row
        for (row, height) in zip(self.iter_rows(), self.__class__.row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY

    def set_bottom_top_direction(self, **kwargs):
        super().set_bottom_top_direction()

    def set_text(self):
        for index, cell in enumerate(self._table.column_cells(0)):
            cell.add_paragraph(self.__class__.cell_text[index], super().paragraph_styles["Table_11_pt_Centered"])

    def set_table_position(self, **kwargs):
        super().set_table_position("header_footer")


class CustomEvenFooterTable(CustomTable):
    _instance = None

    column_width: tuple[int, ...] = (284, 397)
    row_height: tuple[int, ...] = (3402, 3402)

    cell_text: tuple[str, ...] = ("Подпись и дата", "Справ. №")

    def __init__(self, table: Table):
        super().__init__(CustomFirstPage(), table)
        self._custom_page: CustomFirstPage = CustomFirstPage()
        self._table: Table = table
        self.header_footer_type: LiteralFooter = "even_page_footer"
        self.__class__._instance = self

    def __call__(self, *args, **kwargs):
        return self.__class__._instance

    @property
    def header_footer_index(self):
        # noinspection PyProtectedMember
        return self._custom_page.header_footer(self.header_footer_type)._hdrftr_index

    @classmethod
    def even_first_page_footer_table(cls):
        table = CustomFirstPage().add_header_footer_table(2, 5, 681, "even_page_footer")
        return cls(table)

    def set_cell_sizes(self, **kwargs):
        column: _Column
        for (column, width) in zip(self.iter_columns(), self.__class__.column_width):
            column.width = width
        row: _Row
        for (row, height) in zip(self.iter_rows(), self.__class__.row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY

    def set_table_position(self, position_name: str):
        super().set_table_position("even_footer_table")


class CustomMajorFooterTable(CustomTable):
    _instance = None

    _cell_bounds = (
        (0, 0, 5, 2),
        (0, 5, 1, 5),
        (0, 6, 1, 6),
        (0, 7, 1, 7),
        (0, 8, 1, 8),
        (0, 9, 1, 9),
        (7, 0, 11, 0),
        (8, 0, 11, 0),
        (5, 2, 11, 4),
        (5, 5, 6, 9),
        (7, 5, 9, 5),
        (7, 6, 9, 11)
    )

    column_width: tuple[int, ...] = (498, 807, 1342, 998, 674, 818, 2801, 283, 284, 284, 851, 852)
    row_height: tuple[int, ...] = (794, 454, 284, 284, 284, 284, 284, 284, 284, 284)

    cell_text: tuple[str, ...] = (
        "Стр.", "Справ. №", "Изм.", "Лист", "№ докум.", "Подп.", "Дата", "Разраб.", "Лит.", "Листов", "Пров.",
        "М. экспертиза", "Н. контр.", "Утв.")

    doc_properties = ("DecimalNum", "DocTypeShort", "ProjectName", "ShortName", "DocType")

    def __init__(self, table: Table):
        super().__init__(CustomSecondPage(), table)
        self._custom_page: CustomSecondPage = CustomSecondPage()
        self._table: Table = table
        self.__class__._instance = self

    def __call__(self, *args, **kwargs):
        return self.__class__._instance

    @property
    def header_footer_index(self):
        # noinspection PyProtectedMember
        return self._custom_page.header_footer(self.header_footer_type)._hdrftr_index

    @classmethod
    def major_footer_table(cls):
        table = CustomSecondPage().add_header_footer_table(10, 12, 10433, "first_page_footer")
        return cls(table)

    def cell_groups(self):
        return [CustomCellGroup(self, row_idx_start, row_idx_end, column_idx_start, column_idx_end)
                for row_idx_start, column_idx_start, row_idx_end, column_idx_end in self.__class__._cell_bounds]

    def merge_cell_groups(self):
        for custom_cell_group in self.cell_groups():
            custom_cell_group.merge()

    def set_cell_sizes(self, **kwargs):
        column: _Column
        for (column, width) in zip(self.iter_columns(), self.__class__.column_width):
            column.width = width
        row: _Row
        for (row, height) in zip(self.iter_rows(), self.__class__.row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY

    def merge_cells(self):
        cell_group_bounds = (
            (self._table.cell(0, 0), self._table.cell(4, 1)),
            (self._table.cell(7, 0), self._table.cell(11, 0)),
            (self._table.cell(7, 1), self._table.cell(11, 1)),
            (self._table.cell(5, 2), self._table.cell(11, 4)),
            (self._table.cell(0, 5), self._table.cell(1, 5)),
            (self._table.cell(0, 6), self._table.cell(1, 6)),
            (self._table.cell(0, 7), self._table.cell(1, 7)),
            (self._table.cell(0, 8), self._table.cell(1, 8)),
            (self._table.cell(0, 9), self._table.cell(1, 9)),
            (self._table.cell(5, 5), self._table.cell(6, 9)),
            (self._table.cell(7, 5), self._table.cell(9, 5)),
            (self._table.cell(7, 7), self._table.cell(11, 9))
        )

        for cell_lt, cell_br in cell_group_bounds:
            cell_lt.merge(cell_br)

    def set_table_position(self, position_name: str):
        super().set_table_position("major_footer_table")


class CustomCellGroup:
    def __init__(
            self,
            table: CustomTable,
            row_idx_start: int = None,
            row_idx_end: int = None,
            column_idx_start: int = None,
            column_idx_end: int = None):

        if row_idx_start is None:
            row_idx_start = 0
        if row_idx_end is None:
            row_idx_end = table.count_rows
        if column_idx_start is None:
            column_idx_start = 0
        if column_idx_end is None:
            column_idx_end = table.count_columns

        self._parent = table
        self.start = (row_idx_start, row_idx_end)
        self.end = (column_idx_start, column_idx_end)

    def iter_columns(self) -> Generator[_Column, None, None]:
        return self._parent.iter_columns()

    def iter_rows(self) -> Generator[_Row, None, None]:
        return self._parent.iter_rows()

    def iter_row_cells(self) -> Generator[_Cell, None, None]:
        return self._parent.iter_row_cells()

    def iter_column_cells(self) -> Generator[_Cell, None, None]:
        return self._parent.iter_column_cells()

    @property
    def cells(self):
        row_idx_start, column_idx_start = self.start
        row_idx_end, column_idx_end = self.end
        if row_idx_end < row_idx_start:
            table_logger.error("ValueError, the row indexes are invalid, so they are swapped.")
            row_idx_start, row_idx_end = row_idx_end, row_idx_start
        if column_idx_end < column_idx_start:
            table_logger.error("ValueError, the column indexes are invalid, so they are swapped.")
            column_idx_start, column_idx_end = column_idx_end, column_idx_start
        return [
            self._parent.cell(row, column)
            for row in range(row_idx_start, row_idx_end + 1)
            for column in range(column_idx_start, column_idx_end + 1)
        ]

    def cell_border_attrs(self, val: str = "single", sz: int = 8):
        dict_cell_attrs = dict()
        space: int = 0
        color: str = "auto"
        attrs = (val, sz, space, color)
        dict_attrs = {f"{attr}": attr for attr in attrs}
        dict_null = {"val": "nil"}
        cell_edges = ("start", "end", "top", "bottom", "inside_h", "inside_v")
        null_edges = ("tl2br", "tl2bl")
        dict_cell_edges = {cell_edge: dict_attrs for cell_edge in cell_edges}
        dict_cell_edges.update({null_edge: dict_null for null_edge in null_edges})
        for cell in self.cells:
            dict_cell_attrs[cell] = dict_cell_edges
        return dict_cell_attrs

    def set_cell_border(self, val: str, sz: int):
        cell: _Cell
        for cell, cell_values in self.cell_border_attrs(val, sz).items():
            # noinspection PyProtectedMember
            tc_pr: CT_TcPr = cell._tc.get_or_add_tcPr()
            tc_borders: _Element = OxmlElement("w:tcBorders")
            for edge, edge_values in cell_values.items():
                el = tc_borders.find(qn(f"w:{edge}"))
                if el is None:
                    el = OxmlElement(f"w:{edge}")
                    tc_borders.append(el)
                for k, v in edge_values.items():
                    el.set(qn(f"w:{k}"), str(v))
            tc_pr.set(qn("w:tcBorders"), tc_borders)

    @staticmethod
    def cell_border_dict(
            val: str = "single",
            size: int = 8,
            space: int = 0,
            color: str = "auto"):
        return {"sz": f"{size}", "val": val, "color": color, "space": f"{space}"}

    def set_cell_group_values(
            self,
            text_lines: Sequence[str],
            text_style: str,
            flag: Literal["row", "column"] = "column"):
        if len(self.cells) != len(text_lines):
            table_logger.error(
                f"The number of values, {len(text_lines)}, does not coinside with the number of cells, "
                f"{len(self.cells)}")
            return
        cell: _Cell
        text_line: str
        if flag == "column":
            for (cell, text_line) in zip(self.iter_column_cells(), text_lines):
                try:
                    paragraph: Paragraph = cell.add_paragraph(text_line, text_style)
                    paragraph.text = text_line
                except OSError:
                    table_logger.error(f"OSError, the text {text_lines} is not added to the cell {cell}")
                    continue
        if flag == "row":
            for (cell, text_line) in zip(self.iter_row_cells(), text_lines):
                try:
                    paragraph: Paragraph = cell.add_paragraph(text_line, text_style)
                    paragraph.text = text_line
                except OSError:
                    table_logger.error(f"OSError, the text {text_lines} is not added to the cell {cell}")
                    continue
        return

    def merge(self) -> _Cell:
        row_start, column_start = self.start
        row_end, column_end = self.end
        cell_start: _Cell = self._parent.cell(row_start, column_start)
        cell_end: _Cell = self._parent.cell(row_end, column_end)
        return cell_start.merge(cell_end)
