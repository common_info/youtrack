import logging
from abc import abstractmethod
from logging import Logger
from typing import Union, Any, Optional, Generator, Literal
# noinspection PyProtectedMember
from docx.enum.table import WD_ROW_HEIGHT_RULE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
# noinspection PyProtectedMember
from docx.oxml import OxmlElement, CT_TblPr, CT_Tc, CT_TcPr, CT_Tbl, CT_TblGrid
from docx.oxml.ns import qn
# noinspection PyProtectedMember
from docx.section import _Header, _Footer, Section, _BaseHeaderFooter
# noinspection PyProtectedMember
from docx.styles.style import _ParagraphStyle
# noinspection PyProtectedMember
from docx.table import Table, _Cell, _Column, _Row
from docx.text.paragraph import Paragraph
# noinspection PyProtectedMember
from lxml.etree import _Element

from docx_document import _CustomBaseDocument, _CustomDocPropertyField
from docx_section import _CustomSection, LiteralHF

__all__ = ["_CustomHeaderFooter", "_CustomHeaderFooterTable"]

page_logger: Logger = logging.getLogger(__name__)
LiteralDirection = Literal["horizontal", "vertical"]


class _CustomHeaderFooter(_CustomSection):
    _instance = None
    _dict_instances = {}

    def __new__(cls, index: int, hdrftr_type: LiteralHF):
        return object.__new__(cls)

    def __init__(self, index: int, hdrftr_type: LiteralHF):
        self._custom_section = super().__init__(index)
        self.hdrftr_type = hdrftr_type
        self._name = f"custom_section_{self.index}_{self.hdrftr_type}"
        self.__class__._dict_instances[self._name] = self

    def __call__(self, name: str):
        try:
            value = self.__class__._dict_instances[name]
        except KeyError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.args}", exc_info=True)
            return
        except TypeError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.args}", exc_info=True)
            return
        except OSError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.strerror}", exc_info=True)
            return
        else:
            return value

    def __repr__(self):
        return f"<_CustomHeaderFooter(index={self.index}, hdrftr_type={self.hdrftr_type})>"

    def __str__(self):
        return f"_CustomHeaderFooter: custom section = {self.index}, header footer type = {self.hdrftr_type}"

    @property
    def header(self, **kwargs) -> Optional[_Header]:
        if self.hdrftr_type in ("first_page_header", "even_page_header", "header"):
            return getattr(self.section, self.hdrftr_type)
        else:
            return None

    @property
    def footer(self, **kwargs) -> Optional[_Footer]:
        if self.hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
            return getattr(self.section, self.hdrftr_type)
        else:
            return None

    def _default_paragraph_style(self) -> _ParagraphStyle:
        return self._parent.styles["Paragraph_Normal_Style"]

    @property
    def section(self) -> Section:
        return super().section

    @property
    def base_header_footer(self) -> _BaseHeaderFooter:
        return super().header_footer(self.hdrftr_type)

    @property
    def tables(self) -> list[Table]:
        return self.base_header_footer.tables

    def new_table(self, rows: int = 0, cols: int = 0, width: Union[int, float] = 0) -> Table:
        return self.base_header_footer.add_table(rows, cols, width)

    @property
    def paragraphs(self) -> list[Paragraph]:
        return self.base_header_footer.paragraphs

    def new_paragraph(self, text: str = None, style: _ParagraphStyle = None) -> Paragraph:
        if style is None:
            style = self.__class__._default_paragraph_style
        return self.base_header_footer.add_paragraph(text, style)

    @property
    def _element(self) -> _Element:
        # noinspection PyProtectedMember
        return self.base_header_footer._element

    def iter_tables(self) -> Generator[Table, None, None]:
        for table in self.tables:
            yield table

    def iter_paragraphs(self) -> Generator[Paragraph, None, None]:
        for paragraph in self.paragraphs:
            yield paragraph

    @property
    def doc_property_names(self) -> list[str]:
        return super().doc_property_names

    def doc_properties(self) -> list[_CustomDocPropertyField]:
        return super().doc_properties()

    def get_word_doc_property(self, property_name: str) -> _CustomDocPropertyField:
        return super().get_word_doc_property(property_name)

    def table_styles(self):
        return [super()._parent.styles["Table_Normal_Style"]]

    def paragraph_styles(self):
        return [
            super()._parent.styles[style] for style in (
                "Paragraph_Normal_Style", "Table_11_pt_Left", "Table_11_pt_Centered", "Table_13_pt_Left",
                "Table_13_pt_Centered"
            )
        ]


class _CustomHeaderFooterTable(_CustomHeaderFooter):
    index = 0
    _parent = _CustomBaseDocument()
    _dict_instances: dict[str, Any] = {}

    _cell_margins: dict[str, int] = {
        "top": 0,
        "bottom": 0,
        "left": 57,
        "right": 28
    }

    def __new__(cls, index: int, hdrftr_type: LiteralHF, table: Table, direction: LiteralDirection = "vertical"):
        super().__new__(cls, index, hdrftr_type)

    def __init__(self, index: int, hdrftr_type: LiteralHF, table: Table, direction: LiteralDirection = "vertical"):
        self.custom_header_footer: _CustomHeaderFooter = super().__init__(index, hdrftr_type)
        self.hdrftr_type = hdrftr_type
        self.table = table
        self.direction = direction
        self._name = f"custom_section_{self.index}_{self.hdrftr_type}_{self.__class__.index}"
        self.__class__._dict_instances[self._name] = self

        self.__class__.index += 1

    def __call__(self, **kwargs):
        return self.__class__._dict_instances[self._name]

    def __repr__(self):
        return f"<CustomFooterHeaderTable(custom_section={self._custom_section}, hdrftr_type={self.hdrftr_type}, " \
               f"table={self.table.part})>"

    def __str__(self):
        return f"CustomFooterHeaderTable: custom_section = {self._custom_section}, hdrftr_type = {self.hdrftr_type}, " \
               f"table={self.table}"

    def __hash__(self):
        return hash((self.index, self.hdrftr_type))

    def __key(self):
        return self.index, self.hdrftr_type

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @property
    def element(self):
        # noinspection PyProtectedMember
        return self.table._element

    @property
    def tbl(self) -> CT_Tbl:
        # noinspection PyProtectedMember
        return self.table._tbl

    @property
    def tbl_pr(self) -> CT_TblPr:
        return self.tbl.tblPr

    @property
    def tbl_grid(self) -> CT_TblGrid:
        return self.tbl.tblGrid

    @classmethod
    def custom_header_footer_table(cls, index: int, hdrftr_type: LiteralHF, direction: LiteralDirection = "vertical"):
        custom_section: _CustomSection = _CustomSection(index)
        table: Table = custom_section.header_footer(hdrftr_type).add_table(0, 0, 0)
        return cls(index, hdrftr_type, table, direction)

    @abstractmethod
    @property
    def column_width(self):
        pass

    @abstractmethod
    @property
    def row_height(self):
        pass

    @property
    def hdrftr_index(self) -> int:
        # noinspection PyProtectedMember
        return self._custom_section.header_footer(self.hdrftr_type)._hdrftr_index

    def set_alignment(self, alignment: WD_PARAGRAPH_ALIGNMENT):
        self.table.alignment = alignment
        return self

    def set_cell_sizes(self):
        column: _Column
        # noinspection PyTypeChecker
        for (column, width) in zip(self.table.columns, self.column_width):
            column.width = width
        row: _Row
        # noinspection PyTypeChecker
        for (row, height) in zip(self.table.rows, self.row_height):
            row.height = height
            row.height_rule = WD_ROW_HEIGHT_RULE.EXACTLY
        return self

    def set_bottom_top_direction(self, direction: Literal["tbRl", "btLr"] = "btLr"):
        if self.direction == "vertical":
            cell: _Cell
            for cell in self.iter_column_cells():
                # noinspection PyProtectedMember
                tc_pr: _Element = cell._tc.get_or_add_tcPr()
                text_direction: _Element = OxmlElement('w:textDirection')
                text_direction.set(qn('w:val'), direction)
                tc_pr.append(text_direction)
        return self

    def iter_row_cells(self) -> Generator[_Cell, None, None]:
        row: _Row
        cell: _Cell
        # noinspection PyTypeChecker
        for row in self.table.rows:
            for cell in row.cells:
                yield cell

    def iter_column_cells(self) -> Generator[_Cell, None, None]:
        column: _Column
        cell: _Cell
        # noinspection PyTypeChecker
        for column in self.table.columns:
            for cell in column.cells:
                yield cell

    def set_table_position(self, position_name: str):
        tbl_ppr: _Element = OxmlElement("w:tblpPr")
        for k, v in self.header_footer_position[position_name]:
            tbl_ppr.set(qn(f"w:{k}"), v)
            self.tbl_pr.append(tbl_ppr)
        return self

    @abstractmethod
    @property
    def header_footer_position(self):
        pass

    @abstractmethod
    def table_text(self):
        pass


class _CustomCell(_CustomHeaderFooterTable):
    _instance = None

    def __new__(cls, row_idx: int, col_idx: int, **kwargs):
        return object.__new__(cls)

    def __init__(self, index: int, hdrftr_type: LiteralHF, table: Table, row_idx: int, col_idx: int):
        super().__init__(index, hdrftr_type, table)
        self.row_idx: int = row_idx
        self.col_idx: int = col_idx
        self.__cell: _Cell = self.table.cell(self.row_idx, self.col_idx)

    def __call__(self):
        return super().table.cell(self.row_idx, self.col_idx)

    def __repr__(self):
        return f"<_CustomCell(index={self.index}, hdrftr_type={self.hdrftr_type}, table={self.table}, " \
               f"row_idx={self.row_idx}, col_idx={self.col_idx})>"

    def __str__(self):
        return f"CustomCell: row_idx = {self.row_idx}, col_idx = {self.col_idx}"

    @property
    def tc(self) -> CT_Tc:
        # noinspection PyProtectedMember
        return self.__cell._tc

    @property
    def tc_pr(self) -> CT_TcPr:
        return self.tc.tcPr

    @property
    def grid_span(self) -> int:
        return self.tc_pr.grid_span

    @property
    def v_align(self) -> str:
        return self.tc_pr.vAlign_val

    @property
    def v_merge(self) -> str:
        return self.tc_pr.vMerge_val

    def set_cell_margins(self):
        # noinspection PyProtectedMember
        tc_pr: CT_TcPr = self.tc.get_or_add_tcPr()
        tc_mar: _Element = OxmlElement('w:tcMar')

        for k, v in super()._cell_margins:
            cell_margins: _Element = OxmlElement(f"w:{k}")
            cell_margins.set(qn('w:w'), v)
            cell_margins.set(qn('w:type'), 'dxa')
            tc_mar.append(cell_margins)
        tc_pr.append(tc_mar)

    @property
    def column_width(self):
        return self.__cell.width

    @property
    def row_height(self):
        return

    @property
    def header_footer_position(self):
        return super().header_footer_position

    @property
    def table_text(self):
        return

    @property
    def tbl(self) -> CT_Tbl:
        return self.tc.tbl

    @property
    def tbl_pr(self) -> CT_TblPr:
        return self.tbl.tblPr

    @property
    def tbl_grid(self) -> CT_TblGrid:
        return self.tbl.tblGrid


class CustomHeaderTable(_CustomHeaderFooterTable):
    def __repr__(self):
        return "<CustomHeaderTable()>"

    def __str__(self):
        return "CustomHeaderTable"

    def cell_text(self):
        pass

    @property
    def column_width(self) -> tuple[int, int]:
        return 284, 397

    @property
    def row_height(self) -> tuple[int, ...]:
        return 3402, 3402

    @property
    def header_footer_position(self):
        return {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "273"
        }

    def table_text(self):
        return "Подпись и дата", "Справ. №"


class CustomEvenFooterTable(_CustomHeaderFooterTable):
    def __repr__(self):
        return "<CustomEvenFooterTable()>"

    def __str__(self):
        return "CustomEvenFooterTable"

    @property
    def column_width(self):
        return 284, 397

    @property
    def row_height(self):
        return 1985, 1418, 1418, 1985, 1418

    @property
    def header_footer_position(self):
        return {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8257"
        }

    @property
    def table_text(self):
        return "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл."


class CustomOddFooterTable(_CustomHeaderFooterTable):
    def __repr__(self):
        return "<CustomOddFooterTable()>"

    def __str__(self):
        return "CustomOddFooterTable"

    @property
    def column_width(self):
        return 284, 397

    @property
    def row_height(self):
        return 1985, 1418, 1418, 1985, 1418

    @property
    def header_footer_position(self):
        return {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "438",
            "tblpY": "8257"
        }

    @property
    def table_text(self):
        return "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл."


class CustomMajorFooter(_CustomHeaderFooterTable):
    @property
    def column_width(self):
        return 498, 807, 1342, 998, 674, 818, 2801, 283, 284, 284, 851, 852

    @property
    def row_height(self):
        return 794, 454, 284, 284, 284, 284, 284, 284, 284, 284

    @property
    def header_footer_position(self):
        return {
            "vertAnchor": "page",
            "horzAnchor": "page",
            "tblpX": "-283",
            "tblpY": "12985"
        }

    @property
    def table_text(self):
        return "Стр.", "Изм.", "Лист", "№ докум.", "Подп.", "Дата", "Разраб.", "Лит.", "Листов", "Пров.", \
               "М. экспертиза", "Н. контр.", "Утв."

# class CustomDocumentPage(ABC):
#     _instance = None
#
#     def __new__(cls, *args, **kwargs):
#         if cls._instance is None:
#             cls._instance = super().__new__(cls, *args, **kwargs)
#         return cls._instance
#
#     def __init__(
#             self,
#             headers: Optional[list[Any]] = None,
#             footers: Optional[list[Any]] = None):
#
#         if self.__class__._instance is None:
#             if headers is None:
#                 headers = []
#             if footers is None:
#                 footers = []
#             super().__init__(headers, footers)
#             self.headers = headers
#             self.footers = footers
#             self.__class__._instance = self
#         return
#
#     def __call__(self):
#         if self.__class__._instance is None:
#             self.__class__.__new__(self.__class__)
#             self.__init__()
#         return self.__class__._instance
#
#     def __getitem__(self, item_class: type):
#         if issubclass(item_class, self.__class__) or item_class == self.__class__:
#             return item_class()
#         else:
#             return NotImplemented
#
#     @abstractmethod
#     def header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         pass
#
#     @abstractmethod
#     def header_footer_tables(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         pass
#
#     @abstractmethod
#     def footer_position(self):
#         pass
#
#     @abstractmethod
#     def header_position(self):
#         pass


# class CustomPage(CustomDocumentPage):
#     _instance = None
#     _parent: Document = _CustomBaseDocument().document
#
#     header_footer_position = {
#         "footer_1": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "footer_2_1": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         },
#         "footer_2_2": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "-1443",
#             "tblpY": "15707"
#         },
#         "footer_3": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_4": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_5": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_6": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_7": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_8": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_9": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#     }
#
#     _headers_footers = (
#         "first_page_header", "even_page_header", "header", "first_page_footer", "even_page_footer", "footer")
#
#     def __init__(
#             self,
#             custom_section: _CustomSection,
#             headers: list[Any] = None,
#             footers: list[Any] = None):
#         super().__init__()
#         if headers is None:
#             headers = []
#         if footers is None:
#             footers = []
#         self._document: Document = self.__class__._parent
#         self._section: _CustomSection = custom_section
#         self._headers: list[_Header] = headers
#         self._footers: list[_Footer] = footers
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     def header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]) -> Union[_Header, _Footer, None]:
#         if hdrftr_type in ("first_page_header", "even_page_header", "header"):
#             return getattr(self._section, hdrftr_type)
#         elif hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
#             return getattr(self._section, hdrftr_type)
#         return
#
#     def header_footer_tables(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         if hdrftr_type in ("first_page_header", "even_page_header", "header"):
#             header: _Header = self.header_footer(hdrftr_type)
#             if header:
#                 return header.tables
#             else:
#                 page_logger.error(f"The header of the {hdrftr_type} is not found")
#                 return
#         elif hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
#             footer: _Footer = self.header_footer(hdrftr_type)
#             if footer:
#                 return footer.tables
#             else:
#                 page_logger.error(f"The footer of the {hdrftr_type} is not found")
#                 return
#
#     @abstractmethod
#     @property
#     def footer_position(self):
#         super().footer_position()
#
#     @abstractmethod
#     @property
#     def header_position(self):
#         super().footer_position()
#
#     @property
#     def table_styles(self) -> dict[str, _TableStyle]:
#         table_styles: dict[str, _TableStyle] = dict()
#         table_style_table_grid: _TableStyle = self._document.styles["Table_Normal_Style"]
#         table_styles[table_style_table_grid.name] = table_style_table_grid
#         return table_styles
#
#     @property
#     def paragraph_styles(self) -> dict[str, _ParagraphStyle]:
#         style_names: tuple[str, ...] = (
#             "Paragraph_Normal_Style", "Table_13_pt_Centered", "Table_13_pt_Left",
#             "Table_11_pt_Centered", "Table_11_pt_Left")
#         paragraph_styles: dict[str, _ParagraphStyle] = dict()
#         for style_name in style_names:
#             paragraph_style: _ParagraphStyle = self._document.styles[style_name]
#             paragraph_styles[paragraph_style.name] = paragraph_style
#         return paragraph_styles
#
#     @property
#     def styles(self) -> dict[str, Union[_TableStyle, _ParagraphStyle]]:
#         return {**self.table_styles, **self.paragraph_styles}
#
#     def get_style(self, style_name: str):
#         if style_name in self.styles:
#             return self.styles[style_name]
#         else:
#             page_logger.error(f"The style {style_name} is not found")
#             return
#
#     def set_page(self):
#         self._section.set_section_parameters()
#         self._section.set_page_borders()
#
#     def clear_header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         self.header_footer(hdrftr_type).is_linked_to_previous = True
#
#     def add_header_footer_table(
#             self,
#             rows: int,
#             cols: int,
#             width: Union[int, float],
#             hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         # noinspection PyTypeChecker
#         return self.header_footer(hdrftr_type).add_table(rows, cols, width)
#
#
# class CustomFirstPage(CustomPage):
#     _instance = None
#
#     def __init__(self):
#         super().__init__(_CustomSection(0))
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     @property
#     def header_position(self):
#         return super().header_position
#
#     def footer_position(self):
#         return {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#
#     _text_header = (
#         "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.", "", "", "", "", "")
#
#     header_footer_position = {
#         "footer_position_0": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "footer_position_1": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#     }
#
#     @property
#     def custom_page(self) -> CustomPage:
#         return super()()
#
#     def sect_pr(self):
#         return self._section.sect_pr
#
#     def first_page_footer_table(self):
#         footer: _Footer = self.header_footer("first_page_footer")
#         table: Table = footer.add_table(5, 2, Pt(34.05))
#         custom_table: CustomTable = CustomTable(self.custom_page, table)
#         custom_table.set_table_position("footer_position_0")
#         custom_table.set_bottom_top_direction()
#         custom_table.set_style("Table_Normal_Style", self.__class__._text_header, "column")
#         custom_table.set_cell_margins()
#         custom_table.set_alignment()
#
#
# class CustomSecondPage(CustomPage):
#     @property
#     def header_position(self):
#         return {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#
#     def footer_position(self):
#         return {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "273"
#         }
#
#     @property
#     def major_footer_position(self):
#         return {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "-283",
#             "tblpY": "12985"
#         }
#
#     _instance = None
#     _text_header_table = ("Справ. №", "Подпись и дата")
#     _text_footer_vert = ("Подпись и дата", "Инв. № дубл.", "Взам.инв. №", "Подпись и дата", "Инв. № подл.")
#     _text_footer_horiz = (
#         "Изм.", "Лист", "№ докум." "Подп.", "Дата", "Разраб.", "Пров.", "М. экспертиза", "Н. контр.", "Утв."
#     )
#     table_label = {
#         "height": [
#             Pt(39.7), Pt(22.7), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2)],
#         "width": [
#             Pt(24.8), Pt(40.1), Pt(66.7), Pt(49.6), Pt(33.5), Pt(40.65), Pt(139.25), Pt(14.1), Pt(14.15), Pt(14.15),
#             Pt(42.3), Pt(42.35)
#         ]
#     }
#     # footer_position_2 = {
#     #     "vertAnchor": "page",
#     #     "horzAnchor": "page",
#     #     "tblpX": "-1443",
#     #     "tblpY": "15707"
#     # }
#
#     footer_position_3 = {
#         "leftFromText": "181",
#         "rightFromText": "181",
#         "vertAnchor": "page",
#         "horzAnchor": "page",
#         "tblpX": "438",
#         "tblpY": "8313"
#     }
#
#     def __init__(self):
#         super().__init__(self._parent.sections[1])
#         self.headers = []
#         self.footers = []
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     @property
#     def custom_page(self) -> CustomPage:
#         return super()()
#
#     def second_page_footer_table_label(self):
#         footer: _Footer = self.header_footer("first_page_footer")
#         table: Table = footer.add_table(10, 12, sum(self.__class__.table_label["width"]))
#         custom_table: CustomTable = CustomTable(self.custom_page, table)
#         cell_group_unbound = CustomCellGroup(custom_table, 0, 1, 0, 4)
#         cell_group_unbound.cell_border_attrs(val="none", sz=0)
#
#
# class CustomOtherPage(CustomPage):
#     _instance = None
#     _text_header = (
#         "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.", "", "", "", "", "")
#
#     header_footer_position = {
#         "footer_position_4": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "273"
#         },
#         "footer_position_5": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "header_position_6": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         }
#     }
#
#     def __init__(self):
#         super().__init__(self._parent.sections[2])
#         self.headers = []
#         self.footers = []
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     def footer_position(self):
#         pass
#
#     def header_position(self):
#         pass
