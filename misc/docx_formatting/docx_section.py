import logging
from logging import Logger
from typing import Union
from docx.enum.section import WD_ORIENTATION, WD_SECTION_START
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
from docx.oxml.section import CT_SectPr
# noinspection PyProtectedMember
from lxml.etree import _Element
# noinspection PyProtectedMember
from docx.section import Section, _Header, _Footer, _BaseHeaderFooter
from docx_document import _CustomBaseDocument, LiteralHeader, LiteralFooter
from docx_property import _CustomZipWordDocument, _CustomDocPropertyField

__all__ = ["_CustomSection", "LiteralHF", "ImproperDocPropertyName", "ImproperSectionIndex"]


LiteralHF = Union[LiteralHeader, LiteralFooter]


class ImproperDocPropertyName(BaseException):
    """The DocProperty name is not found"""


class ImproperSectionIndex(BaseException):
    """The section index is invalid"""


class _CustomSection:
    _instance = None
    _parent = _CustomBaseDocument()

    _page_border_space = {
        "top": 31,
        "bottom": 0,
        "left": 14,
        "right": 14
    }

    section_parameters: dict[str, Union[str, bool, int]] = {
        "orientation": WD_ORIENTATION.PORTRAIT,
        "page_height": 16838,
        "page_width": 11906,
        "start_type": WD_SECTION_START.NEW_PAGE,
        "different_first_page_header_footer": True,
        "top_margin": 992,
        "bottom_margin": 1701,
        "left_margin": 1418,
        "right_margin": 624,
        "gutter": 0,
        "header_distance": 907,
        "footer_distance": 340
    }

    # noinspection PyProtectedMember
    def __init__(self, index: int = -1):
        if index == -1:
            logging.error("_CustomSection initialization error, index = -1", exc_info=True)
            raise ImproperSectionIndex
        self.index: int = index
        self.section: Section = self._parent.document.sections[self.index]
        self._instance = self

    def __call__(self):
        return self

    def __repr__(self):
        return f"<_CustomSection(index={self.index})>"

    def __str__(self):
        return f"_CustomSection: index = {self.index}, path = {self.path}"

    def __hash__(self):
        return hash((self.index, self.path))

    def __key(self):
        return self.index, self.path

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    # noinspection PyProtectedMember
    @property
    def path(self) -> str:
        return self._parent._path

    @property
    def sect_pr(self) -> CT_SectPr:
        # noinspection PyProtectedMember
        return self.section._sectPr

    def header_footer(self, hdrftr_type: LiteralHF) -> _BaseHeaderFooter:
        return getattr(self.section, hdrftr_type)

    def header(self, header_type: LiteralHeader) -> _Header:
        # noinspection PyTypeChecker
        return getattr(self.section, header_type)

    def footer(self, footer_type: LiteralFooter) -> _Footer:
        # noinspection PyTypeChecker
        return getattr(self.section, footer_type)

    @property
    def page_border_lines(self):
        page_border_lines: dict[str, dict[str, str]] = dict()
        for border in ("top", "bottom", "left", "right", "gutter", "header", "footer"):
            page_border_lines[border] = {
                "val": "single",
                "sz": "8",
                "space": self._page_border_space[border],
                "color": "auto"
            }
        return page_border_lines

    def set_page_borders(self):
        pg_borders: _Element = OxmlElement('w:pgBorders')
        pg_borders.set(qn('w:offsetFrom'), 'text')

        for key, value_dict in self.page_border_lines.items():
            border_el: _Element = OxmlElement(f"w:{key}")
            for k, v in value_dict.items():
                border_el.set(qn(f"w:{k}"), v)
            pg_borders.append(border_el)

        self.sect_pr.append(pg_borders)
        return self

    def set_section_parameters(self):
        for k, v in self.section_parameters.items():
            setattr(self.section, k, v)
        return self

    def set_section(self):
        return (
            self.set_page_borders()
                .set_section_parameters()
        )

    @classmethod
    def word_zip_file(cls) -> _CustomZipWordDocument:
        return _CustomZipWordDocument.from_base_document()

    @property
    def doc_property_names(self) -> list[str]:
        return self.word_zip_file().property_values()

    def doc_properties(self) -> list[_CustomDocPropertyField]:
        return self.word_zip_file().custom_doc_property_fields()

    def get_word_doc_property(self, property_name: str) -> _CustomDocPropertyField:
        if property_name in self.doc_property_names:
            return _CustomDocPropertyField.dict_doc_properties[property_name]
        else:
            raise ImproperDocPropertyName
