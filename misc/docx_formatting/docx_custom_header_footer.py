import logging
from logging import Logger
from typing import Union, Optional, Generator, Literal
# noinspection PyProtectedMember
# noinspection PyProtectedMember
from docx.section import _Header, _Footer, Section, _BaseHeaderFooter
# noinspection PyProtectedMember
from docx.styles.style import _ParagraphStyle
# noinspection PyProtectedMember
from docx.table import Table
from docx.text.paragraph import Paragraph
# noinspection PyProtectedMember
from lxml.etree import _Element
from docx_property import _CustomDocPropertyField
from docx_section import _CustomSection, LiteralHF

__all__ = ["_CustomHeaderFooter", "LiteralDirection"]

page_logger: Logger = logging.getLogger(__name__)
LiteralDirection = Literal["horizontal", "vertical"]


class _CustomHeaderFooter(_CustomSection):
    _instance = None
    _dict_instances = {}

    def __new__(cls, index: int, hdrftr_type: LiteralHF):
        return object.__new__(cls)

    def __init__(self, index: int, hdrftr_type: LiteralHF):
        self._custom_section = super().__init__(index)
        self.hdrftr_type = hdrftr_type
        self._name = f"custom_section_{self.index}_{self.hdrftr_type}"
        self.__class__._dict_instances[self._name] = self

    def __call__(self, name: str):
        try:
            value = self.__class__._dict_instances[name]
        except KeyError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.args}", exc_info=True)
            return
        except TypeError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.args}", exc_info=True)
            return
        except OSError as e:
            page_logger.exception(f"{e.__class__.__name__}, {e.strerror}", exc_info=True)
            return
        else:
            return value

    def __repr__(self):
        return f"<_CustomHeaderFooter(index={self.index}, hdrftr_type={self.hdrftr_type})>"

    def __str__(self):
        return f"_CustomHeaderFooter: custom section = {self.index}, header footer type = {self.hdrftr_type}"

    @property
    def header(self, **kwargs) -> Optional[_Header]:
        if self.hdrftr_type in ("first_page_header", "even_page_header", "header"):
            return getattr(self.section, self.hdrftr_type)
        else:
            return None

    @property
    def footer(self, **kwargs) -> Optional[_Footer]:
        if self.hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
            return getattr(self.section, self.hdrftr_type)
        else:
            return None

    def _default_paragraph_style(self) -> _ParagraphStyle:
        return self._parent.styles["Paragraph_Normal_Style"]

    @property
    def section(self) -> Section:
        return super().section

    @property
    def base_header_footer(self) -> _BaseHeaderFooter:
        return super().header_footer(self.hdrftr_type)

    @property
    def tables(self) -> list[Table]:
        return self.base_header_footer.tables

    def new_table(self, rows: int = 0, cols: int = 0, width: Union[int, float] = 0) -> Table:
        return self.base_header_footer.add_table(rows, cols, width)

    @property
    def paragraphs(self) -> list[Paragraph]:
        return self.base_header_footer.paragraphs

    def new_paragraph(self, text: str = None, style: _ParagraphStyle = None) -> Paragraph:
        if style is None:
            style = self.__class__._default_paragraph_style
        return self.base_header_footer.add_paragraph(text, style)

    @property
    def _element(self) -> _Element:
        # noinspection PyProtectedMember
        return self.base_header_footer._element

    def iter_tables(self) -> Generator[Table, None, None]:
        for table in self.tables:
            yield table

    def iter_paragraphs(self) -> Generator[Paragraph, None, None]:
        for paragraph in self.paragraphs:
            yield paragraph

    @property
    def doc_property_names(self) -> list[str]:
        return super().doc_property_names

    def doc_properties(self) -> list[_CustomDocPropertyField]:
        return super().doc_properties()

    def get_word_doc_property(self, property_name: str) -> _CustomDocPropertyField:
        return super().get_word_doc_property(property_name)

    def table_styles(self):
        return [super()._parent.styles["Table_Normal_Style"]]

    @property
    def paragraph_styles(self):
        return [
            super()._parent.styles[style] for style in (
                "Paragraph_Normal_Style", "Table_11_pt_Left", "Table_11_pt_Centered", "Table_13_pt_Left",
                "Table_13_pt_Centered"
            )
        ]


# class CustomDocumentPage(ABC):
#     _instance = None
#
#     def __new__(cls, *args, **kwargs):
#         if cls._instance is None:
#             cls._instance = super().__new__(cls, *args, **kwargs)
#         return cls._instance
#
#     def __init__(
#             self,
#             headers: Optional[list[Any]] = None,
#             footers: Optional[list[Any]] = None):
#
#         if self.__class__._instance is None:
#             if headers is None:
#                 headers = []
#             if footers is None:
#                 footers = []
#             super().__init__(headers, footers)
#             self.headers = headers
#             self.footers = footers
#             self.__class__._instance = self
#         return
#
#     def __call__(self):
#         if self.__class__._instance is None:
#             self.__class__.__new__(self.__class__)
#             self.__init__()
#         return self.__class__._instance
#
#     def __getitem__(self, item_class: type):
#         if issubclass(item_class, self.__class__) or item_class == self.__class__:
#             return item_class()
#         else:
#             return NotImplemented
#
#     @abstractmethod
#     def header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         pass
#
#     @abstractmethod
#     def header_footer_tables(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         pass
#
#     @abstractmethod
#     def footer_position(self):
#         pass
#
#     @abstractmethod
#     def header_position(self):
#         pass


# class CustomPage(CustomDocumentPage):
#     _instance = None
#     _parent: Document = _CustomBaseDocument().document
#
#     header_footer_position = {
#         "footer_1": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "footer_2_1": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         },
#         "footer_2_2": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "-1443",
#             "tblpY": "15707"
#         },
#         "footer_3": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_4": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_5": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_6": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_7": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_8": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#         "footer_9": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         },
#     }
#
#     _headers_footers = (
#         "first_page_header", "even_page_header", "header", "first_page_footer", "even_page_footer", "footer")
#
#     def __init__(
#             self,
#             custom_section: _CustomSection,
#             headers: list[Any] = None,
#             footers: list[Any] = None):
#         super().__init__()
#         if headers is None:
#             headers = []
#         if footers is None:
#             footers = []
#         self._document: Document = self.__class__._parent
#         self._section: _CustomSection = custom_section
#         self._headers: list[_Header] = headers
#         self._footers: list[_Footer] = footers
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     def header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]) -> Union[_Header, _Footer, None]:
#         if hdrftr_type in ("first_page_header", "even_page_header", "header"):
#             return getattr(self._section, hdrftr_type)
#         elif hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
#             return getattr(self._section, hdrftr_type)
#         return
#
#     def header_footer_tables(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         if hdrftr_type in ("first_page_header", "even_page_header", "header"):
#             header: _Header = self.header_footer(hdrftr_type)
#             if header:
#                 return header.tables
#             else:
#                 page_logger.error(f"The header of the {hdrftr_type} is not found")
#                 return
#         elif hdrftr_type in ("first_page_footer", "even_page_footer", "footer"):
#             footer: _Footer = self.header_footer(hdrftr_type)
#             if footer:
#                 return footer.tables
#             else:
#                 page_logger.error(f"The footer of the {hdrftr_type} is not found")
#                 return
#
#     @abstractmethod
#     @property
#     def footer_position(self):
#         super().footer_position()
#
#     @abstractmethod
#     @property
#     def header_position(self):
#         super().footer_position()
#
#     @property
#     def table_styles(self) -> dict[str, _TableStyle]:
#         table_styles: dict[str, _TableStyle] = dict()
#         table_style_table_grid: _TableStyle = self._document.styles["Table_Normal_Style"]
#         table_styles[table_style_table_grid.name] = table_style_table_grid
#         return table_styles
#
#     @property
#     def paragraph_styles(self) -> dict[str, _ParagraphStyle]:
#         style_names: tuple[str, ...] = (
#             "Paragraph_Normal_Style", "Table_13_pt_Centered", "Table_13_pt_Left",
#             "Table_11_pt_Centered", "Table_11_pt_Left")
#         paragraph_styles: dict[str, _ParagraphStyle] = dict()
#         for style_name in style_names:
#             paragraph_style: _ParagraphStyle = self._document.styles[style_name]
#             paragraph_styles[paragraph_style.name] = paragraph_style
#         return paragraph_styles
#
#     @property
#     def styles(self) -> dict[str, Union[_TableStyle, _ParagraphStyle]]:
#         return {**self.table_styles, **self.paragraph_styles}
#
#     def get_style(self, style_name: str):
#         if style_name in self.styles:
#             return self.styles[style_name]
#         else:
#             page_logger.error(f"The style {style_name} is not found")
#             return
#
#     def set_page(self):
#         self._section.set_section_parameters()
#         self._section.set_page_borders()
#
#     def clear_header_footer(self, hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         self.header_footer(hdrftr_type).is_linked_to_previous = True
#
#     def add_header_footer_table(
#             self,
#             rows: int,
#             cols: int,
#             width: Union[int, float],
#             hdrftr_type: Union[LiteralHeader, LiteralFooter]):
#         # noinspection PyTypeChecker
#         return self.header_footer(hdrftr_type).add_table(rows, cols, width)
#
#
# class CustomFirstPage(CustomPage):
#     _instance = None
#
#     def __init__(self):
#         super().__init__(_CustomSection(0))
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     @property
#     def header_position(self):
#         return super().header_position
#
#     def footer_position(self):
#         return {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#
#     _text_header = (
#         "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.", "", "", "", "", "")
#
#     header_footer_position = {
#         "footer_position_0": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "footer_position_1": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#     }
#
#     @property
#     def custom_page(self) -> CustomPage:
#         return super()()
#
#     def sect_pr(self):
#         return self._section.sect_pr
#
#     def first_page_footer_table(self):
#         footer: _Footer = self.header_footer("first_page_footer")
#         _table: Table = footer.add_table(5, 2, Pt(34.05))
#         custom_table: CustomTable = CustomTable(self.custom_page, _table)
#         custom_table.set_table_position("footer_position_0")
#         custom_table.set_bottom_top_direction()
#         custom_table.set_style("Table_Normal_Style", self.__class__._text_header, "column")
#         custom_table.set_cell_margins()
#         custom_table.set_alignment()
#
#
# class CustomSecondPage(CustomPage):
#     @property
#     def header_position(self):
#         return {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8313"
#         }
#
#     def footer_position(self):
#         return {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "273"
#         }
#
#     @property
#     def major_footer_position(self):
#         return {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "-283",
#             "tblpY": "12985"
#         }
#
#     _instance = None
#     _text_header_table = ("Справ. №", "Подпись и дата")
#     _text_footer_vert = ("Подпись и дата", "Инв. № дубл.", "Взам.инв. №", "Подпись и дата", "Инв. № подл.")
#     _text_footer_horiz = (
#         "Изм.", "Лист", "№ докум." "Подп.", "Дата", "Разраб.", "Пров.", "М. экспертиза", "Н. контр.", "Утв."
#     )
#     table_label = {
#         "height": [
#             Pt(39.7), Pt(22.7), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2), Pt(14.2)],
#         "width": [
#             Pt(24.8), Pt(40.1), Pt(66.7), Pt(49.6), Pt(33.5), Pt(40.65), Pt(139.25), Pt(14.1), Pt(14.15), Pt(14.15),
#             Pt(42.3), Pt(42.35)
#         ]
#     }
#     # footer_position_2 = {
#     #     "vertAnchor": "page",
#     #     "horzAnchor": "page",
#     #     "tblpX": "-1443",
#     #     "tblpY": "15707"
#     # }
#
#     footer_position_3 = {
#         "leftFromText": "181",
#         "rightFromText": "181",
#         "vertAnchor": "page",
#         "horzAnchor": "page",
#         "tblpX": "438",
#         "tblpY": "8313"
#     }
#
#     def __init__(self):
#         super().__init__(self._parent.sections[1])
#         self.headers = []
#         self.footers = []
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     @property
#     def custom_page(self) -> CustomPage:
#         return super()()
#
#     def second_page_footer_table_label(self):
#         footer: _Footer = self.header_footer("first_page_footer")
#         _table: Table = footer.add_table(10, 12, sum(self.__class__.table_label["width"]))
#         custom_table: CustomTable = CustomTable(self.custom_page, _table)
#         cell_group_unbound = CustomCellGroup(custom_table, 0, 1, 0, 4)
#         cell_group_unbound.cell_border_attrs(val="none", sz=0)
#
#
# class CustomOtherPage(CustomPage):
#     _instance = None
#     _text_header = (
#         "Подпись и дата", "Инв. № дубл.", "Взам. инв. №", "Подпись и дата", "Инв. № подл.", "", "", "", "", "")
#
#     header_footer_position = {
#         "footer_position_4": {
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "273"
#         },
#         "footer_position_5": {
#             "horzAnchor": "page",
#             "vertAnchor": "page",
#             "tblpX": "336",
#             "tblpY": "15707"
#         },
#         "header_position_6": {
#             "leftFromText": "181",
#             "rightFromText": "181",
#             "vertAnchor": "page",
#             "horzAnchor": "page",
#             "tblpX": "438",
#             "tblpY": "8257"
#         }
#     }
#
#     def __init__(self):
#         super().__init__(self._parent.sections[2])
#         self.headers = []
#         self.footers = []
#         self.__class__._instance = self
#
#     def __call__(self):
#         return self.__class__._instance
#
#     def footer_position(self):
#         pass
#
#     def header_position(self):
#         pass
