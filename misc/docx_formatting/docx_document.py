from abc import ABC, abstractmethod
import logging
from logging import Logger
from pathlib import Path
from typing import Union, Literal, Type, Generator
from docx import Document as Doc
from docx.document import Document
# noinspection PyProtectedMember
from docx.section import Sections, Section
from docx.shared import ElementProxy
# noinspection PyProtectedMember
from docx.styles.style import _TableStyle, _ParagraphStyle, _CharacterStyle, _NumberingStyle, BaseStyle
from docx.styles.styles import Styles
from docx.table import Table
# noinspection PyProtectedMember

document_logger: Logger = logging.getLogger(__name__)

__all__ = [
    "_CustomDocument", "_CustomBaseDocument", "document_logger", "LiteralHeader", "LiteralFooter", "DocStyle"]

LiteralHeader: Type[str] = Literal["first_page_header", "even_page_header", "header"]
LiteralFooter: Type[str] = Literal["first_page_footer", "even_page_footer", "footer"]
DocStyle = Union[_ParagraphStyle, _TableStyle, _CharacterStyle, _NumberingStyle]


class _CustomDocument(ABC):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, *args, **kwargs):
        if self._instance is None:
            super().__init__(*args, **kwargs)
            self._instance = self
        else:
            return

    def __call__(self, *args, **kwargs):
        return self._instance

    @abstractmethod
    def __str__(self):
        return f"_CustomDocument()"

    @abstractmethod
    def __repr__(self):
        return f"<_CustomDocument()>"

    def empty(self):
        self._instance = None
        return self


class _CustomBaseDocument(_CustomDocument):
    _instance = None
    _doc_properties: tuple[str, ...] = ("DecimalNum", "DocTypeShort", "ProjectName", "ShortName", "DocType")

    def __init__(self, path: str = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._path: str = path
        self.document: Document = Doc(self._path)

    def __call__(self):
        return self._instance

    def __repr__(self):
        return f"<_CustomBaseDocument(path={self._path})>"

    def __str__(self):
        return f"_CustomBaseDocument: path = {self._path}"

    @property
    def sections(self) -> Sections:
        return self.document.sections

    def iter_sections(self) -> Generator[Section, None, None]:
        for section in self.sections:
            yield section

    def iter_tables(self) -> Generator[Table, None, None]:
        for table in self.tables:
            yield table

    def iter_styles(self) -> Generator[BaseStyle, None, None]:
        for style in self.styles:
            yield style

    @property
    def tables(self) -> list[Table]:
        return self.document.tables

    def element(self) -> ElementProxy:
        return self.document.element

    def blob(self) -> bytes:
        return self.document.part.blob

    def content(self) -> str:
        return self.blob().decode("utf-8")

    @property
    def stem(self) -> str:
        return Path(self._path).stem

    @property
    def styles(self) -> Styles:
        return self.document.styles

    @property
    def new_document_stem(self) -> str:
        return f"{self.stem}_рамка"

    @property
    def new_document_path(self) -> str:
        return str(Path(self._path).with_stem(self.new_document_stem))

    def save(self):
        self.document.save(self.new_document_path)
