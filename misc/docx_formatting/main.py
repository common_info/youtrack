import docx
from docx.document import Document
import logging
from logging import Logger

from docx.section import _Header, _Footer
from docx_document import *
from docx_section import _CustomSection
from docx_style import *
from docx_custom_header_footer import *
from docx_table import *


logger: Logger = logging.getLogger(__name__)


def main():
    path = ""
    # prompt: str = "Укажите путь до файла, абсолютный или относительный:"
    while True:
        # path = input(prompt)
        try:
            with open(path, "r+") as file:
                file.close()
            # document: Document = docx.Document(path)
            base_document = _CustomBaseDocument(path)

        except FileNotFoundError:
            print("Файл с указанным путем не найден. Введите корректный путь до файла:")
            logger.error(f"FileNotFoundError, the improper path {path}", exc_info=True)
        except IsADirectoryError:
            print("Указанный путь указывает на директорию вместо файла. Введите корректный путь до файла:")
            logger.error(
                f"IsADirectoryError, the path {path} points to the directory, not the file", exc_info=True)
        except PermissionError:
            print("Недостаточно прав для изменения файла. Введите корректный путь до файла:")
            logger.error(
                f"IsADirectoryError, the access rights are mot enough to modify the file {path}", exc_info=True)
        except RuntimeError:
            print("Истекло время ожидания ответа системы. Попробуйте снова:")
            logger.error(
                f"IsADirectoryError, the access rights are mot enough to modify the file {path}", exc_info=True)
        except OSError:
            print("Общая внутренняя ошибка системы. Попробуйте снова (лучше через некоторое время):")
            logger.error(
                f"OSError, the general error while operating with the file {path}", exc_info=True)
        else:
            break

    section_zero: _CustomSection = _CustomSection(0)
    section_zero.set_page_borders()

    # noinspection PyTypeChecker
    section_zero_header: _Header = section_zero.header
    # noinspection PyTypeChecker
    section_zero_footer: _Footer = section_zero.footer
