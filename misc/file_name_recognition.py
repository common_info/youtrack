from pathlib import Path
from re import Pattern
from typing import Union, NamedTuple, Optional
import cv2
from fitz import Document, Rect, IRect, paper_size, Page, Point
import easyocr
import re
import os
from decimal import Decimal
from enum import Enum


class PaperSize(Enum):
    """Page size. Height x Width"""
    # A3 landscape 841.69 x 1190.4
    A3_LANDSCAPE: Point = Point(paper_size('A3-L')[0], paper_size('A3-L')[1])
    # A3 portrait 1190.4 x 841.69
    A3_PORTRAIT: Point = Point(paper_size('A3-P')[0], paper_size('A3-P')[1])
    # A4 landscape 841.69 x 595.2
    A4_LANDSCAPE: Point = Point(paper_size('A4-L')[0], paper_size('A4-L')[1])
    # A4 portrait 595.2 x 841.69
    A4_PORTRAIT: Point = Point(paper_size('A4-P')[0], paper_size('A4-P')[1])


# accuracy
EPS: int = 1

ACCURACY_LIMIT: Decimal = Decimal(0.50)


class FilePDF:
    path: Path

    def __init__(self, name: str, page: Optional[int] = 0):
        """

        :param name:
        """
        self.name: str = name
        self.document: Document = Document(filename=Path(self.__class__.path.joinpath(self.name)).__str__())
        self.page = self.document.load_page(page)

    @property
    def file_bounds(self) -> Rect:
        """

        :return:
        :rtype:
        """
        return self.page.rect.normalize()

    def page_bounds(self):
        for size_name, top_right in PaperSize.__members__.items():
            if self.page.rect.top_right.distance_to(top_right) < EPS:
                return size_name.split("_")
            else:
                continue
        raise ValueError

    @property
    def page_size(self):
        return self.page_bounds()[0]

    @property
    def orientation(self):
        return self.page_bounds()[1]

    @property
    def text_page(self):
        return self.page.get_textpage()

    @property
    def file_text(self):
        # (x0, y0, x1, y1, "lines in the block", block_no, block_type)
        return self.text_page.extractBLOCKS()

    def search_text(self, text: str):
        # FIXME
        pattern: Pattern = re.compile(text)
        for item in re.finditer(pattern, self.file_text):
            print(item)


class Block(NamedTuple):
    x0: Decimal
    y0: Decimal
    x1: Decimal
    y1: Decimal
    text: str
    block_no: int
    block_type: int

    def validate(self):
        return self.block_type in (0, 1)

