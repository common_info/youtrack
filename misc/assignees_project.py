#!/usr/bin/python
# -*- encoding: utf-8 -*-
from smtplib import SMTPServerDisconnected, SMTPException, SMTPResponseException, SMTPConnectError, \
    SMTPAuthenticationError, SMTPDataError, SMTPHeloError, SMTPRecipientsRefused, SMTPSenderRefused, \
    SMTPNotSupportedError, SMTP
from ssl import SSLContext, create_default_context
import requests
from requests import Response
from requests.exceptions import InvalidURL, InvalidHeader, HTTPError, RequestException
import json
from json import JSONDecodeError
from pathlib import Path
from typing import NamedTuple, Optional, Union
from datetime import date, datetime, timedelta
from email.message import EmailMessage
from functools import cached_property
import logging
from logging import FileHandler, Formatter, Logger, RootLogger
from logging.handlers import RotatingFileHandler

# the authentication token
AUTH_TOKEN: str = "perm:dGFyYXNvdi1h.NjEtMTUw.gRyarbzX5ieIgKEfgRalpYj1DZ8vA5"

# the headers for the statistics request
HEADERS: dict[str, str] = {
    "Authorization": " ".join(("Bearer", AUTH_TOKEN)),
    "Accept": "application/json",
    "Content-Type": "application/json",
}

# the states to filter the issues
STATES_NOT_COMPLETED: tuple[str, ...] = ("Active", "Discuss", "New", "Paused", "Review")
STATES_NOT_COMPLETED_JOIN: str = ",".join(STATES_NOT_COMPLETED)
STATES = f"State: {STATES_NOT_COMPLETED_JOIN}"

# the additional assignee fields
ASSIGNEES_PROJECT: tuple[str, ...] = (
    "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST"
)

# the DNS name of the SMTP server
SMTP_SERVER = "smtp.protei.ru"
# the listening port of the SMTP server
PORT = 587
# the login to authorize
USER = "tarasov-a"
# the password to authorize
PASSWORD = "FXYfC6dh"

# the email address of the sender
SENDER_EMAIL = "tarasov-a@protei.ru"

# mapping logins and names
LOGIN_NAME: dict[str, str] = {
    "bochkova": "Софья Бочкова",
    "brovko": "Максим Бровко",
    "demyanenko": "Светлана Демьяненко",
    "fesenko": "Виктор Фесенко",
    "kuksina": "Ольга Куксина",
    "lyalina": "Кристина Лялина",
    "matyushina": "Вероника Мозглякова",
    "mazyarova": "Дарья Мазярова",
    "nigrej": "Дмитрий Нигрей",
    "nikitina": "Наталья Никитина",
    "vykhodtsev": "Алексей Выходцев",
    "tarasov-a": "Андрей Тарасов"
}

# the text before the main message content
TEXT_PROLOGUE_RU: str = """\
Добрый день, $USERNAME$.
Краткая информация о задачах:
"""
# the text after the main message content
TEXT_EPILOGUE_RU: str = """\
\n\n--
Чтобы отписаться от рассылки, высказать пожелания, негодования или проклятья, или же письмо пришло ошибочно:
Mattermost: @tarasov-a (https://mattermost.protei.ru/protei/messages/@tarasov-a);
Telegram: @andrewtar (https://t.me/andrewtar).
"""

SEPARATOR: str = "----------"
HEADING_SEPARATOR: str = "----------Heading----------"
TIMESTAMP: str = "\n".join(("", SEPARATOR * 2, SEPARATOR * 2, date.today().isoformat(), ""))

PATH: Path = Path("./info.txt").resolve()


def convert_long_date(long: Optional[int]) -> date:
    """
    Convert the long int value to the datetime.\n
    The considered value is provided up to milliseconds, so it is required to divide by 1000.

    :param int long: the timestamp
    :return: the date associated with the timestamp.
    :rtype: date
    """
    return date.fromtimestamp(long / 1000) if long else date(1, 1, 1)


def json_file():
    """
    Reads the JSON file with the information.

    :return: the issue parameters for each user.
    :rtype: dict[str, list[dict[str, str]]]
    """
    if not Path("info.json").resolve().exists():
        Path("info.json").touch()
    with open("info.json", "r+", encoding="utf-8") as file:
        info_file = json.load(file)
    return info_file


def write_log_file(text: str):
    if not PATH.exists():
        PATH.touch()
    try:
        with open(PATH, "a", encoding="utf-8") as file:
            file.write(text)
    except UnicodeError as e:
        print(f"UnicodeError, {e.args}")


class RequestResult(NamedTuple):
    """
    Contains the parsed response instance.

    Params:
        issue --- the issue name (idReadable), str;\n
        state --- the issue state (customFields.State), str;\n
        summary --- the short issue description (summary), str;\n
        deadline --- the last day to complete the work (customFields.Дедлайн), str (ISO format);\n
        priority --- the issue importance (customFields.Priority), str (Low/Basic/Critical).\n

    Properties:
        hyperlink --- the YouTrack link to the issue, str.

    Functions:
        convert_for_json() --- format the instance to write to the JSON file, dict[str, str].
    """
    issue: str
    state: str
    summary: str
    deadline: str
    priority: str

    def __repr__(self):
        return f"<RequestResult(issue={self.issue}, state={self.state}, summary={self.summary}," \
               f"deadline={self.deadline}, priority={self.priority})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            if self.deadline:
                deadline_string = f", дедлайн: {self.deadline},"
            else:
                deadline_string = ","
            return f"Задача {self.issue}, {self.summary}, статус: {self.state}{deadline_string} " \
                   f"приоритет: {self.priority}.\nСсылка: {self.hyperlink}"
        elif format_spec == "new":
            if self.deadline:
                deadline_string = f", дедлайн: {self.deadline},"
            else:
                deadline_string = ","
            return f"Задача {self.issue}, {self.summary}, статус: {self.state}{deadline_string} " \
                   f"приоритет: {self.priority}.\nСсылка: {self.hyperlink}"
        elif format_spec == "changed":
            return f"Задача {self.issue}, статус изменился на {self.state}, приоритет: {self.priority}.\n" \
                   f"Ссылка: {self.hyperlink}"

    @property
    def hyperlink(self) -> str:
        """
        Specifies the hyperlink to the issue in the YouTrack.

        :return: the active hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    def convert_for_json(self) -> dict[str, str]:
        """
        Formats the instance to the more convenient object to write into the JSON file.

        :return: the dictionary of the attributes and their values.
        :rtype: dict[str, str]
        """
        dict_result = dict()
        dict_result["issue"] = self.issue
        dict_result["summary"] = self.summary
        dict_result["state"] = self.state
        dict_result["priority"] = self.priority
        if self.deadline:
            dict_result["deadline"] = self.deadline
        else:
            dict_result["deadline"] = ""
        return dict_result


class RequestYT(NamedTuple):
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        url --- the webhook to send the API request, str;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET.

    Functions:
         send_request() --- send the request to the YouTrack and get the response in the JSON format,
         list[dict] or None;\n
         parse_response() --- handle the response to represent as the RequestResult objects,
         list[RequestResult].
    """
    params: tuple[tuple[str, str], ...]
    url: str = "https://youtrack.protei.ru/api/issues"
    headers: dict[str, str] = HEADERS
    method: str = "get"

    def __repr__(self):
        return f"<RequestYT(params={self.params}, url={self.url}, headers={self.headers}, method={self.method})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYT: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    def send_request(self) -> Optional[list[dict]]:
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            response: Response = requests.request(self.method, url=self.url, params=self.params, headers=self.headers)
        except InvalidURL as e:
            raise InvalidURL(f"{e.errno}, {e.strerror}. Incorrect request URL.")
        except InvalidHeader as e:
            raise InvalidHeader(f"{e.errno}, {e.strerror}. Incorrect headers.")
        except ConnectionError as e:
            raise ConnectionError(f"{e.errno}, {e.strerror}. Connection failed.")
        except HTTPError as e:
            raise HTTPError(f"{e.errno}, {e.strerror}. General HTTP request error.")
        except RequestException as e:
            raise RequestException(f"{e.errno}, {e.strerror}. Main request error.")
        except OSError as e:
            raise OSError(f"{e.errno}, {e.strerror}. Global error occurred.")
        except JSONDecodeError as e:
            raise JSONDecodeError(e.msg, e.doc, e.pos)
        else:
            return response.json()

    def parse_response(self) -> list[RequestResult]:
        """
        Converts the response to the RequestResult objects.

        :return: the RequestResult instances.
        :rtype: list[RequestResult]
        """
        parsed_items: list[RequestResult] = []
        for response_item in self.send_request():
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: date = date(1, 1, 1)
            state: str = ""
            priority: str = ""

            item: dict[str, Union[str, int, dict, None]]

            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = convert_long_date(item["value"])
                # specify the issue priority
                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]
                else:
                    continue
            # nullify the improper date or stringify the deadline date
            if deadline == date(1, 1, 1):
                final_deadline = ""
            else:
                final_deadline = deadline.isoformat()
            parsed_items.append(RequestResult(issue, state, summary, final_deadline, priority))

        return parsed_items


class User:
    """
    Specifies the user to get information about.

    Params:
        login --- the user login in the YouTrack, str;
        name --- the user name due to the login, default = LOGIN_NAME[login], else login; str.

    Properties:
        params_assignee --- the query assignees_{} param of the request, str;
        params --- the params value for the API request to the YouTrack, tuple[tuple[str, str], ...];
        request_youtrack --- the full API request to the YouTrack, RequestYT;
        receiver_email --- the user email address, str;
        json_section --- the user part of the JSON file, list[RequestResult] or None;
        file_issues --- the issue names from the JSON file, set[str] or None;
        message_text --- the text of the email message, str or None;
        info_log_file --- the text of the log file, str;
        message
        server

    Functions:
        request_results() --- get the list of parsed response objects, list[RequestResult];
        send_email_message() --- send the message to the user, None;
        write_file() --- write the response information to the JSON file, None;
        response_issues() --- get the issue names from the response, set[str] or None;
        new_issues() --- specify the new issue names, set[str];
        common_issues() --- specify the non-changed issue names, set[str] or None;
        file_unit_by_issue() --- get the issue from the JSON file by its name if exists, RequestResult or None;
        response_unit_by_issue() --- get the issue from the response by its name if exists, RequestResult or None;
        changed_state() --- specify the issues with the changed states, list[RequestResult] or None;
        payload() --- specify the message text, str or None.
    """
    def __init__(self, login: str):
        self.login = login
        if login in LOGIN_NAME:
            self.name = LOGIN_NAME[login]
        else:
            self.name = self.login

    def __repr__(self):
        return f"<User({self.login}, {self.name})>"

    @cached_property
    def params_assignees(self) -> str:
        """
        Specifies the query param assignees_{} of the API request to the YouTrack.

        :return: the param string.
        :rtype: str
        """
        assignees: str = " OR ".join([f"{assignee}: {self.login}" for assignee in ASSIGNEES_PROJECT])
        return "".join(("(", assignees, ")"))

    @property
    def params(self) -> tuple[tuple[str, str], ...]:
        """
        Specifies the params value of the API request to the YouTrack.

        :return: the params attribute of the request.
        :rtype: tuple[tuple[str, str], ...]
        """
        params_field: str = ",".join(
            ("idReadable", "summary",
             "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"))
        params_query: str = " AND ".join((STATES, self.params_assignees))
        params_state: str = "State"
        params_deadline: str = "Дедлайн"
        params_priority: str = "Priority"

        params: tuple[tuple[str, str], ...] = (
            ("fields", params_field),
            ("query", params_query),
            ("customFields", params_state),
            ("customFields", params_deadline),
            ("customFields", params_priority),
        )
        logging.debug(f"Params: {params}")
        return params

    @property
    def request_youtrack(self) -> RequestYT:
        """
        Specifies the API request to the YouTrack.

        :return: the request.
        :rtype: RequestYT
        """
        request: RequestYT = RequestYT(self.params)
        logging.debug(f"RequestYT: {format(request, 'output')}")
        return request

    def request_results(self) -> list[RequestResult]:
        """
        Specifies the parsed YouTrack response instances.

        :return: the parsed response.
        :rtype: list[RequestResult]
        """
        response: list[RequestResult] = self.request_youtrack.parse_response()
        logging.debug(f"Parsed RequestYT: {[format(item, 'output') for item in response]}")
        return response

    @property
    def receiver_email(self) -> str:
        """
        Specifies the recipient email address.

        :return: the recipient email address.
        :rtype: str
        """
        receiver_email: str = f"{self.login}@protei.ru"
        logging.info(f"Receiver email: {receiver_email}")
        return receiver_email

    @property
    def message(self):
        if not self.payload():
            print("Empty messages are not sent.")
            logging.info("No messages to send")
            return
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)
        # specify the message parameters
        message["From"] = "Оповещение о задачах <tarasov-a@protei.ru>"
        message["To"] = f"{self.name} <{self.login}@protei.ru>"
        message["Subject"] = "Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST"
        message["Content-Type"] = "text/plain; charset=UTF-8; format=flowed"
        message["Content-Transfer-Encoding"] = "8bit"
        message["Content-Language"] = "ru"

        final_message: str = message.as_string()
        logging.info(f"Message: {final_message}")

        return final_message.encode("utf-8")

    @property
    def server(self) -> Optional[SMTP]:
        context: SSLContext = create_default_context()
        server: SMTP = SMTP(SMTP_SERVER, PORT)
        try:
            # set the connection
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            # authenticate
            server.login(USER, PASSWORD)
        except SMTPConnectError as e:
            print(f"SMTPConnectError, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error}, {e.strerror}")
            logging.error("SMTPConnectError", exc_info=True)
            return
        except SMTPServerDisconnected as e:
            print(f"SMTPServerDisconnected, {e.errno}, {e.filename}, {e.strerror}")
            logging.error("SMTPServerDisconnected", exc_info=True)
            return
        except SMTPNotSupportedError as e:
            print(f"SMTPNotSupportedError, {e.errno}, {e.filename}, {e.strerror}")
            logging.error("SMTPNotSupportedError", exc_info=True)
            return
        except SMTPAuthenticationError as e:
            print(f"SMTPAuthenticationError, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error}, {e.strerror}")
            logging.error("SMTPAuthenticationError", exc_info=True)
            return
        except SMTPResponseException as e:
            print(f"SMTPResponseException, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error}, {e.strerror}")
            logging.error("SMTPResponseException", exc_info=True)
            return
        except SMTPException as e:
            print(f"SMTPException, {e.errno}, {e.filename}, {e.strerror}")
            logging.error("SMTPException", exc_info=True)
            return
        except RuntimeError as e:
            print(f"RuntimeError, {e.args}")
            logging.error("RuntimeError", exc_info=True)
            return
        else:
            logging.info("SMTP server is ok")
            return server

    def send_email_message(self):
        """Sends the email."""
        if self.message is None:
            logging.info("No message is sent")
            return
        if self.server is None:
            logging.exception("No SMTP server is found", exc_info=True)
            raise Exception

        try:
            self.server.sendmail(SENDER_EMAIL, self.receiver_email, self.message)
        except SMTPHeloError as e:
            print(f"SMTPHeloError, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error}, {e.strerror}")
            logging.error("SMTPHeloError", exc_info=True)
            return
        except SMTPSenderRefused as e:
            print(f"SMTPSenderRefused, sender = {e.sender}, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error},"
                  f" {e.strerror}")
            logging.error("SMTPSenderRefused", exc_info=True)
            return
        except SMTPDataError as e:
            print(f"SMTPDataError, {e.errno}, {e.filename}, {e.smtp_code}, {e.smtp_error}, {e.strerror}")
            logging.error("SMTPDataError", exc_info=True)
            return
        except SMTPNotSupportedError as e:
            print(f"SMTPNotSupportedError, {e.errno}, {e.filename}, {e.strerror}")
            logging.error("SMTPNotSupportedError", exc_info=True)
            return
        except SMTPRecipientsRefused as e:
            print(f"SMTPRecipientsRefused, {e.recipients.values()}, {e.errno}, {e.filename}, {e.strerror}")
            logging.error("SMTPRecipientsRefused", exc_info=True)
            return
        else:
            print("Success")
        finally:
            self.server.quit()

    def write_file(self):
        """Writes the response information to the JSON file."""
        json_part: list[dict[str, str]] = [req_result.convert_for_json() for req_result in self.request_results()]
        dict_json_file: dict[str, list[dict[str, str]]] = json_file()
        dict_json_file[self.login] = json_part
        with open("info.json", "w+", encoding="utf-8") as file:
            json.dump(dict_json_file, file, indent=2, ensure_ascii=False)

    @property
    def json_section(self) -> Optional[list[RequestResult]]:
        """
        Reads the user part of the JSON file if exists.

        :return: the list of the issues.
        :rtype: list[RequestResult] or None
        """
        if not json_file():
            return

        dict_json_file: dict[str, list[dict[str, str]]] = json_file()
        dict_items: list[str] = [f"{k}: {v}" for k, v in dict_json_file.items()]
        logging.debug(f"JSON file: {', '.join(dict_items)}")
        parsed: list[dict[str, str]] = dict_json_file[self.login]
        return [RequestResult(item["issue"], item["state"], item["summary"], item["deadline"], item["priority"])
                for item in parsed] if parsed else None

    @property
    def file_issues(self) -> Optional[set[str]]:
        """
        Reads the issue names from the JSON file if the user part is not empty.

        :return: the issue names.
        :rtype: set[str] or None
        """
        file_issues: set[str] = set([item.issue for item in self.json_section]) if self.json_section else None
        logging.debug(f"File issues: {file_issues}")
        return file_issues

    def response_issues(self) -> Optional[set[str]]:
        """
        Gets the issue names from the response.

        :return: the issue names.
        :rtype: set[str] or None
        """
        response_issues: Optional[set[str]]
        if self.request_results():
            response_issues = set([item.issue for item in self.request_results()])
            logging.debug(f"Response issues: {response_issues}")
        else:
            response_issues = None
            logging.debug(f"Response issues: None")
        return response_issues

    def new_issues(self) -> set[str]:
        """
        Specifies the new issue names.

        :return: the new issue names.
        :rtype: set[str]
        """
        new_issues: set[str]
        if self.file_issues:
            new_issues = self.response_issues().difference(self.file_issues)
        else:
            new_issues = self.response_issues()

        logging.debug(f"New issues: {new_issues}")
        return new_issues

    def common_issues(self) -> Optional[set[str]]:
        """
        Specifies the non-changed issue names.

        :return: the non-changed issue names.
        :rtype: set[str]
        """
        common_issues: Optional[set[str]]

        if self.file_issues:
            common_issues = set.intersection(self.file_issues, self.response_issues())
            logging.debug(f"Common issues: {common_issues}")
        else:
            common_issues = None
            logging.debug("Common issues: None")
        return common_issues

    def file_unit_by_issue(self, issue: str) -> Optional[RequestResult]:
        """
        Get the issue from the JSON file by its name if exists.

        :param str issue: the issue name
        :return: the issue.
        :rtype: RequestResult or None
        """
        if self.json_section:
            for unit in self.json_section:
                if unit.issue == issue:
                    logging.debug(f"Issue: {issue}, file: {str(unit)}")
                    return unit
                else:
                    continue
            logging.error(f"KeyError, the file for the issue {issue} is not found")
            raise KeyError(f"The issue {issue} is not found.")
        else:
            logging.warning("JSON file not found")
            return

    def response_unit_by_issue(self, issue: str) -> Optional[RequestResult]:
        """
        Get the issue from the response by its name if exists.

        :param str issue: the issue name
        :return: the issue.
        :rtype: RequestResult or None
        """
        for unit in self.request_results():
            if unit.issue == issue:
                return unit
            else:
                continue
        logging.error(f"KeyError, the response for the issue {issue} is not found")
        raise KeyError(f"The issue {issue} is not found.")

    def changed_state(self) -> Optional[list[RequestResult]]:
        """
        Specifies the issues with the changed states.

        :return: the issues.
        :rtype: list[RequestResult] or None
        """
        if not self.common_issues():
            logging.debug("user.changed_state is broken")
            return

        items = []
        for issue in self.common_issues():
            file_unit: RequestResult = self.file_unit_by_issue(issue)
            response_unit: RequestResult = self.response_unit_by_issue(issue)
            if file_unit.state != response_unit.state:
                items.append(file_unit)
            else:
                continue
        logging.info(f"Changed state: {items}")
        return items

    def payload(self) -> Optional[str]:
        """
        Specifies the message content.

        :return: the message text.
        :rtype: str or None
        """
        # if nothing is changed
        if not (self.new_issues() or self.changed_state()):
            print("No new information.")
            logging.info("No data changed")
            return

        new_items: Optional[list[RequestResult]] = [self.response_unit_by_issue(issue) for issue in self.new_issues()]

        if new_items:
            text_new_ru: str = "\n".join([format(item, "new") for item in new_items])
            text_new_introduction_ru: str = "Новые запросы:"
        else:
            text_new_ru: str = ""
            text_new_introduction_ru: str = "Новых запросов нет."

        if self.changed_state():
            text_changed_introduction_ru: str = "Задачи с изменившимся статусом:"
            text_changed_ru: str = "\n".join([format(item, "changed") for item in self.changed_state()])
        else:
            text_changed_introduction_ru: str = "Статусы задач не изменились."
            text_changed_ru: str = ""

        payload = "\n".join((text_new_introduction_ru, text_new_ru, text_changed_introduction_ru, text_changed_ru))
        logging.debug(f"Payload: {payload}")
        return payload

    @property
    def message_text(self) -> str:
        """
        Specifies the text of the email message.

        :return: the email message text.
        :rtype: str
        """
        message_text: str = "\n".join((TEXT_PROLOGUE_RU, self.payload(), TEXT_EPILOGUE_RU))
        logging.debug(f"Message text: {message_text}")
        return message_text

    @property
    def info_log_file(self) -> str:
        """
        Specifies the text of the log file.

        :return: the log file text.
        :rtype: str
        """
        return "\n".join((SEPARATOR, self.name, self.receiver_email, HEADING_SEPARATOR, str(self.payload()), ""))


def main():
    # start the global timer
    datetime_start: datetime = datetime.now()
    logging.info("Global timer started")
    for login in LOGIN_NAME:
        # start the user timer
        user_timer_start: datetime = datetime.now()
        logging.debug("Timer started")
        user: User = User(login)
        logging.info(f"User {user.name} started")
        # user.send_email_message()
        logging.info(f"User {user.name} finished")
        # write the log file
        write_log_file(TIMESTAMP)
        logging.info(user.info_log_file)
        write_log_file(user.info_log_file)
        # write the JSON file
        user.write_file()
        # stop the user timer
        user_timer_end: datetime = datetime.now()
        logging.debug("Timer stopped")
        user_time: timedelta = user_timer_end - user_timer_start
        logging.debug(f"Processing time: {user_time}")
        print(f"{user.name}: {str(user_time)}")

    # stop the global timer
    datetime_end: datetime = datetime.now()
    work_time: timedelta = datetime_end - datetime_start
    logging.info("Global timer stopped")
    logging.info(f"Global processing time: {str(work_time)}")
    print(f"Время работы: {str(work_time)}")


if __name__ == "__main__":
    # logging._srcfile = None
    logging.logThreads = False
    logging.logProcesses = False
    logging.logMultiprocessing = False

    fmt: str = "{name}:{levelname}:{asctime} --- {filename}:{lineno} --- {message}"
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    formatter: Formatter = Formatter(fmt, datefmt, "{")

    basic_handler: FileHandler = FileHandler("basic_log.log", "a", "utf-8")
    basic_handler.setLevel(logging.INFO)
    basic_handler.setFormatter(formatter)

    debug_handler: FileHandler = FileHandler("debug_log.log", "w", "utf-8")
    debug_handler.setLevel(logging.DEBUG)
    debug_handler.setFormatter(formatter)

    logging.basicConfig(style="{", level=logging.DEBUG, handlers=(basic_handler, debug_handler))
    logger: Logger = logging.getLogger()

    print(logger.name)
    print(logger.manager.loggerDict)
    print(logger.manager.logRecordFactory)
    main()
