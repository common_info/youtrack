import json
import re
from pathlib import Path
from pprint import pprint
from typing import Union
from re import Pattern


class JsonHandling:
    _no_quotation: tuple[str, ...] = ("int", "bool", "isdn_addr", "flag", "ext_bs", "bs")
    _need_quotation: tuple[str, ...] = ("string", "octet", "bit_string")
    _pattern_need_quotation: Pattern = re.compile(
        r"([\"<]*string[>\"]*)|([\"<]*octet[>\"]*)|([\"<]*bit_string[>\"]*)")

    _pattern_no_quotation: Pattern = re.compile(
        r"([\"<]int[>\"]*)|([\"<]*bool[>\"]*)|([\"<]*isdn_addr[>\"]*)|([\"<]*flag[>\"]*)|"
        r"([\"<]*ext_bs[>\"]*)|([\"<]*bs[>\"]*)")

    _replace_dict: dict[str, str] = {
        "integer": "int",
        "isdn_address": "isdn_addr",
        "octet_string": "octet"
    }

    def __init__(self, content: bytes):
        self.content = content
        self.text = self.content.decode("utf-8")

    @classmethod
    def from_file(cls, file_path: Union[Path, str]):
        try:
            with open(file_path, "rb") as file:
                content: bytes = file.read()
        except OSError as e:
            print(e.__class__.__name__, e.strerror)
            return
        else:
            return cls(content)

    @classmethod
    def from_string(cls, text: str):
        content = bytes(json.dumps(text, indent=2), "utf-8")
        return cls(content)

    def replace(self):
        for k, v in self.__class__._replace_dict.items():
            self.text = re.sub(re.compile(k), v, self.text)
        return self

    def delete_quotation(self):
        match = re.match(self.__class__._pattern_no_quotation, self.text)
        # print(self.__class__._pattern_need_quotation)
        # print(match)
        if match:
            print(f"match={match}")
            for match_group in match.groups():
                self.text = re.sub(match_group, f'<{match_group}>', self.text)
        return self

    def add_quotation(self):
        # re.sub(self.__class__._pattern_need_quotation, )
        match = re.search(self.__class__._pattern_need_quotation, self.text)
        if match:
            for match_group in match.groups():
                print(match_group)
                self.text = re.sub(match_group, f'"<{match_group}>"', self.text)
        return self

    def jsonify(self):
        self.text = json.loads(json.dumps(self.text, indent=2))
        return self

    def handle(self):
        # print(self.__class__._pattern_need_quotation)
        return self.replace().delete_quotation().jsonify().text


if __name__ == "__main__":
    str_text: str = """
{
  "MO_SMS_CSI": {
    "SmsCamelTDPDataList": [
      {
        "SmsTriggerDetectionPoint": "<string>",
        "ServiceKey": "<int>",
        "GsmSCF_Address": "<isdn_addr>",
        "DefaultCallHandling": "<string>"
      }
    ],
    "CamelCapabilityHandling": "<int>",
    "NotificationToCSE": "<flag>",
    "CSI_Active": "<flag>"
  }
}
    """
    test = bytes(str_text, "utf-8")
    json_handle = JsonHandling.from_string(str_text)
    test_handle = JsonHandling(test)
    # print(json_handle.text)
    # print(test_handle.text)
    print(test_handle.handle())
