import logging
import requests
from base64 import standard_b64decode as base64_decode
from textwrap import dedent
from typing import Optional, Union, Any
from sys import stdout
from os import environ
from platform import uname
from bisect import bisect_left
from logging import Logger, Formatter, StreamHandler, LogRecord
from requests import Response
from re import Pattern, Match, compile, match
from operator import lshift

terms_logger: Logger = logging.getLogger(__name__)
http_logger: Logger = logging.getLogger("http_logger")

allowed_modify: list[str] = ["AndrewTarasov", "tarasov-a"]


def log_round_trip(response: Response, *args, **kwargs):
    extra: dict[str, Any] = {"request": response.request, "response": response}
    http_logger.debug(f"HTTP roundtrip", extra=extra)


class HTTPFormatter(Formatter):
    @staticmethod
    def format_headers(headers: dict[str, Any]):
        return "\n".join(f"{header}: {header_value}" for header, header_value in headers.items())

    def formatMessage(self, record: LogRecord) -> str:
        result = super().formatMessage(record)
        if record.name == "http_logger":
            result += dedent("""
            ----------request----------
            {request.method} {request.url}
            {request_headers}

            {request.body}
            ----------response----------
            {response.status_code} {response.reason} {response.url}
            {response_headers}

            {response.text}
            ----------------------------
            """).format(
                request=record.request,
                response=record.response,
                request_headers=self.format_headers(record.request.headers),
                response_headers=self.format_headers(record.response.headers),
            )
        return result


def analyze_user():
    platform_name = uname().system

    if platform_name == "win32":
        username = environ.get("USERNAME")
    else:
        username = environ.get("USER")
    allowed = username in allowed_modify

    terms_logger.info(f"User {username}, OS name: {platform_name}, allowed: {allowed}")
    terms_logger.info(f"User info: OS = {uname().system}, {uname().version}, {uname().release}, {uname().machine}, "
                      f"{uname().processor}, node = {uname().node}")
    return allowed


class _Singleton:
    """ A metaclass that creates a Singleton base class when called. """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *args, **kwargs):
        if self._instance is None:
            super().__init__(*args, **kwargs)
            self.__class__._instance = self
        else:
            return

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self.__class__.__new__(self.__class__, *args, **kwargs)
            self.__init__(*args, **kwargs)
        return self._instance


def binary_search(
        values: Union[list[str], list[int]],
        search_value: Union[str, int],
        min_index: int = 0,
        max_index: int = None) -> int:
    """
    Specifies the binary search to accelerate the common one.

    :param values: the list to search the value
    :type values: list[str]
    :param str search_value: the value to search
    :param int min_index: the lower bound, default = 0
    :param max_index: the higher bound, default = len(values)
    :return: the searched value position in the list if the value is in the list.
    :rtype: int
    """
    if max_index is None:
        max_index = len(values)
    # sort the values
    sorted_values = sorted(values)
    index = bisect_left(sorted_values, search_value, min_index, max_index)
    # print the note
    return index if index != max_index and sorted_values[index] == search_value else -1


class CustomRequest(_Singleton):
    def __init__(
            self,
            project_id: Union[str, int] = "common_info%2Fannex",
            file_path: str = "terms.md",
            ref: str = "main"):
        super().__init__()
        self.method: str = "GET"
        self.url: str = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/{file_path}"
        self.params: dict = {"ref": ref}

    @property
    def response(self) -> dict[str, Union[str, bytes]]:
        response: Response = requests.request(method=self.method, url=self.url, params=self.params)
        if response.status_code != 200:
            http_logger.info("Error HTTP connection")
        return response.json()

    @property
    def binary_content(self) -> bytes:
        return self.response["content"]


class TermDictionary(_Singleton):
    def __init__(
            self,
            binary_content: bytes = CustomRequest().binary_content,
            separator: str = "\n",
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.allowed = analyze_user()
        self.binary_content = binary_content
        self.separator = separator
        self.terms: dict[int, Term] = {}
        self.short_id: dict[str, list[int]] = {}

    def __len__(self):
        return len(self.terms)

    def __bool__(self):
        return len(self) > 0

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.short_id
        elif isinstance(item, Term):
            return item in self.terms.values()
        elif isinstance(item, int):
            return item in self.terms

    def __getitem__(self, item):
        if not isinstance(item, str):
            terms_logger.exception(f"The item {item} has an invalid type, {type(item)}", exc_info=True)
            raise TypeError
        else:
            if item not in self.short_terms:
                terms_logger.exception(f"The key {item} is not in the dictionary", exc_info=True)
                raise KeyError
            else:
                return self.binary_by_short(item)

    @property
    def content_decoded(self) -> str:
        return base64_decode(self.binary_content).decode("utf-8")

    @property
    def content_splitted(self) -> list[str]:
        return self.content_decoded.split(self.separator)

    def heading(self) -> list[str]:
        return self.content_splitted[:4]

    def content(self) -> list[str]:
        return self.content_splitted[4:-1]

    def full_terms(self):
        return [Term(raw_line) for raw_line in self.content()]

    @property
    def term_indexes(self) -> list[int]:
        return [key for key in self.terms.keys()]

    @property
    def short_terms(self) -> list[str]:
        return [key for key in self.short_id.keys()]

    def binary_by_id(self, index: int):
        """
        Get the Term instance by the identifier using the binary search.

        :param index: the term identifier
        :return: the Term instance if exists.
        :rtype: TermUnit or None
        """
        position = binary_search(self.term_indexes, index)
        # if the term exists
        if position != -1:
            return self.terms[position]
        # if the term does not exist
        else:
            terms_logger.warning(f"Invalid index, {index}")
            return

    def binary_by_short(self, short: str):
        """
        Get the Term instance by the short using the binary search.

        :param short: the short term
        :return: the Term instance if exists.
        :rtype: TermUnit or list[TermUnit] or None
        """
        if short not in self.short_terms:
            terms_logger.warning(f"{short} not found")
            return f"{short} not found"
        indexes = self.short_id[short]
        return [self.terms[idx] for idx in indexes]

    def get_term(self, search_value: str, attr: Optional[str] = None):
        return [term.get_term_attr(attr) for term in self.binary_by_short(search_value)]


class Term:
    PATTERN: Pattern = compile(r"\|\s(.+)\s\|\s(.*)\s\|\s(.*)\s\|\s(.*)\s\|")
    _parent: TermDictionary = TermDictionary()
    identifier: int = 0

    def __init__(self, raw_line: str):
        self.index: int = self.__class__.identifier
        self.raw_line: str = raw_line
        self._parent.terms[self.index] = self

        if self.short not in self._parent.short_id:
            self._parent.short_id[self.short] = []
        self._parent.short_id[self.short].append(self.index)

        self.__class__.identifier += 1

    def __repr__(self):
        return f"<Term(raw_line={self.raw_line})>"

    def __str__(self):
        return f"Term: {self.raw_line}"

    def _format_type(self):
        is_full_en: bool = self.full_en is True
        is_full_ru: bool = self.full_ru is True
        is_commentary: bool = self.commentary is True
        format_type: list[bool] = [is_full_en, is_full_ru, is_commentary]
        return sum(lshift(v, i) for i, v in enumerate(reversed(format_type)))

    def _format_string(self):
        if self._format_type() == 7:
            return f"{self.full_en}; {self.full_ru} – {self.commentary}"
        elif self._format_type() == 6:
            return f"{self.full_en}; {self.full_ru}"
        elif self._format_type() == 5:
            return f"{self.full_en} – {self.commentary}"
        elif self._format_type() == 3:
            return f"{self.full_ru} – {self.commentary}"

    def __format__(self, format_spec):
        if format_spec == "en":
            return f"{self.full_en}" if self.full_en else "-----"
        elif format_spec == "ru":
            return f"{self.full_ru}" if self.full_ru else "-----"
        elif format_spec == "comment":
            return f"{self.commentary}" if self.commentary else "-----"
        elif format_spec == "full":
            return self._format_string()
        elif format_spec is None:
            return self._format_string()

    def _pattern_match(self) -> Match:
        return match(self.__class__.PATTERN, self.raw_line)

    @property
    def short(self) -> str:
        if self._pattern_match() is None:
            terms_logger.exception(f"The line is not parsed, raw_line = {self.raw_line}", exc_info=True)
            raise AttributeError
        return self._pattern_match().group(1)

    @property
    def full_en(self) -> Optional[str]:
        return self._pattern_match().group(2)

    @property
    def full_ru(self) -> Optional[str]:
        return self._pattern_match().group(3)

    @property
    def commentary(self) -> Optional[str]:
        return self._pattern_match().group(4)

    def get_term_attr(self, attr: Optional[str] = None):
        print(f"self.full_en = {self.full_en}, self.full_ru = {self.full_ru}, self.commentary = {self.commentary}")
        print(self.full_en is None, self.full_ru is None, self.commentary is None)
        if attr is None:
            return format(self, "full")
        elif attr in ("full_en", "full_ru", "commentary"):
            return format(self, attr)
        else:
            logging.warning(f"The attribute {attr} cannot be displayed")
            return


def main():
    fmt = "{asctime}:{levelname}:{name} | {filename}:{lineno} | {message}"
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    formatter: Formatter = Formatter(fmt, datefmt, "{")

    stream_handler: StreamHandler = StreamHandler(stdout)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formatter)

    terms_logger.setLevel(logging.DEBUG)
    terms_logger.addHandler(stream_handler)


if __name__ == "__main__":
    main()
    term_dictionary: TermDictionary = TermDictionary()
    term_dictionary.full_terms()

    terms = [line.strip() for line in input("").split(";")]
    print([term_dictionary.get_term(search_term) for search_term in terms])
