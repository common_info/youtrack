#!/usr/bin/python
# -*- encoding: utf-8 -*-
import sys
import textwrap
from smtplib import SMTPServerDisconnected, SMTPException, SMTPResponseException, SMTPConnectError, \
    SMTPAuthenticationError, SMTPDataError, SMTPHeloError, SMTPRecipientsRefused, SMTPSenderRefused, \
    SMTPNotSupportedError, SMTP
from ssl import SSLContext, create_default_context
import requests
from requests import Response, Session, PreparedRequest, Request
from requests.exceptions import InvalidURL, InvalidHeader, HTTPError, RequestException
import json
from json import JSONDecodeError
from pathlib import Path
from typing import NamedTuple, Optional, Union, Any
from datetime import date, datetime, timedelta
from email.message import EmailMessage
from functools import cached_property
import logging
from logging import Formatter, Logger, LogRecord, StreamHandler, FileHandler
from logging.handlers import RotatingFileHandler


# the authentication token
AUTH_TOKEN: str = "perm:dGFyYXNvdi1h.NjEtMTUw.gRyarbzX5ieIgKEfgRalpYj1DZ8vA5"

# the headers for the statistics request
HEADERS: dict[str, str] = {
    "Authorization": " ".join(("Bearer", AUTH_TOKEN)),
    "Accept": "application/json",
    "Content-Type": "application/json",
}

HEADERS_TUPLE = (
    ("Authorization", " ".join(("Bearer", AUTH_TOKEN))),
    ("Accept", "application/json"),
    ("Content-Type", "application/json"),
)

# the states to filter the issues
STATES_NOT_COMPLETED: tuple[str, ...] = ("Active", "Discuss", "New", "Paused", "Review")
STATES_NOT_COMPLETED_JOIN: str = ",".join(STATES_NOT_COMPLETED)
STATES = f"State: {STATES_NOT_COMPLETED_JOIN}"

# the additional assignee fields
ASSIGNEES_PROJECT: tuple[str, ...] = (
    "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST"
)

# the DNS name of the SMTP server
SMTP_SERVER = "smtp.protei.ru"
# the listening port of the SMTP server
PORT = 587
# the login to authorize
USER = "tarasov-a"
# the password to authorize
PASSWORD = "FXYfC6dh"

# the email address of the sender
SENDER_EMAIL = "tarasov-a@protei.ru"

# mapping logins and names
LOGIN_NAME: dict[str, str] = {
    "bochkova": "Софья Бочкова",
    "brovko": "Максим Бровко",
    "demyanenko": "Светлана Демьяненко",
    "fesenko": "Виктор Фесенко",
    "kuksina": "Ольга Куксина",
    "lyalina": "Кристина Лялина",
    "matyushina": "Вероника Мозглякова",
    "mazyarova": "Дарья Мазярова",
    "nigrej": "Дмитрий Нигрей",
    "nikitina": "Наталья Никитина",
    "vykhodtsev": "Алексей Выходцев",
    "tarasov-a": "Андрей Тарасов"
}

# the text before the main message content
TEXT_PROLOGUE_RU: str = """\
Добрый день, $USERNAME$.
Краткая информация о задачах:
"""
# the text after the main message content
TEXT_EPILOGUE_RU: str = """\
\n\n--
Чтобы отписаться от рассылки, высказать пожелания, негодования или проклятья, или же письмо пришло ошибочно:
Mattermost: @tarasov-a (https://mattermost.protei.ru/protei/messages/@tarasov-a);
Telegram: @andrewtar (https://t.me/andrewtar).
"""

SEPARATOR: str = "----------"
HEADING_SEPARATOR: str = "----------Heading----------"
TIMESTAMP: str = "\n".join(("", SEPARATOR * 2, SEPARATOR * 2, date.today().isoformat(), ""))

PATH: Path = Path("./info.txt").resolve()

# the table to replace non-printing Unicode symbols
CONVERSION_TABLE = {
    "\u2011": "-",
    "\u2010": "-",
    "\t": " "
}


logger: Logger = logging.getLogger("send_emails")
requests_logger: Logger = logging.getLogger("http_logger")


def replace_unicode_chars(text: Optional[str]):
    # TODO
    if text is None:
        return
    if any(key in text for key in CONVERSION_TABLE):
        for key, value in CONVERSION_TABLE.items():
            if key in text:
                text = text.replace(key, value)
    return text


def log_round_trip(response: Response, *args, **kwargs):
    extra: dict[str, Any] = {"request": response.request, "response": response}
    requests_logger.debug(f"HTTP roundtrip", extra=extra)


class HTTPFormatter(Formatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # print("HTTPFormatter")

    @staticmethod
    def format_headers(headers: dict[str, Any]):
        return "\n".join(f"{header}: {header_value}" for header, header_value in headers.items())

    def formatMessage(self, record: LogRecord) -> str:
        result = super().formatMessage(record)
        if record.name == "http_logger":
            result += textwrap.dedent("""
            ----------request----------
            {request.method} {request.url}
            {request_headers}

            {request.body}
            ----------response----------
            {response.status_code} {response.reason} {response.url}
            {response_headers}

            {response.text}
            ----------------------------
            """).format(
                request=record.request,
                response=record.response,
                request_headers=self.format_headers(record.request.headers),
                response_headers=self.format_headers(record.response.headers),
            )
        print(record.request, record.response)
        return result


def convert_long_date(long: Optional[int]) -> date:
    """
    Convert the long int value to the datetime.\n
    The considered value is provided up to milliseconds, so it is required to divide by 1000.

    :param int long: the timestamp
    :return: the date associated with the timestamp.
    :rtype: date
    """
    return date.fromtimestamp(long / 1000) if long else date(1, 1, 1)


def json_file():
    """
    Reads the JSON file with the information.

    :return: the issue parameters for each user.
    :rtype: dict[str, list[dict[str, str]]]
    """
    if not Path("info.json").resolve().exists():
        Path("info.json").touch()
    with open("info.json", "r+", encoding="utf-8") as file:
        info_file = json.load(file)
    return info_file


class RequestResult(NamedTuple):
    """
    Contains the parsed response instance.

    Params:
        issue --- the issue name (idReadable), str;\n
        state --- the issue state (customFields.State), str;\n
        summary --- the short issue description (summary), str;\n
        deadline --- the last day to complete the work (customFields.Дедлайн), str (ISO format);\n
        priority --- the issue importance (customFields.Priority), str (Low/Basic/Critical).\n

    Properties:
        hyperlink --- the YouTrack link to the issue, str.

    Functions:
        convert_for_json() --- format the instance to write to the JSON file, dict[str, str].
    """
    issue: str
    state: str
    summary: str
    deadline: str
    priority: str

    def __repr__(self):
        return f"<RequestResult(issue={self.issue}, state={self.state}, summary={self.summary}," \
               f"deadline={self.deadline}, priority={self.priority})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            if self.deadline:
                deadline_string = f", дедлайн: {self.deadline},"
            else:
                deadline_string = ","
            return f"Задача {self.issue}, {self.summary}, статус: {self.state}{deadline_string} " \
                   f"приоритет: {self.priority}.\nСсылка: {self.hyperlink}"
        elif format_spec == "new":
            if self.deadline:
                deadline_string = f", дедлайн: {self.deadline},"
            else:
                deadline_string = ","
            return f"Задача {self.issue}, {self.summary}, статус: {self.state}{deadline_string} " \
                   f"приоритет: {self.priority}.\nСсылка: {self.hyperlink}"
        elif format_spec == "changed":
            return f"Задача {self.issue}, статус изменился на {self.state}, приоритет: {self.priority}.\n" \
                   f"Ссылка: {self.hyperlink}"

    @property
    def hyperlink(self) -> str:
        """
        .. py:function::
        Specifies the hyperlink to the issue in the YouTrack.

        :return: the active hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    def convert_for_json(self) -> dict[str, str]:
        """
        Formats the instance to the more convenient object to write into the JSON file.

        :return: the dictionary of the attributes and their values.
        :rtype: dict[str, str]
        """
        dict_result = dict()
        dict_result["issue"] = self.issue
        dict_result["summary"] = self.summary
        dict_result["state"] = self.state
        dict_result["priority"] = self.priority
        if self.deadline:
            dict_result["deadline"] = self.deadline
        else:
            dict_result["deadline"] = ""
        return dict_result


class RequestYT:
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        url --- the webhook to send the API request, str;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET.

    Functions:
         send_request() --- send the request to the YouTrack and get the response in the JSON format,
         list[dict] or None;\n
         parse_response() --- handle the response to represent as the RequestResult objects,
         list[RequestResult].
    """
    def __init__(
            self,
            session: Session,
            params: tuple[tuple[str, str], ...],
            url: str = "https://youtrack.protei.ru/api/issues",
            headers: tuple[tuple[str, str], ...] = HEADERS_TUPLE,
            method: str = "get"):
        self.session = session
        self.params = params
        self.url = url
        self.headers: dict[str, str] = dict([(header[0], header[1]) for header in headers])
        self.method = method

    def __repr__(self):
        return f"<RequestYT(params={self.params}, url={self.url}, headers={self.headers}, method={self.method})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYT: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    @cached_property
    def send_request(self) -> Optional[list[dict]]:
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            prepared_request: PreparedRequest = Request(
                method=self.method, url=self.url, headers=self.headers, params=self.params).prepare()
            response: Response = self.session.send(prepared_request)
            json_response: list[dict] = response.json()
        except InvalidURL:
            logger.exception(f"InvalidURL. Incorrect request URL", exc_info=True)
            raise
        except InvalidHeader:
            logger.exception("InvalidHeader. Incorrect headers", exc_info=True)
            raise
        except ConnectionError:
            logger.exception("ConnectionError. Connection failed", exc_info=True)
            raise
        except HTTPError:
            logger.exception(f"HTTPError. General HTTP request error", exc_info=True)
            raise
        except RequestException:
            logger.exception("RequestException. Main request error", exc_info=True)
            raise
        except OSError:
            logger.exception("OSError. Global error occurred")
            raise
        except JSONDecodeError:
            logger.exception("JSONDecodeError. JSON decoding error occurred.", exc_info=True)
            raise
        else:
            logger.debug("Response")

            return json_response

    def parse_response(self) -> list[RequestResult]:
        """
        Converts the response to the RequestResult objects.

        :return: the RequestResult instances.
        :rtype: list[RequestResult]
        """
        parsed_items: list[RequestResult] = []
        for response_item in self.send_request:
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: date = date(1, 1, 1)
            state: str = ""
            priority: str = ""

            item: dict[str, Union[str, int, dict, None]]

            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = convert_long_date(item["value"])
                # specify the issue priority
                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]
                else:
                    continue
            # nullify the improper date or stringify the deadline date
            if deadline == date(1, 1, 1):
                final_deadline = ""
            else:
                final_deadline = deadline.isoformat()
            parsed_items.append(RequestResult(issue, state, summary, final_deadline, priority))

        return parsed_items


class User:
    """
    Specifies the user to get information about.

    Params:
        login --- the user login in the YouTrack, str;
        name --- the user name due to the login, default = LOGIN_NAME[login], else login; str.

    Properties:
        params_assignee --- the query assignees_{} param of the request, str;
        params --- the params value for the API request to the YouTrack, tuple[tuple[str, str], ...];
        request_youtrack --- the full API request to the YouTrack, RequestYT;
        receiver_email --- the user email address, str;
        json_section --- the user part of the JSON file, list[RequestResult] or None;
        file_issues --- the issue names from the JSON file, set[str] or None;
        message_text --- the text of the email message, str or None;
        info_log_file --- the text of the log file, str;
        message
        server

    Functions:
        request_results() --- get the list of parsed response objects, list[RequestResult];
        send_email_message() --- send the message to the user, None;
        write_file() --- write the response information to the JSON file, None;
        response_issues() --- get the issue names from the response, set[str] or None;
        new_issues() --- specify the new issue names, set[str];
        common_issues() --- specify the non-changed issue names, set[str] or None;
        file_unit_by_issue() --- get the issue from the JSON file by its name if exists, RequestResult or None;
        response_unit_by_issue() --- get the issue from the response by its name if exists, RequestResult or None;
        changed_state() --- specify the issues with the changed states, list[RequestResult] or None;
        payload() --- specify the message text, str or None.
    """

    def __init__(self, login: str, http_session: Session):
        self.login = login
        if login in LOGIN_NAME:
            self.name = LOGIN_NAME[login]
        else:
            self.name = self.login
        self.session = http_session
        logger.info(f"User {self.name} started")
        logger.info(f"Params: {self.params}")
        logger.info(f"Receiver email: {self.receiver_email}")
        logger.info(f"RequestYT: {format(self.request_youtrack, 'output')}")

    def __repr__(self):
        return f"<User({self.login}, {self.name})>"

    @cached_property
    def params_assignees(self) -> str:
        """
        Specifies the query param assignees_{} of the API request to the YouTrack.

        :return: the param string.
        :rtype: str
        """
        assignees: str = " OR ".join([f"{assignee}: {self.login}" for assignee in ASSIGNEES_PROJECT])
        return "".join(("(", assignees, ")"))

    @property
    def params(self) -> tuple[tuple[str, str], ...]:
        """
        Specifies the params value of the API request to the YouTrack.

        :return: the params attribute of the request.
        :rtype: tuple[tuple[str, str], ...]
        """
        params_field: str = ",".join(
            ("idReadable", "summary",
             "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"))
        params_query: str = " AND ".join((STATES, self.params_assignees))
        params_state: str = "State"
        params_deadline: str = "Дедлайн"
        params_priority: str = "Priority"

        return (
            ("fields", params_field),
            ("query", params_query),
            ("customFields", params_state),
            ("customFields", params_deadline),
            ("customFields", params_priority),
        )

    @cached_property
    def request_youtrack(self) -> RequestYT:
        """
        Specifies the API request to the YouTrack.

        :return: the request.
        :rtype: RequestYT
        """
        return RequestYT(self.session, self.params)

    def request_results(self) -> list[RequestResult]:
        """
        Specifies the parsed YouTrack response instances.

        :return: the parsed response.
        :rtype: list[RequestResult]
        """
        return self.request_youtrack.parse_response()

    @property
    def receiver_email(self) -> str:
        """
        Specifies the recipient email address.

        :return: the recipient email address.
        :rtype: str
        """
        return f"{self.login}@protei.ru"

    @property
    def message(self):
        if not self.payload():
            logger.info("No messages to send")
            return
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)
        # specify the message parameters
        message["From"] = "Оповещение о задачах <tarasov-a@protei.ru>"
        message["To"] = f"{self.name} <{self.login}@protei.ru>"
        message["Subject"] = "Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST"
        message["Content-Type"] = "text/plain; charset=UTF-8; format=flowed"
        message["Content-Transfer-Encoding"] = "8bit"
        message["Content-Language"] = "ru"

        return message.as_string().encode("utf-8")

    def message_string(self):
        # TODO
        return self.message.decode("utf-8")

    @property
    def server(self) -> Optional[SMTP]:
        context: SSLContext = create_default_context()
        server: SMTP = SMTP(SMTP_SERVER, PORT)
        try:
            # set the connection
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            # authenticate
            server.login(USER, PASSWORD)
        except SMTPConnectError:
            logger.exception("SMTPConnectError", exc_info=True)
            raise
        except SMTPServerDisconnected:
            logger.exception("SMTPServerDisconnected", exc_info=True)
            raise
        except SMTPNotSupportedError:
            logger.exception("SMTPNotSupportedError", exc_info=True)
            raise
        except SMTPAuthenticationError:
            logger.exception("SMTPAuthenticationError", exc_info=True)
            return
        except SMTPResponseException:
            logger.exception("SMTPResponseException", exc_info=True)
            raise
        except SMTPException:
            logger.exception("SMTPException", exc_info=True)
            raise
        except RuntimeError:
            logger.exception("RuntimeError", exc_info=True)
            raise
        else:
            logger.info("SMTP connection is ok")
            return server

    def send_email_message(self):
        """Sends the email."""
        if self.message is None:
            logger.info("No message is sent")
            return
        if self.server is None:
            logger.exception("No SMTP server is found", exc_info=True)
            raise

        try:
            self.server.sendmail(SENDER_EMAIL, self.receiver_email, self.message)
        except SMTPHeloError:
            logger.exception("SMTPHeloError", exc_info=True)
            raise
        except SMTPSenderRefused:
            logger.exception("SMTPSenderRefused", exc_info=True)
            raise
        except SMTPDataError:
            logger.exception("SMTPDataError", exc_info=True)
            raise
        except SMTPNotSupportedError:
            logger.exception("SMTPNotSupportedError", exc_info=True)
            raise
        except SMTPRecipientsRefused:
            logger.exception("SMTPRecipientsRefused", exc_info=True)
            raise
        else:
            logger.info(f"Message: {self.message_string()}")
        finally:
            self.server.quit()

    def write_file(self):
        """Writes the response information to the JSON file."""
        json_part: list[dict[str, str]] = [req_result.convert_for_json() for req_result in self.request_results()]
        dict_json_file: dict[str, list[dict[str, str]]] = json_file()
        dict_json_file[self.login] = json_part
        with open("info.json", "w+", encoding="utf-8") as file:
            json.dump(dict_json_file, file, indent=2, ensure_ascii=False)

    @property
    def json_section(self) -> Optional[list[RequestResult]]:
        """
        Reads the user part of the JSON file if exists.

        :return: the list of the issues.
        :rtype: list[RequestResult] or None
        """
        if not json_file():
            logger.warning("No JSON file")
            return

        dict_json_file: dict[str, list[dict[str, str]]] = json_file()
        parsed: list[dict[str, str]] = dict_json_file[self.login]
        return [RequestResult(item["issue"], item["state"], item["summary"], item["deadline"], item["priority"])
                for item in parsed] if parsed else None

    @property
    def file_issues(self) -> Optional[set[str]]:
        """
        Reads the issue names from the JSON file if the user part is not empty.

        :return: the issue names.
        :rtype: set[str] or None
        """
        file_issues: set[str] = set([item.issue for item in self.json_section]) if self.json_section else None
        return file_issues

    def response_issues(self) -> Optional[set[str]]:
        """
        Gets the issue names from the response.

        :return: the issue names.
        :rtype: set[str] or None
        """
        response_issues: Optional[set[str]]
        if self.request_results():
            response_issues = set([item.issue for item in self.request_results()])
        else:
            response_issues = None
        return response_issues

    def new_issues(self) -> set[str]:
        """
        Specifies the new issue names.

        :return: the new issue names.
        :rtype: set[str]
        """
        new_issues: set[str]
        if self.file_issues:
            new_issues = self.response_issues().difference(self.file_issues)
        else:
            new_issues = self.response_issues()
        return new_issues

    def common_issues(self) -> Optional[set[str]]:
        """
        Specifies the non-changed issue names.

        :return: the non-changed issue names.
        :rtype: set[str]
        """
        common_issues: Optional[set[str]]

        if self.file_issues:
            common_issues = set.intersection(self.file_issues, self.response_issues())
        else:
            common_issues = None
        return common_issues

    def file_unit_by_issue(self, issue: str) -> Optional[RequestResult]:
        """
        Get the issue from the JSON file by its name if exists.

        :param str issue: the issue name
        :return: the issue.
        :rtype: RequestResult or None
        """
        if self.json_section:
            for unit in self.json_section:
                if unit.issue == issue:
                    return unit
                else:
                    continue
            logger.exception(f"KeyError, the file for the issue {issue} is not found", exc_info=True)
            raise KeyError(f"The issue {issue} is not found")
        else:
            logger.warning("JSON file not found")
            return

    def response_unit_by_issue(self, issue: str) -> Optional[RequestResult]:
        """
        Get the issue from the response by its name if exists.

        :param str issue: the issue name
        :return: the issue.
        :rtype: RequestResult or None
        """
        for unit in self.request_results():
            if unit.issue == issue:
                return unit
            else:
                continue
        logger.exception(f"KeyError, the response for the issue {issue} is not found", exc_info=True)
        raise KeyError

    def changed_state(self) -> Optional[list[RequestResult]]:
        """
        Specifies the issues with the changed states.

        :return: the issues.
        :rtype: list[RequestResult] or None
        """
        if not self.common_issues():
            logger.info("user.changed_state is broken")
            return

        items = []
        for issue in self.common_issues():
            file_unit: RequestResult = self.file_unit_by_issue(issue)
            response_unit: RequestResult = self.response_unit_by_issue(issue)
            if file_unit.state != response_unit.state:
                items.append(response_unit)
            else:
                continue

        return items

    def payload(self) -> Optional[str]:
        """
        Specifies the message content.

        :returns: the message text.
        :rtype: str or None
        """
        # if nothing is changed
        if not self.new_issues() and not self.changed_state():
            logger.info("No data changed")
            return

        new_items: Optional[list[RequestResult]] = [self.response_unit_by_issue(issue) for issue in self.new_issues()]
        if new_items:
            text_new_introduction_ru: str = "Новые запросы:"
            text_new_ru: str = "\n".join([format(item, "new") for item in new_items])
        else:
            text_new_introduction_ru: str = "Новых запросов нет."
            text_new_ru: str = ""

        if self.changed_state():
            text_changed_introduction_ru: str = "Задачи с изменившимся статусом:"
            text_changed_ru: str = "\n".join([format(item, "changed") for item in self.changed_state()])
        else:
            text_changed_introduction_ru: str = "Статусы задач не изменились."
            text_changed_ru: str = ""

        payload = "\n".join((text_new_introduction_ru, text_new_ru, text_changed_introduction_ru, text_changed_ru))
        return replace_unicode_chars(payload)

    @property
    def message_text(self) -> str:
        """
        Specifies the text of the email message.

        :return: the email message text.
        :rtype: str
        """
        message_text: str = "\n".join((TEXT_PROLOGUE_RU, self.payload(), TEXT_EPILOGUE_RU))
        logger.info(f"Message text: {message_text}")
        return message_text


def main():
    # start the global timer
    datetime_start: datetime = datetime.now()
    logger.info("Global timer started")

    # TODO
    session = requests.Session()
    session.hooks["response"].append(log_round_trip)

    for login in LOGIN_NAME:
        # start the user timer
        user_timer_start: datetime = datetime.now()
        user: User = User(login, session)
        user.send_email_message()
        user.write_file()
        logger.info(f"User {user.name} finished")
        # stop the user timer
        user_timer_end: datetime = datetime.now()
        logger.info("Timer stopped")
        user_time: timedelta = user_timer_end - user_timer_start
        logger.info(f"Processing time: {user_time}")
    session.close()
    # stop the global timer
    datetime_end: datetime = datetime.now()
    work_time: timedelta = datetime_end - datetime_start
    logger.info("Global timer stopped")
    logger.info(f"Global processing time: {str(work_time)}")


if __name__ == "__main__":
    custom_fmt: str = "{name}:{levelname}:{asctime} | {filename}:{lineno} | {message}"
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    custom_formatter: Formatter = Formatter(custom_fmt, datefmt, "{")

    requests_fmt: str = "{name}:{levelname}:{asctime} | {filename}:{lineno} | {message}"
    requests_formatter: HTTPFormatter = HTTPFormatter(requests_fmt, datefmt, "{", True)

    requests_handler: RotatingFileHandler = RotatingFileHandler(
        f"{Path(sys.argv[0]).stem}_http_debug.log", "a", 2 ** 24, 10, "utf-8")

    requests_handler.setLevel(logging.DEBUG)
    requests_handler.setFormatter(requests_formatter)

    requests_logger.setLevel(logging.DEBUG)
    requests_logger.addHandler(requests_handler)

    # basic_handler: StreamHandler = StreamHandler(sys.stdout)
    # basic_handler.setLevel(logging.INFO)
    # basic_handler.setFormatter(custom_formatter)
    #
    # info_handler: FileHandler = FileHandler(
    #     f"{Path(sys.argv[0]).stem}_debug.log", "a", "utf-8")
    # info_handler.setLevel(logging.DEBUG)
    # info_handler.setFormatter(custom_formatter)
    #
    # # logger.addHandler(info_handler)
    # logger.addHandler(basic_handler)
    # logger.setLevel(logging.DEBUG)
    main()
