from datetime import date
from typing import Literal, Optional, Any, Sequence
import logging
from logging import Logger


logger: Logger = logging.getLogger(__name__)


class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for k, v in kwargs:
            setattr(self, k, v)
        for arg in args:
            setattr(self, str(arg), arg)
        self.__class__._instance = self
        logger.info(f"The singleton {self.__class__.__name__} is created")
        return

    def __call__(self, *args, **kwargs):
        if self.__class__._instance is None:
            self.__class__.__new__(self.__class__, *args, **kwargs)
            self.__init__(*args, **kwargs)
        return self.__class__._instance

    def __bool__(self):
        return True

    @classmethod
    def _subclass_names(cls):
        return [subclass.__name__ for subclass in cls.__subclasses__()]


class GlobalDict(Singleton):
    _instance = None
    dict_products: dict[str, Any] = dict()
    dict_certificates: dict[str, Any] = dict()
    dict_customers: dict[str, Any] = dict()
    dict_deliveries: dict[str, Any] = dict()
    dict_acts: dict[str, Any] = dict()
    dict_implementations: dict[str, Any] = dict()
    dict_documents: dict[str, Any] = dict()
    dict_aliases: dict[str, list[str]] = dict()
    dict_images: dict[str, bytes] = dict()

    def __repr__(self):
        return f"<GlobalDict(dict_products={self.__class__.dict_products}, " \
               f"dict_customers={self.__class__.dict_customers}, dict_deliveries={self.__class__.dict_deliveries}, " \
               f"dict_acts={self.__class__.dict_acts}, dict_implementations={self.__class__.dict_implementations}, " \
               f"dict_documents={self.__class__.dict_documents})>"

    def __str__(self):
        return f"GlobalDict: dict_products = {self.__class__.dict_products}; " \
               f"dict_customers = {self.__class__.dict_customers}; " \
               f"dict_deliveries = {self.__class__.dict_deliveries}; " \
               f"dict_acts = {self.__class__.dict_acts}; " \
               f"dict_implementations = {self.__class__.dict_implementations}; " \
               f"dict_documents = {self.__class__.dict_documents};"

    def __getitem__(self, item: str, item_class: str):
        if item_class not in self._subclass_names():
            logger.error(f"TypeError, the item class {item_class} is improper. "
                         f"The valid ones are:\n{self._subclass_names()}")
            return
        if item_class == "Product":
            if item not in self.dict_products:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_products.__getitem__(item)
        elif item_class == "Customer":
            if item not in self.dict_customers:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_customers.__getitem__(item)
        elif item_class == "Delivery":
            if item not in self.dict_deliveries:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_deliveries.__getitem__(item)
        elif item_class == "Act":
            if item not in self.dict_acts:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_acts.__getitem__(item)
        elif item_class == "Implementation":
            if item not in self.dict_implementations:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_implementations.__getitem__(item)
        elif item_class == "Document":
            if item not in self.dict_products:
                logger.error(f"KeyError, the item {item} is not found in {item_class}s")
                return
            else:
                return self.dict_products.__getitem__(item)

    def __len__(self):
        return len(self.__class__.dict_products) + len(self.__class__.dict_customers) + \
               len(self.__class__.dict_deliveries) + len(self.__class__.dict_acts) + \
               len(self.__class__.dict_implementations) + len(self.__class__.dict_documents)

    @classmethod
    def get_product(cls, product_name: str):
        if product_name in cls.dict_products.keys():
            return cls.dict_products[product_name]
        else:
            logger.error(f"KeyError, the product name {product_name} is not found")
            return

    @classmethod
    def get_customer(cls, customer_name: str):
        if customer_name in cls.dict_customers.keys():
            return cls.dict_customers[customer_name]
        else:
            logger.error(f"KeyError, the customer {customer_name} is not found")
            return

    @classmethod
    def get_delivery(cls, delivery_number: str):
        if delivery_number in cls.dict_deliveries.keys():
            return cls.dict_deliveries[delivery_number]
        else:
            logger.error(f"KeyError, the delivery number {delivery_number} is not found")
            return

    @classmethod
    def get_act(cls, act_number: str):
        if act_number in cls.dict_acts.keys():
            return cls.dict_acts[act_number]
        else:
            logger.error(f"KeyError, the act number {act_number} is not found")
            return

    @classmethod
    def get_implementation(cls, implementation_object: str):
        if implementation_object in cls.dict_implementations.keys():
            return cls.dict_implementations[implementation_object]
        else:
            logger.error(f"KeyError, the object {implementation_object} is not found")
            return

    @classmethod
    def get_document(cls, document_name: str):
        if document_name in cls.dict_documents.keys():
            return cls.dict_documents[document_name]
        else:
            logger.error(f"KeyError, the document {document_name} is not found")
            return

    @classmethod
    def get_certificate(cls, certificate_number: str):
        if certificate_number in cls.dict_certificates.keys():
            return cls.dict_certificates[certificate_number]
        else:
            logger.error(f"KeyError, the certificate {certificate_number} is not found")
            return

    @classmethod
    def get_image(cls, image_name: str):
        if image_name in cls.dict_images.keys():
            return cls.dict_certificates[image_name]
        else:
            logger.error(f"KeyError, the image {image_name} is not found")
            return


class Customer:
    _parent = GlobalDict()

    def __init__(
            self,
            name: str, *,
            deliveries: Optional[Sequence[str]] = None,
            acts: Optional[Sequence[str]] = None):
        if deliveries is None:
            deliveries = {}
        if acts is None:
            acts = {}

        self.name: str = name
        self.deliveries: Optional[set[str]] = set(deliveries)
        self.acts: Optional[set[str]] = set(acts)

        self.__class__._parent.dict_customers[self.name] = self

    def __repr__(self):
        return f"<Customer(name={self.name}, deliveries={self.deliveries}, acts={self.acts})>"

    def __str__(self):
        return f"Customer: name = {self.name}, deliveries = {self.deliveries}; acts = {self.acts};"

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.name == other.name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.name != other.name
        else:
            return NotImplemented

    def __len__(self):
        return len(self.deliveries) + len(self.acts)

    def all_deliveries(self):
        return [self._parent.get_delivery(delivery_name) for delivery_name in self.deliveries]

    @classmethod
    def get_delivery(cls, delivery_name: str):
        return cls._parent.get_delivery(delivery_name)

    def add_delivery(self, delivery_number: str):
        self.deliveries.add(delivery_number)
        return self.deliveries

    def delete_delivery(self, delivery_number: str):
        try:
            self.deliveries.remove(delivery_number)
        except KeyError:
            logger.warning(f"KeyError, the delivery number {delivery_number} is not found, so it cannot be deleted")
        finally:
            return self.deliveries

    def all_acts(self):
        return [self._parent.get_act(act_name) for act_name in self.acts]

    @classmethod
    def get_act(cls, act_number: str):
        return cls._parent.get_act(act_number)

    def add_act(self, act_number: str):
        self.acts.add(act_number)
        return self.acts

    def delete_act(self, act_number: str):
        try:
            self.acts.remove(act_number)
        except KeyError:
            logger.warning(f"KeyError, the act number {act_number} is not found, so it cannot be deleted")
        finally:
            return self.acts


class Product:
    _parent = GlobalDict()

    def __init__(
            self,
            name: str,
            manufacturer: Literal["PROTEI RD", "PROTEI ST"], *,
            aliases: Optional[Sequence[str]] = None,
            images: Optional[Sequence[bytes]] = None,
            certificates: Sequence[str] = None):
        if aliases is None:
            aliases = {}
        if images is None:
            images = {}
        if certificates is None:
            certificates = {}

        self.name: str = name
        self.manufacturer: Literal["PROTEI RD", "PROTEI ST"] = manufacturer
        self.aliases: Optional[set[str]] = set(aliases)
        self.images: Optional[set[bytes]] = set(images)
        self.certificates = set(certificates)

        self.dict_modifications: dict[str, Any] = dict()
        self._parent.dict_products[self.name] = self
        self._parent.dict_aliases[self.name] = []
        self._parent.dict_aliases[self.name].extend(aliases)

    def __call__(self, *args, **kwargs):
        return self.__class__._parent.dict_products.items()

    def __repr__(self):
        return f"<Product(name={self.name}, manufacturer={self.manufacturer}, aliases={self.aliases}, " \
               f"images={self.images}, certificates={self.certificates})>"

    def __str__(self):
        return f"Product: name = {self.name}, manufacturer = {self.manufacturer}, aliases = {self.aliases}; " \
               f"images = {self.images}; certificates = {self.certificates};"

    def __hash__(self):
        return hash((self.name, self.manufacturer))

    def __key(self):
        return self.name, self.manufacturer

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __iter__(self):
        return iter(self.dict_modifications.keys())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.dict_modifications.keys()
        elif isinstance(item, Modification):
            return item in self.dict_modifications.values()

    @property
    def modification_names(self) -> list[str]:
        return [key for key in self.dict_modifications]

    def get_modification(self, modification_name: str):
        if modification_name in self.modification_names:
            return self.dict_modifications[modification_name]
        else:
            logger.error(f"KeyError, the modification name {modification_name} is not found")
            return

    def add_modification(self, modification: Any):
        if isinstance(modification, Modification):
            self.dict_modifications[modification.name] = modification
        else:
            logger.error(f"TypeError, the modification must be the Modification type, "
                         f"but {type(modification)} received")
        return self.dict_modifications

    def add_alias(self, alias: str):
        self.aliases.add(alias)
        return self.aliases

    def delete_alias(self, alias: str):
        try:
            self.aliases.remove(alias)
        except KeyError:
            logger.warning(f"KeyError, the alias {alias} is not found, so it cannot be deleted")
        finally:
            return self.aliases

    def get_certificate(self, certificate_number: str):
        if certificate_number in self.certificates:
            return self._parent.get_certificate(certificate_number)
        else:
            logger.error(f"KeyError, the certificate {certificate_number} is not found")
            return

    def add_certificate(self, certificate_number: str):
        self.certificates.add(certificate_number)
        return self.certificates

    def delete_certificate(self, certificate_number: str):
        try:
            self.certificates.remove(certificate_number)
        except KeyError:
            logger.warning(f"KeyError, the certificate {certificate_number} is not found, so it cannot be deleted")
        finally:
            return self.certificates

    def get_image(self, image_name: str):
        return self._parent.get_image(image_name)


class Certificate:
    _parent = GlobalDict()

    def __init__(
            self,
            product_name: str,
            certificate_number: Optional[str] = None,
            date_issued: Optional[date] = None,
            date_expired: Optional[date] = None):
        self.certificate_number: certificate_number
        self.product: Product = self._parent.get_product(product_name)
        self.date_issued: Optional[date] = date_issued
        self.date_expired: Optional[date] = date_expired

        self._parent.dict_certificates[certificate_number] = self
        self.product.certificates.add(certificate_number)

    def __repr__(self):
        return f"<Certificate(product_name={self.product.name}, date_issued={self.date_issued}, " \
               f"date_expired={self.date_expired})>"

    def __str__(self):
        return f"Certificate: product_name = {self.product.name}, date_issued = {self.date_issued}, " \
               f"date_expired = {self.date_expired}"

    def __hash__(self):
        return hash((self.product.name, self.date_issued))

    def __key(self):
        return self.product.name, self.date_issued

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def is_active(self):
        if self.date_expired is not None:
            return self.date_expired > date.today()
        else:
            return True

    def is_valid(self):
        if self.date_issued is not None:
            if self.date_expired is not None:
                return self.date_issued < self.date_expired
            else:
                return True
        else:
            if self.date_expired is not None:
                return True
            else:
                return False


class ProxyObject(GlobalDict):
    _parent = GlobalDict()
    _instance = None
    dict_modification_product: dict[str, str] = {}

    def __repr__(self):
        return f"<ProxyObject(_parent=GlobalDict(), dict_modification={self.__class__.dict_modification_product})>"

    def __str__(self):
        return f"ProxyObject: dict_modification = {self.__class__.dict_modification_product}"

    @classmethod
    def get_customer(cls, customer_name: str) -> Customer:
        return cls._parent.get_customer(customer_name)

    @classmethod
    def get_delivery(cls, delivery_number: str):
        return cls._parent.get_delivery(delivery_number)

    @classmethod
    def get_act(cls, act_number: str):
        return cls._parent.get_act(act_number)

    @classmethod
    def get_document(cls, document_name: str):
        return cls._parent.get_document(document_name)

    @classmethod
    def get_product(cls, product_name: str) -> Product:
        return cls._parent.get_product(product_name)

    @classmethod
    def get_product_name(cls, modification_name: str) -> str:
        return cls.dict_modification_product[modification_name]


class Modification:
    _parent = GlobalDict()
    _proxy = ProxyObject()

    def __init__(
            self,
            product_name: str,
            name: str, *,
            deliveries: set[str] = None,
            acts: set[str] = None,
            implementations: set[str] = None,
            equipment: list[str] = None,
            documents: set[str] = None):
        if deliveries is None:
            deliveries = {}
        if acts is None:
            acts = {}
        if implementations is None:
            implementations = {}
        if equipment is None:
            equipment = []
        if documents is None:
            documents = {}

        self.product_name = product_name
        self.name = name
        self.deliveries = deliveries
        self.acts = acts
        self.implementations = implementations
        self.equipment = equipment
        self.documents = documents
        self.product = self.__class__._parent.get_product(self.product_name)

        if self.name not in self.product.dict_modifications:
            self.product.dict_modifications[self.name] = self

        self._proxy.dict_modification_product[self.name] = self.product_name

    def __repr__(self):
        return f"<Modification(product_name={self.product_name}, name={self.name}, deliveries={self.deliveries}, " \
               f"acts={self.acts}, implementations={self.implementations}, equipment={self.equipment}, " \
               f"documents={self.documents})>"

    def __str__(self):
        return f"Modification: product name = {self.product_name}; name = {self.name}; " \
               f"deliveries = {self.deliveries}; acts = {self.acts}; implementations = {self.implementations}; " \
               f"equipment = {self.equipment}; documents = {self.documents}"

    def __hash__(self):
        return hash((self.product_name, self.name))

    def __key(self):
        return self.product_name, self.name

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __len__(self):
        return len(self.deliveries) + len(self.acts) + len(self.implementations) + len(self.documents)

    def __bool__(self):
        return len(self) > 0

    @property
    def manufacturer(self) -> str:
        return self.product.manufacturer

    def get_all_deliveries(self):
        return [self.__class__._parent.get_delivery(delivery_name) for delivery_name in self.deliveries]

    def add_delivery(self, delivery_number: str):
        self.deliveries.add(delivery_number)
        return self.deliveries

    def get_delivery(self, delivery_number: str):
        if delivery_number not in self.deliveries:
            logger.error(f"KeyError, the delivery number {delivery_number} is not found")
            return
        else:
            return self.__class__._parent.get_delivery(delivery_number)

    def delete_delivery(self, delivery_number: str):
        try:
            self.deliveries.remove(delivery_number)
        except KeyError:
            logger.warning(f"KeyError, the delivery number {delivery_number} is not found, so it cannot be deleted")
        finally:
            return self.deliveries

    def get_all_acts(self) -> list:
        return [self.__class__._parent.get_act(act_number) for act_number in self.acts]

    def add_act(self, act_number: str):
        self.acts.add(act_number)
        return self.acts

    def get_act(self, act_number: str) -> Optional[Any]:
        if act_number not in self.acts:
            logger.error(f"KeyError, the act number {act_number} is not found")
            return
        else:
            return self.__class__._parent.get_act(act_number)

    def delete_act(self, act_number: str):
        try:
            self.acts.remove(act_number)
        except KeyError:
            logger.warning(f"KeyError, the act number {act_number} is not found, so it cannot be deleted")
        finally:
            return self.acts

    def get_all_implementations(self):
        return [
            self.__class__._parent.get_implementation(implementation_object)
            for implementation_object in self.implementations]

    def add_implementation(self, implementation_object: str):
        self.implementations.add(implementation_object)
        return self.implementations

    def get_implementation(self, implementation_object: str):
        if implementation_object not in self.implementations:
            logger.error(f"KeyError, the implementation object {implementation_object} is not found")
            return
        else:
            return self.__class__._parent.get_implementation(implementation_object)

    def delete_implementation(self, implementation_object: str):
        try:
            self.implementations.remove(implementation_object)
        except KeyError:
            logger.warning(f"KeyError, the implementation object {implementation_object} is not found, "
                           f"so it cannot be deleted")
        finally:
            return self.implementations

    def get_all_documents(self):
        return [self.__class__._parent.get_document(document_name) for document_name in self.documents]

    def add_document(self, document_name: str):
        self.documents.add(document_name)
        return self.documents

    def get_document(self, document_name: str):
        if document_name not in self.documents:
            logger.error(f"KeyError, the document name {document_name} is not found")
            return
        else:
            return self.__class__._parent.get_document(document_name)

    def delete_document(self, document_name: str):
        try:
            self.documents.remove(document_name)
        except KeyError:
            logger.warning(f"KeyError, the document name {document_name} is not found, so it cannot be deleted")
        finally:
            return self.documents

    def add_equipment(self, equipment_object: str):
        self.equipment.append(equipment_object)
        return self.equipment

    def delete_equipment(self, equipment_object: str):
        try:
            self.equipment.remove(equipment_object)
        except KeyError:
            logger.warning(f"KeyError, the equipment {equipment_object} is not found, so it cannot be deleted")
        finally:
            return self.documents


class Delivery:
    _parent = ProxyObject()
    __slots__ = ("delivery_number", "delivery_customer", "modification_name", "delivery_date", "proxy_object_name")

    def __init__(
            self,
            modification_name: str,
            delivery_number: str = None,
            delivery_customer: str = None,
            delivery_date: date = None):
        super().__init__()
        self.modification_name = modification_name
        self.delivery_number = delivery_number
        self.delivery_customer = delivery_customer
        self.delivery_date = delivery_date

        self.__class__._parent.dict_deliveries[self.delivery_number] = self

    def __repr__(self):
        return f"<Delivery(modification_name={self.modification_name}, delivery_number={self.delivery_number}, " \
               f"delivery_customer={self.delivery_customer}, delivery_date={self.delivery_date})>"

    def __str__(self):
        return f"Delivery: modification_name = {self.modification_name}, delivery_number = {self.delivery_number}, " \
               f"delivery_customer = {self.delivery_customer}, delivery_date = {self.delivery_date}"

    def __bool__(self):
        return self.delivery_number is not None

    def __hash__(self):
        return hash((self.modification_name, self.delivery_number))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.delivery_number == other.delivery_number
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.delivery_number != other.delivery_number
        else:
            return NotImplemented

    @property
    def modification(self) -> Modification:
        return self.product.get_modification(self.modification_name)

    @property
    def product_name(self) -> str:
        return self.__class__._parent.dict_modification_product[self.modification_name]

    @property
    def product(self) -> Product:
        return self.__class__._parent.dict_products[self.product_name]

    @property
    def customer(self):
        if not self.delivery_customer:
            return None
        else:
            return self.__class__._parent.get_customer(self.delivery_customer)


class Act:
    _parent = ProxyObject()

    __slots__ = ("act_number", "act_customer", "modification_name")

    def __init__(
            self,
            modification_name: str,
            act_number: str = None,
            act_customer: str = None):
        super().__init__()
        self.modification_name = modification_name
        self.act_number = act_number
        self.act_customer = act_customer

        self.__class__._parent.dict_acts[self.act_number] = self

    def __repr__(self):
        return f"<Act(modification_name={self.modification_name}, act_number={self.act_number}, " \
               f"act_customer={self.act_customer})>"

    def __str__(self):
        return f"Act: modification_name = {self.modification_name}, act_number = {self.act_number}, " \
               f"act_customer = {self.act_customer}"

    def __bool__(self):
        return self.act_number is not None

    def __hash__(self):
        return hash((self.modification_name, self.act_number))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.act_number == other.act_number
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.act_number != other.act_number
        else:
            return NotImplemented

    @property
    def modification(self) -> Modification:
        return self.product.get_modification(self.modification_name)

    @property
    def product_name(self) -> str:
        return self.__class__._parent.dict_modification_product[self.modification_name]

    @property
    def product(self) -> Product:
        return self.__class__._parent.dict_products[self.product_name]

    @property
    def customer(self) -> Optional[Customer]:
        if not self.act_customer:
            return None
        else:
            return self.__class__._parent.get_customer(self.act_customer)


class Implementation:
    _parent = ProxyObject()

    __slots__ = ("object_implemented", "employer", "modification_name")

    def __init__(
            self,
            modification_name: str,
            object_implemented: str = None,
            employer: str = None):
        super().__init__()
        self.modification_name = modification_name
        self.object_implemented = object_implemented
        self.employer = employer

        self.__class__._parent.dict_implementations[object_implemented] = self

    def __repr__(self):
        return f"<Implementation(modification_name={self.modification_name}, " \
               f"object_implemented={self.object_implemented}, " \
               f"employer={self.employer})>"

    def __str__(self):
        return f"Implementation: modification_name = {self.modification_name}, " \
               f"object_implemented = {self.object_implemented}, " \
               f"employer = {self.employer}"

    def __bool__(self):
        return self.object_implemented is not None

    def __hash__(self):
        return hash((self.modification_name, self.object_implemented))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.object_implemented == other.object_implemented
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.object_implemented != other.object_implemented
        else:
            return NotImplemented

    @property
    def modification(self) -> Modification:
        return self.product.get_modification(self.modification_name)

    @property
    def product_name(self) -> str:
        return self.__class__._parent.dict_modification_product[self.modification_name]

    @property
    def product(self) -> Product:
        return self.__class__._parent.dict_products[self.product_name]

    @property
    def customer(self) -> Optional[Customer]:
        if not self.employer:
            return None
        else:
            return self.__class__._parent.get_customer(self.employer)


class Document:
    _parent = ProxyObject()

    __slots__ = ("document_name", "document_link", "modification_name")

    def __init__(
            self,
            modification_name: str,
            document_name: str = None,
            document_link: str = None):
        super().__init__()
        self.modification_name = modification_name
        self.document_name = document_name
        self.document_link = document_link

        self.__class__._parent.dict_documents[self.document_name] = self

    def __repr__(self):
        return f"<Document(modification_name={self.modification_name}, document_name={self.document_name}, " \
               f"document_link={self.document_link})>"

    def __str__(self):
        return f"Document: modification_name = {self.modification_name}, document_name = {self.document_name}, " \
               f"document_link = {self.document_link}"

    def __bool__(self):
        return self.document_name is not None

    def __hash__(self):
        return hash((self.modification_name, self.document_name))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.document_name == other.document_name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.document_name != other.document_name
        else:
            return NotImplemented

    @property
    def modification(self) -> Modification:
        return self.product.get_modification(self.modification_name)

    @property
    def product_name(self) -> str:
        return self.__class__._parent.dict_modification_product[self.modification_name]

    @property
    def product(self) -> Product:
        return self.__class__._parent.dict_products[self.product_name]


def main():
    global_dict = GlobalDict()
    customer = Customer("customer")
    print(Implementation.__mro__)
    print(customer.deliveries)

    product = Product("product", "PROTEI RD")
    print(product.dict_modifications)
    proxy_object = ProxyObject()
    print(repr(proxy_object))
    modification = Modification(product.name, "modification")
    print(global_dict.dict_products)
    print(product.dict_modifications)
    print(type(proxy_object))
    delivery = Delivery(modification.name, "000.000")
    print(repr(delivery.delivery_number))
    print(repr(delivery))
    document = Document(modification.name, "document")
    print(repr(document))
    print(str(document.document_name))
    print(global_dict.dict_documents)
    print(Document.__class__.__name__)
    print(document.document_link)
    print(Document.__mro__)


if __name__ == "__main__":
    main()
