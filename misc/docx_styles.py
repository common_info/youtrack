from typing import Optional, Union
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_LINE_SPACING
from docx import Document as Doc
from docx.document import Document, Section
from docx.styles.style import _TableStyle, _ParagraphStyle, BaseStyle, _CharacterStyle, _NumberingStyle
from docx.styles.styles import Styles
from docx.shared import Length, Pt, RGBColor, Cm
from docx.dml.color import MSO_COLOR_TYPE
from docx.enum.dml import MSO_THEME_COLOR_INDEX
import logging
from logging import Logger

logger: Logger = logging.getLogger(__name__)


class CustomDocument:
    _instance = None
    document: Document = Doc()

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self, *args, **kwargs):
        if self.__class__._instance is None:
            super().__init__(*args, **kwargs)
            self.__class__._instance = self
        else:
            return

    def __call__(self, *args, **kwargs):
        if self.__class__._instance is None:
            self.__class__.__new__(self.__class__, *args, **kwargs)
            self.__init__(self, *args, **kwargs)
        return self.__class__._instance

    def __contains__(self, item):
        return item in self.__class__.document

    def __getattribute__(self, item):
        if item == "document":
            return object.__getattribute__(self, item)
        else:
            return object.__getattribute__(self.__class__.document, item)

    def __setattr__(self, key, value):
        return object.__setattr__(self.__class__.document, key, value)


class CustomStyleFactory:
    _parent: Document = CustomDocument().document
    styles: Styles = _parent.styles

    def __init__(
            self,
            name: str,
            style_type: WD_STYLE_TYPE,
            builtin: Optional[bool]):
        self.style: Union[_ParagraphStyle, _TableStyle, _CharacterStyle, _NumberingStyle]
        self.style = self.__class__.styles.add_style(name, style_type, builtin)

    def inherit(self, base_style: Optional[str]):
        if base_style is None:
            self.style.base_style = None
            logger.error("The base style is not specified")
            return
        base: Union[_ParagraphStyle, _TableStyle, _CharacterStyle, _NumberingStyle]
        base = self.__class__._parent.styles[base_style]

        self.style.paragraph_style.base_style = base
        self.style.paragraph_style.quick_style = None
        self.style.paragraph_style.priority = None
        self.style.paragraph_format.alignment = None
        self.style.paragraph_format.keep_with_next = None
        self.style.paragraph_format.keep_together = None
        self.style.paragraph_format.space_before = None
        self.style.paragraph_format.space_after = None
        self.style.paragraph_format.left_indent = None
        self.style.paragraph_format.right_indent = None
        self.style.paragraph_format.first_line_indent = None
        self.style.paragraph_format.line_spacing = None
        self.style.paragraph_format.line_spacing_rule = None
        self.style.paragraph_format.page_break_before = None
        self.style.paragraph_format.widow_control = None
        self.style.font.all_caps = None
        self.style.font.bold = None
        self.style.font.complex_script = None
        self.style.font.cs_bold = None
        self.style.font.cs_italic = None
        self.style.font.double_strike = None
        self.style.font.emboss = None
        self.style.font.hidden = None
        self.style.font.highlight_color = None
        self.style.font.italic = None
        self.style.font.imprint = None
        self.style.font.math = None
        self.style.font.name = None
        self.style.font.no_proof = None
        self.style.font.outline = None
        self.style.font.rtl = None
        self.style.font.shadow = None
        self.style.font.size = None
        self.style.font.small_caps = None
        self.style.font.snap_to_grid = None
        self.style.font.spec_vanish = None
        self.style.font.strike = None
        self.style.font.subscript = None
        self.style.font.superscript = None
        self.style.font.underline = None
        self.style.font.web_hidden = None
        self.style.font.color.type = None
        self.style.font.color.theme_color = None
        self.style.font.color.rgb = None

        return self

    def modify(self, **kwargs):
        for k, v in kwargs:
            try:
                setattr(self.style, k, v)
            except KeyError:
                logger.warning(f"The key {k} and value {v} cannot be assigned to the style")
                continue
            except ValueError:
                logger.warning(f"The key {k} cannot have a value {v}")
                continue
            except AttributeError:
                logger.warning(f"The style does not have a key {k}")
                continue
            except TypeError:
                logger.warning(f"The key {k} must be str, {type(k)} received; the value of the {type(v)} received")
                continue
        return self.style


def style_normal():
    custom_style = CustomStyleFactory("Normal", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Normal",
        "paragraph_style.base_style": None,
        "paragraph_style.quick_style": True,
        "paragraph_style.priority": None,
        "paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
        "paragraph_style.builtin": True,
        "paragraph_format.alignment": None,
        "paragraph_format.keep_with_next": None,
        "paragraph_format.keep_together": None,
        "paragraph_format.space_before": None,
        "paragraph_format.space_after": None,
        "paragraph_format.left_indent": None,
        "paragraph_format.right_indent": None,
        "paragraph_format.first_line_indent": None,
        "paragraph_format.line_spacing": WD_LINE_SPACING.SINGLE,
        "paragraph_format.line_spacing_rule": None,
        "paragraph_format.page_break_before": None,
        "paragraph_format.widow_control": True,
        "font.all_caps": None,
        "font.bold": None,
        "font.complex_script": None,
        "font.cs_bold": None,
        "font.cs_italic": None,
        "font.double_strike": None,
        "font.emboss": None,
        "font.hidden": None,
        "font.highlight_color": None,
        "font.italic": None,
        "font.imprint": None,
        "font.math": None,
        "font.name": "Times New Roman",
        "font.no_proof": None,
        "font.outline": None,
        "font.rtl": None,
        "font.shadow": None,
        "font.size": Pt(14.0),
        "font.small_caps": None,
        "font.snap_to_grid": None,
        "font.spec_vanish": None,
        "font.strike": None,
        "font.subscript": None,
        "font.superscript": None,
        "font.underline": None,
        "font.web_hidden": None,
        "font.color.type": MSO_COLOR_TYPE.THEME,
        "font.color.theme_color": MSO_THEME_COLOR_INDEX.TEXT_1,
        "font.color.rgb": RGBColor(0, 0, 0)
    }
    return custom_style.modify(**modify_attrs)


def style_main():
    custom_style = CustomStyleFactory("Main", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.priority": 1,
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
        "paragraph_format.first_line_indent": Cm(1),
        "paragraph_format.space_before": Pt(6.0),
        "paragraph_format.space_after": Pt(6.0)
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_basic():
    custom_style = CustomStyleFactory("_Heading Basic", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.priority": 1,
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
        "paragraph_format.first_line_indent": Cm(1),
        "paragraph_format.keep_with_next": True,
        "paragraph_format.keep_together": True,
        "paragraph_format.space_before": Pt(6.0),
        "paragraph_format.space_after": Pt(6.0),
        "font.bold": True
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_heading_1():
    custom_style = CustomStyleFactory("Heading 1", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.base_style": CustomStyleFactory.styles["_Heading Basic"],
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
        "paragraph_style.priority": 1,
        "paragraph_format.space_before": Pt(0.0),
        "paragraph_format.space_after": Pt(12.0),
        "paragraph_format.page_break_before": True,
        "font.all_caps": True,
        "font.size": Pt(16.0),
        "font.bold": True
    }
    return custom_style.inherit("Basic").modify(**modify_attrs)


def style_heading_2():
    custom_style = CustomStyleFactory("Heading 2", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Heading 2",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Heading 1"],
        "paragraph_format.space_before": Pt(12.0),
        "font.all_caps": False
    }
    return custom_style.inherit("Heading 1").modify(**modify_attrs)


def style_heading_3():
    custom_style = CustomStyleFactory("Heading 3", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Heading 3",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles['Main'],
        "paragraph_style.base_style": CustomStyleFactory.styles['Heading 2'],
        "paragraph_format.space_before": Pt(6.0),
        "paragraph_format.space_after": Pt(6.0)
    }
    return custom_style.inherit("Heading 2").modify(**modify_attrs)


def style_heading_4():
    custom_style = CustomStyleFactory("Heading 4", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Heading 4",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles['Main'],
        "paragraph_style.base_style": CustomStyleFactory.styles['Heading 3'],
        "font.bold": True
    }
    return custom_style.inherit("Heading 3").modify(**modify_attrs)


def style_heading_5():
    custom_style = CustomStyleFactory("Heading 5", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Heading 5",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles['Main'],
        "paragraph_style.base_style": CustomStyleFactory.styles['Heading 4'],
        "paragraph_format.space_before": Pt(10.0)
    }
    return custom_style.inherit("Heading 4").modify(**modify_attrs)


def style_table_text():
    custom_style = CustomStyleFactory("Table Text", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Table Text",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Text"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Main"],
        "paragraph_format.first_line_indent": Cm(0),
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.LEFT,
        "paragraph_format.space_before": Pt(4.0),
        "paragraph_format.space_after": Pt(4.0)
    }
    return custom_style.inherit("Main").modify(**modify_attrs)


def style_table_text_center():
    custom_style = CustomStyleFactory("Table Text Center", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Table Text Center",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Text Center"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Table Text"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER
    }
    return custom_style.inherit("Table Text").modify(**modify_attrs)


def style_table_heading():
    custom_style = CustomStyleFactory("Table Heading", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Table Heading",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Heading"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Table Text"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "paragraph_format.first_line_indent": Cm(0),
        "paragraph_format.space_before": Pt(4.0),
        "paragraph_format.space_after": Pt(4.0)
    }
    return custom_style.inherit("Table Text").modify(**modify_attrs)


def style_table_label():
    custom_style = CustomStyleFactory("Table Label", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Table Label",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Label"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Main"],
        "paragraph_format.keep_with_next": True,
        "paragraph_format.space_before": Pt(4.0),
        "paragraph_format.space_after": Pt(4.0)
    }
    return custom_style.inherit("Main").modify(**modify_attrs)


def style_image():
    custom_style = CustomStyleFactory("Image", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Image",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Main"],
        "paragraph_format.keep_with_next": True,
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "paragraph_format.space_before": Pt(5.5),
        "paragraph_format.space_after": Pt(8.5)
    }
    return custom_style.inherit("Main").modify(**modify_attrs)


def style_image_label():
    custom_style = CustomStyleFactory("Image Label", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Image Label",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Image"],
        "paragraph_format.keep_with_next": False,
    }
    return custom_style.inherit("Image").modify(**modify_attrs)


def style_table_footer_left():
    custom_style = CustomStyleFactory("Table Footer Left", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Table Footer Left",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Footer Left"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.LEFT,
        "font.size": Pt(11.0)
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_table_footer_center():
    custom_style = CustomStyleFactory("Table Footer Center", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Image Label",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Footer Center"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Table Footer Left"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER
    }
    return custom_style.inherit("Table Footer Left").modify(**modify_attrs)


def style_table_footer_text():
    custom_style = CustomStyleFactory("Table Footer Text", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Image Label",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table Footer Text"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "font.size": Pt(16.0)
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_bullet_list_1():
    custom_style = CustomStyleFactory("List Bullet 1", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "List Bullet 1",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["List Bullet 1"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Basic"],
        "paragraph_format.space_before": Pt(3.0),
        "paragraph_format.space_after": Pt(3.0),
    }
    return custom_style.inherit("Basic").modify(**modify_attrs)


def style_list_paragraph():
    custom_style = CustomStyleFactory("List Paragraph", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "List Paragraph",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["List Paragraph"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"]
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_caption():
    custom_style = CustomStyleFactory("Caption", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Caption",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_format.space_after": Pt(10.0),
        "font.size": Pt(9.0),
        "font.italic": True,
        "font.color.theme_color": MSO_THEME_COLOR_INDEX.TEXT_2,
        "font.color.rgb": RGBColor(68, 84, 106)
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_toc_heading():
    custom_style = CustomStyleFactory("TOC Heading", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "TOC Heading",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Heading 1"],
        "paragraph_format.space_before": Pt(12.0),
        "paragraph_format.space_after": Pt(0.0),
        "font.color.type": MSO_COLOR_TYPE.THEME,
        "font.color.theme_color": MSO_THEME_COLOR_INDEX.ACCENT_1,
        "font.color.rgb": RGBColor(47, 84, 150)
    }
    return custom_style.inherit("Heading 1").modify(**modify_attrs)


def style_intro():
    custom_style = CustomStyleFactory("Intro", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Intro",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_format.space_after": Pt(12.0),
        "paragraph_format.page_break_before": True,
        "font.bold": True
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_annex():
    custom_style = CustomStyleFactory("Annex", WD_STYLE_TYPE.PARAGRAPH, False)
    modify_attrs = {
        "paragraph_style.name": "Annex",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Annex"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
        "paragraph_format.page_break_before": True,
        "font.bold": True,
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def style_footer():
    custom_style = CustomStyleFactory("Footer", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Footer",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Footer"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.quick_style": False
    }
    return custom_style.inherit("Footer").modify(**modify_attrs)


def style_header():
    custom_style = CustomStyleFactory("Header", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "Header",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Header"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.quick_style": False
    }
    return custom_style.inherit("Header").modify(**modify_attrs)


def style_toc_1():
    custom_style = CustomStyleFactory("Normal", WD_STYLE_TYPE.PARAGRAPH, True)
    modify_attrs = {
        "paragraph_style.name": "toc 1",
        "paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
        "paragraph_style.quick_style": False,
        "paragraph_format.space_before": Pt(9.0),
        "paragraph_format.space_after": Pt(6.0)
    }
    return custom_style.inherit("Normal").modify(**modify_attrs)


def main():
    custom_document: CustomDocument = CustomDocument()
    print(id(custom_document))
    new_custom_document: CustomDocument = CustomDocument()
    print(id(new_custom_document))
    print(custom_document is new_custom_document)
    print(CustomDocument().document)
    print(new_custom_document.document)


if __name__ == "__main__":
    main()

"""

----------Paragraph Style Приложение----------
"paragraph_style.name": Приложение,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Приложение"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.priority": 99,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.page_break_before": True,
"font.bold": True,
----------Paragraph Style Приложение----------
----------Paragraph Style Основной----------
"paragraph_style.name": Основной,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Основной"],
"paragraph_style.base_style": None,
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(6.0),
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.first_line_indent": Pt(28.35),
"font.size": Pt(14.0),
----------Paragraph Style Основной----------
----------Paragraph Style Таблица_список_тире----------
"paragraph_style.name": Таблица_список_тире,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_список_тире"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
"font.size": Pt(13.0),
----------Paragraph Style Таблица_список_тире----------
----------Paragraph Style Список_номер----------
"paragraph_style.name": Список_номер,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_номер"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_after": Pt(6.0),
----------Paragraph Style Список_номер----------
----------Paragraph Style Список_буква----------
"paragraph_style.name": Список_буква,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_буква"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_after": Pt(6.0),
----------Paragraph Style Список_буква----------
----------Paragraph Style toc 2----------
"paragraph_style.name": toc 2,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": False,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": True,
"paragraph_format.space_after": Pt(4.0),
"paragraph_format.left_indent": Pt(14.0),
----------Paragraph Style toc 2----------
----------Paragraph Style toc 3----------
"paragraph_style.name": toc 3,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": False,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": True,
"paragraph_format.space_after": Pt(4.0),
"paragraph_format.left_indent": Pt(28.0),
----------Paragraph Style toc 3----------
----------Paragraph Style Таблица_11_центр----------
"paragraph_style.name": Таблица_11_центр,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_11_центр"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"font.size": Pt(11.0),
----------Paragraph Style Таблица_11_центр----------
----------Paragraph Style Таблица_11_лев----------
"paragraph_style.name": Таблица_11_лев,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_11_лев"],
"paragraph_style.base_style": CustomStyleFactory.styles["Таблица_11_центр"],
"paragraph_style.quick_style": True,
"paragraph_style.priority": 7,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.LEFT,
----------Paragraph Style Таблица_11_лев----------
----------Paragraph Style frame_decimal----------
"paragraph_style.name": frame_decimall,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["frame_decimal"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"font.name": Times New Roman,
"font.size": Pt(16.0),
----------Paragraph Style frame_decimal----------
----------Paragraph Style annotation text----------
"paragraph_style.name": annotation text,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["annotation text"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": False,
"paragraph_style.priority": 99,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": True,
"font.size": Pt(10.0),
----------Paragraph Style annotation text----------
----------Paragraph Style annotation subject----------
"paragraph_style.name": annotation subject,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["annotation text"],
"paragraph_style.base_style": CustomStyleFactory.styles["annotation text"],
"paragraph_style.quick_style": False,
"paragraph_style.priority": 99,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": True,
"font.bold": True,
----------Paragraph Style annotation subject----------
----------Paragraph Style Таблица_название----------
"paragraph_style.name": Таблица_название,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_название"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.priority": 7,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.keep_with_next": True,
"paragraph_format.space_after": Pt(6.0),
----------Paragraph Style Таблица_название----------
----------Paragraph Style Таблица_13_центр----------
"paragraph_style.name": Таблица_13_центр,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_13_центр"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
"font.name": Times New Roman,
"font.size": Pt(13.0),
----------Paragraph Style Таблица_13_центр----------
----------Paragraph Style Таблица_13_лев----------
"paragraph_style.name": Таблица_13_лев,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_13_лев"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
"font.name": Times New Roman,
"font.size": Pt(13.0),
----------Paragraph Style Таблица_13_лев----------
----------Paragraph Style Список_тире_1ур----------
"paragraph_style.name": Список_тире_1ур,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_тире_1ур"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
----------Paragraph Style Список_тире_1ур----------
----------Paragraph Style Колонтитул_рамка----------
"paragraph_style.name": "Колонтитул_рамка",
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Колонтитул_рамка"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.line_spacing": Pt(0.0),
"paragraph_format.line_spacing": Pt(0.0),
"paragraph_format.line_spacing_rule": MULTIPLE (5),
"font.size": Pt(1.0),
----------Paragraph Style Колонтитул_рамка----------
----------Paragraph Style Marker_12----------
"paragraph_style.name": Marker_12,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Marker_12"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": False,
"paragraph_style.priority": 99,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(4.0),
"paragraph_format.space_after": Pt(4.0),
"font.name": Arial,
"font.size": Pt(12.0),
----------Paragraph Style Marker_12----------
----------Paragraph Style Список сокращений_таблица----------
"paragraph_style.name": Список сокращений_таблица,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список сокращений_таблица"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.space_before": Pt(4.0),
"paragraph_format.space_after": Pt(4.0),
"font.name": Times New Roman,
----------Paragraph Style Список сокращений_таблица----------
----------Paragraph Style Рисунок----------
"paragraph_style.name": Рисунок,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Рисунок"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.keep_with_next": True,
"paragraph_format.space_before": Pt(12.0),
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.right_indent": Pt(2.85),
----------Paragraph Style Рисунок----------
----------Paragraph Style Рисунок_название----------
"paragraph_style.name": Рисунок_название,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Рисунок_название"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.space_before": Pt(6.0),
"paragraph_format.space_after": Pt(12.0),
"paragraph_format.right_indent": Pt(2.85),
"font.name": Times New Roman,
----------Paragraph Style Рисунок_название----------
----------Paragraph Style Рисунок_список позиций----------
"paragraph_style.name": Рисунок_список позиций,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Рисунок_список позиций"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.keep_with_next": True,
"paragraph_format.space_before": Pt(6.0),
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.right_indent": Pt(2.85),
----------Paragraph Style Рисунок_список позиций----------
----------Paragraph Style Предупреждение----------
"paragraph_style.name": Предупреждение,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(12.0),
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.first_line_indent": Pt(28.35),
"font.name": Times New Roman,
----------Paragraph Style Предупреждение----------
----------Paragraph Style Заголовок_Приложение----------
"paragraph_style.name": Заголовок_Приложение,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Заголовок_Приложение"],
"paragraph_style.base_style": CustomStyleFactory.styles["Heading 1"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
----------Paragraph Style Заголовок_Приложение----------
----------Paragraph Style Таблица_список_цифры----------
"paragraph_style.name": Таблица_список_цифры,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_список_цифры"],
"paragraph_style.base_style": CustomStyleFactory.styles["Таблица_13_центр"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
----------Paragraph Style Таблица_список_цифры----------
----------Paragraph Style Список_точка_2ур----------
"paragraph_style.name": Список_точка_2ур,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_точка_2ур"],
"paragraph_style.base_style": CustomStyleFactory.styles["Список_тире_1ур"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
----------Paragraph Style Список_точка_2ур----------
----------Paragraph Style Список_буква_1ур----------
"paragraph_style.name": Список_буква_1ур,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_буква_1ур"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": False,
"paragraph_style.priority": 5,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
----------Paragraph Style Список_буква_1ур----------
----------Paragraph Style Список_нумерация_2ур----------
"paragraph_style.name": Список_нумерация_2ур,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_нумерация_2ур"],
"paragraph_style.base_style": CustomStyleFactory.styles["Список_точка_2ур"],
"paragraph_style.quick_style": False,
"paragraph_style.priority": 5,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
----------Paragraph Style Список_нумерация_2ур----------
----------Paragraph Style Таблица_отступ после----------
"paragraph_style.name": Таблица_отступ после,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_отступ после"],
"paragraph_style.base_style": CustomStyleFactory.styles["Основной"],
"paragraph_style.quick_style": True,
"paragraph_style.priority": 7,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": None,
"paragraph_format.space_before": Pt(0.0),
"paragraph_format.space_before": Pt(0.0),
----------Paragraph Style Таблица_отступ после----------
----------Paragraph Style Таблица_список_цифры_сноски----------
"paragraph_style.name": Таблица_список_цифры_сноски,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица_список_цифры_сноски"],
"paragraph_style.base_style": CustomStyleFactory.styles["Таблица_список_цифры"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.right_indent": Pt(2.85),
"font.size": Pt(12.0),
----------Paragraph Style Таблица_список_цифры_сноски----------
----------Paragraph Style Список_буква_2ур----------
"paragraph_style.name": Список_буква_2ур,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Список_буква_2ур"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
----------Paragraph Style Список_буква_2ур----------
----------Paragraph Style Table_12----------
"paragraph_style.name": Table_12,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table_12"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(4.0),
"paragraph_format.space_after": Pt(4.0),
"font.name": Times New Roman,
"font.size": Pt(12.0),
----------Paragraph Style Table_12----------
----------Paragraph Style Table----------
"paragraph_style.name": Table,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.space_before": Pt(4.0),
"paragraph_format.space_after": Pt(4.0),
"font.name": Times New Roman,
----------Paragraph Style Table----------
----------Paragraph Style Marker----------
"paragraph_style.name": Marker,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Marker"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.left_indent": Pt(56.7),
"paragraph_format.right_indent": Pt(21.25),
"paragraph_format.first_line_indent": -Pt(7.05),
"paragraph_format.line_spacing": Pt(7.874015748031496)e-05,
"paragraph_format.line_spacing_rule": MULTIPLE (5),
"font.name": Times New Roman,
----------Paragraph Style Marker----------
----------Paragraph Style Таблица----------
"paragraph_style.name": Таблица,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Таблица"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.space_before": Pt(2.0),
"paragraph_format.space_after": Pt(2.0),
"font.name": Times New Roman,
----------Paragraph Style Таблица----------
----------Paragraph Style TableHead----------
"paragraph_style.name": TableHead,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["TableHead"],
"paragraph_style.base_style": CustomStyleFactory.styles["Table"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
----------Paragraph Style TableHead----------
----------Paragraph Style Table_name----------
"paragraph_style.name": Table_name,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Table"],
"paragraph_style.base_style": CustomStyleFactory.styles["Table"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.keep_with_next": True,
"paragraph_format.first_line_indent": Pt(28.35),
----------Paragraph Style Table_name----------
----------Paragraph Style Drawing----------
"paragraph_style.name": Drawing,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["DrawingSign"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.keep_with_next": True,
"paragraph_format.space_before": Pt(5.65),
"paragraph_format.space_after": Pt(8.5),
"paragraph_format.right_indent": Pt(2.85),
"font.name": Times New Roman,
----------Paragraph Style Drawing----------
----------Paragraph Style DrawingSign----------
"paragraph_style.name": DrawingSign,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Main"],
"paragraph_style.base_style": CustomStyleFactory.styles["Drawing"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.keep_with_next": False,
----------Paragraph Style DrawingSign----------
----------Paragraph Style ЗагТабл----------
"paragraph_style.name": ЗагТабл,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["ЗагТабл"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.CENTER,
"paragraph_format.space_before": Pt(3.0),
"paragraph_format.space_after": Pt(3.0),
"paragraph_format.widow_control": False,
"font.name": Times New Roman,
----------Paragraph Style ЗагТабл----------
----------Paragraph Style Примечание_текста----------
"paragraph_style.name": Примечание_текста,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Примечание_текста"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.first_line_indent": Pt(42.55),
"paragraph_format.widow_control": False,
"font.name": Times New Roman,
"font.size": Pt(13.0),
----------Paragraph Style Примечание_текста----------
----------Paragraph Style Стиль Документа----------
"paragraph_style.name": Стиль Документа,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["Стиль Документа"],
"paragraph_style.base_style": CustomStyleFactory.styles["Normal"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.alignment": WD_PARAGRAPH_ALIGNMENT.JUSTIFY,
"paragraph_format.space_before": Pt(6.0),
"paragraph_format.space_after": Pt(6.0),
"paragraph_format.first_line_indent": Pt(35.45),
"font.name": Times New Roman,
----------Paragraph Style Стиль Документа----------
----------Paragraph Style СписокДок----------
"paragraph_style.name": СписокДок,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["СписокДок"],
"paragraph_style.base_style": CustomStyleFactory.styles["Стиль Документа"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.left_indent": Pt(42.55),
"paragraph_format.first_line_indent": -Pt(14.2),
----------Paragraph Style СписокДок----------
----------Paragraph Style СписокДок2----------
"paragraph_style.name": СписокДок2,
"paragraph_style.next_paragraph_style": CustomStyleFactory.styles["СписокДок2"],
"paragraph_style.base_style": CustomStyleFactory.styles["СписокДок"],
"paragraph_style.quick_style": True,
"paragraph_style.type": WD_STYLE_TYPE.PARAGRAPH,
"paragraph_style.builtin": False,
"paragraph_format.left_indent": Pt(63.8),
----------Paragraph Style СписокДок2----------
"""
