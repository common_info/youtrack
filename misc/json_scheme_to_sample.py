import json
from typing import Sequence, Union, Any
from pathlib import Path
import logging
from logging import Logger, Formatter
from logging.handlers import RotatingFileHandler


logger: Logger = logging.getLogger(__qualname__)


def indent(line: str, line_indent: int = 0):
    return "".join(("" * line_indent, line))


class Converter:
    def __init__(self, raw_text: Sequence[str]):
        self.raw_text = raw_text
        self.hierarchy: dict[str, Union[str, list[dict]]] = dict()
        self.headers: list[str] = []

    @classmethod
    def from_file(cls, path_file: Union[str, Path]):
        try:
            with open(path_file, "r+") as file:
                raw_text: Sequence[str] = file.readlines()
        except FileNotFoundError:
            logging.exception(f"The file {path_file} is not found", exc_info=True)
            raise
        except PermissionError:
            logging.exception(f"You have no permissions to modify the file {path_file}", exc_info=True)
            raise
        except IsADirectoryError:
            logging.exception(f"The path {path_file} leads to the directory, not the file", exc_info=True)
            raise
        except OSError:
            logging.exception(f"The error with the {path_file} file occurs", exc_info=True)
            raise
        except RuntimeError:
            logging.exception(f"The runtime error with the {path_file} file occurs", exc_info=True)
            raise
        else:
            return cls(raw_text)

    @classmethod
    def from_input(cls):
        sentinel: str = ""
        raw_text = [line for line in iter(input, sentinel)]
        return cls(raw_text)

    def jsonify(self):
        try:
            single_string: str = "\n".join(self.raw_text)
            json.loads(single_string)
