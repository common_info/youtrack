from typing import Literal, Sequence, Optional, Any

from catalog_objects import Product, Singleton
from catalog_database import TableProduct


class ProxyGlobalDict(Singleton):
    dict_products: dict[str, Any] = dict()
    dict_certificates: dict[str, Any] = dict()
    dict_customers: dict[str, Any] = dict()
    dict_deliveries: dict[str, Any] = dict()
    dict_acts: dict[str, Any] = dict()
    dict_implementations: dict[str, Any] = dict()
    dict_documents: dict[str, Any] = dict()
    dict_aliases: dict[str, list[str]] = dict()
    dict_images: dict[str, bytes] = dict()
    
    def get_dict_products(self):


class ProxyCatalogDatabase:
    def __init__(
            self,
            path: str,
            name: str,
            manufacturer: Literal["PROTEI RD", "PROTEI ST"], *,
            aliases: Optional[Sequence[str]] = None,
            images: Optional[Sequence[bytes]] = None,
            certificates: Sequence[str] = None):
        self.path = path
        self.name = name
        self.manufacturer = manufacturer
        self.aliases = aliases
        self.images = images
        self.certificates = certificates
    
    def from_product(self, product: Product, path: str):
        product_name = product.name
        table_product = TableProduct(path)
        product_id = table_product.get_id_by_name(product_name)
        
        
