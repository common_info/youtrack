import re
from collections import ChainMap
from functools import cached_property
from pathlib import Path
from re import Pattern, Match
from typing import Literal, Optional, Iterable, Union
import logging
from logging import Logger


logger: Logger = logging.getLogger(__name__)


class _Singleton:
    """ A metaclass that creates a Singleton base class when called. """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


class TextParser:
    root_pattern: Pattern = re.compile(r"^([a-zA-Z_\-\d]*)\s*\{")
    object_pattern: Pattern = re.compile(r"^\s*([a-zA-Z_\-\d]*).*[{}]")
    enumerated_pattern: Pattern = re.compile(r"\s*([a-zA-Z_\-\d]*)\s*\(\d+\),?")

    def __init__(self, file: Union[str, Path]):
        self.file = file
        self.root_line = -1

    @cached_property
    def content(self):
        try:
            with open(self.file, "r+") as file:
                content = file.readlines()
        except FileNotFoundError:
            pass
        except IsADirectoryError:
            pass
        except PermissionError:
            pass
        except OSError:
            pass
        else:
            return content

    @cached_property
    def root(self):
        first_line = re.match(self.__class__.root_pattern, self.content[0])
        if first_line:
            self.root_line = 0
            return first_line.group(1)
        else:
            for index, line in enumerate(self.content[1:]):
                if re.match(self.__class__.root_pattern, line):
                    self.root_line = index
                    return re.match(self.__class__.root_pattern, line).group(1)
                else:
                    continue
        logger.exception("No root object", exc_info=True)
        raise OSError

    def line_object(self, line: str):
        match_line_object: Match = re.match(self.__class__.object_pattern, line)
        match_line_enumerated: Match = re.match(self.__class__.enumerated_pattern, line)
        if match_line_object:
            level: int = (len(line) - len(line.lstrip(" "))) // 4
            object_class_name: str = re.match(self.__class__.object_pattern, line).group(1)
            object_type: Literal["root", "object", "enumerated"] = "object"
            return object_class_name, level, object_type
        if match_line_enumerated:
            level: int = (len(line) - len(line.lstrip(" "))) // 4
            object_class_name: str = re.match(self.__class__.object_pattern, line).group(1)
            object_type = "enumerated"
            return object_class_name, (level, object_type)
        else:
            logger.warning("No object found")

    @cached_property
    def chain_levels(self):
        if self.root_line == -1:
            logging.exception("The root is not found", exc_info=True)
            raise AttributeError
        k = list(filter(lambda x: self.line_object(x)[0], self.content[self.root_line:]))
        v = list(filter(lambda x: self.line_object(x)[1], self.content[self.root_line:]))
        dict(zip(k, v))
        return ChainMap(dict(zip(k, v)))

    def get_object_classes(self):
        return [ObjectClass.from_object_class_name(name) for name in self.chain_levels]


@_Singleton
class ObjectHiearchy:
    def __init__(self, text_parser: TextParser):
        self.text_parser = text_parser
        self.object_hierarchy: ChainMap = ChainMap()
        self.dict_object_class: dict[str, ObjectClass] = dict()

    def root_object(self):
        root_name: str = self.text_parser.root
        self.object_hierarchy[root_name] = None
        object_root: ObjectClass = ObjectClass(root_name)
        object_root.level = 0
        object_root.parent = None
        self.dict_object_class[root_name] = object_root


class ObjectClass:
    _hierarchy: ObjectHiearchy = ObjectHiearchy()

    def __init__(
            self,
            name: str,
            level: int = -1,
            parent: Optional[str] = None,
            child: Optional[list[str]] = None,
            enum_values: Optional[Iterable] = None,
            object_type: Literal["basic", "root", "object", "enumerated"] = "basic"):
        if child is None:
            child = []

        self.name: str = name
        self.level: int = level
        self.parent: Optional[str] = parent
        self.child: Optional[list[str]] = child
        self.enum_values: Optional[Iterable[str], Iterable[int]] = enum_values
        self.object_type = object_type
        self._verify_init()

    def _verify_init(self):
        if self.object_type == "root":
            self._verify_root()
        elif self.object_type == "object":
            self._verify_object()
        elif self.object_type == "enumerated":
            self._verify_enumerated()

    def _verify_root(self):
        if self.parent is not None:
            logger.exception("The root cannot have a parent", exc_info=True)
            raise AttributeError
        if self.enum_values is not None:
            logger.exception("The root cannot have enumerated values", exc_info=True)
            raise AttributeError
        if self.level != 0:
            logger.exception("The root must have level 0", exc_info=True)
            raise ValueError

    def _verify_object(self):
        if any(self.__class__._hierarchy.dict_object_class[item].object_type == "root" for item in self.child):
            logger.exception("The object cannot have the root in their child", exc_info=True)
            raise ValueError
        if self.level < 1:
            logger.exception("The object cannot have a level less than 1", exc_info=True)
            raise ValueError
        if self.enum_values is not None:
            logger.exception("The object cannot have enumerated values", exc_info=True)
            raise AttributeError

    def _verify_enumerated(self):
        if any(self.__class__._hierarchy.dict_object_class[item].object_type == "root" for item in self.child):
            logger.exception("The enumerated cannot have the root in their child", exc_info=True)
            raise ValueError
        if self.level < 1:
            logger.exception("The enumerated cannot have a level less than 1", exc_info=True)
            raise ValueError
        if self.child is not None:
            logger.exception("The enumerated cannot have child", exc_info=True)
            raise AttributeError

    @classmethod
    def from_object_class_name(cls, name):
        return cls._hierarchy.dict_object_class[name]

    def __key(self):
        return self.name, self.level, self.parent, self.child

    def __eq__(self, other):
        if isinstance(other, ObjectClass):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, ObjectClass):
            return self.name != other.name
        else:
            return NotImplemented

    def __gt__(self, other):
        if not isinstance(other, ObjectClass):
            return NotImplemented
        else:
            if other.name in self.child:
                return True
            elif self.name in other.child:
                return False
            else:
                return self.level > other.level

    def __lt__(self, other):
        if not isinstance(other, ObjectClass):
            return NotImplemented
        else:
            if other.name in self.child:
                return False
            elif self.name in other.child:
                return True
            else:
                return self.level < other.level

    def __ge__(self, other):
        if not isinstance(other, ObjectClass):
            return NotImplemented
        else:
            if self == other:
                return True
            elif other.name in self.child:
                return True
            elif self.name in other.child:
                return False
            else:
                return self.level >= other.level

    def __le__(self, other):
        if not isinstance(other, ObjectClass):
            return NotImplemented
        else:
            if self == other:
                return True
            elif other.name in self.child:
                return False
            elif self.name in other.child:
                return True
            else:
                return self.level <= other.level

    @cached_property
    def get_parent(self):
        return self.__class__._hierarchy.dict_object_class[self.parent]

    def get_child(self, child_name: str):
        if child_name not in self.child:
            logger.exception(f"The child {child_name} is not found", exc_info=True)
            raise ValueError
        else:
            return self.__class__._hierarchy.dict_object_class[child_name]

    @property
    def get_all_child(self):
        return [self.get_child(child) for child in self.child]


class ObjectMap:
    def __init__(self, object_class_1: ObjectClass, object_class_2: ObjectClass):
        self.parent_object = None
        self.child_object = None
        self.objects = (object_class_1, object_class_2)

    def mapping_to_root(self):
        object_class_items = list(*self.objects)
