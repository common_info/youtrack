import sys
from pathlib import Path

import docx
from docx import Document as Doc
from docx.document import Document
from docx.document import Section
from docx.enum.style import WD_STYLE_TYPE
from docx.section import _Footer, _Header
from docx.settings import Settings
from docx.table import Table
from docx.styles.latent import _LatentStyle, LatentStyles
from docx.styles.style import _TableStyle, _ParagraphStyle
from docx.styles.styles import Styles
from docx.text.font import Font
from docx.dml.color import ColorFormat
from docx.text.parfmt import ParagraphFormat
from docx.table import _Row, _Rows, _Column, _Columns, _Cell
from docx.text.paragraph import Paragraph
from docx.shared import Emu, Cm, Inches, Length
from docx.text.tabstops import TabStop
import logging
from logging import Logger, Formatter, FileHandler
from docx.opc.coreprops import CoreProperties

logger: Logger = logging.getLogger(__name__)

fmt: str = "{asctime}:{name}:{levelname} | {filename}:{lineno} | {message}"
datefmt: str = "%Y-%m-%d %H:%M:%S"
formatter: Formatter = Formatter(fmt, datefmt, "{", True)

handler: FileHandler = FileHandler(f"{Path(sys.argv[0]).stem}_info.log", "w", "utf-8")
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

logger.setLevel(logging.DEBUG)
logger.addHandler(handler)


def get_table_stops(style_par_format: ParagraphFormat):
    style_table_stop: TabStop
    for style_table_stop in iter(style_par_format.tab_stops):
        logger.info(f"style_table_stop.leader = {style_table_stop.leader}")
        logger.info(f"style_table_stop.alignment = {style_table_stop.alignment}")
        logger.info(f"style_table_stop.position = {style_table_stop.position}")


def get_table_style_format(style: _TableStyle):
    style_par_format: ParagraphFormat = style.paragraph_format
    logger.info(f"style_par_format.alignment = {style_par_format.alignment}")
    # logger.info(f"style_par_format.line_spacing = {Length(style_par_format.line_spacing).mm}")
    logger.info(f"style_par_format.line_spacing = {style_par_format.line_spacing}")
    # logger.info(f"style_par_format.line_spacing_rule = {Length(style_par_format.line_spacing_rule).mm}")
    logger.info(f"style_par_format.line_spacing_rule = {style_par_format.line_spacing_rule}")

    if style_par_format.first_line_indent is not None:
        logger.info(f"style_par_format.first_line_indent = {Length(style_par_format.first_line_indent).mm}")
        logger.info(f"style_par_format.first_line_indent = {Length(style_par_format.first_line_indent).pt}")
    else:
        logger.info(f"style_par_format.first_line_indent = {style_par_format.first_line_indent}")

    if style_par_format.left_indent is not None:
        logger.info(f"style_par_format.left_indent = {Length(style_par_format.left_indent).mm}")
        logger.info(f"style_par_format.left_indent = {Length(style_par_format.left_indent).pt}")
    else:
        logger.info(f"style_par_format.left_indent = {style_par_format.left_indent}")

    if style_par_format.right_indent is not None:
        logger.info(f"style_par_format.right_indent = {Length(style_par_format.right_indent).mm}")
        logger.info(f"style_par_format.right_indent = {Length(style_par_format.right_indent).pt}")
    else:
        logger.info(f"style_par_format.right_indent = {style_par_format.right_indent}")

    if style_par_format.space_before is not None:
        logger.info(f"style_par_format.space_before = {Length(style_par_format.space_before).mm}")
        logger.info(f"style_par_format.space_before = {Length(style_par_format.space_before).pt}")
    else:
        logger.info(f"style_par_format.space_before = {style_par_format.space_before}")

    if style_par_format.space_after is not None:
        logger.info(f"style_par_format.space_after = {Length(style_par_format.space_after).mm}")
        logger.info(f"style_par_format.space_after = {Length(style_par_format.space_after).pt}")
    else:
        logger.info(f"style_par_format.space_after = {style_par_format.space_after}")

    logger.info(f"style_par_format.keep_together = {style_par_format.keep_together}")
    logger.info(f"style_par_format.keep_with_next = {style_par_format.keep_with_next}")
    logger.info(f"style_par_format.tab_stops = {style_par_format.tab_stops}")
    logger.info(f"style_par_format.widow_control = {style_par_format.widow_control}")


def get_style_font(style: _TableStyle):
    style_font: Font = style.font
    logger.info(f"style_font.name = {style_font.name}")
    if style_font.size is not None:
        logger.info(f"style_font.size = {Length(style_font.size).pt}")
    else:
        logger.info(f"style_font.size = {style_font.size}")
    logger.info(f"style_font.rtl = {style_font.rtl}")
    logger.info(f"style_font.outline = {style_font.outline}")
    logger.info(f"style_font.emboss = {style_font.emboss}")
    logger.info(f"style_font.bold = {style_font.bold}")


def get_style_color(style: _TableStyle):
    style_font: Font = style.font
    style_color: ColorFormat = style_font.color
    logger.info(f"style_color.type = {style_color.type}")
    logger.info(f"style.base_style = {style.base_style}")
    logger.info(f"style_color.theme_color = {style_color.theme_color}")
    logger.info(f"style_color.rgb = {style_color.rgb}")


def get_table_style(styles: Styles, style_name: str):
    style: _TableStyle = styles[style_name]
    logger.info(f"style.name = {style.name}")
    logger.info(f"style.type = {style.type}")
    logger.info(f"style.font = {style.font}")
    logger.info(f"style.builtin = {style.builtin}")
    logger.info(f"style.style_id = {style.style_id}")
    logger.info(f"style.quick_style = {style.quick_style}")
    logger.info(f"style.base_style = {style.base_style}")
    logger.info(f"style.priority = {style.priority}")
    logger.info(f"style.next_paragraph_style = {style.next_paragraph_style}")


def get_table_cell(section: Section):
    section_tables: list[Table] = section.header.tables
    for table in section_tables:
        logger.info(f"len(table.rows) = {len(table.rows)}")
        logger.info(f"table._column_count = {table._column_count}")
        for row_id in range(len(table.rows[:])):
            cell: _Cell
            for cell in table.row_cells(row_id):
                logger.info(f"row_id = {row_id}")
                logger.info(f"cell.text = {cell.text}")
                logger.info(f"cell.width = {Length(cell.width).mm}")
                logger.info(f"cell.width = {Length(cell.width).pt}")
                logger.info(f"cell.paragraphs = {cell.paragraphs}")
                logger.info(f"cell.vertical_alignment = {cell.vertical_alignment}")


def get_table_row_column(table: Table):
    row: _Row
    for index, row in enumerate(iter(table.rows[:])):
        logger.info(f"-----Row {index}-----")
        logger.info(f"row.height = {Length(row.height).mm}")
        logger.info(f"row.height = {Length(row.height).pt}")
        logger.info(f"row.height = {Length(row.height).inches}")
        logger.info(f"row.height_rule = {row.height_rule}")
        logger.info(f"-----Row {index}-----")
    column: _Column
    for index, column in enumerate(iter(table.columns)):
        logger.info(f"-----Column {index}-----")
        logger.info(f"column.width = {Length(column.width).mm}")
        logger.info(f"column.width = {Length(column.width).pt}")
        logger.info(f"column.width = {Length(column.width).inches}")
        logger.info(f"-----Column {index}-----")


def get_section_info(section: Section):
    logger.info(f"section.orientation = {section.orientation}")

    if section.page_height is not None:
        logger.info(f"section.page_height = {Length(section.page_height).mm}")
        logger.info(f"section.page_height = {Length(section.page_height).pt}")
    else:
        logger.info(f"section.page_height = {section.page_height}")

    if section.page_width is not None:
        logger.info(f"section.page_width = {Length(section.page_width).mm}")
        logger.info(f"section.page_width = {Length(section.page_width).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.top_margin}")

    if section.header_distance is not None:
        logger.info(f"section.header_distance = {Length(section.header_distance).mm}")
        logger.info(f"section.header_distance = {Length(section.header_distance).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.header_distance}")

    if section.footer_distance is not None:
        logger.info(f"section.footer_distance = {Length(section.footer_distance).mm}")
        logger.info(f"section.footer_distance = {Length(section.footer_distance).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.footer_distance}")

    logger.info(f"section.start_type = {section.start_type}")

    if section.top_margin is not None:
        logger.info(f"section.top_margin = {Length(section.top_margin).mm}")
        logger.info(f"section.top_margin = {Length(section.top_margin).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.top_margin}")

    if section.bottom_margin is not None:
        logger.info(f"section.bottom_margin = {Length(section.bottom_margin).mm}")
        logger.info(f"section.bottom_margin = {Length(section.bottom_margin).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.bottom_margin}")

    if section.left_margin is not None:
        logger.info(f"section.left_margin = {Length(section.left_margin).mm}")
        logger.info(f"section.left_margin = {Length(section.left_margin).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.left_margin}")

    if section.right_margin is not None:
        logger.info(f"section.right_margin = {Length(section.right_margin).mm}")
        logger.info(f"section.right_margin = {Length(section.right_margin).pt}")
    else:
        logger.info(f"section.bottom_margin = {section.right_margin}")


def get_section_header_footer(section: Section):
    logger.info(f"section_header.tables = {section.header.tables}")
    if section.header.tables is not None:
        header_table: Table
        for header_table in section.header.tables:
            logger.info(f"len(header_table.rows) = {len(header_table.rows)}")
            logger.info(f"len(header_table.columns) = {len(header_table.columns)}")
            logger.info(f"header_table.table_direction = {header_table.table_direction}")
    logger.info(f"section_header.is_linked_to_previous = {section.header.is_linked_to_previous}")
    logger.info(f"section_footer.tables = {section.footer.tables}")
    if section.footer.tables is not None:
        footer_table: Table
        for footer_table in section.footer.tables:
            logger.info(f"len(footer_table.rows) = {len(footer_table.rows)}")
            logger.info(f"len(footer_table.columns) = {len(footer_table.columns)}")
            logger.info(f"footer_table.table_direction = {footer_table.table_direction}")
    logger.info(f"section_footer.is_linked_to_previous = {section.footer.is_linked_to_previous}")


def get_paragraph_style(paragraph: Paragraph):
    paragraph_style: _ParagraphStyle = paragraph.style
    logger.info(f"paragraph_style.name = {paragraph_style.name}")
    logger.info(f"paragraph_style.style_id = {paragraph_style.style_id}")
    logger.info(f"paragraph_style.next_paragraph_style = {paragraph_style.next_paragraph_style}")
    logger.info(f"paragraph_style.paragraph_format = {paragraph_style.paragraph_format}")
    logger.info(f"paragraph_style.base_style = {paragraph_style.base_style}")
    logger.info(f"paragraph_style.quick_style = {paragraph_style.quick_style}")
    logger.info(f"paragraph_style.priority = {paragraph_style.priority}")
    logger.info(f"paragraph_style.type = {paragraph_style.type}")
    logger.info(f"paragraph_style.font = {paragraph_style.font}")
    logger.info(f"paragraph_style.builtin = {paragraph_style.builtin}")


def get_paragraph_style_name(styles: Styles, style_name: str):
    paragraph_style: _ParagraphStyle = styles[style_name]
    logger.info(f"paragraph_style.name = {paragraph_style.name}")
    logger.info(f"paragraph_style.style_id = {paragraph_style.style_id}")
    logger.info(f"paragraph_style.next_paragraph_style = {paragraph_style.next_paragraph_style}")
    logger.info(f"paragraph_style.paragraph_format = {paragraph_style.paragraph_format}")
    logger.info(f"paragraph_style.base_style = {paragraph_style.base_style}")
    logger.info(f"paragraph_style.quick_style = {paragraph_style.quick_style}")
    logger.info(f"paragraph_style.priority = {paragraph_style.priority}")
    logger.info(f"paragraph_style.type = {paragraph_style.type}")
    logger.info(f"paragraph_style.font = {paragraph_style.font}")
    logger.info(f"paragraph_style.builtin = {paragraph_style.builtin}")


def get_character_style_name(styles: Styles, style_name: str):
    paragraph_style: _ParagraphStyle = styles[style_name]
    logger.info(f"paragraph_style.name = {paragraph_style.name}")
    logger.info(f"paragraph_style.style_id = {paragraph_style.style_id}")

    logger.info(f"paragraph_style.base_style = {paragraph_style.base_style}")
    logger.info(f"paragraph_style.quick_style = {paragraph_style.quick_style}")
    logger.info(f"paragraph_style.priority = {paragraph_style.priority}")
    logger.info(f"paragraph_style.type = {paragraph_style.type}")
    logger.info(f"paragraph_style.font = {paragraph_style.font}")
    logger.info(f"paragraph_style.builtin = {paragraph_style.builtin}")


def get_paragraph_style_format(paragraph_style: _ParagraphStyle):
    paragraph_format: ParagraphFormat = paragraph_style.paragraph_format
    logger.info(f"paragraph_format.alignment = {paragraph_format.alignment}")
    logger.info(f"paragraph_format.keep_with_next = {paragraph_format.keep_with_next}")
    logger.info(f"paragraph_format.keep_together = {paragraph_format.keep_together}")

    if paragraph_format.space_before is not None:
        logger.info(f"paragraph_format.space_before = {Length(paragraph_format.space_before).mm}")
        logger.info(f"paragraph_format.space_before = {Length(paragraph_format.space_before).pt}")
    else:
        logger.info(f"paragraph_format.space_before = {paragraph_format.space_before}")

    if paragraph_format.space_after is not None:
        logger.info(f"paragraph_format.space_after = {Length(paragraph_format.space_after).mm}")
        logger.info(f"paragraph_format.space_after = {Length(paragraph_format.space_after).pt}")
    else:
        logger.info(f"paragraph_format.space_after = {paragraph_format.space_after}")

    if paragraph_format.left_indent is not None:
        logger.info(f"paragraph_format.left_indent = {Length(paragraph_format.left_indent).mm}")
        logger.info(f"paragraph_format.left_indent = {Length(paragraph_format.left_indent).pt}")
    else:
        logger.info(f"paragraph_format.left_indent = {paragraph_format.left_indent}")

    if paragraph_format.right_indent is not None:
        logger.info(f"paragraph_format.right_indent = {Length(paragraph_format.right_indent).mm}")
        logger.info(f"paragraph_format.right_indent = {Length(paragraph_format.right_indent).pt}")
    else:
        logger.info(f"paragraph_format.right_indent = {paragraph_format.right_indent}")

    if paragraph_format.first_line_indent is not None:
        logger.info(f"paragraph_format.first_line_indent = {Length(paragraph_format.first_line_indent).mm}")
        logger.info(f"paragraph_format.first_line_indent = {Length(paragraph_format.first_line_indent).pt}")
    else:
        logger.info(f"paragraph_format.first_line_indent = {paragraph_format.first_line_indent}")

    if paragraph_format.line_spacing is not None:
        logger.info(f"paragraph_format.line_spacing = {Length(paragraph_format.line_spacing).mm}")
        logger.info(f"paragraph_format.line_spacing = {Length(paragraph_format.line_spacing).pt}")
    else:
        logger.info(f"paragraph_format.line_spacing = {paragraph_format.line_spacing}")

    logger.info(f"paragraph_format.line_spacing_rule = {paragraph_format.line_spacing_rule}")
    logger.info(f"paragraph_format.page_break_before = {paragraph_format.page_break_before}")
    logger.info(f"paragraph_format.widow_control = {paragraph_format.widow_control}")


def get_font(font: Font):
    logger.info(f"font.name = {font.name}")
    logger.info(f"font.rtl = {font.rtl}")
    logger.info(f"font.outline = {font.outline}")
    if font.size is not None:
        logger.info(f"font.size = {Length(font.size).pt}")
    else:
        logger.info(f"font.size = {font.size}")
    logger.info(f"font.bold = {font.bold}")
    logger.info(f"font.italic = {font.italic}")
    logger.info(f"font.color.type = {font.color.type}")
    logger.info(f"font.color.theme_color = {font.color.theme_color}")
    logger.info(f"font.color.rgb = {font.color.rgb}")


def get_core_properties(document: Document):
    core_props: CoreProperties = document.core_properties
    logger.info(f"core_props.title = {core_props.title}")
    logger.info(f"core_props.author = {core_props.author}")
    logger.info(f"core_props.version = {core_props.version}")
    logger.info(f"core_props.subject = {core_props.subject}")
    logger.info(f"core_props.identifier = {core_props.identifier}")
    logger.info(f"core_props.category = {core_props.category}")
    logger.info(f"core_props.language = {core_props.language}")
    logger.info(f"core_props.content_status = {core_props.content_status}")
    logger.info(f"core_props.comments = {core_props.comments}")
    logger.info(f"core_props.created = {core_props.created}")
    logger.info(f"core_props.keywords = {core_props.keywords}")
    logger.info(f"core_props.revision = {core_props.revision}")
    logger.info(f"core_props.modified = {core_props.modified}")
    logger.info(f"core_props.last_modified_by = {core_props.last_modified_by}")

    settings: Settings = document.settings
    logger.info(f"settings.odd_and_even_pages_header_footer = {settings.odd_and_even_pages_header_footer}")


def main():
    document: Document = Doc("/Users/user/Desktop/Шаблон/ПДРА.465684.020РЭ (АТС-Протей)_рамка.docx")
    styles: Styles = document.styles
    list_styles = list(filter(lambda x: x.type == WD_STYLE_TYPE.LIST, styles))
    logger.info("----------List Styles----------")
    logger.info([list_style.name for list_style in list_styles])
    logger.info("----------List Styles----------")

    paragraph_styles = list(filter(lambda x: x.type == WD_STYLE_TYPE.PARAGRAPH, styles))
    logger.info("----------Paragraph Styles----------")
    logger.info([paragraph_style.name for paragraph_style in paragraph_styles])
    for paragraph_style in paragraph_styles:
        logger.info(f"----------Paragraph Style {paragraph_style.name}----------")
        get_paragraph_style_name(styles, paragraph_style.name)
        get_paragraph_style_format(paragraph_style)
        get_font(paragraph_style.font)
        get_style_color(paragraph_style)
        logger.info(f"----------Paragraph Style {paragraph_style.name}----------")
    logger.info("----------Paragraph Styles----------")

    character_styles = list(filter(lambda x: x.type == WD_STYLE_TYPE.CHARACTER, styles))
    logger.info("----------Character Styles----------")
    logger.info([character_style.name for character_style in character_styles])
    for character_style in character_styles:
        logger.info(f"----------Character Style {character_style.name}----------")
        get_character_style_name(styles, character_style.name)
        get_font(character_style.font)
        get_style_color(character_style)
        logger.info(f"----------Character Style {character_style.name}----------")
    logger.info("----------Character Styles----------")

    table_styles = list(filter(lambda x: x.type == WD_STYLE_TYPE.TABLE, styles))
    logger.info("----------Table Styles----------")
    logger.info([table_style.name for table_style in table_styles])
    for table_style in table_styles:
        logger.info(f"----------Table Style {table_style.name}----------")
        get_paragraph_style_name(styles, table_style.name)
        get_table_style_format(table_style)
        get_style_font(table_style)
        get_style_color(table_style)
        logger.info(f"----------Table Style {table_style.name}----------")
    logger.info("----------Table Styles----------")

    latent_styles: LatentStyles = styles.latent_styles
    # table_style: _TableStyle = styles["Normal Table"]
    # get_core_properties(document)
    # get_table_style(styles, "Normal Table")
    # get_table_style_format(table_style)
    # get_style_font(table_style)
    # get_style_color(table_style)
    # get_paragraph_style_format(table_style)
    # get_table_style(styles, "Table Grid")
    # get_table_style_format(styles["Table Grid"])
    # get_style_font(styles["Table Grid"])
    # get_style_color(styles["Table Grid"])
    # get_paragraph_style_format(styles["Table Grid"])
    # get_table_stops(styles["Table Grid"].paragraph_format)

    for index, section in enumerate(document.sections):
        logger.info(f"----------Section {index}----------")
        get_section_info(section)
        get_section_header_footer(section)
        get_table_cell(section)
        logger.info(f"----------Section {index} Table Style----------")
        if section.header.tables:
            logger.info("__Header__ __Table__ start")
            logger.info([get_table_row_column(header_table) for header_table in section.header.tables])
            logger.info("__Header__ __Table__ end")
        if section.footer.tables:
            logger.info("__Footer__ __Table__ start")
            logger.info([get_table_row_column(footer_table) for footer_table in section.footer.tables])
            logger.info("__Footer__ __Table__ end")
        logger.info(f"----------Section {index} Table Style----------")
        logger.info(f"----------Section {index}----------")

    # for idx, table in enumerate(document.tables):
    #     logger.info(f"----------Table {idx}----------")
    #     style: _TableStyle = table.style
    #     get_table_style(styles, style.name)
    #     get_table_style_format(style)
    #     get_style_font(style)
    #     get_style_color(style)
    #     get_paragraph_style_format(style)
    #     logger.info(f"----------Table {idx}----------")

    # for index, paragraph in enumerate(document.paragraphs):
    #     logger.info(f"----------Paragraph {index}----------")
    #     logger.info(f"paragraph.text = {paragraph.text}")
    #     logger.info(f"paragraph.alignment = {paragraph.alignment}")
    #     get_paragraph_style(paragraph)
    #     get_font(paragraph.style.font)
    #     get_paragraph_style_format(paragraph.style)


if __name__ == "__main__":
    main()
