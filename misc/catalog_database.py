import logging
from abc import abstractmethod
from logging import Logger
import sqlite3
from sqlite3.dbapi2 import Connection, Cursor, OperationalError, IntegrityError, DataError, NotSupportedError, \
    ProgrammingError, InternalError, DatabaseError
from typing import Optional, Union

logger: Logger = logging.getLogger(__name__)


sql_requests: dict = {
    "table_names": "SELECT * FROM sqlite_master where type='table';",
    "table_info": "PRAGMA table_info([{table_name}]);",
    "select": "SELECT {attr} FROM {table_name} WHERE {attr_name} = {attr_value}",
    "select_attr": "SELECT {attr} FROM {table_name}",
    "select_all": "SELECT * FROM {table_name}"
}


class Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, *args, **kwargs):
        if self.__class__._instance is None:
            super().__init__(*args, **kwargs)
            self.__class__._instance = self
        return

    def __call__(self, *args, **kwargs):
        if self.__class__._instance is None:
            self.__class__._instance = self.__class__.__new__(self.__class__, *args, **kwargs)
            self.__init__(*args, **kwargs)
        return self.__class__._instance


class Database(Singleton):
    _instance = None
    dict_tables = dict()

    def __init__(self, path: str):
        if self.__class__._instance is None:
            self.__class__._instance = super().__new__(self.__class__)
            super().__init__()
        self._path = path
        self.connection: Connection = Connection()
        self.__class__._instance = self
        try:
            self.connection = sqlite3.connect(self._path)
            self.cursor: Cursor = self.connection.cursor()
        except OperationalError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")

        except IntegrityError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except DataError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except NotSupportedError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except ProgrammingError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except InternalError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except DatabaseError as e:
            logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")

    def __call__(self):
        if self.__class__._instance is None:
            self.__class__._instance = self.__class__.__new__(self.__class__)
            self.__init__(self._path)
            self.__class__._instance = self
        return self.__class__._instance

    def __repr__(self):
        return f"<Database(path={self._path})>"

    def __str__(self):
        return f"Database: path = {self._path}"

    def __getitem__(self, item):
        return self.dict_tables.get(item)

    def __setitem__(self, key, value):
        setattr(self.dict_tables, key, value)

    def __iter__(self):
        yield self.dict_tables.items()

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.table_names
        if isinstance(item, Table):
            return item in self.dict_tables.values()

    def __len__(self):
        return len(self.dict_tables)

    @property
    def table_names(self):
        __sql = sql_requests["table_names"]
        return [table[1] for table in self.sql_results(__sql, False)]

    def table_info(self, table_name: str):
        return [
            f"parameter {item[0]}: {item[1]}, type: {item[2]}, default: {item[4]}\n"
            for item in self.sql_results("PRAGMA table_info([{table_name}]);", False, table_name=table_name)]

    def table_parameters(self, table_name: str):
        return [item[1] for item in self.table_info(table_name)]

    def sql_results(self, __sql: str, is_one: bool, **kwargs):
        self.cursor = self.cursor.execute(__sql.format(**kwargs))
        return self.cursor.fetchone() if is_one else self.cursor.fetchall()

    def select_items(self, table_name: str, is_one: bool, __sql: str, attr: Optional[str] = None, **kwargs):
        return self.sql_results(__sql, is_one, attr=attr, table_name=table_name, **kwargs)

    def rollback(self):
        self.connection.rollback()
        logger.warning("Database transaction rollback")

    def commit(self):
        self.connection.commit()
        logger.info("Database transaction is committed")

    def close(self):
        self.cursor.close()
        self.connection.close()


class Table:
    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    def __init__(self, name: str, path: str):
        self._parent = Database(path)
        self.path = path
        self.name = name
        self._parent.dict_tables[self.name] = self.name

    def __repr__(self):
        return f"<Table(name={self.name})>"

    def __hash__(self):
        return hash((self.name, self.path))

    def __key(self):
        return self.name, self.path

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @abstractmethod
    @property
    def records(self):
        pass

    def table_info(self, table_name: str = None):
        if table_name is None:
            table_name = self.name
        return self._parent.table_info(table_name)

    def get_attr_one(self, **kwargs):
        return self._parent.select_items(self.name, True, sql_requests["select"], **kwargs)

    def get_attr_many(self, **kwargs):
        return self._parent.select_items(self.name, False, sql_requests["select"], **kwargs)

    def get_attr_one_other(self, table_name: str, **kwargs):
        return self._parent.select_items(table_name, True, sql_requests["select"], **kwargs)

    def get_attr_many_other(self, table_name: str, **kwargs):
        return self._parent.select_items(table_name, False, sql_requests["select"], **kwargs)


class TableProduct(Table):
    def __init__(self, path: str):
        self.name = "product"
        super().__init__(self.name, path)

    @property
    def records(self):
        return self._parent.select_items(self.name, False, sql_requests["select_all"])

    @property
    def products(self):
        return self._parent.select_items(self.name, False, sql_requests["select_attr"], attr="product_name")

    @property
    def product_ids(self):
        return self._parent.select_items(self.name, False, sql_requests["select_attr"], attr="product_id")

    def get_name_by_id(self, product_id: int):
        return super().get_attr_one(attr="product_name", attr_name="product_id", attr_value=product_id)

    def get_id_by_name(self, product_name: str):
        return super().get_attr_one(attr="product_name", attr_name="product_name", attr_value=product_name)

    def get_manufacturer(self, product: Union[str, int]):
        if isinstance(product, int):
            return super().get_attr_one(attr="manufacturer", attr_name="product_id", attr_value=product)
        elif isinstance(product, str):
            return super().get_attr_one(attr="manufacturer", attr_name="product_name", attr_value=product)

    def get_product_aliases(self, product: Union[str, int]):
        if isinstance(product, int):
            return super().get_attr_many_other(
                "product_alias", attr="manufacturer", attr_name="product_id", attr_value=product)
        elif isinstance(product, str):
            return super().get_attr_many_other(
                "product_alias", attr="manufacturer", attr_name="product_name", attr_value=product)

    def get_product_images(self, product: Union[str, int]):
        if isinstance(product, int):
            return super().get_attr_many_other(
                "product_image", attr="image", attr_name="product_id", attr_value=product)
        elif isinstance(product, str):
            return super().get_attr_many_other(
                "product_image", attr="image", attr_name="product_name", attr_value=product)

    def get_certificates(self, product: Union[str, int]):
        return super().get_attr_many_other(
            "product_certificate", attr="certificate_id", attr_name="product_id", attr_value=product)

    def get_modifications(self, product: Union[str, int]):
        return super().get_attr_many_other(
            "product_modification", attr="modification_id", attr_name="product_id", attr_value=product)





class TableModification(Table):
    def __init__(self, path: str):
        self.name = "modification"
        super().__init__(self.name, path)

    @property
    def records(self):
        return self._parent.select_items(self.name, False, sql_requests["select_all"])


def main():
    database = Database("/Users/user/Desktop/catalog_products.db")
    print(database.__class__._instance)
    # database.tables()
    print(database.table_names)
    print(database.table_info("product"))
    # table = Table.get_table("product_modification")
    # print([repr(item) for item in database.tables()])
    print(repr(Table("modification", database._path)))
    table_delivery = Table("delivery", database._path)
    table_act = Table("act", database._path)
    print(database.table_parameters("act"))
    print(table_act.table_info)
    # print(table.table_attributes)
    print(database.dict_tables.items())
    # print(table)
    print(id(table_delivery))
    print(id(table_act))
    database.close()


if __name__ == "__main__":
    main()
