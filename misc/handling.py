import json
from pprint import pprint
from typing import Any, Union


class JsonAvpParser:
    def __init__(self, path: str):
        self.path: str = path
        self.list_avp = list()

    @property
    def context(self) -> list[dict[str, Union[str, int]]]:
        with open(self.path, "r") as file:
            context = json.loads(file.read())
        return context

    @property
    def listed(self):
        return [item for item in self.context]

    def avp_items(self):
        return {k: v for item in self.context for k, v in item.items()}

    def items(self):
        items = [{k: v} for item in self.context for k, v in item.items()]
        return *items,

    def formatting_items(self):
        for _ in self.context:
            item = Item(_["Code"], _["Name"], _["Type"], _["Value"])
            print(format(item, "output"))


class Item:
    def __init__(
            self,
            code: int = None,
            name: str = None,
            type_: str = None,
            value: Any = None):
        self.code: int = code
        self.name: str = name
        self.type_: str = type_
        self.value: Any = value

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"{self.code}\t{self.name}\t{self.type_}\t{self.value}"


def main():
    json_path: str = "json_pretty.json"
    json_avp_parser: JsonAvpParser = JsonAvpParser(json_path)
    json_avp_parser.formatting_items()


if __name__ == "__main__":
    main()
