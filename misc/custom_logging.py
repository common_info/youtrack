import logging
import sys
from pathlib import Path
from logging import Formatter, Logger, RootLogger, Handler, LoggerAdapter, StreamHandler, Filter, LogRecord
from logging.handlers import RotatingFileHandler
from typing import Literal, Union, Sequence, TypeVar, Any, TextIO, Optional, MutableMapping, NamedTuple, Mapping
import requests
from requests.cookies import RequestsCookieJar
from requests.models import Request, Response
from urllib3 import HTTPResponse
from urllib3._collections import HTTPHeaderDict
from urllib3.connection import HTTPConnection
from ssl import SSLSocket

FORMATTING_ATTRS: tuple[str, ...] = (
    "name", "levelno", "levelname", "pathname", "filename", "module", "lineno", "funcName", "created", "asctime",
    "thread", "threadName", "process", "message")

LevelType = TypeVar("LevelType", bound=Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL", "NOTSET"])
StyleType = TypeVar("StyleType", bound=Literal["s", "%", "{"])
_StreamT = TypeVar("_StreamT", bound=TextIO)
ModeType = TypeVar("ModeType", bound=Literal["r", "b", "rb", "wb", "a", "r+", "w+"])

logging.setLoggerClass(Logger)


class CustomLoggerBase(logging.getLoggerClass()):
    @staticmethod
    def generate():
        return logging.getLogger()


class CustomFormatter(Formatter):
    def __init__(
            self, *,
            fmt: str = "{name}:{levelname}:{asctime} --- {filename}:{lineno} --- {message}",
            datefmt: str = "%Y-%m-%d %H:%M:%S",
            style: StyleType = "{"):
        super().__init__(fmt, datefmt, style)


class CustomRotatingFileHandler(RotatingFileHandler):
    def __init__(
            self, *,
            filename: Union[str, Path, None] = None,
            mode: ModeType = "a",
            max_bytes: int = 2 ** 24,
            backup_count: int = 10,
            encoding: Optional[str] = None,
            level: LevelType = logging.DEBUG,
            formatter: Union[Formatter, CustomFormatter] = None,
            filters: Optional[Sequence[Filter]] = None):
        if filename is None:
            filename = ".".join((__file__[:-3].lower(), self.__class__.__mro__[0].__qualname__, "log"))
        super().__init__(filename, mode, max_bytes, backup_count, encoding)
        self.setLevel(level)
        if formatter:
            self.setFormatter(formatter)
        if filters:
            for item in filters:
                self.addFilter(item)


class CustomRotatingFileFilter(Filter):
    def __init__(self, name: str):
        super().__init__(name)

    def filter(self, record: LogRecord, **kwargs: Optional[Sequence]) -> bool:
        if kwargs:
            for k, v in kwargs.items():
                setattr(record, k, v)
        return True


def rotating_file_logging():
    rotating_file_formatter = CustomFormatter()
    print(rotating_file_formatter.datefmt)
    rotating_file_handler = CustomRotatingFileHandler(formatter=rotating_file_formatter)
    print(rotating_file_handler.maxBytes)

    rotating_file_logger = logging.getLogger()
    rotating_file_logger.addHandler(rotating_file_handler)
    # logging.basicConfig()
    print(rotating_file_logger.handlers)
    rotating_file_logger.setLevel(logging.DEBUG)


class HTTPCustomFormatter(CustomFormatter):
    def __init__(
            self, *,
            fmt: str = "{name}:{levelname}:{asctime} | {filename}:{lineno} | {message}",
            datefmt: str = "%Y-%m-%dT%H:%M:%S.%s",
            style: StyleType = "{"):
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)
        self.default_msec_format = "%s,%03d"


class HTTPCustomHandler(Handler):
    def __init__(
            self,
            level: LevelType = logging.DEBUG,
            filename: Union[str, Path, None] = None,
            stream: TextIO = None,
            formatter: Formatter = None,
            request: Request = Request()):
        if bool(filename is None) ^ bool(stream is None):
            raise ValueError("Both filename and stream cannot be None or specified at the same time.")

        if stream is None:
            CustomRotatingFileHandler(filename=filename)

        if filename is None:


        super().__init__(level)
        self.request = request


def get_http_handler(
        filename: Union[str, Path, None] = None,
        stream: TextIO = None,
        formatter: Formatter = None):
    if bool(filename is None) ^ bool(stream is None):
        raise ValueError("Both filename and stream cannot be None or specified at the same time.")

    if stream is None:
        return CustomRotatingFileHandler(filename=filename, formatter=formatter)

    if filename is None:
        return StreamHandler(stream)




def main():
    rotating_file_formatter = CustomFormatter()
    rotating_file_handler = CustomRotatingFileHandler(formatter=rotating_file_formatter)
    rotating_file_logger = logging.getLogger()
    rotating_file_logger.addHandler(rotating_file_handler)
    rotating_file_logger.setLevel(logging.DEBUG)

    logging.debug("test")
    logging.info("test")
    logging.warning("test")
    logging.error("test")
    logging.critical("test")



# class CustomFormatter(Formatter):
#     def __init__(
#             self,
#             fmt_values: FormatterValues = FormatterValues.custom_formatter()):
#         super().__init__(fmt_values.fmt, fmt_values.datefmt, fmt_values.style)
#
#     def __repr__(self):
#         return f"<CustomFormatter({self._fmt}, {self.datefmt}, {self._style})>"
#
#     def __format__(self, format_spec):
#         if format_spec == "output":
#             return f"CustomFormatter(fmt={self._fmt}, datefmt={self.datefmt}, style={self._style})"
#
#     def bound_arg(self, arg: str) -> str:
#         if self._style == "s":
#             return "".join(("%(", arg, ")s"))
#
#         if self._style == "%":
#             return "".join(("$", arg))
#
#         if self._style == "{":
#             return "".join(("{", arg, "}"))
#
#     def custom_fmt(self, separators: Sequence[str], *args) -> str:
#         proper_args: list[str] = [arg for arg in args if arg in FORMATTING_ATTRS]
#         num_args: int = len(proper_args)
#
#         try:
#             separators_to_string: Sequence[str] = separators[:-1]
#             zip_args_separators: list[str] = [
#                 f"{self.bound_arg(arg)}{separator}"
#                 for arg, separator in zip(args[:num_args - 1], separators_to_string)]
#             zip_args_separators.append(separators[-1])
#             custom_format: str = "".join(zip_args_separators)
#         except OSError as e:
#             raise OSError(f"{e.filename}, {e.errno}, {e.strerror}")
#         else:
#             return custom_format


# class CustomRotatingFileHandler(RotatingFileHandler):
#     @classmethod
#     def file_name(cls):
#         return ".".join((super().__class__.__name__, cls.__name__, ".log"))
#
#     def __init__(
#             self, *,
#             filename: Union[str, Path, None] = None,
#             mode: Literal["r", "rb", "w", "wb", "a", "r+", "w+"] = "a",
#             max_bytes: int = 2 ** 24,
#             backup_count: int = 10,
#             formatter: Union[CustomFormatter, Formatter] = CustomFormatter(),
#             level: LevelType = "INFO",
#             encoding_utf8: bool = True):
#         if filename is None:
#             filename = self.__class__.file_name()
#
#         if level == "NOTSET":
#             level = "INFO"
#
#         if not Path(filename).exists():
#             Path(filename).touch()
#
#         super().__init__(filename, mode, max_bytes, backup_count)
#         self.setFormatter(formatter)
#         self.setLevel(level)
#
#         if encoding_utf8:
#             self.encoding = "utf-8"
#
#     def __repr__(self):
#         return f"<CustomRotatingFileHandler({self.name}, {self.mode}, {self.maxBytes}, {self.backupCount}, " \
#                f"{self.encoding})>"
#
#     def __format__(self, format_spec):
#         if format_spec == "output":
#             if self.encoding:
#                 encoding_string = f", encoding={self.encoding};"
#             else:
#                 encoding_string = ";"
#             return f"CustomRotatingFileHandler(name={self.name}, mode={self.mode}, maxBytes={self.maxBytes}, " \
#                    f"backupCount={self.backupCount}{encoding_string})"
#
#
# class CustomStreamHandler(StreamHandler):
#     def __init__(
#             self, *,
#             stream: _StreamT = None,
#             formatter: Union[Formatter, CustomFormatter] = CustomFormatter(),
#             level: LevelType = "DEBUG"):
#         if stream is None:
#             stream = sys.stdout
#
#         self.stream: TextIO = stream
#         super().__init__(stream)
#         self.setFormatter(formatter)
#         self.setLevel(level)
#
#     def __repr__(self):
#         return f"<CustomStreamHandler({self.stream.name}, {self.formatter}, {self.level})>"
#
#
# class CustomLoggerClass(Logger):
#     @property
#     def root(self):
#         return logging.getLogger()
#
#     def name(self):
#         return ".".join((self.root.name, self.__class__.__name__))
#
#
# class CustomLogger(CustomLoggerClass):
#     def __init__(
#             self, *,
#             level: LevelType,
#             handlers: Sequence[Union[Handler, CustomRotatingFileHandler]],
#             formatter: Union[Formatter, CustomFormatter]):
#         name = ".".join((self.root.name, self.__class__.__name__))
#         super().__init__(name)
#
#         for handler in handlers:
#             handler.setFormatter(formatter)
#             self.addHandler(handler)
#         self.setLevel(level)
#
#     # @property
#     # def name(self):
#     #     return ".".join((self.root.name, self.__class__.__name__))
#
#     def add_adapter(self, adapter: Optional[LoggerAdapter]):
#         if adapter is not None:
#             adapter.logger = self
#
#     def __repr__(self):
#         return f"CustomLogger(name={self.name}, root={self.root})"
#
#
# class CustomFileLogger(CustomLogger):
#     def __init__(
#             self, *,
#             level: LevelType = logging.INFO,
#             handlers: Sequence[Handler],
#             formatter: Union[Formatter, CustomFormatter]):
#         super().__init__(level=level, handlers=handlers, formatter=formatter)
#
#     @property
#     def parent(self) -> Logger:
#         return logging.getLogger()
#
#
# class CustomLoggerAdapter(LoggerAdapter):
#     def __init__(
#             self,
#             logger: Union[Logger, RootLogger, CustomFileLogger],
#             # extra: Mapping[str, Any] = None,
#             **kwargs):
#         super().__init__(logger=logger, extra=kwargs)
#
#     def process(self, msg: Any, kwargs: MutableMapping[str, Any]) -> tuple[Any, MutableMapping[str, Any]]:
#         return f"[{kwargs.values()}] {msg}", kwargs
#
#
# class CustomStreamLogger(CustomLogger):
#     def __init__(
#             self, *,
#             level: LevelType = logging.WARNING,
#             handlers: Sequence[Handler] = CustomStreamHandler(),
#             formatter: Union[Formatter, CustomFormatter] = CustomFormatter()):
#         super().__init__(level=level, handlers=handlers, formatter=formatter)
#
#     @property
#     def parent(self) -> Logger:
#         return logging.getLogger()
#
#
# class HTTPCustomFormatter(CustomFormatter):
#     def __init__(self, formatter_values: FormatterValues = FormatterValues.http_formatter()):
#         super().__init__(formatter_values)
#
#
# class HTTPCustomLogger(CustomLogger):
#     def __init__(
#             self, *,
#             level: LevelType,
#             handlers: Sequence[Union[Handler, CustomRotatingFileHandler]],
#             formatter: Union[Formatter, CustomFormatter],
#             request: Optional[Request] = None):
#         super().__init__(level=level, handlers=handlers, formatter=formatter)
#         self.request = request
#
#
# class HTTPCustomLoggerAdapter(CustomLoggerAdapter):
#     attrs = (
#         "url", "cipher", "source", "destination", "session_id", "request_status", "socket_protocol", "alpn_protocol",
#         "headers", "response_text")
#
#     def __init__(
#             self,
#             logger: Union[Logger, RootLogger, HTTPCustomLogger]):
#         super().__init__(logger)
#         self.logger: Union[Logger, RootLogger, CustomFileLogger] = logger
#         if isinstance(logger, HTTPCustomLogger):
#             self.request: Optional[Request] = logger.request
#         else:
#             self.request = None
#         self.extra: dict[str, Any] = dict()
#
#     def __getitem__(self, item):
#         return self.extra.__getitem__(item)
#
#     def __setitem__(self, key, value):
#         self.extra.__setitem__(key, value)
#
#     @property
#     def response(self) -> Response:
#         method: str = self.request.method
#         headers: dict = self.request.headers
#         params: Union[dict, Sequence[tuple], None] = self.request.params
#         url: str = self.request.url
#         cookies: Optional[RequestsCookieJar] = self.request.cookies
#         print(requests.request(method=method, url=url, params=params, headers=headers, cookies=cookies, stream=True))
#         return requests.request(method=method, url=url, params=params, headers=headers, cookies=cookies, stream=True)
#
#     @property
#     def response_raw(self) -> HTTPResponse:
#         print(self.response.raw)
#         return self.response.raw
#
#     @property
#     def connection(self) -> HTTPConnection:
#         print(self.response.raw.connection)
#         return self.response.raw.connection
#
#     def socket(self) -> SSLSocket:
#         print(self.response.raw.connection.sock)
#         return self.response.raw.connection.sock
#
#     def alpn_protocol(self) -> str:
#         protocol_attrs: list[str] = self.socket().selected_alpn_protocol().split("/")
#         return f"ALPN protocol: {protocol_attrs[0].upper()} v.{protocol_attrs[1]}"
#
#     def raw_headers(self) -> HTTPHeaderDict[str, str]:
#         return self.response_raw.headers
#
#     def headers(self):
#         prologue, epilogue = "\n-----HTTP Headers-----", "-----HTTP Headers-----"
#         text = ["\n ".join((f"{k}", *v.split(";"))) for k, v in self.raw_headers().items()]
#         return "\n".join((prologue, *text, epilogue))
#
#     def socket_protocol(self) -> str:
#         return f"Socket protocol: {self.socket().version}, SSLv.{self.response_raw.version}"
#
#     def request_status(self) -> str:
#         return f"HTTP status: {self.response_raw.status} {self.response_raw.reason}"
#
#     def session_id(self):
#         return f"Session ID: {self.socket().session.id.hex()}"
#
#     def destination(self):
#         return f"Destination | IP address: {self.socket().getpeername()[0]}, DNS name: {self.connection.host}, " \
#                f"port: {self.socket().getpeername()[1]}"
#
#     def source(self) -> str:
#         return f"Source | IP address: {self.socket().getsockname()[0]}, port: {self.socket().getsockname()[1]}"
#
#     def cipher(self) -> str:
#         print(self.socket().cipher())
#         return f"Cipher: {self.socket().cipher()[0]}, key: {self.socket().cipher()[2]} bits"
#
#     def url(self) -> str:
#         return f"URL: {self.response_raw.geturl()}"
#
#     def response_text(self) -> str:
#         return self.response.text
#
#     def full_extra(self):
#         # connection
#         print(self.extra)
#         self.extra["url"] = self.url()
#         # self.extra["source"] = self.source()
#         print(self.extra)
#         self.extra["destination"] = self.destination()
#         self.extra["session_id"] = self.session_id()
#         print(self.extra)
#         self.extra["request_status"] = self.request_status()
#         self.extra["socket_protocol"] = self.socket_protocol()
#         print(self.extra)
#         self.extra["alpn_protocol"] = self.alpn_protocol()
#         self.extra["headers"] = self.headers()
#         print(self.extra)
#         self.extra["response_text"] = self.response_text()
#         self.extra["cipher"] = self.cipher()
#
#     def process(self, msg: Any, kwargs: MutableMapping[str, Any] = None) -> tuple[Any, MutableMapping[str, Any]]:
#         return f"[{[self.extra[item] for item in self.__class__.attrs]}], {msg}", self.extra
#

# def main():
#     request: Request = Request(method="GET", url="https://8.8.8.8", headers={"Content-Type": "text/plain"})
#     http_formatter: HTTPCustomFormatter = HTTPCustomFormatter()
#     http_handler: CustomStreamHandler = CustomStreamHandler(stream=sys.stdout, formatter=http_formatter, level="DEBUG")
#     http_logger: HTTPCustomLogger = HTTPCustomLogger(
#         level="DEBUG", handlers=(http_handler,), formatter=http_formatter, request=request)
#
#     http_adapter = HTTPCustomLoggerAdapter(http_logger)
#     print(http_adapter.request)
#     print(http_adapter.response_text())
#     print(http_adapter.extra)
#     # http_adapter.full_extra()
#     http_logger.add_adapter(http_adapter)
#
#     logging.debug("test")


if __name__ == '__main__':
    main()
