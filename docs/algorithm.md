# Основной алгоритм работы #

## Объекты ##

1. `main.py`:
* из файла `youtrack.json` получить `MultipleConfig(path: Path, json_file: dict[str, Union[list[str], str]])`;
2. `yt_requests.py`:
* из `MultipleConfig()` получить `list[User]`, `User(login: str)`;
* из `User()` получить:
  * `list[Issue]`, `Issue(issue: str, state: str, summary: str, parent: Optional[str] = None, deadline: datetime64[D] = datetime64("NaT"))`;
  * `list[IssueWorkItem]`, `IssueWorkItem(issue: str, date: datetime64[D], spent_time: Decimal("1.00"))`;
* объединить `list[IssueWorkItem]`, у которых совпадают `IssueWorkItem.issue`, `IssueWorkItem.date` (несколько записей затраченного времени на одну и ту же задачу в один и тот же день);
3. `xl_style.py`:
* из `STYLES_CONST` получить `list[_Style]`, `_Style(name: str, is_named: bool, number_format: str, alignment: Alignment, border: Border, fill: PatternFill, font: Font, protection: Protection, data_type: str = "n")`;
* из `list[_Style()]` создать `_StyleList()`;
4. `xl_table.py`:
* из `MultipleConfig()` и `_StyleList()` получить `list[ExcelProp]`, `ExcelProp(wb: Workbook, styles: _StyleList, login: str = None)`;
* из ``
