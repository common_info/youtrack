# Файл `youtrack.json` #

## Назначение ##
Файл является конфигурационным и хранит базовые параметры для запуска программы.

## Структура ##

### Формат ###

```json
{
    "login": [
        "<login 1>", "<login N>"
    ],
    "path_dir": "<path to directory>"
}
```

### Пример ###

```json
{
  "login": [
    "bochkova", "brovko", "fesenko", "kuksina", "lyalina", "matyushina", "mazyarova", "nigrej", "vykhodtsev"
  ],
  "path_dir": "/Users/user/Desktop/Auto reports"
}
```

### Описание параметров ###

| Параметр | Тип | Описание | Комментарий |
| :------- | :-- | :------- | :---------- |
| login | list | Список логинов на YouTrack. | Логин и фамилия могут не совпадать. |
| path_dir | string/Path | Путь до директории с файлами `*.xlsx`. | Путь может быть как абсолютным, так и относительным. |

#### Примечания ####

1. Хотя обычно логин пользователя совпадает с фамилией, транскрибированной на английский язык, это правило соблюдается не всегда. В случае указания некорректного логина он будет пропущен.
2. Путь может задаваться в любом виде, который определяет:
  * существующий объект файловой системы;
  * объект, являющийся директорией;
  * в директории находятся Excel-файлы `*.xlsx`.
Путь автоматически преобразуется в абсолютный и используется именно он.

## Использование ##

Файл является ключевым, поскольку определяет объект MultipleConfig (`yt_requests.py`).
