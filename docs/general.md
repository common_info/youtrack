# Общая информация #

## Основные файлы ##

* `youtrack.json` --- конфигурационный файл, который:
  * задает логины для отправки запросов;
  * задает директорию с отчетами;
* `const_values.py` --- скрипт, который:
  * хранит константы;
  * определяет значения, которые после разового вычисления заносятся в память для ускорения работы;
* `yt_requests.py` --- скрипт, который:
  * обрабатывает файл конфигурации;
  * преобразует в объект `MultiConfig`, являющийся корневым для остальных объектов;
  * определяет пользователей в виде объектов `User`, для которых собирается информация;
  * отправляет запросы на YouTrack;
  * принимает ответы от YouTrack;
  * преобразует ответы в объекты `Issue` и `IssueWorkItem`;
* `style_work_item.py` --- скрипт, который:
  * задает стили для ячеек Excel-файлов `_Style`;
  * задает список стилей `_StyleList` для применения к ячейкам Excel-файлов;
* `xl_table` --- скрипт, который:
  * задает рабочую книгу `Worksheet` и рабочий лист `Worksheet` для добавления и изменения информации с помощью объекта `ExcelProp`;
  * обрабатывает текущий лист;
  * преобразует информацию из таблицы в объекты `TableRow`;
  * обновляет данные в таблице;
  * применяет стили к ячейкам таблицы;
  * добавляет и удаляет строки;
* `join_user_xl.py` --- скрипт, который:
  * объединяет объекты `User` и `ExcelProp`;
  * задает конвертацию `Issue` и `IssueWorkItem` в `TableRow` и обратно;
  * добавляет новые задачи в таблицу;
  * обновляет статусы уже внесенных в таблицу задач;
  * добавляет затраченное время в таблицу;
  * применяет стили к ячейкам;
* `main.py` --- скрипт, который:
  * задает основной алгоритм работы;
  * объединяет другие скрипты в один файл;
  * записывает новую информацию в Excel-файлы;
  * сохраняет Excel-файлы;
  * записывает информацию из YouTrack в текстовый файл в удобочитаемом виде;
  * выступает в роли точки входа и выхода из программы.

## Используемые типы ##

| Тип | Модуль | Описание |
| :-- | :----- | :------- |
| string | sys | Строка |
| int | sys | Целое число |
| datetime64[X] | numpy | Дата/время<br>X=D --- день<br>X=ms --- миллисекунды |
| Path | pathlib | Путь (абослютный или относительный) |
| Decimal("X") | decimal | Число двойной точности<br>X=1.00 --- число с 2 знаками после запятой<br>X=1 --- целое число |

### Формат даты ###

Формат даты по умолчанию --- ISO, `YYYY-MM-DD`.

| Параметр | Описание | Количество цифр |
| :------- | :------- | :-------------- |
| YYYY | Год | 4 цифры |
| MM | Месяц | 2 цифры |
| DD | День | 2 цифры |

### Формат времени ###

Формат времени по умолчанию --- `hh:mm:ss.mssmss`.

| Параметр | Описание | Количество цифр |
| :------- | :------- | :-------------- |
| hh | Час | 2 цифры |
| mm | Минута | 2 цифры |
| ss | Секунда | 2 цифры |
| mssmss | Миллисекунда | 6 цифр |

### Формат длительности ###

| Параметр | Описание | Тип |
| :------- | :------- | :-------------- |
| duration h | Длительность в часах | Decimal("1.00") |
| duration m | Минута | int |

## Вывод программы ##

### Структура вывода ###

```
----------
Пользователь: <user name>
Задачи:
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
----------
Затраченное время по задачам:
Задача: <issue>
Дата: <date ISO>, затраченное время: <duration h> ч, <duration m> мин;
----------
Затраченное время по датам:
Дата: <date ISO>
Задача: <issue>, затраченное время: <duration h> ч, <duration m> мин;
----------
Не закрытые задачи:
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
----------
Задачи с истекшим крайним сроком (дедлайном):
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
----------
Задачи добавлены.
Параметры задач (название, родительская задача, дедлайн) обновлены.
Статусы задач обновлены.
Затраченное время добавлено.
Пост-обработка завершена.
Работа завершена. Путь до файла:
<path dir>/generated_<year>_<login>_report.xlsx
----------

Время работы: <time>
Нажмите любую клавишу, чтобы закрыть окно...
```

### Описание значений ###

| Параметр | Тип | Описание | По умолчанию | Комментарий |
| :------- | :-- | :------- | :----------- | :---------- |
| user name | string | Фамилия пользователя | - | Связан с login |
| issue | string | Название задачи | - | `Issue.idReadable` |
| state | string | Состояние задачи | - | `Issue.customFields.State` |
| summary | string | Заголовок задачи | - | `Issue.summary` |
| parent | string | Название родительской задачи | None | `Issue.parent.idReadable` |
| deadline date | datetime64[D] | Дата крайнего срока | datetime64("NaT") | `Issue.customFields.Дедлайн` |
| date ISO | datetime64[D] | Дата выполнения работы | - | `IssueWorkItem.date` |
| duration h | Decimal("1.00") | Длительность работы в часах | - | `IssueWorkItem.duration` |
| duration m | int | Длительность работы в часах | - | `IssueWorkItem.duration` |
| path dir | Path | Путь до директории с файлами | `youtrack.json[path_dir]` | Используется значение из конфигурации |
| year | int | Текущий год | datetime64("today").item().year | - |
| login | string | Логин пользователя | - | Связан с user_name |
| time | datetime64[ms] | Время работы программы | - | - |

### Структура файла с данными ###

Название файла --- `data_\<login\>.txt`.

В него записывается вся информация с YouTrack в виде текстовой информацией. Содержит следующие части:
* задачи в текущем году;
* затраченное время, разбитое по задачам;
* затраченное время, разбитое по дате выполнения работ;
* текущие задачи, не отмеченные как закрытые или завершенные;
* текущие задачи, не отмеченные как закрытые или завершенные, у которых прошел крайний срок выполнения (дедлайн);

```
----------
Пользователь: <user name>
Задачи:
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
----------
Затраченное время по задачам:
Задача: <issue>
Дата: <date ISO>, затраченное время: <duration h> ч, <duration m> мин;
----------
Затраченное время по датам:
Дата: <date ISO>
Задача: <issue>, затраченное время: <duration h> ч, <duration m> мин;
----------
Не закрытые задачи:
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
----------
Задачи с истекшим крайним сроком (дедлайном):
Задача: <issue>, статус: <state>, описание: <summary>;
Задача: <issue>, статус: <state>, описание: <summary>, родительская задача: <parent>;
Задача: <issue>, статус: <state>, описание: <summary>, дедлайн: <deadline date>;
Задача: <issue, статус: <state>, описание: <summary>, родительская задача: <parent>, дедлайн: <deadline date>;
```

## Архитектура объектов ##

![arch.png](./images/YT_report.png)
