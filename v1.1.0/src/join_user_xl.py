#!/usr/bin/python
# -*- coding: cp1251 -*-
from typing import Any, Optional, Union
from numpy import datetime64
from openpyxl.cell.cell import Cell
from openpyxl.worksheet.worksheet import Worksheet
from yt_requests import User, Issue, IssueWorkItem, get_current_states
from xl_table import ExcelProp, TableRow
from const_values import DICT_STATES, DICT_ATTR_COLUMN, DICT_HEADERS_SHORT


def convert_issue_state(state: str) -> str:
    """
    Convert the state to the table headers.

    :param str state: the issue state
    :return: the modified state.
    :rtype: str
    """
    if state in DICT_STATES:
        return DICT_STATES[state]
    else:
        print(f"Не ожидаемое состояние {state}.")
        return state


def _index(state: str) -> int:
    """
    Get the state index.

    :param str state: the state
    :return: the header index.
    :rtype: int
    """
    return DICT_HEADERS_SHORT[state]


class JoinUserXL:
    """
    Join the User and ExcelProp instances.

    Class params:
        dict_attr_column --- the dictionary of the issue attributes and columns;\n

    Params:
        user --- the User instance;\n
        excel_prop --- the ExcelProp instance;\n

    Properties:
        ws --- the worksheet;\n
        issues_to_add --- the issue names to add to the table;\n
        issues_to_modify --- the issue names to modify;\n
        issues_to_update_state --- the issue names to update the state;\n
        work_item_names --- the issue work item names;\n

    Functions:
        _get_issue_by_name(issue_name) --- get the Issue and TableRow instances by the issue name;\n
        _parse_attr_seq(attrs, values, row) --- add the attribute values to the table;\n
        add_all_issues() --- add all issues to the table;\n
        add_new_work_items() --- add the new work items;\n
        modify_all_issues() --- modify all attribute values in the table;\n
        modify_table_cell(issue_name, state) --- modify the table cell state;\n
        new_row(state) --- get the new issue row in the table;\n
        table_cell_item(table_base) ---get the TableRow instance based on the row, cell, or issue name;\n
        update_states() --- update the issue states in the table;\n
    """

    def __init__(self, user: User, excel_prop: ExcelProp):
        self.user = user
        self.excel_prop = excel_prop

    def __str__(self):
        return f"User: {self.user.login}, Excel table: {str(self.excel_prop)}"

    def __repr__(self):
        return f"JoinUserXL({repr(self.user)}, {repr(self.excel_prop)})"

    def __hash__(self):
        return hash((self.user.login, self.excel_prop.ws.title))

    def __key(self):
        return self.user, self.excel_prop

    def __eq__(self, other):
        if isinstance(other, JoinUserXL):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, JoinUserXL):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __len__(self):
        return min(len(self.user.issue_names()), len(self.excel_prop.table_cells))

    def __lt__(self, other):
        if isinstance(other, JoinUserXL):
            return self.user.login < other.user.login
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, JoinUserXL):
            return self.user.login > other.user.login
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, JoinUserXL):
            return self.user.login <= other.user.login
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, JoinUserXL):
            return self.user.login >= other.user.login
        else:
            return NotImplemented

    @property
    def ws(self) -> Worksheet:
        """
        Get the worksheet.

        :return: the worksheet.
        :rtype: Worksheet
        """
        return self.excel_prop.ws

    @property
    def issues_to_add(self) -> list[str]:
        """
        Specify the issue names to add to the table.

        :return: the issue names.
        :rtype: list[str]
        """
        return [issue for issue in self.user.issue_names().difference(self.excel_prop.table_row_names)
                if issue is not None]

    @property
    def issues_to_modify(self) -> list[str]:
        """
        Specify the issue names to modify.

        :return: the issue names.
        :rtype: list[str]
        """
        return [issue for issue in set.intersection(self.user.issue_names(), self.excel_prop.table_row_names)
                if issue is not None]

    @property
    def issues_to_update_state(self) -> list[str]:
        """
        Specify the issue names to update the state.

        :return: the issue names.
        :rtype: list[str]
        """
        return [issue for issue in self.excel_prop.table_row_names.difference(self.user.issue_names())
                if issue is not None]

    @property
    def work_item_names(self) -> list[str]:
        """
        Get the IssueWorkItem instance names.

        :return: the issue work item names.
        :rtype: list[str]
        """
        return list(self.user.dict_issue_work_item)

    def _get_issue_by_name(self, issue_name: str) -> tuple[Issue, TableRow]:
        """
        Get the Issue and TableRow instances by the issue name.

        :param str issue_name: the issue name
        :return: the Issue and TableRow instances.
        :rtype: tuple[Issue, TableRow]
        """
        return self.user.dict_issue[issue_name], self.table_row_item(issue_name)

    def _parse_attr_seq(self, attrs: list[str], values: list[Any], row: int):
        """
        Add the attribute values to the table.

        :param attrs: the attribute names
        :type attrs: list[str]
        :param values: the attribute values
        :type values: list[Any]
        :param int row: the row in the table
        :return: None.
        """
        if len(attrs) != len(values):
            print("Improper lengths.")
            return
        for attr, value in zip(attrs, values):
            if attr not in DICT_ATTR_COLUMN:
                print(f"improper attr: {attr}")
                continue
            else:
                column_letter: str = DICT_ATTR_COLUMN[attr]
                cell: Cell = self.ws[f"{column_letter}{row}"]
                if attr == "deadline":
                    if value and isinstance(value, datetime64):
                        cell.value = value.item()
                    else:
                        cell.value = None
                    self.excel_prop.styles.set_style("deadline_issue", cell)
                    continue
                else:
                    cell.value = value
                    self.excel_prop.styles.set_style("basic_issue", cell)
                    continue
        return

    def add_all_issues(self):
        """Add all new issues."""
        for issue_name in self.issues_to_add:
            # prepare the new row
            issue: Issue = self.user.dict_issue[issue_name]
            add_row: int = self.new_row(issue.state)
            # add the new row
            self.excel_prop.insert_row(add_row)
            # add the values to the associated cells
            attrs = ["parent", "issue", "summary", "deadline"]
            values = [getattr(issue, attr) for attr in attrs]
            self._parse_attr_seq(attrs, values, add_row)
        return

    def add_new_work_items(self):
        """Add the new work items."""
        for issue_name in self.work_item_names:
            table_row: TableRow = self.table_row_item(issue_name)
            # get the work items from the YouTrack
            work_items_issue: list[IssueWorkItem] = self.user.dict_issue_work_item[issue_name]
            # get the work item dates from the YouTrack
            work_item_dates: set[datetime64] = set(work_item.date for work_item in work_items_issue)
            # get the work item dates from the table
            table_row_dates: set[datetime64] = set(date for _, date, _ in table_row.work_items())
            # get the work item dates to add
            new_work_items_dates: set[datetime64] = work_item_dates.difference(table_row_dates)
            # get the new work items
            new_work_items: list[IssueWorkItem] = [
                work_item for work_item in work_items_issue if work_item.date in new_work_items_dates]
            # add the new work items to the table
            for work_item in new_work_items:
                table_row.add_work_item((work_item.issue, work_item.date, work_item.spent_time))
        return

    def modify_all_issues(self):
        """Modify all issues in the table."""
        for issue_name in self.issues_to_modify:
            issue, table_cell = self._get_issue_by_name(issue_name)
            row = table_cell.row
            # get the attributes to modify
            modify_attrs: list[str] = [
                attr for attr in ("parent", "summary", "deadline") if getattr(table_cell, attr) != getattr(issue, attr)]
            # get the values to apply
            modify_values = [getattr(issue, attr) for attr in modify_attrs]
            # add the values to the table
            self._parse_attr_seq(modify_attrs, modify_values, row)
            # operate with the state changes separately
            if convert_issue_state(issue.state) != table_cell.state:
                self.modify_table_cell(issue.issue, issue.state)
        return

    def modify_table_cell(self, issue_name: str, state: str):
        """
        Modify the table cell state.

        :param str issue_name: the issue name
        :param str state: the issue state
        :return: None.
        """
        table_cell: TableRow = self.table_row_item(issue_name)
        # the new row number
        add_row: int = self.new_row(state)
        # add the new row
        self.excel_prop.insert_row(add_row)
        # the current row number
        row: int = table_cell.row
        # replace the work item cells
        for cell in table_cell.pyxl_cells():
            self.excel_prop.replace_cell(from_=cell, to_=self.ws[f"{cell.column_letter}{add_row}"])

        # replace the issue attribute values
        table_cell.replace_issue(add_row)
        # delete the previous row
        self.excel_prop.delete_row(row)
        return

    def new_row(self, state: str) -> int:
        """
        Get the row for the new issue in the table.

        :param str state: the issue state
        :return: the row number.
        :rtype: int
        """
        return self.excel_prop.headers_row[_index(convert_issue_state(state)) + 1] - 1

    def table_row_item(self, table_base: Union[int, Cell, str]) -> Optional[TableRow]:
        """
        Get the TableRow instance based on the row, cell, or issue name.

        :param table_base: the object to link to the TableRow instance
        :type table_base: int or Cell or str
        :return: the TableRow instance.
        :rtype: TableRow or None
        """
        if isinstance(table_base, int):
            cell = self.ws[f"C{table_base}"]
            return TableRow(self.excel_prop, cell)
        elif isinstance(table_base, str):
            return self.excel_prop.dict_table_rows[table_base]
        elif isinstance(table_base, Cell):
            return TableRow(self.excel_prop, table_base)
        else:
            print(f"Something went wrong. Table base: {table_base}, type: {type(table_base)}.")
            return None

    def update_states(self):
        """Update the issue states in the table."""
        if not self.issues_to_update_state:
            return
        dict_updated_states: dict[str, str] = get_current_states(self.issues_to_update_state)
        for issue_name, state in dict_updated_states.items():
            if self.table_row_item(issue_name).state != state:
                self.modify_table_cell(issue_name, state)
        return
