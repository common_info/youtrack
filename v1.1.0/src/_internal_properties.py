#!/usr/bin/python
# -*- coding: cp1251 -*-
import weakref

from numpy.core import datetime64
from yt_requests import MultipleConfig, User, Issue, IssueWorkItem, multiple_config
from xl_table import _StyleList


class _GlobalProperties:
    is_exists = False

    def __init__(self):
        if not _GlobalProperties.is_exists:
            self.version: str = "1.1.1"
            self.multi_config: MultipleConfig = multiple_config()
            self.styles = _StyleList
            _GlobalProperties.is_exists = True
        else:
            print("_GlobalProperties already exists.")

    def __bool__(self):
        return True


class _InternalProperties(_GlobalProperties):
    def __init__(self, login: str):
        super().__init__()
        self.timestamp = datetime64("now", "s")
        self.login = login

    def __repr__(self):
        return f"<_InterrnalProperties(version={self.version}, user={self.login})>"

    def __str__(self):
        return f"version = {self.version}, timestamp = {self.timestamp.item().isoformat()}, user = {str(self.login)}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"Версия {self.version}, время: {self.timestamp.item().isoformat()}"
        if format_spec == "internal":
            return f"_InternalProperties: version {self.version}, timestamp: {self.timestamp.item().isoformat()}"

    # def __getattribute__(self, item):
    #     return object.__getattribute__(self.)



def main():
    global_properties = _GlobalProperties()
    print(weakref.ref(global_properties)().version)
    print(_GlobalProperties.__dict__)
    print(global_properties)
    # print(global_properties.is_exists)
    internal = _InternalProperties("test")
    # print(internal.__get__(global_properties, _GlobalProperties))
    print(internal.is_exists)


if __name__ == "__main__":
    main()
