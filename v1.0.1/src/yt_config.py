import datetime
import json
from functools import cached_property
from pathlib import Path
from json.decoder import JSONDecodeError
import os
from typing import Optional, Union, Any, NamedTuple


REPLACE_LOGIN: dict[str, str] = {"mozglyakova": "matyushina", "AndrewTarasov": "tarasov-a"}


def sys_login() -> str:
    """
    Get the system user from the OS environment.

    :return: the name of the user.
    :rtype: str
    """
    if os.uname().sysname == "Windows":
        key = "USERNAME"
    else:
        key = "USER"
    sys_name = os.environ[key]
    return sys_name if sys_name not in REPLACE_LOGIN else REPLACE_LOGIN[sys_name]


def replace_login(login: str) -> Optional[str]:
    """
    Replace the login if in the special dict.

    :param str login: the login to replace
    :return: the replaced login if necessary.
    :rtype: str
    """
    if login:
        return REPLACE_LOGIN[login] if login in REPLACE_LOGIN else login
    else:
        return dict_attrs["login"]


TODAY: datetime.date = datetime.date.today()
LOGIN: str = replace_login(sys_login())
AUTH_TOKEN: str = "perm:dGFyYXNvdi1h.NjEtMTUw.gRyarbzX5ieIgKEfgRalpYj1DZ8vA5"
PERIOD_START: datetime.date = datetime.date(TODAY.year, 1, 1)
PERIOD_END: datetime.date = TODAY
PATH_TABLE: Path = Path.cwd().with_name(f"2022_report_{LOGIN}.xlsx")

dict_attrs = {
    "login": LOGIN,
    "auth_token": AUTH_TOKEN,
    "period_start": PERIOD_START,
    "period_end": PERIOD_END,
    "path_table": PATH_TABLE
}


class UserConfig(NamedTuple):
    """
    Specify the user config instance.

    Params:
        login --- the YouTrack login;\n
        auth_token --- the authentication permanent token;\n
        period_start --- the first day of the period;\n
        period_end --- the last day of the period;\n
        path_table --- the path to the Excel report;\n
    """
    login: str = LOGIN
    auth_token: str = AUTH_TOKEN
    period_start: datetime.date = PERIOD_START
    period_end: datetime.date = PERIOD_END
    path_table: Path = PATH_TABLE

    def __str__(self):
        return f"UserConfig: login = {self.login}, auth_token = {self.auth_token}, " \
               f"period_start = {self.period_start}, period_end = {self.period_end}, path_table = {self.path_table}"

    def __repr__(self):
        return f"<UserConfig(login={self.login}, auth_token={self.auth_token}, period_start={self.period_start}, " \
               f"period_end={self.period_end}, path_table={self.path_table})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"UserConfig: path = {self.path_table}"
        if format_spec == "login":
            return f"UserConfig: login = {self.login}"
        if format_spec == "attrs":
            return ", ".join(
                [f"{attr} = {getattr(self, attr)}" for attr in
                 ("login", "auth_token", "period_start", "period_end", "path_table")])


class MultipleConfig:
    """
    Specify the config with several logins.

    Class params:
        attrs --- the configuration file attributes;\n

    Params:
        path --- the path to the configuration file;\n
        dict_attr --- the dictionary of the configuration file attributes;\n

    Properties:
        _logins --- the list of logins;\n
        _auth_token --- the authentication permanent token;\n
        _period_start --- the start of the period;\n
        _period_end --- the end of the period;\n

    Functions:
        _path_table(login) --- get the path to the report;\n
        write_json() --- write the values to the file;\n
        user_config(login) --- specify the user config;\n
        full_user_config() --- specify all user configs;\n
    """

    attrs = ("login", "auth_token", "period_start", "period_end", "path_table", "dict_attr")

    def __init__(self, path: Union[str, Path]):
        self.path = Path(path).resolve()
        self.dict_attr = dict()
        with open(self.path, "r") as file:
            items = json.load(file)
        for key, value in items.items():
            self.dict_attr[key] = value

    def __str__(self):
        return f"MultipleConfig: {str(self.path)}"

    def __repr__(self):
        return f"<MultipleConfig(path={repr(self.path)})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"Config({str(self.path)})"
        elif format_spec == "login":
            return f"Logins: {self._login}"
        elif format_spec == "attrs":
            return ", ".join([f"{attr} = {value}" for attr, value in self.dict_attr.items()])

    def __eq__(self, other):
        if isinstance(other, MultipleConfig):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other: Any):
        if isinstance(other, MultipleConfig):
            return self.path != other.path
        else:
            return NotImplemented

    def __getitem__(self, attr: str):
        if attr in MultipleConfig.attrs:
            return self.dict_attr[attr]
        else:
            print(f"AttributeError, the attribute {attr} is not available.")
            return

    def __setitem__(self, attr: str, value: Any):
        if attr not in MultipleConfig.attrs:
            print(f"KeyError, the attribute {attr} cannot be assigned.")
            return
        else:
            if attr == "login" and isinstance(value, list):
                self.dict_attr[attr] = value
                return
            if not isinstance(value, str):
                print(f"ValueError, the value {value} must be of the str type.")
                return
            self.dict_attr[attr] = value
            return

    def __add__(self, login: str) -> Optional[list[str]]:
        """
        Add the login to the list.

        :param str login: the login to add
        :return: the logins in the dict.
        :rtype: list[str] or None
        """
        if login in self._login:
            print(f"The login {login} is already in the list.")
            return
        else:
            self._login.append(login)
            return self.dict_attr["login"]

    def write_json(self):
        """Write the values to the file."""
        with open(self.path, "w+") as json_file:
            json.dump(self.dict_attr, json_file)
            return

    @cached_property
    def _login(self) -> Optional[list[str]]:
        """
        Get the logins in the dict.

        :return: the logins.
        :rtype: list[str] or None
        """
        return [replace_login(login) for login in self["login"]]

    @cached_property
    def _auth_token(self) -> Optional[str]:
        """
        Get the authentication token.

        :return: the authentication token.
        :rtype: str or None
        """
        return self["auth_token"]

    @cached_property
    def _period_start(self) -> Optional[datetime.date]:
        """
        Get the start of the period.

        :return: the period start.
        :rtype: datetime.date or None
        """
        return datetime.date.fromisoformat(self["period_start"])

    @cached_property
    def _period_end(self) -> Optional[datetime.date]:
        """
        Get the end of the period.

        :return: the period end.
        :rtype: datetime.date or None
        """
        return datetime.date.fromisoformat(self["period_end"])

    def _path_table(self, login: str) -> Optional[Path]:
        """
        Get the path to the report.

        :param str login: the login to get the report path
        :return: the path to the Excel file.
        :rtype: Path or None
        """
        return Path(self["path_table"]).resolve().with_name(f"2022_report_{login}.xlsx")

    def user_config(self, login: str) -> UserConfig:
        """
        Specify the user config.

        :param str login: the login in the YouTrack
        :return: the user config.
        :rtype: UserConfig
        """
        return UserConfig(login, self._auth_token, self._period_start, self._period_end, self._path_table(login))

    def full_user_config(self) -> list[UserConfig]:
        """
        Specify the user configs for all logins.

        :return: the list of user configs.
        :rtype: list[UserConfig]
        """
        return [self.user_config(login) for login in self._login]


class SingleConfig:
    """
    Specify the config with several logins.

    Class params:
        attrs --- the configuration file attributes;\n

    Params:
        path --- the path to the configuration file;\n
        dict_attr --- the dictionary of the configuration file attributes;\n

    Properties:
        _login --- the login;\n
        _auth_token --- the authentication permanent token;\n
        _period_start --- the start of the period;\n
        _period_end --- the end of the period;\n
        _path_table --- the path to the report;\n

    Functions:
        write_json() --- write the values to the file;\n
        user_config() --- specify the user config;\n
    """

    attrs = ("login", "auth_token", "period_start", "period_end", "path_table")

    def __init__(self, path: Union[str, Path]):
        self.path = Path(path).resolve()
        self.dict_attr = dict()
        with open(self.path, "r") as file:
            items = json.load(file)
        for key, value in items.items():
            self.dict_attr[key] = value

    def __str__(self):
        return f"SingleConfig: {str(self.path)}"

    def __repr__(self):
        return f"<SingleConfig(path={repr(self.path)})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"Config({str(self.path)})"
        elif format_spec == "logins":
            return f"Logins: {self._login}"
        elif format_spec == "attrs":
            return ", ".join([f"{attr} = {value}" for attr, value in self.dict_attr.items()])

    def __eq__(self, other):
        if isinstance(other, MultipleConfig):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other: Any):
        if isinstance(other, MultipleConfig):
            return self.path != other.path
        else:
            return NotImplemented

    def __getitem__(self, attr: str):
        if attr in MultipleConfig.attrs:
            return self.dict_attr[attr]
        else:
            print(f"AttributeError, the attribute {attr} is not available.")
            return

    def __setitem__(self, attr: str, value: Any):
        if attr not in MultipleConfig.attrs:
            print(f"KeyError, the attribute {attr} cannot be assigned.")
            return
        else:
            if attr == "login" and isinstance(value, list):
                self.dict_attr[attr] = value
                return
            if not isinstance(value, str):
                print(f"ValueError, the value {value} must be of the str type.")
                return
            self.dict_attr[attr] = value
            return

    def write_json(self):
        """Write the values to the file."""
        with open(self.path, "w+") as json_file:
            json.dump(self.dict_attr, json_file)
            return

    @cached_property
    def _login(self) -> Optional[str]:
        """
        Get the logins in the dict.

        :return: the login.
        :rtype: str or None
        """
        return replace_login(getattr(self, "login"))

    @cached_property
    def _auth_token(self) -> Optional[str]:
        """
        Get the authentication token.

        :return: the authentication token.
        :rtype: str or None
        """
        return getattr(self, "auth_token") if getattr(self, "auth_token") else dict_attrs["auth_token"]

    @cached_property
    def _period_start(self) -> Optional[datetime.date]:
        """
        Get the start of the period.

        :return: the period start.
        :rtype: datetime.date
        """
        if getattr(self, "period_start"):
            return datetime.date.fromisoformat(getattr(self, "period_start"))
        else:
            print(f"The period start is not specified.")
            return dict_attrs["period_start"]

    @cached_property
    def _period_end(self) -> Optional[datetime.date]:
        """
        Get the end of the period.

        :return: the period end.
        :rtype: datetime.date
        """
        if getattr(self, "period_end"):
            return datetime.date.fromisoformat(getattr(self, "period_end"))
        else:
            print(f"The period end is not specified.")
            return dict_attrs["period_end"]

    @cached_property
    def _path_table(self):
        if getattr(self, "path_table"):
            return Path(getattr(self, "period_end")).resolve()
        else:
            print(f"The path is not specified.")
            return dict_attrs["path_table"]

    def user_config(self) -> UserConfig:
        """
        Specify the user config.

        :return: the user config.
        :rtype: UserConfig
        """
        return UserConfig(self._login, self._auth_token, self._period_start, self._period_end, self._path_table)


def type_config() -> Union[None, UserConfig, list[UserConfig]]:
    """
    Get the config type and specify the user config.

    :return: the user configs.
    :rtype: UserConfig or list[UserConfig] or None
    """
    try:
        path = Path.cwd().resolve().joinpath("youtrack.json")
        with open(path, "r") as file:
            config_type = json.load(file)["type"]
    except JSONDecodeError as e:
        print(f"JSONDecodeError, {e.pos}, {e.lineno}, {e.msg}")
        return None
    except FileNotFoundError as e:
        print(f"FileNotFoundError, {e.errno}, {e.strerror}")
        return None
    except RuntimeError as e:
        print(f"RuntimeError, {str(e)}")
        return None
    except ValueError as e:
        print(f"ValueError, {str(e)}")
        return None
    except KeyError as e:
        print(f"KeyError, {str(e)}")
        return None
    else:
        if config_type == "single":
            return SingleConfig(path).user_config()
        elif config_type == "multiple":
            return MultipleConfig(path).full_user_config()
        elif not config_type:
            return UserConfig()
        else:
            return
