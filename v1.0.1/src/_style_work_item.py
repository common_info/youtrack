from typing import Union
from openpyxl.cell.cell import Cell
from openpyxl.styles.named_styles import NamedStyle
from openpyxl.workbook.workbook import Workbook
from const_styles import _Style, const_styles


def get_named(style: _Style) -> Union[NamedStyle, _Style]:
    """
    Convert the style to the NamedStyle instance if required.

    :param style: the style
    :type style: _Style
    :return: the updated style.
    :rtype: NamedStyle or _Style
    """
    if style.is_named:
        return NamedStyle(
            name=style.name, font=style.font, fill=style.fill, border=style.border,
            alignment=style.alignment, number_format=style.number_format, protection=style.protection)
    else:
        return style


class _StyleList:
    """
    Define the style list.

    Class params:
        attrs --- the cell attributes;

    Params:
        styles --- the dictionary of the styles, dict[style_name, _Style];\n

    Functions:
        set_style(style_name, cell) --- set the style to the cell;\n
        get_wb_style(wb, style_name) --- add the style from the workbook;\n
    """

    attrs = ("name", "number_format", "alignment", "border", "fill", "font", "protection", "data_type")

    def __init__(self):
        self.styles: dict[str, Union[_Style, NamedStyle]] = dict()
        style: _Style
        for style in const_styles:
            self.styles[style.name] = get_named(style)

    def __str__(self):
        str_styles = [style_name for style_name in self.styles.keys()]
        unified_str_styles = ", ".join(str_styles)
        return f"_StyleList: styles: {unified_str_styles}"

    def __repr__(self):
        repr_styles = [repr(style_name) for style_name in self.styles.keys()]
        unified_repr_styles = ",".join(repr_styles)
        return f"_StyleList(styles=[{unified_repr_styles}])"

    def set_style(self, style_name: str, cell: Cell):
        """
        Specify the style of the cell.

        :param str style_name: the style name
        :param cell: the cell
        :type cell: Cell
        :return: None.
        """
        style = self.styles[style_name]
        if isinstance(style, NamedStyle):
            cell.style = style.name
        elif isinstance(style, _Style):
            for attr in _StyleList.attrs:
                setattr(cell, attr, getattr(style, attr))
        else:
            print(f"Something went wrong. Cell {cell.coordinate}. Style: {style_name}.")

    def get_wb_style(self, wb: Workbook, style_name: str):
        """
        Add the named style from the workbook to the style list.

        :param Workbook wb: the workbook
        :param str style_name: the style name
        :return: None.
        """
        if style_name in wb.style_names:
            style: NamedStyle
            for style in wb._named_styles:
                if style.name == style_name:
                    style_item = NamedStyle(
                        name=style_name, number_format=style.number_format, alignment=style.alignment,
                        border=style.border, fill=style.fill, font=style.font, protection=style.protection)
                    self.styles[style_name] = style_item.get_named
        return
