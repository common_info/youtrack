import decimal
from collections import Counter
from functools import cache
from typing import Optional, Union, Any
import datetime

import openpyxl
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.cell.cell import Cell
from openpyxl.utils.cell import coordinate_to_tuple, get_column_letter, column_index_from_string
from decimal import Decimal
from _style_work_item import _StyleList
from copy import copy
from numpy.core import datetime64, ndarray, busdaycalendar, arange
from numpy.core.multiarray import is_busday

__doc__ = """
Functions:
    check_coord(coord) --- validate the cell coordinate;\n
    range_coord(start_coord, end_coord) --- convert the range string to the tuple;\n
    convert_spent_time(spent_time) --- convert the spent time to the specified format;\n
    convert_datetime_date(value) --- convert the datetime or None to the date;\n
"""


@cache
def year_days() -> ndarray:
    # TODO
    # the current year
    year: int = datetime.date.today().year
    # the date range
    start_day: str = datetime.date(year, 1, 1).isoformat()
    end_day: str = datetime.date(year + 1, 1, 1).isoformat()
    # specify the date range
    return arange(datetime64(start_day), datetime64(end_day),  dtype="datetime64[D]")


@cache
def year_days_indexes():
    # TODO
    col_idx_g = column_index_from_string("G")
    return [index + date.astype(datetime.date).month - 1 + col_idx_g for index, date in enumerate(year_days())]


@cache
def business_week_days():
    """
    Get the business day and weekend dates.

    :return: the date separated by the type.
    :rtype: tuple[list[datetime64], list[datetime64]]
    """
    # TODO
    # the current year
    year: int = datetime.date.today().year
    # the weekend days
    holidays: set[datetime.date] = {
        datetime.date(year, 1, 1), datetime.date(year, 1, 2), datetime.date(year, 1, 3), datetime.date(year, 1, 4),
        datetime.date(year, 1, 5), datetime.date(year, 1, 6), datetime.date(year, 1, 7), datetime.date(year, 2, 23),
        datetime.date(year, 3, 7), datetime.date(year, 3, 8), datetime.date(year, 5, 1), datetime.date(year, 5, 2),
        datetime.date(year, 5, 3), datetime.date(year, 5, 8), datetime.date(year, 5, 9), datetime.date(year, 5, 10),
        datetime.date(year, 6, 12), datetime.date(year, 6, 13), datetime.date(year, 11, 4), datetime.date(year, 12, 31)
    }
    # convert to the ISO
    holidays_iso: list[str] = [date_holiday.isoformat() for date_holiday in holidays]
    # specify the business day calendar
    business_cal: busdaycalendar = busdaycalendar(holidays=holidays_iso)
    # get the bool values if the date is a business one
    return is_busday(dates=year_days(), busdaycal=business_cal)


def convert_date_datetime64(date: Union[datetime64, datetime.date]) -> Optional[datetime64]:
    # TODO
    if isinstance(date, datetime.date):
        return datetime64(date.isoformat())
    elif isinstance(date, datetime64):
        return date
    return None


def convert_datetime64_date(date: Union[datetime64, datetime.date]) -> Optional[datetime.date]:
    # TODO
    if isinstance(date, datetime64):
        return date.astype(datetime.date)
    if isinstance(date, datetime.date):
        return date
    return None


@cache
def dict_days():
    """

    :return:
    """
    return dict(zip(year_days(), year_days_indexes()))


def get_column_by_date(date_year: Union[datetime64, datetime.date]) -> Optional[str]:
    """

    :param date_year:
    :return:
    """
    print(date_year)
    index = dict_days()[convert_date_datetime64(date_year)]
    return get_column_letter(index)


def check_coord(coord: str) -> bool:
    """
    Validate the cell coordinate.

    :param str coord: the cell coordinates
    :return: the validation flag.
    :rtype: bool
    """
    flag = False
    try:
        coordinate_to_tuple(coord)
    except TypeError as e:
        print(f'TypeError {str(e)}. Incorrect value type.\n')
        print(f'coordinate = {coord}')
    except ValueError as e:
        print(f'ValueError {str(e)}. Incorrect value.\n')
        print(f'coordinate = {coord}')
    except OSError as e:
        print(f'OSError {e.errno}, {e.strerror}.\n')
        print(f'coordinate = {coord}')
    else:
        flag = True
    finally:
        return flag


def range_coord(start_coord: str, end_coord: str):
    """
    Convert the range string with the start and end coordinates to the tuple:\n
    (min_row, min_col, max_row, max_col)\n

    :param start_coord: the start cell coordinate, str
    :param end_coord: the end cell coordinate, str
    :return: the values for iteration of the tuple[int, int, int, int] type.
    """
    # convert start coordinate
    start_row, start_column = coordinate_to_tuple(start_coord)
    # convert end coordinate
    end_row, end_column = coordinate_to_tuple(end_coord)
    # find min and max values
    min_row = min(start_row, end_row)
    max_row = max(start_row, end_row)

    min_col = min(start_column, end_column)
    max_col = max(start_column, end_column)
    return min_row, max_row, min_col, max_col


def convert_spent_time(spent_time: Any) -> Decimal:
    """
    Convert the spent time to the specified format.

    :param spent_time: the converted value.
    :return: the decimal value.
    :rtype: Decimal
    """
    decimal.getcontext().prec = 3
    return Decimal(spent_time).normalize()


def convert_datetime_date(value: Any) -> Optional[datetime.date]:
    """
    Convert the datetime or None to the date.

    :param value: the value to convert
    :return: the date value.
    :rtype: datetime.date or None
    """
    if isinstance(value, datetime.datetime):
        return value.date()
    if isinstance(value, datetime.date):
        return value
    if isinstance(value, datetime64):
        return value.astype(datetime.date)
    else:
        return None


@cache
def bus_columns() -> tuple:
    """
    Specify the business day columns.

    :return: the business day columns.
    :rtype: tuple[str]
    """
    return tuple(get_column_by_date(bus_day) for bus_day, is_business in zip(year_days(), business_week_days())
                 if is_business)


@cache
def weekend_columns() -> tuple:
    """
    Specify the weekend columns.

    :return: the weekend columns.
    :rtype: tuple[str]
    """
    return tuple(get_column_by_date(bus_day) for bus_day, is_business in zip(year_days(), business_week_days())
                 if not is_business)


class ExcelProp:
    """
    Define the Excel Worksheet properties.

    Constants:
        dict_headers --- the dictionary of the states and indexes;\n
        dict_headers_short --- the dictionary of the issue states and indexes;\n
        dict_state_style --- the dictionary of the states and cell styles;\n
        dict_state_priority --- the dictionary of the states and priorities;\n
        month_columns --- the columns with the month titles;\n
        dates --- the dates in the first row;\n

    Class params:
        dict_table_cell --- the dictionary of the TableCell instances;\n

    Params:
        ws --- the worksheet;\n
        styles --- the styles implemented into the worksheet;\n

    Properties:
        headers --- the header cells;\n
        headers_short --- the header cells without the legend;\n
        headers_row --- the header cell rows;\n
        headers_row_short --- the header cell rows without the legend;\n
        cell_states --- the dictionary of the cell coordinates and states;\n
        table_cells --- the cells for the TableCell instances;\n
        table_cell_names --- the issue names;\n
        bus_columns -- the work day date columns;\n
        weekend_columns --- the weekend date columns;\n

    Functions:
        _get_shift_by_date(date) --- get the column position from the G column;\n
        get_column_by_date(date) --- get the column letter for the specified date;\n
        cell_in_range(start_coord, end_coord) --- generate the cells in the range;\n
        delete_row(row) --- delete the row from the table;\n
        insert_row(row) --- add the row to the table;\n
        _separate_headers() --- unmerge the header cells;\n
        _combine_headers() --- merge the header cells;\n
        _check_empty(item) --- verify if the Cell, coordinate, or row is empty;\n
        _empty_rows() --- delete all empty rows except for the pre-headers;\n
        replace_cell(*, from_, to_) --- replace the cell attribute values to another cell;\n
        add_work_item(work_item) --- add the work item to the table;\n
        get_date_by_cell(cell) --- get the date associated with the cell;\n
        __index(state) --- get the state index;\n
        __index_row_state(state, *, state_index = 0, row_index = 0) --- specify the rows between the state-based
        headers;\n
        process_non_yt() --- process the self-made issues;\n
        pre_processing() --- pre-process the table to get rid of empty rows and repeating issues;\n
        default_row(row) --- specify the default row;\n
        update_header() --- specify the style to the header rows;\n
        update_table_cells() --- update the TableCell instances;\n
        post_processing() --- post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues;\n
    """
    # the headers
    dict_headers = {'Active': 0, 'New/Paused': 1, 'Done/Test': 2, 'Verified': 3, 'Легенда': 4}
    dict_headers_short = {'Active': 0, 'New/Paused': 1, 'Done/Test': 2, 'Verified': 3}
    # mapping states and styles
    dict_state_style = {
        "New": "basic",
        "New/Paused": "paused",
        "Active": "active",
        "Paused": "paused",
        "Active/Paused": "active",
        "Done": "done",
        "Test": "test",
        "Done/Test": "done",
        "Verified": "verified_closed",
        "Closed": "verified_closed",
        "Discuss": "paused",
        "Review": "paused",
        "Canceled": "verified_closed"
    }
    # the columns with the month names
    month_columns = ("F", "AL", "BO", "CU", "DZ", "FF", "GK", "HQ", "IW", "KB", "LH", "MM")
    # specify the state priorities
    dict_state_priority: dict[str, int] = {"Active": 30, "New/Paused": 10, "Done/Test": 20, "Verified": 40}
    # mapping the issue attributes and columns
    dict_attr_column = {"parent": "B", "issue": "C", "summary": "D", "deadline": "E", "commentary": "NT"}

    dict_table_cell = dict()

    def __init__(self, ws: Worksheet, styles: _StyleList):
        self.ws = ws
        self.styles = styles

    def __str__(self):
        return f"Worksheet: {self.ws.title}"

    def __repr__(self):
        return f"ExcelProp(ws={repr(self.ws)})"

    def __format__(self, format_spec):
        if format_spec == "base":
            return str(self)
        elif format_spec == "row_equal":
            return repr(self)
        else:
            return f"{self.ws.title}"

    def __hash__(self):
        return hash(self.ws.title)

    def __key(self):
        return self.ws.title, self.ws.parent

    def __eq__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def cell_in_range(self, start_coord: str, end_coord: str):
        """
        Convert the cell range to the cell generator in the range.

        :param str start_coord: the start cell coordinates
        :param str end_coord: the end cell coordinates
        :return: the generator of cells.
        """
        if check_coord(coord=start_coord) and check_coord(coord=end_coord):
            min_row, max_row, min_col, max_col = range_coord(start_coord=start_coord, end_coord=end_coord)
            for row in range(min_row, max_row + 1):
                for col in range(min_col, max_col + 1):
                    coord = f'{get_column_letter(col)}{row}'
                    cell: Cell = self.ws[coord]
                    yield cell

    @property
    def headers(self) -> list[Cell]:
        """
        Get the header cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        end_coord = f"B{self.ws.max_row}"
        return [cell for cell in self.cell_in_range("B3", end_coord) if cell.value in ExcelProp.dict_headers]

    @property
    def headers_short(self) -> list[Cell]:
        """
        Get the header cells without the legend.

        :return: the cells.
        :rtype: list[Cell]
        """
        return self.headers[:4]

    @property
    def headers_row(self) -> list[int]:
        """
        Get the header row values.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return [header.row for header in self.headers]

    @property
    def headers_row_short(self) -> list[int]:
        """
        Get the header row values without the legend.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return self.headers_row[:4]

    def delete_row(self, row: int):
        """
        Delete the row from the table.

        :param int row: the table row number
        :return: None.
        """
        self._separate_headers()
        self.ws.delete_rows(row)
        self._combine_headers()
        return

    def insert_row(self, row: int):
        """
        Add the row to the table.

        :param int row: the table row number
        :return: None.
        """
        self._separate_headers()
        self.ws.insert_rows(row)
        self.default_row(row)
        self._combine_headers()
        return

    def _separate_headers(self):
        """Unmerge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            start_coord = f"B{header_row}"
            end_coord = f"E{header_row}"
            self.ws.unmerge_cells(f"{start_coord}:{end_coord}")
            for cell_header in self.cell_in_range(start_coord, end_coord):
                cell_header.style = "basic_issue"
        # the legend header
        header_legend_row = self.headers_row[4]
        start_legend_coord = f"B{header_legend_row}"
        end_legend_coord = f"C{header_legend_row}"
        self.ws.unmerge_cells(f"{start_legend_coord}:{end_legend_coord}")
        for cell_legend in self.cell_in_range(start_legend_coord, end_legend_coord):
            cell_legend.style = "basic_issue"
        return

    def _combine_headers(self):
        """Merge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            self.ws.merge_cells(f"B{header_row}:E{header_row}")
            self.update_header()
        # the legend header
        header_legend_row = self.headers_row[4]
        self.ws.merge_cells(f"B{header_legend_row}:C{header_legend_row}")
        return

    def _check_empty(self, item: Union[int, str, Cell]) -> bool:
        """
        Verify if the cell, cell coordinates, or cell row is empty.

        :param item: the cell value
        :type item: int or str or Cell
        :return: the emptiness flag.
        :rtype: bool
        """
        if isinstance(item, Cell):
            return True if not item.value else False
        elif isinstance(item, str):
            return True if not self.ws[f"{item}"].value else False
        elif isinstance(item, int):
            cell_values = (
                self.ws[f"B{item}"].value, self.ws[f"C{item}"].value,
                self.ws[f"D{item}"].value, self.ws[f"E{item}"].value)
            if any(value for value in cell_values):
                return False
            else:
                return True

    def _empty_rows(self):
        """Delete all empty rows except for the pre-headers."""
        list_empty = []
        for state in self.dict_headers_short:
            low_limit = self.__index_row_state(state, row_index=1)
            if self.__index(state) != 3:
                high_limit = self.__index_row_state(state, state_index=1, row_index=-1)
            else:
                high_limit = self.__index_row_state(state, state_index=1, row_index=-3)
            for row in range(low_limit, high_limit):
                if self._check_empty(row):
                    list_empty.append(row)
        for empty_row in sorted(list_empty, reverse=True):
            self.delete_row(empty_row)
        return

    def replace_cell(self, *, from_: Union[Cell, str], to_: Optional[Union[Cell, str]] = None):
        """
        Replace the cell attribute values to another cell. If to_ is None, the values are deleted.

        :param from_: the original cell or cell coordinates
        :type from_: Cell or str
        :param to_: the destination cell or cell coordinates
        :type to_: Cell or str or None
        :return: None.
        """
        cell_proxy: Optional[Cell]
        cell_base: Cell
        # if the cell coordinates
        if isinstance(from_, str):
            cell_base = self.ws[f"{from_}"]
        # if the cell
        else:
            cell_base = from_
        # if the destination cell exists
        if to_:
            if isinstance(to_, str):
                cell_proxy = self.ws[f"{to_}"]
            else:
                cell_proxy = to_
            # copy all required attribute values
            # if the destination cell is empty
            if not cell_proxy.value:
                cell_proxy.value = copy(cell_base.value)
            # if the destination cell has some value
            else:
                cell_proxy.value += cell_base.value
            if cell_base.has_style:
                self.styles.set_style(cell_base.style, cell_proxy)
            else:
                cell_proxy.number_format = copy(cell_base.number_format)
                cell_proxy.style = "basic"
        # set the default cell attribute values
        cell_base.value = None
        cell_base.style = "basic"
        return

    def add_work_item(self, work_item: tuple[str, datetime.date, Union[int, Decimal]]):
        """
        Add the work item to the table.

        issue_name: str, date: datetime.date, spent_time: Union[int, Decimal]
        :param work_item: the work item to add
        :type work_item: tuple[str, datetime.date, Decimal or int]
        :return: None.
        """
        issue_name, *_ = work_item
        self.dict_table_cell[issue_name].add_work_item(work_item)
        return

    def get_date_by_cell(self, cell: Cell) -> Optional[datetime.date]:
        """
        Get the date associated with the cell.

        :param cell: the table cell
        :type cell: Cell
        :return: the date.
        :rtype: datetime.date or None
        """
        raw_date = self.ws[f"{cell.column_letter}1"].value
        return convert_datetime_date(raw_date)

    def __index(self, state: str) -> int:
        """
        Get the state index.

        :param str state: the state
        :return: the header index.
        :rtype: int
        """
        return self.dict_headers_short[state]

    def __index_row_state(self, state: str, *, state_index: int = 0, row_index: int = 0) -> int:
        """
        Specify the row between the headers based on the state.

        :param str state: the state
        :param int state_index: the state index shift inside the dict key, default: 0
        :param int row_index: the row index shift after the dict key, default: 0
        :return: the row number.
        :rtype: int
        """
        return self.headers_row[self.__index(state) + state_index] + row_index

    @property
    def cell_states(self) -> dict[str, str]:
        """
        Get the dictionary of the cell coordinates and states.

        :return: the cell coordinates and state mapping.
        :rtype: dict[str, str]
        """
        dict_cell_states: dict[str, str] = dict()
        for state in self.dict_headers_short:
            # start from the row right after the header
            start_row = self.__index_row_state(state, row_index=1)
            # stop on the row right before the next header
            end_row = self.__index_row_state(state, state_index=1)
            for cell in self.cell_in_range(f"C{start_row}", f"C{end_row}"):
                if not self._check_empty(cell.row):
                    dict_cell_states[cell.coordinate] = state
        return dict_cell_states

    @property
    def table_cells(self) -> list[Cell]:
        """
        Get the cells of the TableCell instances.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [self.ws[f"{coord}"] for coord in self.cell_states if self.ws[f"{coord}"].value]

    def process_non_yt(self):
        """Specify the self-made issues with no identifier in the YouTrack."""
        # specify the self-made issue rows
        non_yt_rows = [coordinate_to_tuple(coord)[0] for coord in self.cell_states
                       if not self.ws[f"{coord}"].value]
        # specify the self-made work item cells
        non_yt_cells = [cell for row in non_yt_rows
                        for cell in self.cell_in_range(f"G{row}", f"NS{row}") if cell.value]
        # apply the active style to the work item cells
        for cell in non_yt_cells:
            if cell.style in ("basic", "Normal"):
                cell.style = "active"

    def table_cell_names(self) -> set[str]:
        """
        Get the issue names.

        :return: the issue names.
        :rtype: set[str]
        """
        return {cell.value for cell in self.table_cells}

    def pre_processing(self):
        """Pre-process the table to get rid of empty rows and repeating issues."""
        counter = Counter(self.table_cell_names())
        # get non-unique issues
        non_unique = [key for key, value in counter.items() if value > 1]
        for issue in non_unique:
            row_eq = _RowEqual(self, issue)
            row_eq.join_cells()
        # delete empty rows
        self._empty_rows()
        # initiate the TableCell instances
        self.update_table_cells()
        return

    def default_row(self, row: int):
        """
        Specify the default row when added.

        :param int row: the row number to add to the table
        :return: None.
        """
        # the basic
        for column in ("B", "C", "D", "NT"):
            self.ws[f"{column}{row}"].style = "basic_issue"
        # the cells with dates
        # the business days
        for column in bus_columns():
            self.ws[f"{column}{row}"].style = "basic"
        # the weekends
        for column in weekend_columns():
            self.ws[f"{column}{row}"].style = "weekend"
        # the business days
        for column in self.month_columns:
            self.styles.set_style("header_month", self.ws[f"{column}{row}"])
        # the issue deadline
        self.styles.set_style("deadline_issue", self.ws[f"E{row}"])
        # the total spent time cell
        self.styles.set_style("sum", self.ws[f"NS{row}"])
        return

    def update_header(self):
        """Specify the style to the header rows."""
        for cell in self.headers_short:
            cell.style = "Normal"
            self.styles.set_style("header_text", cell)
        for row in self.headers_row_short:
            for cell in self.cell_in_range(f"F{row}", f"NR{row}"):
                self.styles.set_style("header", cell)
            for column in self.month_columns:
                self.styles.set_style("header_no_border", self.ws[f"{column}{row}"])
        return

    def update_table_cells(self):
        """Update the TableCell instances."""
        for cell in self.table_cells:
            TableCell(self, cell)
        return

    def post_processing(self):
        """Post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues."""
        for cell in self.table_cells:
            table_cell = TableCell(self, cell)
            table_cell.post_processing()
        self.process_non_yt()
        return


class TableCell:
    """
    Define the table row.

    Params:
        excel_prop --- the ExcelProp instance;\n
        cell --- the base cell;\n
        issue --- the base cell value;\n

    Properties:
        ws --- the worksheet;\n
        row --- the row;\n
        state --- the state;\n
        parent --- the parent issue;\n
        summary --- the issue summary;\n
        deadline --- the issue deadline;\n
        commentary --- the issue commentary;\n
        cell_last --- the last work item cell;\n

    Functions:
        _shift_cell(shift) --- get the shifted cell in the same row;\n
        pyxl_cells() --- get the work item cells;\n
        work_items() --- get the work items;\n
        mapping_work_item_cell(work_item) --- convert the work item to the cell;\n
        add_work_item(work_item) --- add the work item to the row;\n
        proper_cell_style(cell) --- get the cell style based on the state;\n
        post_processing() --- process the work cells after completing the issue modifications:
            the deadline style;\n
            the formula;\n
            the issue and parent issue hyperlinks;\n
            the proper cell styles.\n
        replace_issue(row) --- replace the issue attribute values to the new row;\n
        _deadline_issue() --- set the style to the cell associated with the deadline if it exists;\n
    """

    def __init__(self, excel_prop: ExcelProp, cell: Cell):
        self.excel_prop = excel_prop
        self.cell = cell
        self.issue = str(cell.value)

        self.excel_prop.dict_table_cell[self.issue] = self

    def __str__(self):
        return f"Table cell: {self.cell.coordinate}"

    def __repr__(self):
        return f"TableCell({self.excel_prop}, {self.cell})"

    def __hash__(self):
        return hash((self.cell.coordinate, self.issue))

    def __key(self):
        return self.ws.title, self.cell.coordinate, self.issue

    def __eq__(self, other):
        if isinstance(other, TableCell):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, TableCell):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, TableCell):
            return self.row < other.row
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, TableCell):
            return self.row > other.row
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, TableCell):
            return self.row <= other.row
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, TableCell):
            return self.row >= other.row
        else:
            return NotImplemented

    def __len__(self):
        return len(self.work_items())

    def _deadline_issue(self):
        # TODO
        current_year = datetime.date.today().year
        print(self.deadline)
        if self.deadline and self.deadline.year == current_year:
            column = get_column_by_date(convert_datetime_date(self.deadline))
            print(f"{column}{self.row}")
            self.ws[f"{column}{self.row}"].style = "deadline"

    @property
    def ws(self) -> Worksheet:
        """
        Get the ExcelProp worksheet.

        :return: the worksheet.
        :rtype: Worksheet
        """
        return self.excel_prop.ws

    @property
    def row(self) -> int:
        """
        Get the base cell row.

        :return: the cell row.
        :rtype: int
        """
        return self.cell.row

    def _shift_cell(self, shift: int) -> Optional[Cell]:
        """
        Get the shifted cell in the same row.

        :param int shift: the cell shift
        :return: the shifted cell.
        :rtype: Cell or None
        """
        col_idx = self.cell.col_idx + shift
        if col_idx >= 1:
            column_letter = get_column_letter(col_idx)
            return self.ws[f"{column_letter}{self.row}"]
        else:
            print(f"ValueError, the shift {shift} is out of bounds.")
            return None

    @property
    def state(self) -> str:
        """
        Get the issue state.

        :return: the state.
        :rtype: str
        """
        return self.excel_prop.cell_states[self.cell.coordinate]

    @property
    def deadline(self) -> Optional[datetime.date]:
        """
        Get the issue deadline if exists.

        :return: the deadline.
        :rtype: datetime.date or None
        """
        return self._shift_cell(2).value

    @property
    def parent(self) -> str:
        """
        Get the parent issue name if exists.

        :return: the parent issue name.
        :rtype: str
        """
        return self._shift_cell(-1).value

    @property
    def summary(self) -> str:
        """
        Get the issue summary.

        :return: the issue summary.
        :rtype: str
        """
        return self._shift_cell(1).value

    @property
    def commentary(self) -> str:
        """
        Get the issue commentary.

        :return: the issue commentary.
        :rtype: str
        """
        return self.ws[f"NT{self.row}"]

    def pyxl_cells(self) -> list[Cell]:
        """
        Get the work item cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [cell for cell in self.excel_prop.cell_in_range(f"G{self.row}", f"NR{self.row}")
                if cell.value]

    def work_items(self) -> list[tuple[str, datetime.date, Decimal]]:
        """
        Get the work items.

        :return: the work items.
        :rtype: list[tuple[str, datetime.date, Decimal]]
        """
        decimal.getcontext().prec = 2
        return [(self.issue, self.excel_prop.get_date_by_cell(cell), Decimal(cell.value).normalize())
                for cell in self.pyxl_cells()]

    @property
    def cell_last(self) -> Cell:
        """
        Get the last work item cell.

        :return: the last work item cell.
        :rtype: Cell
        """
        max_col_idx = max(cell.col_idx for cell in self.pyxl_cells())
        return self.ws[f"{get_column_letter(max_col_idx)}{self.row}"]

    def mapping_work_item_cell(self, work_item: tuple[str, datetime.date, Decimal]) -> Cell:
        """
        Convert the work item to the cell.

        :param work_item: the work item
        :type work_item: tuple[str, datetime.date, Decimal]
        :return: the cell.
        :rtype: Cell
        """
        _, date, _ = work_item
        column = get_column_by_date(date)
        return self.ws[f"{column}{self.row}"]

    def add_work_item(self, work_item: tuple[str, datetime.date, Decimal]):
        """
        Add the work item to the row.

        :param work_item: the work item
        :type work_item: tuple[str, datetime.date, Decimal]
        :return: None.
        """
        _, date, spent_time = work_item
        cell = self.mapping_work_item_cell((self.issue, date, spent_time))
        cell.data_type = "n"
        cell.value = spent_time

    def proper_cell_style(self, cell: Cell) -> str:
        """
        Get the cell style based on the state.

        :param cell: the cell
        :type cell: Cell
        :return: the style name.
        :rtype: str
        """
        # if the cell is not a work item
        if cell not in self.pyxl_cells():
            return cell.style
        # if the deadline exists and the cell date coincides with it
        elif self.deadline and self.excel_prop.get_date_by_cell(cell) == self.deadline:
            return "deadline"
        # if the cell is not the last in the row
        elif cell.coordinate != self.cell_last.coordinate:
            return "active"
        # the rest options
        elif self.state:
            return self.excel_prop.dict_state_style[self.state]
        # if the error occurred
        else:
            print(f"Proper style does not work in a correct way. Cell: {cell.coordinate}, TableCell: {self.issue}")
            print(f"Work cells: {str(self.pyxl_cells())}.\nDeadline: {self.deadline}, "
                  f"last cell: {self.cell_last.coordinate}, state: {self.state}")
            return "basic"

    def post_processing(self):
        """Process the work cells after completing the issue modifications:\n
            the deadline style;\n
            the formula;\n
            the issue and parent issue hyperlinks;\n
            the proper cell styles."""
        # specify the deadline style
        cell_deadline = self._shift_cell(2)
        self.excel_prop.styles.set_style("deadline_issue", cell_deadline)
        # specify the formula
        cell_formula: Cell = self.ws[f"NS{self.row}"]
        cell_formula.value = f"=SUM(G{self.row}:NR{self.row})"
        cell_formula.data_type = "f"
        # specify the issue hyperlink if not self-made
        if not self.issue:
            self.cell.hyperlink = f"https://youtrack.protei.ru/issue/{self.issue}"
        # specify the parent issue hyperlink if exists
        if not self.parent:
            self._shift_cell(-1).hyperlink = f"https://youtrack.protei.ru/issue/{self.parent}"
        # specify the proper style if the cell is not defined for more prior state
        for cell in self.pyxl_cells():
            if cell.style not in ("weekend", "deadline", "sick", "vacation"):
                apply_style = self.proper_cell_style(cell)
                self.excel_prop.styles.set_style(apply_style, cell)
            else:
                continue
        # specify the cell style associated with the deadline if exists
        if self.deadline and self.deadline.year == datetime.date.today().year:
            # TODO
            column = get_column_by_date(self.deadline)
            self.ws[f"{column}{self.row}"].style = "deadline"
        return

    def replace_issue(self, row: int):
        """
        Replace the issue attribute values to the new row.

        :param int row: the new row
        :return: None.
        """
        old_row = self.row
        for column in self.excel_prop.dict_attr_column.values():
            self.excel_prop.replace_cell(from_=self.ws[f"{column}{old_row}"], to_=self.ws[f"{column}{row}"])
            self.ws[f"{column}{old_row}"].value = None
            self.ws[f"{column}{old_row}"].style = "basic"
        return


class _RowEqual:
    """
    Specify the instance to work with the issues having the same names.

    Params:
        excel_prop --- the ExcelProp instance;\n
        issue --- the issue name;\n

    Properties:
        cells --- the cells with the same issue names;\n
        dict_issues --- the dictionary of the issue cells and state priorities;\n

    Functions:
        work_item_cells() --- get the cells to join;\n
        cell_final() --- get the final cell to keep in the table;\n
        join_cells() --- join the cells into the one issue;\n
    """

    def __init__(self, excel_prop: ExcelProp, issue: str):
        self.excel_prop = excel_prop
        self.issue = issue

    def __str__(self):
        cell_string = ", ".join([cell.coordinate for cell in self.cells])
        return f"_RowEqual: issue name - {self.issue},\ncells - {cell_string}"

    def __repr__(self):
        return f"_RowEqual({format(self.excel_prop, 'row_equal')}, {self.issue})"

    def __len__(self):
        return len(self.cells)

    def __bool__(self):
        return len(self.cells) > 1

    @property
    def cells(self) -> list[Cell]:
        """
        Get the issue cells with the same names.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [cell for cell in self.excel_prop.table_cells if cell.value == self.issue]

    @property
    def dict_issues(self) -> dict[Cell, int]:
        """
        Get the dictionary of the issue cells and state priorities.

        :return: the cell and state priority mapping.
        :rtype: dict[Cell, int]
        """
        dict_cell_priority: dict[Cell, int] = dict()
        for cell in self.cells:
            state = self.excel_prop.cell_states[cell.coordinate]
            dict_cell_priority[cell] = self.excel_prop.dict_state_priority[state]
        return dict_cell_priority

    def work_item_cells(self) -> list[Cell]:
        """
        Get the work item cells to join.

        :return: the work item cells.
        :rtype: list[Cell]
        """
        work_cells: list[Cell] = []
        for cell in self.cells:
            if cell == self.cell_final():
                continue
            else:
                for item in self.excel_prop.cell_in_range(f"G{cell.row}", f"NR{cell.row}"):
                    if item.value:
                        work_cells.append(item)
                    else:
                        continue
        return work_cells

    def cell_final(self) -> Cell:
        """
        Get the final cell to keep.

        :return: the cell
        :rtype: Cell
        """
        max_priority = max(priority for priority in self.dict_issues.values())
        max_prior_min_row = min(cell.row for cell, priority in self.dict_issues.items() if priority == max_priority)
        for cell in self.cells:
            if cell.row == max_prior_min_row():
                return cell

    def join_cells(self):
        """Join the work items to the final issue."""
        for work_item_cell in self.work_item_cells():
            self.excel_prop.replace_cell(
                from_=work_item_cell, to_=f"{work_item_cell.column_letter}{self.cell_final().row}")
        for issue_cell in self.cells:
            if issue_cell != self.cell_final():
                row = issue_cell.row
                for column in ("B", "C", "D", "E"):
                    self.excel_prop.replace_cell(from_=f"{column}{row}")
