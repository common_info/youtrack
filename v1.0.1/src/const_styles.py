from typing import NamedTuple
from openpyxl.styles.numbers import FORMAT_GENERAL, FORMAT_TEXT, FORMAT_NUMBER_00, FORMAT_DATE_XLSX14, FORMAT_NUMBER
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.borders import Border, Side, BORDER_THIN, BORDER_MEDIUM
from openpyxl.styles.colors import Color, WHITE, BLACK
from openpyxl.styles.fills import PatternFill, FILL_SOLID
from openpyxl.styles.fonts import Font
from openpyxl.styles.protection import Protection


# TODO
__doc__ = """
Contain the constants for Named and Cell Styles.

dict_states_color --- dict of states, PatternFill values
    "weekend", "deadline", "done", "active", "test", "going_start", "paused",
    "verified_closed", "going_finish", "sick", "vacation"\n
"""


class _Style(NamedTuple):
    """
    Specify the cell style.

    Params:
        name --- the style name;\n
        is_named --- flag to convert to the NamedStyle instance;\n
        number_format --- the style number format;\n
        alignment --- the style alignment;\n
        border --- the style border;\n
        fill --- the style fill;\n
        font --- the style font;\n
        protection --- the style protection;\n
        data_type --- the style data type, default: "n";\n
    """
    name: str
    is_named: bool
    number_format: str
    alignment: Alignment
    border: Border
    fill: PatternFill
    font: Font
    protection: Protection
    data_type: str = "n"


# Cell.alignment
TMP_ALIGNMENT = Alignment(
    horizontal='left', vertical='center', wrap_text=True, shrinkToFit=None, indent=0, relativeIndent=0,
    justifyLastLine=None, readingOrder=0)
CENTER_ALIGNMENT = Alignment(
    horizontal='center', vertical='center', wrap_text=True, shrinkToFit=None, indent=0, relativeIndent=0,
    justifyLastLine=None, readingOrder=0)
NONE_ALIGNMENT = Alignment()

# Cell.border
TMP_BORDER = Border(outline=False,
                    left=Side(style=None, color=None),
                    right=Side(style=None, color=None),
                    top=Side(style=None, color=None),
                    bottom=Side(style=None, color=None),
                    diagonal=Side(style=None, color=None))
THIN_BORDER = Border(outline=True,
                     left=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
                     right=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
                     top=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
                     bottom=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
                     diagonal=Side(style=None, color=None))
TOP_BOTTOM_BORDER = Border(outline=True,
                           left=Side(style=None, color=None),
                           right=Side(style=None, color=None),
                           top=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
                           bottom=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
                           diagonal=Side(style=None, color=None))
LEFT_RIGHT_TOP_BOTTOM_BORDER = Border(outline=True,
                                      left=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
                                      right=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
                                      top=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
                                      bottom=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
                                      diagonal=Side(style=None, color=None))
LEFT_TOP_BOTTOM_BORDER = Border(outline=True,
                                left=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
                                right=Side(style=None, color=None),
                                top=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
                                bottom=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
                                diagonal=Side(style=None, color=None))
LEFT_RIGHT_BORDER = Border(outline=True,
                           left=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
                           right=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
                           top=Side(style=None, color=None),
                           bottom=Side(style=None, color=None),
                           diagonal=Side(style=None, color=None))

# Cell.fill
TMP_FILL = PatternFill(fill_type=None,
                       fgColor=Color(rgb=WHITE, type='rgb'),
                       bgColor=Color(rgb=WHITE, type='rgb'))
HEADER_FILL = PatternFill(fill_type=FILL_SOLID,
                          fgColor=Color(theme=3, tint=0.5999938962981048, type='theme'),
                          bgColor=Color(indexed=64, type='indexed'))

# Cell.font
TMP_FONT = Font(name='Calibri', charset=204, family=2, color=Color(rgb=BLACK, type='rgb'), size=11)
THEME_FONT = Font(name='Calibri', charset=204, family=2, color=Color(theme=1, type='theme'), size=11)
# Cell.protection
TMP_PROTECTION = Protection(locked=False, hidden=False)
LOCKED_PROTECTION = Protection(locked=True, hidden=False)
# states and PatternFill values
dict_states_color: dict[str, PatternFill] = {
    "weekend": PatternFill(fill_type=FILL_SOLID,
                           fgColor=Color(rgb="FFFD5635", type='rgb'),
                           bgColor=Color(rgb=WHITE, type='rgb')),
    "deadline": PatternFill(fill_type=FILL_SOLID,
                            fgColor=Color(rgb="FFFF0D0D", type='rgb'),
                            bgColor=Color(rgb=WHITE, type='rgb')),
    "done": PatternFill(fill_type=FILL_SOLID,
                        fgColor=Color(theme=9, tint=0.0, type='theme'),
                        bgColor=Color(indexed=64, type='indexed')),
    "active": PatternFill(fill_type=FILL_SOLID,
                          fgColor=Color(theme=9, tint=0.3999755851924192, type='theme'),
                          bgColor=Color(indexed=64, type='indexed')),
    "test": PatternFill(fill_type=FILL_SOLID,
                        fgColor=Color(rgb="FF117D68", type='rgb'),
                        bgColor=Color(rgb=WHITE, type='rgb')),
    "going_start": PatternFill(fill_type=FILL_SOLID,
                               fgColor=Color(rgb="FF00B0F0", type='rgb'),
                               bgColor=Color(rgb=WHITE, type='rgb')),
    "paused": PatternFill(fill_type=FILL_SOLID,
                          fgColor=Color(theme=0, tint=-0.499984740745262, type='theme'),
                          bgColor=Color(indexed=64, type='indexed')),
    "verified_closed": PatternFill(fill_type=FILL_SOLID,
                                   fgColor=Color(theme=1, tint=0.249977111117893, type='theme'),
                                   bgColor=Color(indexed=64, type='indexed')),
    "going_finish": PatternFill(fill_type=FILL_SOLID,
                                fgColor=Color(theme=4, tint=-0.249977111117893, type='theme'),
                                bgColor=Color(indexed=64, type='indexed')),
    "sick": PatternFill(fill_type=FILL_SOLID,
                        fgColor=Color(theme=7, tint=-0.249977111117893, type='theme'),
                        bgColor=Color(indexed=64, type='indexed')),
    "vacation": PatternFill(fill_type=FILL_SOLID,
                            fgColor=Color(rgb="FFFFFF00", type='rgb'),
                            bgColor=Color(rgb=WHITE, type='rgb'))
}

const_styles: list[_Style] = [
    _Style(
        "basic", True, FORMAT_GENERAL, TMP_ALIGNMENT, TMP_BORDER, TMP_FILL, TMP_FONT, TMP_PROTECTION, "n"),
    _Style(
        "_basic_style", False, FORMAT_NUMBER_00, CENTER_ALIGNMENT, THIN_BORDER, TMP_FILL, THEME_FONT,
        LOCKED_PROTECTION, "n"),
    _Style(
        "header", False, FORMAT_TEXT, CENTER_ALIGNMENT, TOP_BOTTOM_BORDER, HEADER_FILL, THEME_FONT, TMP_PROTECTION,
        "n"),
    _Style(
        "deadline_issue", False, FORMAT_DATE_XLSX14, TMP_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL,
        THEME_FONT, TMP_PROTECTION, "d"),
    _Style(
        "header_no_border", False, FORMAT_GENERAL, NONE_ALIGNMENT, TMP_BORDER, HEADER_FILL, THEME_FONT,
        TMP_PROTECTION, "n"),
    _Style(
        "basic_issue", True, FORMAT_TEXT, TMP_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL, THEME_FONT,
        LOCKED_PROTECTION, "n"),
    _Style(
        "header_text", False, FORMAT_GENERAL, CENTER_ALIGNMENT, LEFT_TOP_BOTTOM_BORDER, HEADER_FILL, THEME_FONT,
        TMP_PROTECTION, "n"),
    _Style(
        "sum", False, FORMAT_NUMBER, CENTER_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL, THEME_FONT,
        LOCKED_PROTECTION, "f"),
    _Style(
        "header_month", False, FORMAT_TEXT, NONE_ALIGNMENT, LEFT_RIGHT_BORDER, HEADER_FILL, THEME_FONT,
        TMP_PROTECTION, "n")
]

for style_name, style_fill in dict_states_color.items():
    const_styles.append(_Style(
        style_name, False, FORMAT_NUMBER_00, CENTER_ALIGNMENT, THIN_BORDER, style_fill, THEME_FONT,
        LOCKED_PROTECTION, "n")
    )
