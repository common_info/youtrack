import json
from functools import cache
from numpy.core import datetime64, busdaycalendar, ndarray, arange
from numpy.core.multiarray import is_busday
from openpyxl.styles.numbers import FORMAT_GENERAL, FORMAT_TEXT, FORMAT_NUMBER_00, FORMAT_DATE_YYYYMMDD2, FORMAT_NUMBER
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.borders import Border, Side, BORDER_THIN, BORDER_MEDIUM
from openpyxl.styles.colors import Color, WHITE, BLACK
from openpyxl.styles.fills import PatternFill, FILL_SOLID
from openpyxl.styles.fonts import Font
from openpyxl.styles.protection import Protection

__doc__ = """
Contain the constants and cached values for other files.

Alignment:
    TMP_ALIGNMENT, CENTER_ALIGNMENT, NONE_ALIGNMENT;\n
Border:
    TMP_BORDER, THIN_BORDER, TOP_BOTTOM_BORDER, LEFT_RIGHT_TOP_BOTTOM_BORDER, LEFT_TOP_BOTTOM_BORDER, 
    LEFT_RIGHT_BORDER;\n
Fill:
    TMP_FILL, HEADER_FILL;\n
Font:
    TMP_FONT, THEME_FONT;\n
Protection:
    TMP_PROTECTION, LOCKED_PROTECTION;\n

HEADERS_REQUEST --- the headers for the statistics request;\n
STYLES_CONST --- the styles to add;\n
MONTH_COLUMNS --- the columns with the month names;\n
HOLIDAYS --- the weekend days;\n
DICT_LOGINS_USERS --- mapping logins and names;\n
DICT_STATE_COLOR --- mapping states and PatternFill values;\n
DICT_STATES --- mapping states;\n
DICT_ATTR_COLUMN --- mapping attributes and column letters;\n
DICT_STATE_STYLE --- mapping states and styles;\n
DICT_HEADERS --- the headers;\n
DICT_HEADERS_SHORT --- the headers without the legend;\n
DICT_LOGIN_SPEC_DAYS --- the sick and vacation days;\n
DICT_START_RENAME_FILES --- the files to rename in the start;\n
DICT_END_RENAME_FILES --- the files to rename in the end;\n
"""

# Cell.alignment
TMP_ALIGNMENT: Alignment = Alignment(
    horizontal='left', vertical='center', wrap_text=True, shrinkToFit=None, indent=0, relativeIndent=0,
    justifyLastLine=None, readingOrder=0)
CENTER_ALIGNMENT: Alignment = Alignment(
    horizontal='center', vertical='center', wrap_text=True, shrinkToFit=None, indent=0, relativeIndent=0,
    justifyLastLine=None, readingOrder=0)
NONE_ALIGNMENT: Alignment = Alignment()

# Cell.border
TMP_BORDER: Border = Border(
    outline=False,
    left=Side(style=None, color=None),
    right=Side(style=None, color=None),
    top=Side(style=None, color=None),
    bottom=Side(style=None, color=None),
    diagonal=Side(style=None, color=None))
THIN_BORDER: Border = Border(
    outline=True,
    left=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
    right=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
    top=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
    bottom=Side(style=BORDER_THIN, color=Color(rgb=BLACK, type="rgb")),
    diagonal=Side(style=None, color=None))
TOP_BOTTOM_BORDER: Border = Border(
    outline=True,
    left=Side(style=None, color=None),
    right=Side(style=None, color=None),
    top=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
    bottom=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
    diagonal=Side(style=None, color=None))
LEFT_RIGHT_TOP_BOTTOM_BORDER: Border = Border(
    outline=True,
    left=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
    right=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
    top=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
    bottom=Side(style=BORDER_THIN, color=Color(indexed=64, type="indexed")),
    diagonal=Side(style=None, color=None))
LEFT_TOP_BOTTOM_BORDER: Border = Border(
    outline=True,
    left=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
    right=Side(style=None, color=None),
    top=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
    bottom=Side(style=BORDER_MEDIUM, color=Color(indexed=64, type="indexed")),
    diagonal=Side(style=None, color=None))
LEFT_RIGHT_BORDER: Border = Border(
    outline=True,
    left=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
    right=Side(style=BORDER_MEDIUM, color=Color(rgb=BLACK, type="rgb")),
    top=Side(style=None, color=None),
    bottom=Side(style=None, color=None),
    diagonal=Side(style=None, color=None))

# Cell.fill
TMP_FILL: PatternFill = PatternFill(
    fill_type=None,
    fgColor=Color(rgb=WHITE, type='rgb'),
    bgColor=Color(rgb=WHITE, type='rgb'))
HEADER_FILL: PatternFill = PatternFill(
    fill_type=FILL_SOLID,
    fgColor=Color(theme=3, tint=0.5999938962981048, type='theme'),
    bgColor=Color(indexed=64, type='indexed'))

# Cell.font
TMP_FONT: Font = Font(name='Calibri', charset=204, family=2, color=Color(rgb=BLACK, type='rgb'), size=11)
THEME_FONT: Font = Font(name='Calibri', charset=204, family=2, color=Color(theme=1, type='theme'), size=11)

# Cell.protection
TMP_PROTECTION: Protection = Protection(locked=False, hidden=False)
LOCKED_PROTECTION: Protection = Protection(locked=True, hidden=False)

# mapping states and PatternFill values
DICT_STATES_COLOR: dict[str, PatternFill] = {
    "weekend": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(rgb="FFFD5635", type='rgb'),
        bgColor=Color(rgb=WHITE, type='rgb')),
    "deadline": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(rgb="FFFF0D0D", type='rgb'),
        bgColor=Color(rgb=WHITE, type='rgb')),
    "done": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=9, tint=0.0, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "active": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=9, tint=0.3999755851924192, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "test": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(rgb="FF117D68", type='rgb'),
        bgColor=Color(rgb=WHITE, type='rgb')),
    "going_start": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(rgb="FF00B0F0", type='rgb'),
        bgColor=Color(rgb=WHITE, type='rgb')),
    "paused": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=0, tint=-0.499984740745262, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "verified_closed": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=1, tint=0.249977111117893, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "going_finish": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=4, tint=-0.249977111117893, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "sick": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(theme=7, tint=-0.249977111117893, type='theme'),
        bgColor=Color(indexed=64, type='indexed')),
    "vacation": PatternFill(
        fill_type=FILL_SOLID,
        fgColor=Color(rgb="FFFFFF00", type='rgb'),
        bgColor=Color(rgb=WHITE, type='rgb'))
}

# the constant styles to add to the table
STYLES_CONST: tuple[tuple, ...] = (
    ("basic", True, FORMAT_GENERAL, TMP_ALIGNMENT, TMP_BORDER, TMP_FILL, TMP_FONT, TMP_PROTECTION, "n"),
    ("_basic_style", False, FORMAT_NUMBER_00, CENTER_ALIGNMENT, THIN_BORDER, TMP_FILL, THEME_FONT,
     LOCKED_PROTECTION, "n"),
    ("header", False, FORMAT_TEXT, CENTER_ALIGNMENT, TOP_BOTTOM_BORDER, HEADER_FILL, THEME_FONT, TMP_PROTECTION, "n"),
    ("deadline_issue", False, FORMAT_DATE_YYYYMMDD2, TMP_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL, THEME_FONT,
     TMP_PROTECTION, "d"),
    ("header_no_border", False, FORMAT_GENERAL, NONE_ALIGNMENT, TMP_BORDER, HEADER_FILL, THEME_FONT,
     TMP_PROTECTION, "n"),
    ("basic_issue", True, FORMAT_TEXT, TMP_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL, THEME_FONT,
     LOCKED_PROTECTION, "n"),
    ("header_text", True, FORMAT_GENERAL, CENTER_ALIGNMENT, LEFT_TOP_BOTTOM_BORDER, HEADER_FILL, THEME_FONT,
     TMP_PROTECTION, "n"),
    ("sum", False, FORMAT_NUMBER, CENTER_ALIGNMENT, LEFT_RIGHT_TOP_BOTTOM_BORDER, TMP_FILL, THEME_FONT,
     LOCKED_PROTECTION, "f"),
    ("header_month", False, FORMAT_TEXT, NONE_ALIGNMENT, LEFT_RIGHT_BORDER, HEADER_FILL, THEME_FONT,
     TMP_PROTECTION, "n"),
)

# mapping attributes and column letters
DICT_ATTR_COLUMN: dict[str, str] = {
    "parent": "B",
    "issue": "C",
    "summary": "D",
    "deadline": "E",
    "commentary": "NT"
}


@cache
def spec_days() -> dict[str, list[tuple[str, str]]]:
    """
    Specify the cells with the predefined style.

    :return: mapping logins and cell columns, cell styles.
    :rtype: dict[str, list[tuple[str, str]]]
    """
    # read the json file
    with open("spec_days.json", "r+") as json_file:
        file_unparsed: dict[str, list[dict[str, str]]] = json.load(json_file)
    dict_spec_days: dict[str, list[tuple[str, str]]] = dict()
    # get logins
    for item in file_unparsed:
        dict_spec_days[item] = []
        # get columns and style
        for dict_cell in file_unparsed[f"{item}"]:
            dict_spec_days[item].append((dict_cell["cell"], dict_cell["cause"]))
    return dict_spec_days


@cache
def today() -> datetime64:
    """
    Get the current day.

    :return: the current day.
    :rtype: datetime64
    """
    return datetime64("today", "D")


@cache
def current_year() -> int:
    """
    Get the current year value.

    :return: the current year.
    :rtype: int
    """
    return today().item().year


@cache
def first_day() -> datetime64:
    """
    Get the first day value.

    :return: the first day in the year.
    :rtype: datetime64
    """
    return datetime64(f"{current_year()}-01-01", "D")


@cache
def year_days() -> ndarray:
    """
    Get the days in the current year.

    :return: the numpy days array.
    :rtype: ndarray[datetime64[D]]
    """
    # the date range
    end_day: datetime64 = datetime64(f"{current_year() + 1}-01-01", "D")
    # specify the date range
    return arange(first_day(), end_day, dtype='datetime64[D]')


@cache
def business_week_days() -> ndarray:
    """
    Get the business day and weekend dates.

    :return: the flags of the business days.
    :rtype: ndarray[bool]
    """
    # specify the business day calendar
    business_cal: busdaycalendar = busdaycalendar(holidays=HOLIDAYS)
    # get the bool values if the date is a business one
    return is_busday(dates=year_days(), busdaycal=business_cal)


@cache
def start_period_stat() -> str:
    return first_day().item().isoformat()


@cache
def end_period_stat() -> str:
    return today().item().isoformat()


@cache
def period():
    return " .. ".join((start_period_stat(), end_period_stat()))


# the non-working days
HOLIDAYS: tuple[datetime64, ...] = (
    datetime64(f"{current_year()}-01-01", "D"), datetime64(f"{current_year()}-01-02", "D"),
    datetime64(f"{current_year()}-01-03", "D"), datetime64(f"{current_year()}-01-04", "D"),
    datetime64(f"{current_year()}-01-05", "D"), datetime64(f"{current_year()}-01-06", "D"),
    datetime64(f"{current_year()}-01-07", "D"), datetime64(f"{current_year()}-02-23", "D"),
    datetime64(f"{current_year()}-03-07", "D"), datetime64(f"{current_year()}-03-08", "D"),
    datetime64(f"{current_year()}-05-01", "D"), datetime64(f"{current_year()}-05-02", "D"),
    datetime64(f"{current_year()}-05-03", "D"), datetime64(f"{current_year()}-05-08", "D"),
    datetime64(f"{current_year()}-05-09", "D"), datetime64(f"{current_year()}-05-10", "D"),
    datetime64(f"{current_year()}-06-12", "D"), datetime64(f"{current_year()}-06-13", "D"),
    datetime64(f"{current_year()}-11-04", "D"), datetime64(f"{current_year()}-12-31", "D")
)

# mapping states and section indexes
DICT_HEADERS_SHORT: dict[str, int] = {'Active': 0, 'New/Paused': 1, 'Done/Test': 2, 'Verified': 3}
