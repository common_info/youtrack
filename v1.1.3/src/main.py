#!/usr/bin/python
# -*- coding: cp1251 -*-
import sys
import coloredlogs
from datetime import datetime, timedelta
from pathlib import Path
from typing import Union
from numpy.core import datetime64, arange, ndarray
from xl_style import _StyleList
from yt_requests import Issue, IssueWorkItem, User, MultipleConfig, multiple_config
from const_values import today, first_day, current_year
from xl_table import ExcelProp
from join_user_xl import JoinUserXL
import logging
from logging import Logger, Formatter, StreamHandler, FileHandler


logger: Logger = logging.getLogger(__name__)


def rename_file(path_dir: Path, name_replace: str, name_sub: str):
    """
    Rename the file since the surname is not the login in the YouTrack.

    :param path_dir: the path to the directory with files
    :type path_dir: Path
    :param str name_replace: the file name to replace
    :param str name_sub: the file name to substitute
    :return: None.
    """
    path_file: Path = path_dir.joinpath(name_replace)
    if path_file.exists() and path_file.is_file():
        path_file.rename(path_file.with_name(name_sub))
        logger.info(f"Файл {name_replace} переименован в {name_sub}.")
    else:
        logger.info(f"Объект {name_replace} не переименован, поскольку не существует или не является файлом.")
    return


def sort_work_items(
        dict_work_items: dict[str, list[IssueWorkItem]],
        start_day: datetime64,
        end_day: datetime64) -> dict[datetime64, list[IssueWorkItem]]:
    """
    Sort the issue work items by the work date.

    :param dict_work_items: the dictionary of the issue work items
    :type dict_work_items: dict[str, list[IssueWorkItem]]
    :param datetime64 start_day: the start day
    :param datetime64 end_day: the end day
    :return: the sorted dictionary.
    :rtype: dict[datetime64, list[IssueWorkItem]]
    """
    days: ndarray = arange(start_day, end_day, dtype='datetime64[D]')
    all_work_items: list[IssueWorkItem] = [value for values in dict_work_items.values() for value in values]
    sorted_work_items: dict[datetime64, list[IssueWorkItem]] = dict()
    for day in days:
        if not any(work_item.date == day for work_item in all_work_items):
            continue
        else:
            sorted_work_items[day] = []
            for work_item in all_work_items:
                if work_item.date == day:
                    sorted_work_items[day].append(work_item)
    return sorted_work_items


def print_sorted_work_items(dict_sorted: dict[datetime64, list[IssueWorkItem]]) -> tuple[str]:
    """
    Output the issue work items sorted by the work date.

    :param dict_sorted: the dictionary of the sorted issue work items
    :type dict_sorted: dict[datetime64, list[IssueWorkItem]]
    :return: the lines to output.
    :rtype: tuple[str]
    """
    lines: list[str] = []
    for k, v in dict_sorted.items():
        logger.info(f"Дата: {k.item().isoformat()}")
        for work_item in v:
            logger.info(format(work_item, "date"))
    return *lines,


def delete_files(path_dir: Union[Path, str], names: list[Union[str, Path]]):
    # TODO
    if not Path(path_dir).resolve().is_dir():
        logger.info(f"Путь {path_dir} указывает не на директорию")
        return
    for name in names:
        path_file: Path = path_dir.joinpath(name)
        try:
            path_file.unlink(missing_ok=False)
        except FileNotFoundError:
            logger.exception(f"Файл {path_file} не найден", exc_info=True)
        else:
            logger.warning(f"Файл {path_file} удален")
        finally:
            continue


def rename():
    # TODO
    multiconfig: MultipleConfig = multiple_config()
    path_dir: Path = multiconfig.path_dir

    for item in path_dir.iterdir():
        if item.name.startswith("2022_"):
            item.rename(item.with_suffix(".xlsx"))


def main():
    # specify the files to rename in the start
    _dict_start_rename_files: dict[str, str] = {
        "2022_report_mozglyakova.xlsx": "2022_report_matyushina.xlsx",
        "data_mozglyakova.txt": "data_matyushina.txt",
        "generated_2022_report_mozglyakova.xlsx": "generated_2022_report_matyushina.xlsx"
    }

    # specify the files to rename in the end
    _dict_end_rename_files: dict[str, str] = {
        "2022_report_matyushina.xlsx": "2022_report_mozglyakova.xlsx",
        "data_matyushina.txt": "data_mozglyakova.txt",
        "generated_2022_report_matyushina.xlsx": "generated_2022_report_mozglyakova.xlsx"
    }

    # mapping logins and names
    _dict_logins_users: dict[str, str] = {
        "bochkova": "Бочкова",
        "brovko": "Бровко",
        "burdin": "Бурдин",
        "demyanenko": "Демьяненко",
        "fesenko": "Фесенко",
        "kuksina": "Куксина",
        "lyalina": "Лялина",
        "matyushina": "Мозглякова",
        "mazyarova": "Мазярова",
        "nigrej": "Нигрей",
        "nikitina": "Никитина",
        "vykhodtsev": "Выходцев",
        "tarasov-a": "Тарасов"
    }

    datetime_start: datetime = datetime.now()
    # specify the multiple config
    multiconfig: MultipleConfig = multiple_config()
    path_dir: Path = multiconfig.path_dir
    # rename the files
    for old_file_name, new_file_name in _dict_start_rename_files.items():
        rename_file(path_dir, old_file_name, new_file_name)
    user: User
    _StyleList()
    for user in multiconfig.all_users():
        user_timer_start: datetime = datetime.now()
        lines: list[str] = []
        path: Path = multiconfig.path_table(user.login)
        path_new_file: Path = path.with_name(f"generated_{path.name}")
        # specify the Excel file with the report
        excel_prop: ExcelProp = ExcelProp.get_excel_prop(path)
        # specify the JoinUserXL instance
        join_user_xl: JoinUserXL = JoinUserXL(user, excel_prop)
        # get the full information from the YouTrack and work with the IssueWorkItem instances
        logger.info("----------")
        lines.append("----------")
        logger.info(f"Пользователь: {_dict_logins_users[user.login]}")
        lines.append(f"Пользователь: {_dict_logins_users[user.login]}")
        user.pre_processing()
        # print the information from the YouTrack
        logger.info("Задачи:")
        lines.append("Задачи:")
        issue: Issue
        for issue in user.dict_issue.values():
            logger.info(format(issue, "output"))
            lines.append(format(issue, "output"))
        logger.info("----------")
        lines.append("----------")
        logger.info("Затраченное время по задачам:")
        lines.append("Затраченное время по задачам:")
        issue_work_item: IssueWorkItem
        for issue_item_name, issue_items in user.dict_issue_work_item.items():
            logger.info(f"Задача: {issue_item_name}")
            lines.append(f"Задача: {issue_item_name}")
            for issue_work_item in issue_items:
                logger.info(format(issue_work_item, "short"))
                lines.append(format(issue_work_item, "short"))
        # print the issue work item information sorted by the work date
        logger.info("----------")
        lines.append("----------")
        logger.info("Затраченное время по датам:")
        lines.append("Затраченное время по датам:")
        # sort the issue work items
        sorted_work_items: dict[datetime64, list[IssueWorkItem]]
        sorted_work_items = sort_work_items(user.dict_issue_work_item, first_day(), today())
        for item in print_sorted_work_items(sorted_work_items):
            print(item)
        lines.extend(print_sorted_work_items(sorted_work_items))
        # print the current issues
        logger.info("----------")
        lines.append("----------")
        logger.info("Не закрытые задачи:")
        lines.append("Не закрытые задачи:")
        for issue in user.current_issues():
            logger.info(format(issue, "output"))
            lines.append(format(issue, "output"))
        # print the issues with the deadline date expired
        logger.info("----------")
        lines.append("----------")
        logger.info("Задачи с истекшим крайним сроком (дедлайном):")
        lines.append("Задачи с истекшим крайним сроком (дедлайном):")
        for issue in user.expired_issues():
            logger.info(format(issue, "output"))
            lines.append(format(issue, "output"))
        # delete the empty rows and join the rows with the same issue name
        excel_prop.pre_processing()
        # add new issues to the table
        join_user_xl.add_all_issues()
        logger.info("Задачи добавлены.")
        # update the TableRow instances since the rows are changed
        excel_prop.update_table_cells()
        # modify the issues in the table
        join_user_xl.modify_all_issues()
        excel_prop.update_table_cells()
        logger.info("Параметры задач (название, родительская задача, дедлайн) обновлены.")
        # update the TableRow instances since the rows are changed
        excel_prop.update_table_cells()
        # update the issue state in the table
        join_user_xl.update_states()
        logger.info("Статусы задач обновлены.")
        excel_prop.update_table_cells()
        # add the IssueWorkItems to the table
        join_user_xl.add_new_work_items()
        logger.info("Затраченное время добавлено.")
        # add cell styles, formulae, and hyperlinks
        excel_prop.post_processing()
        logger.info("Пост-обработка завершена.")
        # save the changes, create the new file, and close it
        excel_prop.close(path_new_file)

        logger.info("Работа завершена. Путь до файла:")
        logger.info(f"{path.resolve()}")

        txt_file: Path = path.with_name(f"data_{user.login}.txt")
        final_data: str = "\n".join(lines)
        with open(txt_file, "w+") as file:
            file.write(final_data)

        user_timer_end: datetime = datetime.now()
        user_time: timedelta = user_timer_end - user_timer_start
        logger.info(f"{user.login}: {str(user_time)}")
    # rename the file
    for old_file_name, new_file_name in _dict_end_rename_files.items():
        rename_file(path_dir, old_file_name, new_file_name)

    # TODO
    files_delete: list[Path] = list(filter(
        lambda x: x.name.startswith(f"{current_year()}_"), multiconfig.path_dir.iterdir()))
    delete_files(path_dir, files_delete)

    # TODO
    files_rename: list[Path] = list(filter(lambda x: x.name.startswith("generated_"), multiconfig.path_dir.iterdir()))
    for file in files_rename:
        new_file_name: str = file.name.replace("generated_", "")
        rename_file(path_dir, file.name, new_file_name)

    datetime_end: datetime = datetime.now()
    work_time: timedelta = datetime_end - datetime_start
    logger.info(f"Время работы: {str(work_time)}")
    print("Нажмите любую клавишу, чтобы закрыть окно...")
    input()


if __name__ == "__main__":

    fmt: str = "{asctime}:{levelname}:{name} | {filename}:{lineno} | {message} "
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    formatter: Formatter = Formatter(fmt=fmt, datefmt=datefmt, style="{")

    stream_handler: StreamHandler = StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    filename: str = f"{Path(sys.argv[0]).stem}_debug.log"
    max_bytes: int = 2 ** 20
    backup_count: int = 10
    file_handler: FileHandler = FileHandler(filename=filename, mode="a", encoding="utf-8")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    coloredlogs.install(level="DEBUG", logger=logger, fmt=fmt, datefmt=datefmt, style="{", stream=sys.stdout)

    main()
    rename()
