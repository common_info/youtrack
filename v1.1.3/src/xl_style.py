from functools import cache
from typing import Union, NamedTuple, Any
from openpyxl.cell.cell import Cell
from openpyxl.styles.named_styles import NamedStyle
from openpyxl.workbook.workbook import Workbook
from openpyxl.styles.alignment import Alignment
from openpyxl.styles.borders import Border
from openpyxl.styles.fills import PatternFill
from openpyxl.styles.fonts import Font
from openpyxl.styles.protection import Protection
from const_values import STYLES_CONST
import logging
from logging import Logger


logger: Logger = logging.getLogger(f"main.{__name__}")


class _Style(NamedTuple):
    """
    Specify the cell style.

    Params:
        name --- the style name;\n
        is_named --- flag to convert to the NamedStyle instance;\n
        number_format --- the style number format;\n
        alignment --- the style alignment;\n
        border --- the style border;\n
        fill --- the style fill;\n
        font --- the style font;\n
        protection --- the style protection;\n
        data_type --- the style data type, default: "n";\n
    """
    name: str
    is_named: bool
    number_format: str
    alignment: Alignment
    border: Border
    fill: PatternFill
    font: Font
    protection: Protection
    data_type: str = "n"

    def __str__(self):
        return f"Style: name = {self.name}, is_named = {self.is_named}, number_format = {self.number_format}, " \
               f"alignment = {self.alignment}, border = {self.border}, fill = {self.fill}, font = {self.font}, " \
               f"protection = {self.protection}, data_type = {self.data_type}"

    def __repr__(self):
        return f"<_Style(name={self.name}, is_named={self.is_named}, number_format={self.number_format}, " \
               f"alignment={self.alignment}, border={self.border}, fill={self.fill}, font={self.font}, " \
               f"protection={self.protection}, data_type={self.data_type})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return str(self)

    def get_named(self):
        """
        Convert the style to the NamedStyle instance if required.

        :returns: the named style or leave the style as is.
        :rtype: NamedStyle or _Style
        """
        if self.is_named:
            return NamedStyle(
                name=self.name, font=self.font, fill=self.fill, border=self.border,
                alignment=self.alignment, number_format=self.number_format, protection=self.protection)
        else:
            return self

    @classmethod
    def set_style(cls, style_attrs: tuple):
        if len(style_attrs) < 9:
            logging.exception(f"The style {filter(lambda x: x, style_attrs)} is not appropriate", exc_info=True)
            raise ValueError
        try:
            name, is_named, number_format, alignment, border, fill, font, protection, data_type = style_attrs
            style: _Style = _Style(name, is_named, number_format, alignment, border, fill, font, protection, data_type)
        except ValueError:
            logging.exception(
                f"The style {filter(lambda x: x, style_attrs)} is not appropriate to set the style", exc_info=True)
            raise
        else:
            return style


@cache
def const_styles() -> list[_Style]:
    """
    Generate the main styles.

    :return: the styles.
    :rtype: list[_Style]
    """
    return [_Style.set_style(style) for style in STYLES_CONST]


# noinspection GrazieInspection
class _StyleList:
    """
    Define the style list.

    Class params:
        attrs --- the cell attributes;

    Params:
        styles --- the dictionary of the styles, dict[style_name, _Style];

    Functions:
        add_styles_wb(wb, style_name) --- add the named styles to the workbook;\n
        get_wb_style(wb, style_name) --- add the style from the workbook;\n
        set_style(style_name, cell) --- set the style to the cell;\n
    """

    _attrs = ("number_format", "alignment", "border", "fill", "font", "protection", "data_type")

    def __init__(self):
        self.styles: dict[str, Union[_Style, NamedStyle]] = dict()
        style: _Style
        for style in const_styles():
            self.styles[style.name] = style.get_named()

    def __str__(self):
        str_styles: list[str] = [style_name for style_name in self.styles]
        unified_str_styles: str = ", ".join(str_styles)
        return f"_StyleList: styles: {unified_str_styles}"

    def __repr__(self):
        repr_styles: list[str] = [repr(style_name) for style_name in self.styles]
        unified_repr_styles: str = ",".join(repr_styles)
        return f"<_StyleList(styles={unified_repr_styles})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return repr(self)

    def __getitem__(self, key: str):
        if key in self.styles:
            return self.styles.get(key)
        else:
            print(f"Стиль {key} не найден.")
            return

    def __setitem__(self, key: str, value: Any):
        if key not in self.styles:
            self.styles[key] = value
        else:
            print(f"Стиль {key} уже создан.")

    def add_styles_wb(self, wb: Workbook, style_name: str):
        """
        Add the named styles to the workbook.

        :param wb: the workbook
        :type wb: Workbook
        :param str style_name: the style name
        :return: None.
        """
        style: Union[_Style, NamedStyle] = self.styles[style_name]
        if isinstance(style, NamedStyle) and style_name not in wb.style_names:
            wb.add_named_style(style_name)
            logger.info(f"The style {style_name} is added to the Workbook")
            print(f"The style {style_name} is added to the Workbook")
        return

    def get_wb_style(self, wb: Workbook, style_name: str):
        """
        Add the named style from the workbook to the style list.

        :param Workbook wb: the workbook
        :param str style_name: the style name
        :return: None.
        """
        if style_name in wb.style_names:
            style: NamedStyle
            # noinspection PyProtectedMember
            for style in wb._named_styles:
                if style.name == style_name:
                    style_item = NamedStyle(
                        name=style_name, number_format=style.number_format, alignment=style.alignment,
                        border=style.border, fill=style.fill, font=style.font, protection=style.protection)
                    self.styles[style_name] = style_item
                    logger.info(f"The style {style_name} is added to the _StyleList")
        else:
            logger.info(f"The style {style_name} is absent")
        return

    def set_style(self, style_name: str, cell: Cell):
        """
        Specify the style of the cell.

        :param str style_name: the style name
        :param cell: the cell
        :type cell: Cell
        :return: None.
        """
        style: Union[_Style, NamedStyle] = self[style_name]
        if isinstance(style, NamedStyle):
            # noinspection PyDunderSlots,PyUnresolvedReferences
            cell.style = style.name
        elif isinstance(style, _Style):
            for attr in _StyleList._attrs:
                setattr(cell, attr, getattr(style, attr))
        else:
            logger.warning(f"Стиль {style_name} к ячейке {cell.coordinate} не применен.")
        return
