#!/usr/bin/python
# -*- coding: cp1251 -*-
from collections import Counter
from functools import cached_property
from pathlib import Path
from typing import Optional, Union
from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.cell.cell import Cell
from openpyxl.utils.cell import coordinate_to_tuple, get_column_letter
from decimal import Decimal, ROUND_CEILING
from xl_style import _StyleList
from copy import copy
from numpy.core import datetime64, isnat
from const_values import DICT_STATE_STYLE, DICT_HEADERS, DICT_HEADERS_SHORT, year_days, business_week_days, \
    MONTH_COLUMNS, DICT_STATE_PRIORITY, DICT_ATTR_COLUMN, current_year, first_day, spec_days


__doc__ = """
The openpyxl module does not work properly with the formulae.
Since the dates in the first row are specified through the formulae,
they have to be operated as objects not related to the table.
So, it is assumed that the first cell associated with the January
is <Cell "Sheet_name".F1>.

Functions:
    check_year(date) --- verify if the date is in the current year;\n
    check_coord(coord) --- validate the cell coordinate;\n
    range_coord(start_coord, end_coord) --- convert the range string to the tuple;\n
    _index(state) --- get the state index.
"""


def check_year(date: datetime64) -> bool:
    """
    Verify if the date is in the current year.

    :param date: the date to verify
    :type date: datetime64
    :return: the verification flag.
    :rtype: bool
    """
    return date.item().year == current_year() if not isnat(date) else False


def check_coord(coord: str) -> bool:
    """
    Validate the cell coordinate.

    :param str coord: the cell coordinates
    :return: the validation flag.
    :rtype: bool
    """
    try:
        coordinate_to_tuple(coord)
    except TypeError as e:
        print(f'TypeError {str(e)}. Incorrect value type.\n')
        print(f'coordinate = {coord}')
        return False
    except ValueError as e:
        print(f'ValueError {str(e)}. Incorrect value.\n')
        print(f'coordinate = {coord}')
        return False
    except OSError as e:
        print(f'OSError {e.errno}, {e.strerror}.\n')
        print(f'coordinate = {coord}')
        return False
    else:
        return True


def range_coord(start_coord: str, end_coord: str):
    """
    Convert the range string with the start and end coordinates to the tuple:\n
    (min_row, min_col, max_row, max_col)\n

    :param start_coord: the start cell coordinate, str
    :param end_coord: the end cell coordinate, str
    :return: the values for iteration of the tuple[int, int, int, int] type.
    """
    # convert start coordinate
    start_row, start_column = coordinate_to_tuple(start_coord)
    # convert end coordinate
    end_row, end_column = coordinate_to_tuple(end_coord)
    # find min and max values
    min_row = min(start_row, end_row)
    max_row = max(start_row, end_row)
    min_col = min(start_column, end_column)
    max_col = max(start_column, end_column)
    return min_row, max_row, min_col, max_col


def _index(state: str) -> int:
    """
    Get the state index.

    :param str state: the state
    :return: the header index.
    :rtype: int
    """
    if state in DICT_HEADERS_SHORT:
        return DICT_HEADERS_SHORT[state]
    else:
        print(f"Неопознанное состояние {state}.")
        return -1


def get_excel_prop(path: Union[Path, str], styles: _StyleList):
    """
    Specify the ExcelProp instance.

    :param path: the path to the Excel file
    :type path: Path or str
    :param styles: the worksheet styles
    :type styles: _StyleList
    :return: the ExcelProp instance.
    :rtype: ExcelProp
    """
    wb: Workbook = load_workbook(path)
    new_styles: tuple[str, ...] = (
        'weekend', 'deadline', 'done', 'active', 'test', 'going_start', 'paused', 'verified_closed',
        'going_finish', 'sick', 'vacation', 'Normal'
    )
    if wb.properties.title:
        login = wb.properties.title
    else:
        file_name: str = Path(path).stem
        if file_name.startswith("2022_report_"):
            login = file_name.replace("2022_report_", "")
        elif file_name.startswith("generated_2022_report_"):
            login = file_name.replace("generated_2022_report_", "")
        else:
            print(f"Файл {path} имеет нестандартное имя.")
            return
    for style in new_styles:
        styles.get_wb_style(wb, style)

    # wb.add_named_style(styles.styles["header_text"])
    return ExcelProp(wb, styles, login)


class ExcelProp:
    """
    Define the Excel Worksheet properties.

    Class params:
        dict_table_rows --- mapping TableRow instances;\n

    Params:
        wb --- the Excel workbook;\n
        styles --- the styles implemented into the worksheet;\n
        login --- the user login in the YouTrack, default = None;\n
        ws --- the worksheet;\n
        dict_headers --- mapping states and indexes;\n
        dict_headers_short --- mapping issue states and indexes;\n
        dict_state_priority --- mapping states and priorities;\n
        dict_state_style --- mapping states and cell styles;\n

    Properties:
        bus_columns -- the work day date columns;\n
        cell_states --- mapping cell coordinates and states;\n
        dict_days --- mapping days and column indexes;\n
        first_shift --- the shift of the first date;\n
        headers --- the header cells;\n
        headers_row --- the header cell rows;\n
        headers_row_short --- the header cell rows without the legend;\n
        headers_short --- the header cells without the legend;\n
        reversed_dict_days --- mapping column indexes and days;\n
        table_cells --- the cells for the TableRow instances;\n
        table_row_names --- the issue names;\n
        weekend_columns --- the weekend date columns;\n
        year_days_indexes --- the day column indexes;\n

    Functions:
        _check_empty(item) --- verify if the Cell, coordinate, or row is empty;\n
        _combine_headers() --- merge the header cells;\n
        _empty_rows() --- delete all empty rows except for the pre-headers;\n
        _index_row_state(state, *, state_index = 0, row_index = 0) --- specify the rows between the state-based
        headers;\n
        _separate_headers() --- unmerge the header cells;\n
        cell_in_range(start_coord, end_coord) --- generate the cells in the range;\n
        close(path_file) --- specify the exit method;\n
        default_row(row) --- specify the default row;\n
        delete_row(row) --- delete the row from the table;\n
        get_column_by_date(date) --- get the column letter for the specified date;\n
        get_date_by_cell(cell) --- get the date associated with the cell;\n
        insert_row(row) --- add the row to the table;\n
        post_processing() --- post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues;\n
        pre_processing() --- pre-process the table to get rid of empty rows and repeating issues;\n
        process_non_yt() --- process the self-made issues;\n
        replace_cell(*, from_, to_) --- replace the cell attribute values to another cell;\n
        update_header() --- specify the style to the header rows;\n
        update_table_cells() --- update the TableRow instances;\n
    """
    dict_table_rows = dict()

    def __init__(self, wb: Workbook, styles: _StyleList, login: str = None):
        if wb.properties.title:
            login = wb.properties.title
        self.wb = wb
        self.ws = wb.worksheets[1]
        self.styles = styles
        self.login = login

    def __str__(self):
        return f"ExcelProp: worksheet = {self.ws.title}, login = {self.login}"

    def __repr__(self):
        return f"<ExcelProp(wb={repr(self.wb)}, styles={repr(self.styles)}, login={self.login})>"

    def __format__(self, format_spec):
        if format_spec == "base":
            return str(self)
        elif format_spec == "row_equal":
            return repr(self)
        else:
            return f"{self.ws.title}"

    def __hash__(self):
        return hash(self.ws.title)

    def __key(self):
        return self.ws.title, self.ws.parent

    def __eq__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @cached_property
    def bus_columns(self) -> tuple[str, ...]:
        """
        Specify the business day columns.

        :return: the business day columns.
        :rtype: tuple[str]
        """
        return tuple(self.get_column_by_date(bus_day) for bus_day, is_business
                     in zip(year_days(), business_week_days()) if is_business)

    @property
    def cell_states(self) -> dict[str, str]:
        """
        Get mapping cell coordinates and states.

        :return: the cell coordinates and state mapping.
        :rtype: dict[str, str]
        """
        dict_cell_states: dict[str, str] = dict()
        for state in DICT_HEADERS_SHORT:
            # start from the row right after the header
            start_row = self._index_row_state(state, row_index=1)
            # stop on the row right before the next header
            end_row = self._index_row_state(state, state_index=1)
            for cell in self.cell_in_range(f"C{start_row}", f"C{end_row}"):
                if not self._check_empty(cell.row):
                    dict_cell_states[cell.coordinate] = state
        return dict_cell_states

    @cached_property
    def dict_days(self) -> dict[datetime64, int]:
        """
        Specify mapping days and column indexes.

        :return: the dict of the days and indexes.
        :rtype: dict[datetime64[D], int]
        """
        return dict(zip(year_days(), self.year_days_indexes))

    @cached_property
    def first_shift(self) -> Optional[int]:
        """
        Specify the shift of the first date.

        :return: the first date column index.
        :rtype: int or None
        """
        row: tuple[Cell, ...]
        cell: Cell
        for cell in self.cell_in_range("A1", "M1"):
            if isinstance(cell.value, str):
                continue
            if not isnat(datetime64(cell.value, "D")) and datetime64(cell.value, "D") == first_day():
                return cell.col_idx
            else:
                continue
        return

    @property
    def headers(self) -> list[Cell]:
        """
        Get the header cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        end_coord = f"B{self.ws.max_row}"
        return [cell for cell in self.cell_in_range("B3", end_coord) if cell.value in DICT_HEADERS]

    @property
    def headers_short(self) -> list[Cell]:
        """
        Get the header cells without the legend.

        :return: the cells.
        :rtype: list[Cell]
        """
        return self.headers[:4]

    @property
    def headers_row(self) -> list[int]:
        """
        Get the header row values.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return [header.row for header in self.headers]

    @property
    def headers_row_short(self) -> list[int]:
        """
        Get the header row values without the legend.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return self.headers_row[:4]

    @cached_property
    def reversed_dict_days(self) -> dict[int, datetime64]:
        """
        Specify mapping column indexes and days.

        :return: the dict of the indexes and days.
        :rtype: dict[int, datetime64[D]]
        """
        return dict(zip(self.year_days_indexes, year_days()))

    @property
    def table_cells(self) -> list[Cell]:
        """
        Get the cells of the TableRow instances.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [self.ws[f"{coord}"] for coord in self.cell_states if self.ws[f"{coord}"].value]

    @property
    def table_row_names(self) -> set[str]:
        """
        Get the issue names.

        :return: the issue names.
        :rtype: set[str]
        """
        return {cell.value for cell in self.table_cells}

    @cached_property
    def weekend_columns(self) -> tuple[str, ...]:
        """
        Specify the weekend columns.

        :return: the weekend columns.
        :rtype: tuple[str]
        """
        return tuple(self.get_column_by_date(bus_day) for bus_day, is_business
                     in zip(year_days(), business_week_days()) if not is_business)

    @cached_property
    def year_days_indexes(self) -> list[int]:
        """
        Specify the day column indexes.

        :return: the column indexes.
        :rtype: list[int]
        """
        index: int
        date: datetime64
        return [index + date.item().month - 1 + self.first_shift
                for index, date in enumerate(year_days())]

    def _check_empty(self, item: Union[int, str, Cell]) -> bool:
        """
        Verify if the cell, cell coordinates, or cell row is empty.

        :param item: the cell value
        :type item: int or str or Cell
        :return: the emptiness flag.
        :rtype: bool
        """
        if isinstance(item, Cell):
            return True if not item.value else False
        elif isinstance(item, str):
            return True if not self.ws[f"{item}"].value else False
        elif isinstance(item, int):
            cell_values = (
                self.ws[f"B{item}"].value, self.ws[f"C{item}"].value,
                self.ws[f"D{item}"].value, self.ws[f"E{item}"].value)
            if any(value for value in cell_values):
                return False
            else:
                return True

    def _combine_headers(self):
        """Merge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            self.ws.merge_cells(f"B{header_row}:E{header_row}")
            self.update_header()
        # the legend header
        header_legend_row = self.headers_row[4]
        self.ws.merge_cells(f"B{header_legend_row}:C{header_legend_row}")
        return

    def _empty_rows(self):
        """Delete all empty rows except for the pre-headers."""
        list_empty = []
        for state in DICT_HEADERS_SHORT:
            low_limit = self._index_row_state(state, row_index=1)
            high_limit = self._index_row_state(state, state_index=1, row_index=-1)
            for row in range(low_limit, high_limit):
                if self._check_empty(row):
                    list_empty.append(row)

        for empty_row in sorted(list_empty, reverse=True):
            self.delete_row(empty_row)
        return

    def _index_row_state(self, state: str, *, state_index: int = 0, row_index: int = 0) -> int:
        """
        Specify the row between the headers based on the state.

        :param str state: the state
        :param int state_index: the state index shift inside the dict key, default = 0
        :param int row_index: the row index shift after the dict key, default = 0
        :return: the row number.
        :rtype: int
        """
        return self.headers_row[_index(state) + state_index] + row_index

    def _separate_headers(self):
        """Unmerge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            start_coord = f"B{header_row}"
            end_coord = f"E{header_row}"
            self.ws.unmerge_cells(f"{start_coord}:{end_coord}")
            for cell_header in self.cell_in_range(start_coord, end_coord):
                cell_header.style = "basic_issue"
        # the legend header
        header_legend_row = self.headers_row[4]
        start_legend_coord = f"B{header_legend_row}"
        end_legend_coord = f"C{header_legend_row}"
        self.ws.unmerge_cells(f"{start_legend_coord}:{end_legend_coord}")
        for cell_legend in self.cell_in_range(start_legend_coord, end_legend_coord):
            cell_legend.style = "basic_issue"
        return

    def cell_in_range(self, start_coord: str, end_coord: str):
        """
        Convert the cell range to the cell generator in the range.

        :param str start_coord: the start cell coordinates
        :param str end_coord: the end cell coordinates
        :return: the generator of cells.
        """
        if check_coord(coord=start_coord) and check_coord(coord=end_coord):
            min_row, max_row, min_col, max_col = range_coord(start_coord=start_coord, end_coord=end_coord)
            for row in range(min_row, max_row + 1):
                for col in range(min_col, max_col + 1):
                    cell: Cell = self.ws[f'{get_column_letter(col)}{row}']
                    yield cell

    def close(self, path_file: Union[Path, str]):
        """
        Specify the exit method.

        :param path_file: the path to the file to save the current work
        :return: None.
        """
        self.wb.properties.creator = self.login
        self.wb.save(path_file)
        self.wb.close()

    def default_row(self, row: int):
        """
        Specify the default row when added.

        :param int row: the row number to add to the table
        :return: None.
        """
        # the basic
        for column in ("B", "C", "D", "NT"):
            self.ws[f"{column}{row}"].style = "basic_issue"
        # the cells with dates
        # the business days
        for column in self.bus_columns:
            self.ws[f"{column}{row}"].style = "basic"
        # the weekends
        for column in self.weekend_columns:
            self.ws[f"{column}{row}"].style = "weekend"
        # the business days
        for column in MONTH_COLUMNS:
            self.styles.set_style("header_month", self.ws[f"{column}{row}"])
        # the issue deadline
        self.styles.set_style("deadline_issue", self.ws[f"E{row}"])
        # the sick and vacation days
        for spec_column, style in spec_days()[self.login]:
            self.ws[f"{spec_column}{row}"].style = style
        # the total spent time cell
        self.styles.set_style("sum", self.ws[f"NS{row}"])
        self.styles.set_style("basic", self.ws[f"BT{row}"])
        return

    def delete_row(self, row: int, number: int = 1):
        """
        Delete the row from the table.

        :param int row: the table row number
        :param int number: the number of rows
        :return: None.
        """
        if number == 0:
            return
        abs_number = number if number > 0 else -number
        idx = 0
        self._separate_headers()
        while idx < abs_number:
            self.ws.delete_rows(row)
            idx += 1
        self._combine_headers()
        return

    def get_column_by_date(self, date_year: datetime64) -> Optional[str]:
        """
        Get the column letter of the date.

        :param date_year: the date
        :type date_year: datetime64
        :return: the cell column letter.
        :rtype: str or None
        """
        if check_year(date_year):
            index = self.dict_days[date_year]
            return get_column_letter(index)
        else:
            print(f"День {date_year.item().isoformat()} не в этом году.")
            return

    def get_date_by_cell(self, cell: Cell) -> datetime64:
        """
        Get the date associated with the cell.

        :param cell: the table cell
        :type cell: Cell
        :return: the date.
        :rtype: datetime64
        """
        col_idx = cell.col_idx - self.first_shift
        return self.reversed_dict_days[col_idx] if col_idx in self.reversed_dict_days else datetime64("NaT")

    def insert_row(self, row: int, number: int = 1):
        """
        Add the row to the table.

        :param int row: the table row number
        :param int number: the number of rows
        :return: None.
        """
        if number == 0:
            return
        abs_number = number if number > 0 else -number
        idx = 0
        self._separate_headers()
        while idx < abs_number:
            self.ws.insert_rows(row)
            self.default_row(row)
            idx += 1
        self._combine_headers()
        return

    def pre_processing(self):
        """Pre-process the table to get rid of empty rows and repeating issues."""
        counter = Counter([cell.value for cell in self.table_cells])
        # get non-unique issues
        non_unique = [key for key, value in counter.items() if value > 1]
        for issue in non_unique:
            row_eq = _RowEqual(self, issue)
            row_eq.join_cells()
        # delete empty rows
        self._empty_rows()
        # initiate the TableRow instances
        self.update_table_cells()
        return

    def post_processing(self):
        """Post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues."""
        self.update_table_cells()

        for cell in self.table_cells:
            table_row = TableRow(self, cell)
            table_row.post_processing()

        for cell in self.headers_short:
            cell.style = "header_text"

        max_row = max(table_cell.row for table_cell in self.table_cells)
        table_row_rows = [cell_table.row for cell_table in self.table_cells]
        for cell in self.cell_in_range("NS4", f"NS{max_row}"):
            if cell.row not in (*table_row_rows, *self.headers_row):
                cell.data_type = "f"
                cell.value = f"=SUM(G{cell.row}:NR{cell.row})"

        self.process_non_yt()
        return

    def process_non_yt(self):
        """Specify the self-made issues with no identifier in the YouTrack."""
        # specify the self-made issue rows
        non_yt_rows = [coordinate_to_tuple(coord)[0] for coord in self.cell_states
                       if not self.ws[f"{coord}"].value]
        # specify the self-made work item cells
        non_yt_cells = [cell for row in non_yt_rows
                        for cell in self.cell_in_range(f"G{row}", f"NS{row}") if cell.value]
        # apply the active style to the work item cells
        for cell in non_yt_cells:
            if cell.style in ("basic", "Normal"):
                cell.style = "active"

    def replace_cell(self, *, from_: Union[Cell, str], to_: Optional[Union[Cell, str]] = None):
        """
        Replace the cell attribute values to another cell. If to_ is None, the values are deleted.

        :param from_: the original cell or cell coordinates
        :type from_: Cell or str
        :param to_: the destination cell or cell coordinates
        :type to_: Cell or str or None
        :return: None.
        """
        cell_proxy: Optional[Cell]
        cell_base: Cell
        # if the cell coordinates
        if isinstance(from_, str):
            cell_base = self.ws[f"{from_}"]
        # if the cell
        else:
            cell_base = from_
        # if the destination cell exists
        if to_:
            if isinstance(to_, str):
                cell_proxy = self.ws[f"{to_}"]
            else:
                cell_proxy = to_
            # copy all required attribute values
            # if the destination cell is empty
            if not cell_proxy.value:
                cell_proxy.value = copy(cell_base.value)
            if cell_base.has_style:
                self.styles.set_style(cell_base.style, cell_proxy)
            else:
                cell_proxy.number_format = copy(cell_base.number_format)
                cell_proxy.style = "basic"
        # set the default cell attribute values
        cell_base.value = None
        cell_base.style = "basic"
        return

    def update_header(self):
        """Specify the style to the header rows."""
        for cell in self.headers_short:
            cell.style = "basic_issue"
            self.styles.set_style("header_text", cell)
        for row in self.headers_row_short:
            for cell in self.cell_in_range(f"F{row}", f"NR{row}"):
                self.styles.set_style("header", cell)
            for column in MONTH_COLUMNS:
                self.styles.set_style("header_no_border", self.ws[f"{column}{row}"])
        return

    def update_table_cells(self):
        """Update the TableRow instances."""
        for cell in self.table_cells:
            TableRow(self, cell)
        return


class TableRow:
    """
    Define the table row.

    Params:
        excel_prop --- the ExcelProp instance;\n
        cell --- the base cell;\n
        issue --- the base cell value;\n

    Properties:
        ws --- the worksheet;\n
        cell_last --- the last work item cell;\n
        deadline --- the issue deadline;\n
        parent --- the parent issue;\n
        row --- the row;\n
        state --- the state;\n
        summary --- the issue summary;\n
        comment --- the issue comment;\n

    Functions:
        _shift_cell(shift) --- get the shifted cell in the same row;\n
        add_work_item(work_item) --- add the work item to the row;\n
        mapping_work_item_cell(work_item) --- convert the work item to the cell;\n
        post_processing() --- process the work cells after completing the issue modifications;\n
        proper_cell_style(cell) --- get the cell style based on the state;\n
        pyxl_cells() --- get the work item cells;\n
        replace_issue(row) --- replace the issue attribute values to the new row;\n
        work_items() --- get the work items;\n
    """

    def __init__(self, excel_prop: ExcelProp, cell: Cell):
        self.excel_prop = excel_prop
        self.cell = cell
        self.issue = str(cell.value)

        self.excel_prop.dict_table_rows[self.issue] = self

    def __str__(self):
        return f"TableRow cell: {self.cell.coordinate}"

    def __repr__(self):
        return f"<TableRow(excel_prop={repr(self.excel_prop)}, cell={repr(self.cell)})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            lines: list[str] = []
            if isnat(self.deadline):
                deadline_string = "не задан"
            else:
                deadline_string = self.deadline.item().isoformat()
            lines.append(f"Задача {self.issue}, {self.summary}, состояние {self.state}, дедлайн: {deadline_string}.")
            for _, date, spent_time in self.work_items():
                lines.append(f"Затраченное время: {spent_time}, дата: {date.item().isoformat()}")
            return "\n".join(lines)

    def __hash__(self):
        return hash((self.cell.coordinate, self.issue))

    def __key(self):
        return self.ws.title, self.cell.coordinate, self.issue

    def __eq__(self, other):
        if isinstance(other, TableRow):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, TableRow):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, TableRow):
            return self.row < other.row
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, TableRow):
            return self.row > other.row
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, TableRow):
            return self.row <= other.row
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, TableRow):
            return self.row >= other.row
        else:
            return NotImplemented

    def __len__(self):
        return len(self.work_items())

    def __bool__(self):
        return len(self) > 0

    @property
    def ws(self) -> Worksheet:
        """
        Get the ExcelProp worksheet.

        :return: the worksheet.
        :rtype: Worksheet
        """
        return self.excel_prop.ws

    @property
    def cell_last(self) -> Optional[Cell]:
        """
        Get the last work item cell.

        :return: the last work item cell.
        :rtype: Cell
        """
        if self.pyxl_cells():
            max_col_idx = max(cell.col_idx for cell in self.pyxl_cells())
            return self.ws[f"{get_column_letter(max_col_idx)}{self.row}"]
        return

    @property
    def deadline(self) -> datetime64:
        """
        Get the issue deadline if exists.

        :return: the deadline.
        :rtype: datetime64
        """
        return datetime64(self._shift_cell(2).value, "D") if self._shift_cell(2).value else datetime64("NaT")

    @property
    def deadline_year(self) -> Optional[int]:
        """
        Get the deadline year.

        :return: the deadline year.
        :rtype: int or None
        """
        return self.deadline.item().year if self.deadline.item() else None

    @property
    def parent(self) -> Optional[str]:
        """
        Get the parent issue name if exists.

        :return: the parent issue name.
        :rtype: str or None
        """
        return self._shift_cell(-1).value

    @property
    def row(self) -> int:
        """
        Get the base cell row.

        :return: the cell row.
        :rtype: int
        """
        return self.cell.row

    @property
    def state(self) -> str:
        """
        Get the issue state.

        :return: the state.
        :rtype: str
        """
        return self.excel_prop.cell_states[self.cell.coordinate]

    @property
    def summary(self) -> str:
        """
        Get the issue summary.

        :return: the issue summary.
        :rtype: str
        """
        return self._shift_cell(1).value

    @property
    def comment(self) -> str:
        """
        Get the issue comment.

        :return: the comment from the YouTrack.
        :rtype: str
        """
        return self.ws[f"NT{self.row}"].value

    def _shift_cell(self, shift: int) -> Optional[Cell]:
        """
        Get the shifted cell in the same row.

        :param int shift: the cell shift
        :return: the shifted cell.
        :rtype: Cell or None
        """
        col_idx = self.cell.col_idx + shift
        if col_idx >= 1:
            column_letter = get_column_letter(col_idx)
            return self.ws[f"{column_letter}{self.row}"]
        else:
            print(f"ValueError, the shift {shift} is out of bounds.")
            return None

    def add_work_item(self, work_item: tuple[str, datetime64, Decimal, str]):
        """
        Add the work item to the row.

        :param work_item: the work item
        :type work_item: tuple[str, datetime64, Decimal]
        :return: None.
        """
        _, date, spent_time, comment = work_item
        cell = self.mapping_work_item_cell((self.issue, date, spent_time))
        cell.data_type = "n"
        cell.value = spent_time
        coord = f"NT{cell.row}"
        if comment:
            if self.comment:
                final_comment = "\n".join((self.comment, comment))
            else:
                final_comment = comment
            self.ws[coord].value = final_comment
        return

    def mapping_work_item_cell(self, work_item: tuple[str, datetime64, Decimal]) -> Cell:
        """
        Convert the work item to the cell.

        :param work_item: the work item
        :type work_item: tuple[str, datetime64, Decimal]
        :return: the cell.
        :rtype: Cell
        """
        _, date, _ = work_item
        column = self.excel_prop.get_column_by_date(date)
        if column:
            return self.ws[f"{column}{self.row}"]

    def post_processing(self):
        """Process the work cells after completing the issue modifications:\n
            the deadline style;\n
            the formula;\n
            the issue and parent issue hyperlinks;\n
            the proper cell styles."""
        # specify the deadline style
        cell_deadline = self._shift_cell(2)
        self.excel_prop.styles.set_style("deadline_issue", cell_deadline)
        if isnat(self.deadline):
            cell_deadline.value = None
            cell_deadline.data_type = "n"
        else:
            cell_deadline.value = self.deadline.item()
            cell_deadline.data_type = "d"
        # specify the formula
        cell_formula: Cell = self.ws[f"NS{self.row}"]
        cell_formula.value = f"=SUM(G{self.row}:NR{self.row})"
        cell_formula.data_type = "f"
        # specify the issue hyperlink if not self-made
        if self.issue:
            self.cell.hyperlink = f"https://youtrack.protei.ru/issue/{self.issue}"
        # specify the parent issue hyperlink if exists
        if self.parent:
            self._shift_cell(-1).hyperlink = f"https://youtrack.protei.ru/issue/{self.parent}"
        # specify the proper style if the cell is not defined for more prior state
        for cell in self.pyxl_cells():
            if cell.style not in ("weekend", "deadline", "sick", "vacation"):
                apply_style = self.proper_cell_style(cell)
                self.excel_prop.styles.set_style(apply_style, cell)
            else:
                continue
        # specify the cell style associated with the deadline if exists
        if not isnat(self.deadline) and self.deadline_year == current_year():
            column = self.excel_prop.get_column_by_date(self.deadline)
            self.ws[f"{column}{self.row}"].style = "deadline"
        return

    def proper_cell_style(self, cell: Cell) -> str:
        """
        Get the cell style based on the state.

        :param cell: the cell
        :type cell: Cell
        :return: the style name.
        :rtype: str
        """
        # if the cell is not a work item
        if cell not in self.pyxl_cells():
            return cell.style
        # if the cell is not the last in the row
        elif cell.coordinate != self.cell_last.coordinate:
            return "active"
        # the rest options
        elif self.state:
            return DICT_STATE_STYLE[self.state]
        # if the error occurred
        else:
            print(f"Должный стиль задан некорректно. Ячейка: {cell.coordinate}, задача: {self.issue}")
            print(f"зарегистриованное время: {str(self.pyxl_cells())}.\nДедлайн: {self.deadline}, "
                  f"последняя ячейка: {self.cell_last.coordinate}, статус: {self.state}")
            return "basic"

    def pyxl_cells(self) -> list[Cell]:
        """
        Get the work item cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        cell: Cell
        return [cell for cell in self.excel_prop.cell_in_range(f"G{self.row}", f"NR{self.row}") if cell.value]

    def replace_issue(self, row: int):
        """
        Replace the issue attribute values to the new row.

        :param int row: the new row
        :return: None.
        """
        old_row = self.row
        for column in DICT_ATTR_COLUMN.values():
            self.excel_prop.replace_cell(from_=self.ws[f"{column}{old_row}"], to_=self.ws[f"{column}{row}"])
            self.ws[f"{column}{old_row}"].value = None
            self.ws[f"{column}{old_row}"].style = "basic"
        return

    def work_items(self) -> list[tuple[str, datetime64, Decimal]]:
        """
        Get the work items.

        :return: the work items.
        :rtype: list[tuple[str, datetime64, Decimal]]
        """
        return [(self.issue, self.excel_prop.get_date_by_cell(cell),
                 Decimal(cell.value).quantize(Decimal("1.00"), ROUND_CEILING)) for cell in self.pyxl_cells()]


class _RowEqual:
    """
    Specify the instance to work with the issues having the same names.

    Params:
        excel_prop --- the ExcelProp instance;\n
        issue --- the issue name;\n

    Properties:
        cells --- the cells with the same issue names;\n
        dict_issues --- mapping issue cells and state priorities;\n

    Functions:
        cell_final() --- get the final cell to keep in the table;\n
        join_cells() --- join the cells into the one issue;\n
        work_item_cells() --- get the cells to join;\n
    """

    def __init__(self, excel_prop: ExcelProp, issue: str):
        self.excel_prop = excel_prop
        self.issue = issue

    def __str__(self):
        cell_string = ", ".join([cell.coordinate for cell in self.cells])
        return f"_RowEqual: issue name - {self.issue},\ncells - {cell_string}"

    def __repr__(self):
        return f"_RowEqual({format(self.excel_prop, 'row_equal')}, {self.issue})"

    def __len__(self):
        return len(self.cells)

    def __bool__(self):
        return len(self.cells) > 1

    @property
    def cells(self) -> list[Cell]:
        """
        Get the issue cells with the same names.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [cell for cell in self.excel_prop.table_cells if cell.value == self.issue]

    @property
    def dict_issues(self) -> dict[Cell, int]:
        """
        Get mapping issue cells and state priorities.

        :return: the cell and state priority mapping.
        :rtype: dict[Cell, int]
        """
        dict_cell_priority: dict[Cell, int] = dict()
        for cell in self.cells:
            state = self.excel_prop.cell_states[cell.coordinate]
            dict_cell_priority[cell] = DICT_STATE_PRIORITY[state]
        return dict_cell_priority

    def cell_final(self) -> Cell:
        """
        Get the final cell to keep.

        :return: the cell
        :rtype: Cell
        """
        max_priority = max(priority for priority in self.dict_issues.values())
        max_prior_min_row = min(cell.row for cell, priority in self.dict_issues.items() if priority == max_priority)
        for cell in self.cells:
            if cell.row == max_prior_min_row:
                return cell

    def join_cells(self):
        """Join the work items to the final issue."""
        for work_item_cell in self.work_item_cells():
            self.excel_prop.replace_cell(
                from_=work_item_cell, to_=f"{work_item_cell.column_letter}{self.cell_final().row}")
        # replace all another rows
        for issue_cell in self.cells:
            if issue_cell != self.cell_final():
                row = issue_cell.row
                for column in ("B", "C", "D", "E"):
                    self.excel_prop.replace_cell(from_=f"{column}{row}")

    def work_item_cells(self) -> list[Cell]:
        """
        Get the work item cells to join.

        :return: the work item cells.
        :rtype: list[Cell]
        """
        work_cells: list[Cell] = []
        for cell in self.cells:
            if cell == self.cell_final():
                continue
            else:
                for item in self.excel_prop.cell_in_range(f"G{cell.row}", f"NR{cell.row}"):
                    if item.value:
                        work_cells.append(item)
                    else:
                        continue
        return work_cells
