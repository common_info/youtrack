from functools import cached_property
from pathlib import Path
import json
import requests
from decimal import Decimal, ROUND_CEILING, ROUND_FLOOR
from json.decoder import JSONDecodeError
from typing import Union, Optional, Iterable, NamedTuple
from numpy.core import datetime64, isnat, divide
from numpy.core.fromnumeric import sum as np_sum
from requests.exceptions import RequestException, HTTPError, ConnectionError, InvalidURL, InvalidHeader
from collections import Counter
from const_values import convert_long_date, today, STATES_NOT_COMPLETED, STATES_NOT_COMPLETED_JOIN, \
    STATES_COMPLETED_JOIN, HEADERS_REQUEST, period, RequestURL, RequestFields, RequestCustomFields, ASSIGNEES_PROJECT


__doc__ = """
Functions:
    request(url, params) --- combine and send the GET request; if improper, print the error;\n
    convert_date_iso(input_date) --- convert different date formats to the ISO standard;\n
    parse_response_work_item(response_item) --- get the parameters from the response item;\n
    parse_response_issue(response_item) --- parse the response in the dict format to get the issue;\n
    get_current_states(issues) --- get the current states of the issues;\n
"""


def request(url: RequestURL, params: tuple[tuple[str, str], ...]) -> Union[list, Optional[dict]]:
    """
    Combine and send the GET request.\n
    If the request is improper, print the error.\n

    :param url: the URL to send the request
    :type url: RequestURL
    :param params: the HTTP parameters
    :type params: tuple[tuple[str, str]]
    :return: the response text or the JSON-converted object
    :rtype: list or dict[str, str] or dict[str, dict] or None
    """
    try:
        response = requests.request(method="get", url=url.value, headers=HEADERS_REQUEST, params=params).json()
    except InvalidURL as e:
        print(f"InvalidURL {e.errno}, {e.strerror}. Incorrect request URL.")
        return None
    except InvalidHeader as e:
        print(f"Invalid Header {e.errno}, {e.strerror}. Incorrect headers.")
        return None
    except ConnectionError as e:
        print(f"ConnectionError {e.errno}, {e.strerror}. Connection failed.")
        return None
    except HTTPError as e:
        print(f"HTTPError {e.errno}, {e.strerror}. Main HTTP request error")
        return None
    except RequestException as e:
        print(f"RequestError {e.errno}, {e.strerror}. Main request error.")
        return None
    except OSError as e:
        print(f"OSError {e.errno}, {e.strerror}. Global error occurred.")
        return None
    except JSONDecodeError as e:
        print(f"JSONDecodeError {e.msg}. Response cannot be parsed as JSON data.")
        return None
    else:
        return response


def parse_response_work_item(response_item: dict) -> tuple[str, datetime64, Decimal, str]:
    """
    Get the parameters from the response item.

    :param dict response_item: the item from the response
    :return: the issue name, date, and modified spent time.
    :rtype: tuple[str, datetime64, Decimal, str]
    """
    # define the issue name of the work item
    issue: str = response_item['issue']['idReadable']
    # define the date of the work item
    date: datetime64 = convert_long_date(response_item['date'])
    # define the spent time of the work item
    spent_time: int = response_item['duration']['minutes']
    # convert to the hours
    modified_spent_time: Decimal = Decimal(divide(spent_time, 60)).quantize(Decimal("1.00"), ROUND_CEILING)
    # define the comment
    comment: str = response_item['text']
    return issue, date, modified_spent_time, comment


def parse_response_issue(response_item: dict) -> tuple[str, str, str, Optional[str], Optional[datetime64]]:
    """
    Parse the response in the dict format to get Issue.

    :param response_item: the response to parse
    :type response_item: dict[str, Union[str, dict]]
    :return: the issue name, state, summary, parent issue name, and deadline.
    :rtype: tuple[str, str, str, str or None, datetime64 or None]
    """
    # specify the issue name
    issue: str = response_item["idReadable"]
    # specify the parent issue name
    parent_issues = response_item["parent"]["issues"]
    parent: Optional[str]
    # check if the parent issue exists
    if len(parent_issues):
        parent = parent_issues[0]["idReadable"]
    else:
        parent = None
    # specify the issue summary
    summary: str = response_item["summary"]
    deadline: datetime64 = datetime64("NaT")
    state: str = ""
    for item in response_item["customFields"]:
        # specify the issue state
        if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
            state = item["value"]["name"]
        # specify the issue deadline
        elif item["$type"] == "DateIssueCustomField":
            deadline = convert_long_date(item["value"])
        else:
            continue
    return issue, state, summary, parent, deadline


def get_current_states(issues: Optional[Iterable[str]]) -> Optional[dict[str, str]]:
    """
    Get the states of the issues.

    :param issues: the issue names
    :type issues: Iterable[str] or None
    :return: the dictionary of the issue names and states.
    :rtype: dict[str, str]
    """
    if not issues:
        return None
    string_issues = ",".join(issues)
    parameters_query = " ".join((f'issue ID:', f'{string_issues}'))
    params = (
        RequestFields.ISSUE_CURRENT.value,
        ('query', parameters_query),
        RequestCustomFields.STATE.value,
    )
    # get the response in the JSON format
    parsed_response = request(RequestURL.ISSUE, params)
    dict_issue_states: dict[str, str] = dict()
    for item in parsed_response:
        issue = item["idReadable"]
        state = item["customFields"][0]["value"]["name"]
        dict_issue_states[issue] = state
    return dict_issue_states


class MultipleConfig:
    """
    Specify the config with several logins.

    Params:
        path --- the path to the configuration file;\n
        json_file --- the serialized json file content;\n

    Properties:
        login --- the list of logins;\n
        path_dir --- th path to the directory with files;\n

    Functions:
        full_user_config() --- specify all user configs;\n
        path_table(login) --- get the path to the report;\n
        user_config(login) --- specify the user config;\n
    """

    def __init__(self, path: Union[str, Path], json_file: dict[str, Union[str, list[str]]]):
        self.path = Path(path).resolve()
        self.json_file = json_file

    def __str__(self):
        return f"MultipleConfig: {str(self.path)}"

    def __repr__(self):
        return f"<MultipleConfig(path={str(self.path)})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"MultipleConfig({str(self.path)})"

    @cached_property
    def login(self) -> list[str]:
        """
        Get the logins in the dict.

        :return: the logins.
        :rtype: list[str] or None
        """
        return [login for login in self.json_file["login"]]

    @property
    def path_dir(self) -> Path:
        """
        Get the path to the directory with the files.

        :return: the path to the directory.
        :rtype: Path
        """
        return Path(self.json_file["path_dir"]).resolve()

    def all_users(self):
        """
        Specify the user configs for all logins.

        :return: the list of user configs.
        :rtype: list[User]
        """
        return [User(login) for login in self.login]

    def path_table(self, login: str) -> Path:
        """
        Get the path to the report.

        :param str login: the login to get the report path
        :return: the path to the Excel file.
        :rtype: Path
        """
        return self.path_dir.joinpath(f"2022_report_{login}.xlsx")


def multiple_config() -> Optional[MultipleConfig]:
    """
    Specify the user configurations.

    :return: the user configurations.
    :rtype: list[UserConfig]
    """
    try:
        path = Path.cwd().resolve().joinpath("youtrack.json")
        with open(path, "r") as file:
            json_file = json.load(file)
    except JSONDecodeError as e:
        print(f"JSONDecodeError, {e.pos}, {e.lineno}, {e.msg}")
        return None
    except FileNotFoundError as e:
        print(f"FileNotFoundError, {e.errno}, {e.strerror}")
        return None
    except RuntimeError as e:
        print(f"RuntimeError, {str(e)}")
        return None
    except PermissionError as e:
        print(f"PermissionError, {e.errno}, {e.strerror}")
        return None
    except ValueError as e:
        print(f"ValueError, {str(e)}")
        return None
    except KeyError as e:
        print(f"KeyError, {str(e)}")
        return None
    except OSError as e:
        print(f"OSError, {e.errno}, {e.strerror}")
        return None
    else:
        return MultipleConfig(path, json_file)


class Issue(NamedTuple):
    """
    Define the Issue entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        state --- the issue state, state;\n
        summary -- the issue short description, summary;\n
        parent --- the parent issue name, parent;\n
        deadline --- the issue deadline, deadline;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    state: str
    summary: str
    parent: str = None
    deadline: datetime64 = datetime64("NaT")

    def __str__(self):
        if self.parent is None:
            parent_string = ""
        else:
            parent_string = f", parent = {self.parent}"
        if isnat(self.deadline):
            deadline_string = ""
        else:
            deadline_string = f", deadline = {self.deadline.item().isoformat()}"
        return f"Issue: issue = {self.issue}, state = {self.state}, summary = {self.summary}" \
               f"{parent_string}{deadline_string}"

    def __repr__(self):
        return f"<Issue(issue={self.issue}, state={self.state}, summary={self.summary}, parent={self.parent}, " \
               f"deadline={self.deadline})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            if self.parent is None:
                parent_string = ""
            else:
                parent_string = f", родительская задача: {self.parent}"
            if isnat(self.deadline):
                deadline_string = ""
            else:
                deadline_string = f", дедлайн: {self.deadline.item().isoformat()}"
            return f"Задача: {self.issue}, статус: {self.state}, описание: {self.summary}" \
                   f"{parent_string}{deadline_string};"


class IssueWorkItem(NamedTuple):
    """
    Define the IssueWorkItem entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        date --- the issue work item date, datetime64;\n
        spent_time -- the issue work item recorded time in minutes, int;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    date: datetime64
    spent_time: Decimal
    comment: str = None

    def __str__(self):
        if self.comment:
            comment_string = f", comment = {self.comment}"
        else:
            comment_string = ""
        return f"IssueWorkItem: issue = {self.issue}, date = {self.date.item().isoformat()}, " \
               f"spent time = {self.spent_time}{comment_string}"

    def __repr__(self):
        return f"<IssueWorkItem(issue={self.issue}, date={self.date}, spent_time={self.spent_time}, " \
               f"comment={self.comment})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            # decimal.getcontext().prec = 2
            return f"Задача: {self.issue}, дата: {self.date.item().isoformat()}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;"
        if format_spec == "short":
            return f"Дата: {self.date.item().isoformat()}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;" \
                   f"comment = {self.comment}"
        if format_spec == "date":
            return f"Задача: {self.issue}, " \
                   f"затраченное время: {self.spent_time.quantize(Decimal('1.00'), ROUND_CEILING)} ч, " \
                   f"{Decimal(self.spent_time * 60).quantize(Decimal('1'), ROUND_FLOOR)} мин;"


class User:
    """
    Define the User entity to send the YouTrack requests.

    Params:
        dict_issue --- the dictionary of Issue instances and issue names;\n
        dict_issue_work_item --- the dictionary of IssueWorkItem instances and issue names;\n
        user_config --- the UserConfig item;\n

    Properties:
        _non_unique --- the dictionary of the non-unique work items;\n
        login --- the user login;\n
        query_assignees --- the query params for assignees_project fields;\n

    Functions:
        _join_work_items() --- combine the non-unique work items;\n
        current_issues() --- get the current non-closed issues sorted by name;\n
        expired_issues() --- get the issues with the expired dealine;\n
        get_completed_issues() --- get the completed issues with no work items;\n
        get_current_issues() --- get the Issue instances with no work items;\n
        get_current_states(issues) --- get the current issue states;\n
        get_issue_work_items() --- get the IssueWorkItem instances;\n
        get_issues() --- get the Issue instances;\n
        issue_names() --- get the issue names;\n
        not_completed_issues() --- get the current non-closed issues;\n
        pre_processing() --- prepare all information from the YouTrack;\n
    """

    def __init__(self, login: str):
        self.login = login
        self.dict_issue: dict[str, Issue] = dict()
        self.dict_issue_work_item: dict[str, list[IssueWorkItem]] = dict()

    def __str__(self):
        return f"User: login = {self.login}"

    def __repr__(self):
        return f"<User(login={self.login})>"

    def __hash__(self):
        return hash(self.login)

    def __eq__(self, other):
        if isinstance(other, User):
            return self.login == other.login
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, User):
            return self.login != other.login
        else:
            return NotImplemented

    def __bool__(self):
        return not bool(self.login)

    @cached_property
    def query_assignees(self) -> str:
        """
        Get the query params for assignees_project fields.

        :return: the string for the request.
        :rtype: str
        """
        return " OR ".join([f"{param}: {self.login}" for param in ASSIGNEES_PROJECT])

    @property
    def _non_unique(self) -> dict[str, list[datetime64]]:
        """
        Get the non-unique work items.

        :return: the dictionary of the issue names and dates.
        :rtype: dict[str, list[datetime64]]
        """
        non_unique: dict[str, list[datetime64]] = dict()
        issue_name: str
        for issue_name, work_items in self.dict_issue_work_item.items():
            counter = Counter([work_item.date for work_item in work_items])
            # get non-unique dates
            non_unique_date = [key for key, value in counter.items() if value > 1]
            if non_unique_date:
                non_unique[issue_name] = non_unique_date
        return non_unique

    def _join_work_items(self):
        """Join the non-unique work items."""
        for issue, dates in self._non_unique.items():
            for date in dates:
                work_item: IssueWorkItem
                # cumulative sum
                cum_spent_time = np_sum(
                    [work_item.spent_time for work_item in self.dict_issue_work_item[issue]
                     if work_item.issue == issue and work_item.date == date])
                #  in case the type of cum_spent_time is not Decimal
                if isinstance(cum_spent_time, Decimal):
                    final_spent_time = cum_spent_time
                else:
                    final_spent_time = Decimal(cum_spent_time.item()).normalize()
                # specify the reversed list of item indexes to delete
                indexes = sorted([index for index, work_item in enumerate(self.dict_issue_work_item[issue])
                                  if work_item.date == date], reverse=True)
                # delete useless items
                for index in indexes:
                    del self.dict_issue_work_item[issue][index]
                # add the cumulative sum of the work items to the dict
                comment = "\n".join([work_item.comment for work_item in self.dict_issue_work_item[issue]
                                     if work_item.comment])
                issue_work_item = IssueWorkItem(issue, date, final_spent_time, comment)
                self.dict_issue_work_item[issue].append(issue_work_item)

    def current_issues(self) -> list[Issue]:
        """
        Get the current non-closed issues sorted by name.

        :return: the sorted issues.
        :rtype: list[Issue]
        """
        return sorted(self.not_completed_issues(), key=lambda item: item.issue)

    def expired_issues(self) -> list[Issue]:
        """
        Get the issues with the expired dealine.

        :return: the issues.
        :rtype: list[Issue]
        """
        return sorted(
            [issue for issue in self.not_completed_issues() if not isnat(issue.deadline) and issue.deadline <= today()],
            key=lambda item: item.issue)

    def get_completed_issues(self):
        """Get the completed Issue instances with no IssueWorkItem instances."""
        parameters_query = " ".join(
            (f'State: {STATES_COMPLETED_JOIN}', f"updated: {period()}", "has: -work", f"{self.query_assignees}")
        )
        params = (
            RequestFields.ISSUE.value,
            ('query', parameters_query),
            RequestCustomFields.STATE.value,
            RequestCustomFields.DEADLINE.value,
        )
        # get the response in the JSON format
        parsed_response = request(RequestURL.ISSUE, params)
        for item in parsed_response:
            issue, state, summary, parent, deadline = parse_response_issue(item)
            issue_item = Issue(issue, state, summary, parent, deadline)
            self.dict_issue[issue] = issue_item

    def get_current_issues(self):
        """Get the non-closed Issue instances with no IssueWorkItem instances."""
        parameters_query = " ".join(
            (f'State: {STATES_NOT_COMPLETED_JOIN}', f"{self.query_assignees}")
        )
        params = (
            RequestFields.ISSUE.value,
            ('query', parameters_query),
            RequestCustomFields.STATE.value,
            RequestCustomFields.DEADLINE.value,
        )
        # get the response in the JSON format
        parsed_response = request(RequestURL.ISSUE, params)
        for item in parsed_response:
            issue, state, summary, parent, deadline = parse_response_issue(item)
            issue_item = Issue(issue, state, summary, parent, deadline)
            self.dict_issue[issue] = issue_item

    def get_issue_work_items(self):
        """Get the IssueWorkItem instances."""
        # define the parameters of the request
        parameters_query = ' '.join((f"work author: {self.login}", f"work date: {period()}"))
        params = (
            RequestFields.WORK_ITEM.value,
            ('query', parameters_query),
            RequestCustomFields.START_DATE.value,
            RequestCustomFields.END_DATE.value,
            ('author', self.login),
        )
        # get the response in the JSON format
        parsed_response = request(RequestURL.WORK_ITEM, params)
        for item in parsed_response:
            issue, date, modified_spent_time, comment = parse_response_work_item(item)
            if issue not in self.dict_issue_work_item:
                self.dict_issue_work_item[issue] = []
            issue_work_item = IssueWorkItem(issue, date, modified_spent_time, comment)
            self.dict_issue_work_item[issue].append(issue_work_item)

    def get_issues(self):
        """Get the Issue instances."""
        if not self.dict_issue_work_item:
            return None
        else:
            issue_names = ",".join([issue for issue in self.dict_issue_work_item])
            parameters_query = f'issue ID: {issue_names}'
            params = (
                RequestFields.ISSUE.value,
                ('query', parameters_query),
                RequestCustomFields.STATE.value,
                RequestCustomFields.DEADLINE.value,
            )
            # get the response in the JSON format
            parsed_response = request(RequestURL.ISSUE, params)
            for item in parsed_response:
                issue, state, summary, parent, deadline = parse_response_issue(item)
                issue_item = Issue(issue, state, summary, parent, deadline)
                self.dict_issue[issue] = issue_item

    def issue_names(self) -> set[str]:
        """
        Get the issue names.

        :return: the issue names.
        :rtype: set[str]
        """
        return set(self.dict_issue)

    def not_completed_issues(self):
        """
        Get the current non-closed issues.

        :return: the open issues.
        :rtype: list[Issue]
        """
        return [issue for issue in self.dict_issue.values() if issue.state in STATES_NOT_COMPLETED]

    def pre_processing(self):
        """Get all YouTrack information."""
        self.get_issue_work_items()
        self.get_issues()
        self.get_current_issues()
        self.get_completed_issues()
        self._join_work_items()
