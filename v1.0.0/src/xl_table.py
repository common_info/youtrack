import decimal
from collections import Counter
from typing import Optional, Union, Any
import datetime
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.cell.cell import Cell
from openpyxl.utils.cell import coordinate_to_tuple, get_column_letter, column_index_from_string
from decimal import Decimal
from _style_work_item import _StyleWorkItemList
from copy import copy


__doc__ = """
Functions:
    check_coord(coord) --- validate the cell coordinate;\n
    range_coord(start_coord, end_coord) --- convert the range string to the tuple;\n
    convert_spent_time(spent_time) --- convert the spent time to the specified format;\n
    convert_datetime_date(value) --- convert the datetime or None to the date;\n
"""


def check_coord(coord: str) -> bool:
    """
    Validate the cell coordinate.

    :param str coord: the cell coordinates
    :return: the validation flag.
    :rtype: bool
    """
    flag = False
    try:
        coordinate_to_tuple(coord)
    except TypeError as e:
        print(f'TypeError {str(e)}. Incorrect value type.\n')
        print(f'coordinate = {coord}')
    except ValueError as e:
        print(f'ValueError {str(e)}. Incorrect value.\n')
        print(f'coordinate = {coord}')
    except OSError as e:
        print(f'OSError {e.errno}, {e.strerror}.\n')
        print(f'coordinate = {coord}')
    else:
        flag = True
    finally:
        return flag


def range_coord(start_coord: str, end_coord: str):
    """
    Convert the range string with the start and end coordinates to the tuple:\n
    (min_row, min_col, max_row, max_col)\n

    :param start_coord: the start cell coordinate, str
    :param end_coord: the end cell coordinate, str
    :return: the values for iteration of the tuple[int, int, int, int] type.
    """
    # convert start coordinate
    start_row, start_column = coordinate_to_tuple(start_coord)
    # convert end coordinate
    end_row, end_column = coordinate_to_tuple(end_coord)
    # find min and max values
    min_row = min(start_row, end_row)
    max_row = max(start_row, end_row)

    min_col = min(start_column, end_column)
    max_col = max(start_column, end_column)
    return min_row, max_row, min_col, max_col


def convert_spent_time(spent_time: Any) -> Decimal:
    """
    Convert the spent time to the specified format.

    :param spent_time: the converted value.
    :return: the decimal value.
    :rtype: Decimal
    """
    decimal.getcontext().prec = 3
    return Decimal(spent_time).normalize()


def convert_datetime_date(value: Any) -> Optional[datetime.date]:
    """
    Convert the datetime or None to the date.

    :param value: the value to convert
    :return: the date value.
    :rtype: datetime.date or None
    """
    if isinstance(value, datetime.datetime):
        return value.date()
    if isinstance(value, datetime.date):
        return value
    else:
        return None


class ExcelProp:
    """
    Define the Excel Worksheet properties.

    Constants:
        dict_headers --- the dictionary of the states and indexes;\n
        dict_headers_short --- the dictionary of the issue states and indexes;\n
        dict_state_style --- the dictionary of the states and cell styles;\n
        dict_state_priority --- the dictionary of the states and priorities;\n
        month_columns --- the columns with the month titles;\n
        dates --- the dates in the first row;\n

    Class params:
        dict_table_cell --- the dictionary of the TableCell instances;\n

    Params:
        ws --- the worksheet;\n
        styles --- the styles implemented into the worksheet;\n

    Properties:
        headers --- the header cells;\n
        headers_short --- the header cells without the legend;\n
        headers_row --- the header cell rows;\n
        headers_row_short --- the header cell rows without the legend;\n
        cell_states --- the dictionary of the cell coordinates and states;\n
        table_cells --- the cells for the TableCell instances;\n
        table_cell_names --- the issue names;\n
        bus_columns -- the work day date columns;\n
        weekend_columns --- the weekend date columns;\n

    Functions:
        _get_shift_by_date(date) --- get the column position from the G column;\n
        get_column_by_date(date) --- get the column letter for the specified date;\n
        cell_in_range(start_coord, end_coord) --- generate the cells in the range;\n
        delete_row(row) --- delete the row from the table;\n
        insert_row(row) --- add the row to the table;\n
        _separate_headers() --- unmerge the header cells;\n
        _combine_headers() --- merge the header cells;\n
        _check_empty(item) --- verify if the Cell, coordinate, or row is empty;\n
        _empty_rows() --- delete all empty rows except for the pre-headers;\n
        replace_cell(*, from_, to_) --- replace the cell attribute values to another cell;\n
        add_work_item(work_item) --- add the work item to the table;\n
        get_date_by_cell(cell) --- get the date associated with the cell;\n
        __index(state) --- get the state index;\n
        __index_row_state(state, *, state_index = 0, row_index = 0) --- specify the rows between the state-based
        headers;\n
        process_non_yt() --- process the self-made issues;\n
        pre_processing() --- pre-process the table to get rid of empty rows and repeating issues;\n
        default_row(row) --- specify the default row;\n
        update_header() --- specify the style to the header rows;\n
        update_table_cells() --- update the TableCell instances;\n
        post_processing() --- post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues;\n
    """
    # the headers
    dict_headers = {'Active': 0, 'New/Paused': 1, 'Done/Test': 2, 'Verified': 3, 'Легенда': 4}
    dict_headers_short = {'Active': 0, 'New/Paused': 1, 'Done/Test': 2, 'Verified': 3}
    # mapping states and styles
    dict_state_style = {
        "New": "basic",
        "New/Paused": "paused",
        "Active": "active",
        "Paused": "paused",
        "Active/Paused": "active",
        "Done": "done",
        "Test": "test",
        "Done/Test": "done",
        "Verified": "verified_closed",
        "Closed": "verified_closed",
        "Discuss": "paused",
        "Review": "paused",
        "Canceled": "verified_closed"
    }
    # the columns with the month names
    month_columns = ("F", "AL", "BO", "CU", "DZ", "FF", "GK", "HQ", "IW", "KB", "LH", "MM")
    # specify the state priorities
    dict_state_priority: dict[str, int] = {"Active": 30, "New/Paused": 10, "Done/Test": 20, "Verified": 40}
    # mapping the issue attributes and columns
    dict_attr_column = {"parent": "B", "issue": "C", "summary": "D", "deadline": "E", "commentary": "NT"}
    # the dates pre-computed
    dates = (datetime.date(2022, 1, 1), datetime.date(2022, 1, 2), datetime.date(2022, 1, 3),
             datetime.date(2022, 1, 4), datetime.date(2022, 1, 5), datetime.date(2022, 1, 6),
             datetime.date(2022, 1, 7), datetime.date(2022, 1, 8), datetime.date(2022, 1, 9),
             datetime.date(2022, 1, 10), datetime.date(2022, 1, 11), datetime.date(2022, 1, 12),
             datetime.date(2022, 1, 13), datetime.date(2022, 1, 14), datetime.date(2022, 1, 15),
             datetime.date(2022, 1, 16), datetime.date(2022, 1, 17), datetime.date(2022, 1, 18),
             datetime.date(2022, 1, 19), datetime.date(2022, 1, 20), datetime.date(2022, 1, 21),
             datetime.date(2022, 1, 22), datetime.date(2022, 1, 23), datetime.date(2022, 1, 24),
             datetime.date(2022, 1, 25), datetime.date(2022, 1, 26), datetime.date(2022, 1, 27),
             datetime.date(2022, 1, 28), datetime.date(2022, 1, 29), datetime.date(2022, 1, 30),
             datetime.date(2022, 1, 31), None, datetime.date(2022, 2, 1), datetime.date(2022, 2, 2),
             datetime.date(2022, 2, 3), datetime.date(2022, 2, 4), datetime.date(2022, 2, 5),
             datetime.date(2022, 2, 6), datetime.date(2022, 2, 7), datetime.date(2022, 2, 8),
             datetime.date(2022, 2, 9), datetime.date(2022, 2, 10), datetime.date(2022, 2, 11),
             datetime.date(2022, 2, 12), datetime.date(2022, 2, 13), datetime.date(2022, 2, 14),
             datetime.date(2022, 2, 15), datetime.date(2022, 2, 16), datetime.date(2022, 2, 17),
             datetime.date(2022, 2, 18), datetime.date(2022, 2, 19), datetime.date(2022, 2, 20),
             datetime.date(2022, 2, 21), datetime.date(2022, 2, 22), datetime.date(2022, 2, 23),
             datetime.date(2022, 2, 24), datetime.date(2022, 2, 25), datetime.date(2022, 2, 26),
             datetime.date(2022, 2, 27), datetime.date(2022, 2, 28), None, datetime.date(2022, 3, 1),
             datetime.date(2022, 3, 2), datetime.date(2022, 3, 3), datetime.date(2022, 3, 4),
             datetime.date(2022, 3, 5), datetime.date(2022, 3, 6), datetime.date(2022, 3, 7),
             datetime.date(2022, 3, 8), datetime.date(2022, 3, 9), datetime.date(2022, 3, 10),
             datetime.date(2022, 3, 11), datetime.date(2022, 3, 12), datetime.date(2022, 3, 13),
             datetime.date(2022, 3, 14), datetime.date(2022, 3, 15), datetime.date(2022, 3, 16),
             datetime.date(2022, 3, 17), datetime.date(2022, 3, 18), datetime.date(2022, 3, 19),
             datetime.date(2022, 3, 20), datetime.date(2022, 3, 21), datetime.date(2022, 3, 22),
             datetime.date(2022, 3, 23), datetime.date(2022, 3, 24), datetime.date(2022, 3, 25),
             datetime.date(2022, 3, 26), datetime.date(2022, 3, 27), datetime.date(2022, 3, 28),
             datetime.date(2022, 3, 29), datetime.date(2022, 3, 30), datetime.date(2022, 3, 31), None,
             datetime.date(2022, 4, 1), datetime.date(2022, 4, 2), datetime.date(2022, 4, 3),
             datetime.date(2022, 4, 4), datetime.date(2022, 4, 5), datetime.date(2022, 4, 6),
             datetime.date(2022, 4, 7), datetime.date(2022, 4, 8), datetime.date(2022, 4, 9),
             datetime.date(2022, 4, 10), datetime.date(2022, 4, 11), datetime.date(2022, 4, 12),
             datetime.date(2022, 4, 13), datetime.date(2022, 4, 14), datetime.date(2022, 4, 15),
             datetime.date(2022, 4, 16), datetime.date(2022, 4, 17), datetime.date(2022, 4, 18),
             datetime.date(2022, 4, 19), datetime.date(2022, 4, 20), datetime.date(2022, 4, 21),
             datetime.date(2022, 4, 22), datetime.date(2022, 4, 23), datetime.date(2022, 4, 24),
             datetime.date(2022, 4, 25), datetime.date(2022, 4, 26), datetime.date(2022, 4, 27),
             datetime.date(2022, 4, 28), datetime.date(2022, 4, 29), datetime.date(2022, 4, 30), None,
             datetime.date(2022, 5, 1), datetime.date(2022, 5, 2), datetime.date(2022, 5, 3),
             datetime.date(2022, 5, 4), datetime.date(2022, 5, 5), datetime.date(2022, 5, 6),
             datetime.date(2022, 5, 7), datetime.date(2022, 5, 8), datetime.date(2022, 5, 9),
             datetime.date(2022, 5, 10), datetime.date(2022, 5, 11), datetime.date(2022, 5, 12),
             datetime.date(2022, 5, 13), datetime.date(2022, 5, 14), datetime.date(2022, 5, 15),
             datetime.date(2022, 5, 16), datetime.date(2022, 5, 17), datetime.date(2022, 5, 18),
             datetime.date(2022, 5, 19), datetime.date(2022, 5, 20), datetime.date(2022, 5, 21),
             datetime.date(2022, 5, 22), datetime.date(2022, 5, 23), datetime.date(2022, 5, 24),
             datetime.date(2022, 5, 25), datetime.date(2022, 5, 26), datetime.date(2022, 5, 27),
             datetime.date(2022, 5, 28), datetime.date(2022, 5, 29), datetime.date(2022, 5, 30),
             datetime.date(2022, 5, 31), None, datetime.date(2022, 6, 1), datetime.date(2022, 6, 2),
             datetime.date(2022, 6, 3), datetime.date(2022, 6, 4), datetime.date(2022, 6, 5),
             datetime.date(2022, 6, 6), datetime.date(2022, 6, 7), datetime.date(2022, 6, 8),
             datetime.date(2022, 6, 9), datetime.date(2022, 6, 10), datetime.date(2022, 6, 11),
             datetime.date(2022, 6, 12), datetime.date(2022, 6, 13), datetime.date(2022, 6, 14),
             datetime.date(2022, 6, 15), datetime.date(2022, 6, 16), datetime.date(2022, 6, 17),
             datetime.date(2022, 6, 18), datetime.date(2022, 6, 19), datetime.date(2022, 6, 20),
             datetime.date(2022, 6, 21), datetime.date(2022, 6, 22), datetime.date(2022, 6, 23),
             datetime.date(2022, 6, 24), datetime.date(2022, 6, 25), datetime.date(2022, 6, 26),
             datetime.date(2022, 6, 27), datetime.date(2022, 6, 28), datetime.date(2022, 6, 29),
             datetime.date(2022, 6, 30), None, datetime.date(2022, 7, 1), datetime.date(2022, 7, 2),
             datetime.date(2022, 7, 3), datetime.date(2022, 7, 4), datetime.date(2022, 7, 5),
             datetime.date(2022, 7, 6), datetime.date(2022, 7, 7), datetime.date(2022, 7, 8),
             datetime.date(2022, 7, 9), datetime.date(2022, 7, 10), datetime.date(2022, 7, 11),
             datetime.date(2022, 7, 12), datetime.date(2022, 7, 13), datetime.date(2022, 7, 14),
             datetime.date(2022, 7, 15), datetime.date(2022, 7, 16), datetime.date(2022, 7, 17),
             datetime.date(2022, 7, 18), datetime.date(2022, 7, 19), datetime.date(2022, 7, 20),
             datetime.date(2022, 7, 21), datetime.date(2022, 7, 22), datetime.date(2022, 7, 23),
             datetime.date(2022, 7, 24), datetime.date(2022, 7, 25), datetime.date(2022, 7, 26),
             datetime.date(2022, 7, 27), datetime.date(2022, 7, 28), datetime.date(2022, 7, 29),
             datetime.date(2022, 7, 30), datetime.date(2022, 7, 31), None, datetime.date(2022, 8, 1),
             datetime.date(2022, 8, 2), datetime.date(2022, 8, 3), datetime.date(2022, 8, 4),
             datetime.date(2022, 8, 5), datetime.date(2022, 8, 6), datetime.date(2022, 8, 7),
             datetime.date(2022, 8, 8), datetime.date(2022, 8, 9), datetime.date(2022, 8, 10),
             datetime.date(2022, 8, 11), datetime.date(2022, 8, 12), datetime.date(2022, 8, 13),
             datetime.date(2022, 8, 14), datetime.date(2022, 8, 15), datetime.date(2022, 8, 16),
             datetime.date(2022, 8, 17), datetime.date(2022, 8, 18), datetime.date(2022, 8, 19),
             datetime.date(2022, 8, 20), datetime.date(2022, 8, 21), datetime.date(2022, 8, 22),
             datetime.date(2022, 8, 23), datetime.date(2022, 8, 24), datetime.date(2022, 8, 25),
             datetime.date(2022, 8, 26), datetime.date(2022, 8, 27), datetime.date(2022, 8, 28),
             datetime.date(2022, 8, 29), datetime.date(2022, 8, 30), datetime.date(2022, 8, 31), None,
             datetime.date(2022, 9, 1), datetime.date(2022, 9, 2), datetime.date(2022, 9, 3),
             datetime.date(2022, 9, 4), datetime.date(2022, 9, 5), datetime.date(2022, 9, 6),
             datetime.date(2022, 9, 7), datetime.date(2022, 9, 8), datetime.date(2022, 9, 9),
             datetime.date(2022, 9, 10), datetime.date(2022, 9, 11), datetime.date(2022, 9, 12),
             datetime.date(2022, 9, 13), datetime.date(2022, 9, 14), datetime.date(2022, 9, 15),
             datetime.date(2022, 9, 16), datetime.date(2022, 9, 17), datetime.date(2022, 9, 18),
             datetime.date(2022, 9, 19), datetime.date(2022, 9, 20), datetime.date(2022, 9, 21),
             datetime.date(2022, 9, 22), datetime.date(2022, 9, 23), datetime.date(2022, 9, 24),
             datetime.date(2022, 9, 25), datetime.date(2022, 9, 26), datetime.date(2022, 9, 27),
             datetime.date(2022, 9, 28), datetime.date(2022, 9, 29), datetime.date(2022, 9, 30), None,
             datetime.date(2022, 10, 1), datetime.date(2022, 10, 2), datetime.date(2022, 10, 3),
             datetime.date(2022, 10, 4), datetime.date(2022, 10, 5), datetime.date(2022, 10, 6),
             datetime.date(2022, 10, 7), datetime.date(2022, 10, 8), datetime.date(2022, 10, 9),
             datetime.date(2022, 10, 10), datetime.date(2022, 10, 11), datetime.date(2022, 10, 12),
             datetime.date(2022, 10, 13), datetime.date(2022, 10, 14), datetime.date(2022, 10, 15),
             datetime.date(2022, 10, 16), datetime.date(2022, 10, 17), datetime.date(2022, 10, 18),
             datetime.date(2022, 10, 19), datetime.date(2022, 10, 20), datetime.date(2022, 10, 21),
             datetime.date(2022, 10, 22), datetime.date(2022, 10, 23), datetime.date(2022, 10, 24),
             datetime.date(2022, 10, 25), datetime.date(2022, 10, 26), datetime.date(2022, 10, 27),
             datetime.date(2022, 10, 28), datetime.date(2022, 10, 29), datetime.date(2022, 10, 30),
             datetime.date(2022, 10, 31), None, datetime.date(2022, 11, 1), datetime.date(2022, 11, 2),
             datetime.date(2022, 11, 3), datetime.date(2022, 11, 4), datetime.date(2022, 11, 5),
             datetime.date(2022, 11, 6), datetime.date(2022, 11, 7), datetime.date(2022, 11, 8),
             datetime.date(2022, 11, 9), datetime.date(2022, 11, 10), datetime.date(2022, 11, 11),
             datetime.date(2022, 11, 12), datetime.date(2022, 11, 13), datetime.date(2022, 11, 14),
             datetime.date(2022, 11, 15), datetime.date(2022, 11, 16), datetime.date(2022, 11, 17),
             datetime.date(2022, 11, 18), datetime.date(2022, 11, 19), datetime.date(2022, 11, 20),
             datetime.date(2022, 11, 21), datetime.date(2022, 11, 22), datetime.date(2022, 11, 23),
             datetime.date(2022, 11, 24), datetime.date(2022, 11, 25), datetime.date(2022, 11, 26),
             datetime.date(2022, 11, 27), datetime.date(2022, 11, 28), datetime.date(2022, 11, 29),
             datetime.date(2022, 11, 30), None, datetime.date(2022, 12, 1), datetime.date(2022, 12, 2),
             datetime.date(2022, 12, 3), datetime.date(2022, 12, 4), datetime.date(2022, 12, 5),
             datetime.date(2022, 12, 6), datetime.date(2022, 12, 7), datetime.date(2022, 12, 8),
             datetime.date(2022, 12, 9), datetime.date(2022, 12, 10), datetime.date(2022, 12, 11),
             datetime.date(2022, 12, 12), datetime.date(2022, 12, 13), datetime.date(2022, 12, 14),
             datetime.date(2022, 12, 15), datetime.date(2022, 12, 16), datetime.date(2022, 12, 17),
             datetime.date(2022, 12, 18), datetime.date(2022, 12, 19), datetime.date(2022, 12, 20),
             datetime.date(2022, 12, 21), datetime.date(2022, 12, 22), datetime.date(2022, 12, 23),
             datetime.date(2022, 12, 24), datetime.date(2022, 12, 25), datetime.date(2022, 12, 26),
             datetime.date(2022, 12, 27), datetime.date(2022, 12, 28), datetime.date(2022, 12, 29),
             datetime.date(2022, 12, 30), datetime.date(2022, 12, 31), None)
    dict_table_cell = dict()

    def __init__(self, ws: Worksheet, styles: _StyleWorkItemList):
        self.ws = ws
        self.styles = styles

    def __str__(self):
        return f"Worksheet: {self.ws.title}"

    def __repr__(self):
        return f"ExcelProp(ws={repr(self.ws)})"

    def __format__(self, format_spec):
        if format_spec == "base":
            return str(self)
        elif format_spec == "row_equal":
            return repr(self)
        else:
            return f"{self.ws.title}"

    def __hash__(self):
        return hash(self.ws.title)

    def __key(self):
        return self.ws.title, self.ws.parent

    def __eq__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, ExcelProp):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def _get_shift_by_date(self, date: datetime.date) -> int:
        """
        Get the column position from the G column.

        :param date: the date
        :type date: datetime.date
        :return: the number of columns up to the G column.
        :rtype: int
        """
        for index, item in enumerate(self.dates):
            if item == date:
                return index

    def get_column_by_date(self, date: datetime.date) -> str:
        """
        Get the column letter for the specified date.

        :param date: the date
        :type date: datetime.date
        :return: the column_letter.
        :rtype: str
        """
        return get_column_letter(self._get_shift_by_date(date) + column_index_from_string("G"))

    def cell_in_range(self, start_coord: str, end_coord: str):
        """
        Convert the cell range to the cell generator in the range.

        :param str start_coord: the start cell coordinates
        :param str end_coord: the end cell coordinates
        :return: the generator of cells.
        """
        if check_coord(coord=start_coord) and check_coord(coord=end_coord):
            min_row, max_row, min_col, max_col = range_coord(start_coord=start_coord, end_coord=end_coord)
            for row in range(min_row, max_row + 1):
                for col in range(min_col, max_col + 1):
                    coord = f'{get_column_letter(col)}{row}'
                    cell: Cell = self.ws[coord]
                    yield cell

    @property
    def headers(self) -> list[Cell]:
        """
        Get the header cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        end_coord = f"B{self.ws.max_row}"
        return [cell for cell in self.cell_in_range("B3", end_coord) if cell.value in ExcelProp.dict_headers.keys()]

    @property
    def headers_short(self) -> list[Cell]:
        """
        Get the header cells without the legend.

        :return: the cells.
        :rtype: list[Cell]
        """
        return self.headers[:4]

    @property
    def headers_row(self) -> list[int]:
        """
        Get the header row values.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return [header.row for header in self.headers]

    @property
    def headers_row_short(self) -> list[int]:
        """
        Get the header row values without the legend.

        :return: the list of header rows.
        :rtype: list[int]
        """
        return self.headers_row[:4]

    def delete_row(self, row: int):
        """
        Delete the row from the table.

        :param int row: the table row number
        :return: None.
        """
        self._separate_headers()
        self.ws.delete_rows(row)
        self._combine_headers()
        return

    def insert_row(self, row: int):
        """
        Add the row to the table.

        :param int row: the table row number
        :return: None.
        """
        self._separate_headers()
        self.ws.insert_rows(row)
        self.default_row(row)
        self._combine_headers()
        return

    def _separate_headers(self):
        """Unmerge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            start_coord = f"B{header_row}"
            end_coord = f"E{header_row}"
            self.ws.unmerge_cells(f"{start_coord}:{end_coord}")
            for cell_header in self.cell_in_range(start_coord, end_coord):
                cell_header.style = "basic_issue"
        # the legend header
        header_legend_row = self.headers_row[4]
        start_legend_coord = f"B{header_legend_row}"
        end_legend_coord = f"C{header_legend_row}"
        self.ws.unmerge_cells(f"{start_legend_coord}:{end_legend_coord}")
        for cell_legend in self.cell_in_range(start_legend_coord, end_legend_coord):
            cell_legend.style = "basic_issue"
        return

    def _combine_headers(self):
        """Merge the header cells."""
        # the common headers
        for header_row in self.headers_row_short:
            self.ws.merge_cells(f"B{header_row}:E{header_row}")
            self.update_header()
        # the legend header
        header_legend_row = self.headers_row[4]
        self.ws.merge_cells(f"B{header_legend_row}:C{header_legend_row}")
        return

    def _check_empty(self, item: Union[int, str, Cell]) -> bool:
        """
        Verify if the cell, cell coordinates, or cell row is empty.

        :param item: the cell value
        :type item: int or str or Cell
        :return: the emptiness flag.
        :rtype: bool
        """
        if isinstance(item, Cell):
            return True if item.value is None else False
        elif isinstance(item, str):
            return True if self.ws[f"{item}"].value is None else False
        elif isinstance(item, int):
            cell_values = (
                self.ws[f"B{item}"].value, self.ws[f"C{item}"].value,
                self.ws[f"D{item}"].value, self.ws[f"E{item}"].value)
            if any(value is not None for value in cell_values):
                return False
            else:
                return True

    def _empty_rows(self):
        """Delete all empty rows except for the pre-headers."""
        list_empty = []
        for state in self.dict_headers_short.keys():
            low_limit = self.__index_row_state(state, row_index=1)
            if self.__index(state) != 3:
                high_limit = self.__index_row_state(state, state_index=1, row_index=-1)
            else:
                high_limit = self.__index_row_state(state, state_index=1, row_index=-3)
            for row in range(low_limit, high_limit):
                if self._check_empty(row):
                    list_empty.append(row)
        for empty_row in sorted(list_empty, reverse=True):
            self.delete_row(empty_row)
        return

    def replace_cell(self, *, from_: Union[Cell, str], to_: Optional[Union[Cell, str]] = None):
        """
        Replace the cell attribute values to another cell. If to_ is None, the values are deleted.

        :param from_: the original cell or cell coordinates
        :type from_: Cell or str
        :param to_: the destination cell or cell coordinates
        :type to_: Cell or str or None
        :return: None.
        """
        cell_proxy: Optional[Cell]
        cell_base: Cell
        # if the cell coordinates
        if isinstance(from_, str):
            cell_base = self.ws[f"{from_}"]
        # if the cell
        else:
            cell_base = from_
        # if the destination cell exists
        if to_ is not None:
            if isinstance(to_, str):
                cell_proxy = self.ws[f"{to_}"]
            else:
                cell_proxy = to_
            # copy all required attribute values
            # if the destination cell is empty
            if cell_proxy.value is None:
                cell_proxy.value = copy(cell_base.value)
            # if the destination cell has some value
            else:
                cell_proxy.value += cell_base.value
            if cell_base.has_style:
                self.styles.set_style(cell_base.style, cell_proxy)
            else:
                cell_proxy.number_format = copy(cell_base.number_format)
                cell_proxy.style = "basic"
        # set the default cell attribute values
        cell_base.value = None
        cell_base.style = "basic"
        return

    def add_work_item(self, work_item: tuple[str, datetime.date, Union[int, Decimal]]):
        """
        Add the work item to the table.

        issue_name: str, date: datetime.date, spent_time: Union[int, Decimal]
        :param work_item: the work item to add
        :type work_item: tuple[str, datetime.date, Decimal or int]
        :return: None.
        """
        issue_name, *_ = work_item
        self.dict_table_cell[issue_name].add_work_item(work_item)
        return

    def get_date_by_cell(self, cell: Cell) -> Optional[datetime.date]:
        """
        Get the date associated with the cell.

        :param cell: the table cell
        :type cell: Cell
        :return: the date.
        :rtype: datetime.date or None
        """
        raw_date = self.ws[f"{cell.column_letter}1"].value
        return convert_datetime_date(raw_date)

    def __index(self, state: str) -> int:
        """
        Get the state index.

        :param str state: the state
        :return: the header index.
        :rtype: int
        """
        return self.dict_headers_short[state]

    def __index_row_state(self, state: str, *, state_index: int = 0, row_index: int = 0) -> int:
        """
        Specify the row between the headers based on the state.

        :param str state: the state
        :param int state_index: the state index shift inside the dict key, default: 0
        :param int row_index: the row index shift after the dict key, default: 0
        :return: the row number.
        :rtype: int
        """
        return self.headers_row[self.__index(state) + state_index] + row_index

    @property
    def cell_states(self) -> dict[str, str]:
        """
        Get the dictionary of the cell coordinates and states.

        :return: the cell coordinates and state mapping.
        :rtype: dict[str, str]
        """
        dict_cell_states: dict[str, str] = dict()
        for state in self.dict_headers_short.keys():
            # start from the row right after the header
            start_row = self.__index_row_state(state, row_index=1)
            # stop on the row right before the next header
            end_row = self.__index_row_state(state, state_index=1)
            for cell in self.cell_in_range(f"C{start_row}", f"C{end_row}"):
                if not self._check_empty(cell.row):
                    dict_cell_states[cell.coordinate] = state
        return dict_cell_states

    @property
    def table_cells(self) -> list[Cell]:
        """
        Get the cells of the TableCell instances.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [self.ws[f"{coord}"] for coord in self.cell_states if self.ws[f"{coord}"].value]

    def process_non_yt(self):
        """Specify the self-made issues with no identifier in the YouTrack."""
        # specify the self-made issue rows
        non_yt_rows = [coordinate_to_tuple(coord)[0] for coord in self.cell_states
                       if self.ws[f"{coord}"].value is None]
        # specify the self-made work item cells
        non_yt_cells = [cell for row in non_yt_rows
                        for cell in self.cell_in_range(f"G{row}", f"NS{row}") if cell.value]
        # apply the active style to the work item cells
        for cell in non_yt_cells:
            if cell.style in ("basic", "Normal"):
                cell.style = "active"

    def table_cell_names(self) -> set[str]:
        """
        Get the issue names.

        :return: the issue names.
        :rtype: set[str]
        """
        return {cell.value for cell in self.table_cells}

    def pre_processing(self):
        """Pre-process the table to get rid of empty rows and repeating issues."""
        counter = Counter(self.table_cell_names())
        # get non-unique issues
        non_unique = [key for key, value in counter.items() if value > 1]
        for issue in non_unique:
            row_eq = _RowEqual(self, issue)
            row_eq.join_cells()
        # delete empty rows
        self._empty_rows()
        # initiate the TableCell instances
        self.update_table_cells()
        return

    @property
    def bus_columns(self) -> tuple:
        """
        Specify the business day columns.

        :return: the business day columns.
        :rtype: tuple[str]
        """
        # return [self.get_column_by_date(date) for date in calendar_days()[0]]
        return (
            'P', 'Q', 'R', 'S', 'T', 'W', 'X', 'Y', 'Z', 'AA', 'AD', 'AE', 'AF', 'AG', 'AH', 'AK', 'AM', 'AN', 'AO',
            'AP', 'AS', 'AT', 'AU', 'AV', 'AW', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BG', 'BH', 'BJ', 'BK', 'BN', 'BP', 'BQ',
            'BR', 'BS', 'BT', 'BX', 'BY', 'BZ', 'CC', 'CD', 'CE', 'CF', 'CG', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CQ', 'CR',
            'CS', 'CT', 'CV', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DM', 'DN', 'DO', 'DP', 'DQ',
            'DT', 'DU', 'DV', 'DW', 'DX', 'ED', 'EE', 'EF', 'EK', 'EL', 'EM', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EW', 'EX',
            'EY', 'EZ', 'FA', 'FD', 'FE', 'FG', 'FH', 'FI', 'FL', 'FM', 'FN', 'FO', 'FP', 'FT', 'FU', 'FV', 'FW', 'FZ',
            'GA', 'GB', 'GC', 'GD', 'GG', 'GH', 'GI', 'GJ', 'GL', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GV', 'GW', 'GX', 'GY',
            'GZ', 'HC', 'HD', 'HE', 'HF', 'HG', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HR', 'HS', 'HT', 'HU', 'HV', 'HY', 'HZ',
            'IA', 'IB', 'IC', 'IF', 'IG', 'IH', 'II', 'IJ', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IT', 'IU', 'IV', 'IX', 'IY',
            'JB', 'JC', 'JD', 'JE', 'JF', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JW', 'JX', 'JY',
            'JZ', 'KA', 'KE', 'KF', 'KG', 'KH', 'KI', 'KL', 'KM', 'KN', 'KO', 'KP', 'KS', 'KT', 'KU', 'KV', 'KW', 'KZ',
            'LA', 'LB', 'LC', 'LD', 'LG', 'LI', 'LJ', 'LK', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LV', 'LW', 'LX', 'LY', 'LZ',
            'MC', 'MD', 'ME', 'MF', 'MG', 'MJ', 'MK', 'ML', 'MN', 'MO', 'MR', 'MS', 'MT', 'MU', 'MV', 'MY', 'MZ', 'NA',
            'NB', 'NC', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NM', 'NN', 'NO', 'NP', 'NQ')

    @property
    def weekend_columns(self) -> tuple:
        """
        Specify the weekend columns.

        :return: the weekend columns.
        :rtype: tuple[str]
        """
        # return [self.get_column_by_date(date) for date in calendar_days()[1]]
        return (
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'U', 'V', 'AB', 'AC', 'AI', 'AJ', 'AQ', 'AR', 'AX', 'AY', 'BE',
            'BF', 'BI', 'BL', 'BM', 'BU', 'BV', 'BW', 'CA', 'CB', 'CH', 'CI', 'CO', 'CP', 'CW', 'CX', 'DD', 'DE', 'DK',
            'DL', 'DR', 'DS', 'DY', 'EA', 'EB', 'EC', 'EG', 'EH', 'EI', 'EJ', 'EN', 'EO', 'EU', 'EV', 'FB', 'FC', 'FJ',
            'FK', 'FQ', 'FR', 'FS', 'FX', 'FY', 'GE', 'GF', 'GM', 'GN', 'GT', 'GU', 'HA', 'HB', 'HH', 'HI', 'HO', 'HP',
            'HW', 'HX', 'ID', 'IE', 'IK', 'IL', 'IR', 'IS', 'IZ', 'JA', 'JG', 'JH', 'JN', 'JO', 'JU', 'JV', 'KC', 'KD',
            'KJ', 'KK', 'KQ', 'KR', 'KX', 'KY', 'LE', 'LF', 'LL', 'LM', 'LN', 'LT', 'LU', 'MA', 'MB', 'MH', 'MI', 'MP',
            'MQ', 'MW', 'MX', 'ND', 'NE', 'NK', 'NL', 'NR')

    def default_row(self, row: int):
        """
        Specify the default row when added.

        :param int row: the row number to add to the table
        :return: None.
        """
        # the basic
        for column in ("B", "C", "D", "NT"):
            self.ws[f"{column}{row}"].style = "basic_issue"
        # the cells with dates
        # the business days
        for column in self.bus_columns:
            self.ws[f"{column}{row}"].style = "basic"
        # the weekends
        for column in self.weekend_columns:
            self.ws[f"{column}{row}"].style = "weekend"
        # the business days
        for column in self.month_columns:
            self.styles.set_style("header_month", self.ws[f"{column}{row}"])
        # the issue deadline
        self.styles.set_style("deadline_issue", self.ws[f"E{row}"])
        # the total spent time cell
        self.styles.set_style("sum", self.ws[f"NS{row}"])
        return

    def update_header(self):
        """Specify the style to the header rows."""
        for cell in self.headers_short:
            cell.style = "Normal"
            self.styles.set_style("header_text", cell)
        for row in self.headers_row_short:
            for cell in self.cell_in_range(f"F{row}", f"NR{row}"):
                self.styles.set_style("header", cell)
            for column in self.month_columns:
                self.styles.set_style("header_no_border", self.ws[f"{column}{row}"])
        return

    def update_table_cells(self):
        """Update the TableCell instances."""
        for cell in self.table_cells:
            TableCell(self, cell)
        return

    def post_processing(self):
        """Post-process the table to apply the work item cell styles, add formulae and hyperlinks,
        and process the self-made non-YouTrack issues."""
        for cell in self.table_cells:
            table_cell = TableCell(self, cell)
            table_cell.post_processing()
        self.process_non_yt()
        return


class TableCell:
    """
    Define the table row.

    Params:
        excel_prop --- the ExcelProp instance;\n
        cell --- the base cell;\n
        issue --- the base cell value;\n

    Properties:
        ws --- the worksheet;\n
        row --- the row;\n
        state --- the state;\n
        parent --- the parent issue;\n
        summary --- the issue summary;\n
        deadline --- the issue deadline;\n
        commentary --- the issue commentary;\n
        cell_last --- the last work item cell;\n

    Functions:
        _shift_cell(shift) --- get the shifted cell in the same row;\n
        pyxl_cells() --- get the work item cells;\n
        work_items() --- get the work items;\n
        mapping_work_item_cell(work_item) --- convert the work item to the cell;\n
        add_work_item(work_item) --- add the work item to the row;\n
        proper_cell_style(cell) --- get the cell style based on the state;\n
        post_processing() --- process the work cells after completing the issue modifications:
            the deadline style;\n
            the formula;\n
            the issue and parent issue hyperlinks;\n
            the proper cell styles.\n
        replace_issue(row) --- replace the issue attribute values to the new row;\n
    """

    def __init__(self, excel_prop: ExcelProp, cell: Cell):
        self.excel_prop = excel_prop
        self.cell = cell
        self.issue = str(cell.value)

        self.excel_prop.dict_table_cell[self.issue] = self

    def __str__(self):
        return f"Table cell: {self.cell.coordinate}"

    def __repr__(self):
        return f"TableCell({self.excel_prop}, {self.cell})"

    def __hash__(self):
        return hash((self.cell.coordinate, self.issue))

    def __key(self):
        return self.ws.title, self.cell.coordinate, self.issue

    def __eq__(self, other):
        if isinstance(other, TableCell):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, TableCell):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, TableCell):
            return self.row < other.row
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, TableCell):
            return self.row > other.row
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, TableCell):
            return self.row <= other.row
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, TableCell):
            return self.row >= other.row
        else:
            return NotImplemented

    def __len__(self):
        return len(self.work_items())

    @property
    def ws(self) -> Worksheet:
        """
        Get the ExcelProp worksheet.

        :return: the worksheet.
        :rtype: Worksheet
        """
        return self.excel_prop.ws

    @property
    def row(self) -> int:
        """
        Get the base cell row.

        :return: the cell row.
        :rtype: int
        """
        return self.cell.row

    def _shift_cell(self, shift: int) -> Optional[Cell]:
        """
        Get the shifted cell in the same row.

        :param int shift: the cell shift
        :return: the shifted cell.
        :rtype: Cell or None
        """
        col_idx = self.cell.col_idx + shift
        if col_idx >= 1:
            column_letter = get_column_letter(col_idx)
            return self.ws[f"{column_letter}{self.row}"]
        else:
            print(f"ValueError, the shift {shift} is out of bounds.")
            return None

    @property
    def state(self) -> str:
        """
        Get the issue state.

        :return: the state.
        :rtype: str
        """
        return self.excel_prop.cell_states[self.cell.coordinate]

    @property
    def deadline(self) -> Optional[datetime.date]:
        """
        Get the issue deadline if exists.

        :return: the deadline.
        :rtype: datetime.date or None
        """
        return self._shift_cell(2).value

    @property
    def parent(self) -> str:
        """
        Get the parent issue name if exists.

        :return: the parent issue name.
        :rtype: str
        """
        return self._shift_cell(-1).value

    @property
    def summary(self) -> str:
        """
        Get the issue summary.

        :return: the issue summary.
        :rtype: str
        """
        return self._shift_cell(1).value

    @property
    def commentary(self) -> str:
        """
        Get the issue commentary.

        :return: the issue commentary.
        :rtype: str
        """
        return self.ws[f"NT{self.row}"]

    def pyxl_cells(self) -> list[Cell]:
        """
        Get the work item cells.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [cell for cell in self.excel_prop.cell_in_range(f"G{self.row}", f"NR{self.row}")
                if cell.value]

    def work_items(self) -> list[tuple[str, datetime.date, Decimal]]:
        """
        Get the work items.

        :return: the work items.
        :rtype: list[tuple[str, datetime.date, Decimal]]
        """
        decimal.getcontext().prec = 2
        return [(self.issue, self.excel_prop.get_date_by_cell(cell), Decimal(cell.value).normalize())
                for cell in self.pyxl_cells()]

    @property
    def cell_last(self) -> Cell:
        """
        Get the last work item cell.

        :return: the last work item cell.
        :rtype: Cell
        """
        max_col_idx = max(cell.col_idx for cell in self.pyxl_cells())
        return self.ws[f"{get_column_letter(max_col_idx)}{self.row}"]

    def mapping_work_item_cell(self, work_item: tuple[str, datetime.date, Decimal]) -> Cell:
        """
        Convert the work item to the cell.

        :param work_item: the work item
        :type work_item: tuple[str, datetime.date, Decimal]
        :return: the cell.
        :rtype: Cell
        """
        _, date, _ = work_item
        column = self.excel_prop.get_column_by_date(date)
        return self.ws[f"{column}{self.row}"]

    def add_work_item(self, work_item: tuple[str, datetime.date, Decimal]):
        """
        Add the work item to the row.

        :param work_item: the work item
        :type work_item: tuple[str, datetime.date, Decimal]
        :return: None.
        """
        _, date, spent_time = work_item
        cell = self.mapping_work_item_cell((self.issue, date, spent_time))
        cell.data_type = "n"
        cell.value = spent_time

    def proper_cell_style(self, cell: Cell) -> str:
        """
        Get the cell style based on the state.

        :param cell: the cell
        :type cell: Cell
        :return: the style name.
        :rtype: str
        """
        # if the cell is not a work item
        if cell not in self.pyxl_cells():
            return cell.style
        # if the deadline exists and the cell date coincides with it
        elif self.deadline and self.excel_prop.get_date_by_cell(cell) == self.deadline:
            return "deadline"
        # if the cell is not the last in the row
        elif cell.coordinate != self.cell_last.coordinate:
            return "active"
        # the rest options
        elif self.state is not None:
            return self.excel_prop.dict_state_style[self.state]
        # if the error occurred
        else:
            print(f"Proper style does not work in a correct way. Cell: {cell.coordinate}, TableCell: {self.issue}")
            print(f"Work cells: {str(self.pyxl_cells())}.\nDeadline: {self.deadline}, "
                  f"last cell: {self.cell_last.coordinate}, state: {self.state}")
            return "basic"

    def post_processing(self):
        """Process the work cells after completing the issue modifications:\n
            the deadline style;\n
            the formula;\n
            the issue and parent issue hyperlinks;\n
            the proper cell styles."""
        # specify the deadline style
        cell_deadline = self._shift_cell(2)
        self.excel_prop.styles.set_style("deadline_issue", cell_deadline)
        # specify the formula
        cell_formula: Cell = self.ws[f"NS{self.row}"]
        cell_formula.value = f"=SUM(G{self.row}:NR{self.row})"
        cell_formula.data_type = "f"
        # specify the issue hyperlink if not self-made
        if self.issue is not None:
            self.cell.hyperlink = f"https://youtrack.protei.ru/issue/{self.issue}"
        # specify the parent issue hyperlink if exists
        if self.parent is not None:
            self._shift_cell(-1).hyperlink = f"https://youtrack.protei.ru/issue/{self.parent}"
        # specify the proper style if the cell is not defined for more prior state
        for cell in self.pyxl_cells():
            if cell.style not in ("weekend", "deadline", "sick", "vacation"):
                apply_style = self.proper_cell_style(cell)
                self.excel_prop.styles.set_style(apply_style, cell)
            else:
                continue
        return

    def replace_issue(self, row: int):
        """
        Replace the issue attribute values to the new row.

        :param int row: the new row
        :return: None.
        """
        old_row = self.row
        for column in self.excel_prop.dict_attr_column.values():
            self.excel_prop.replace_cell(from_=self.ws[f"{column}{old_row}"], to_=self.ws[f"{column}{row}"])
            self.ws[f"{column}{old_row}"].value = None
            self.ws[f"{column}{old_row}"].style = "basic"
        return


class _RowEqual:
    """
    Specify the instance to work with the issues having the same names.

    Params:
        excel_prop --- the ExcelProp instance;\n
        issue --- the issue name;\n

    Properties:
        cells --- the cells with the same issue names;\n
        dict_issues --- the dictionary of the issue cells and state priorities;\n

    Functions:
        work_item_cells() --- get the cells to join;\n
        cell_final() --- get the final cell to keep in the table;\n
        join_cells() --- join the cells into the one issue;\n
    """

    def __init__(self, excel_prop: ExcelProp, issue: str):
        self.excel_prop = excel_prop
        self.issue = issue

    def __str__(self):
        cell_string = ", ".join([cell.coordinate for cell in self.cells])
        return f"_RowEqual: issue name - {self.issue},\ncells - {cell_string}"

    def __repr__(self):
        return f"_RowEqual({format(self.excel_prop, 'row_equal')}, {self.issue})"

    def __len__(self):
        return len(self.cells)

    def __bool__(self):
        return len(self.cells) > 1

    @property
    def cells(self) -> list[Cell]:
        """
        Get the issue cells with the same names.

        :return: the cells.
        :rtype: list[Cell]
        """
        return [cell for cell in self.excel_prop.table_cells if cell.value == self.issue]

    @property
    def dict_issues(self) -> dict[Cell, int]:
        """
        Get the dictionary of the issue cells and state priorities.

        :return: the cell and state priority mapping.
        :rtype: dict[Cell, int]
        """
        dict_cell_priority: dict[Cell, int] = dict()
        for cell in self.cells:
            state = self.excel_prop.cell_states[cell.coordinate]
            dict_cell_priority[cell] = self.excel_prop.dict_state_priority[state]
        return dict_cell_priority

    def work_item_cells(self) -> list[Cell]:
        """
        Get the work item cells to join.

        :return: the work item cells.
        :rtype: list[Cell]
        """
        work_cells: list[Cell] = []
        for cell in self.cells:
            if cell == self.cell_final():
                continue
            else:
                for item in self.excel_prop.cell_in_range(f"G{cell.row}", f"NR{cell.row}"):
                    if item.value is not None:
                        work_cells.append(item)
                    else:
                        continue
        return work_cells

    def cell_final(self) -> Cell:
        """
        Get the final cell to keep.

        :return: the cell
        :rtype: Cell
        """
        max_priority = max(priority for priority in self.dict_issues.values())
        max_prior_min_row = min(cell.row for cell, priority in self.dict_issues.items() if priority == max_priority)
        for cell in self.cells:
            if cell.row == max_prior_min_row():
                return cell

    def join_cells(self):
        """Join the work items to the final issue."""
        for work_item_cell in self.work_item_cells():
            self.excel_prop.replace_cell(
                from_=work_item_cell, to_=f"{work_item_cell.column_letter}{self.cell_final().row}")
        for issue_cell in self.cells:
            if issue_cell != self.cell_final():
                row = issue_cell.row
                for column in ("B", "C", "D", "E"):
                    self.excel_prop.replace_cell(from_=f"{column}{row}")
