from pathlib import Path
import openpyxl
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.cell.cell import Cell
from yt_config import UserConfig
from _style_work_item import _StyleWorkItemList
from yt_requests import User, Issue, IssueWorkItem
from xl_table import ExcelProp
from join_user_xl import JoinUserXL


def main():
    # specify the config file
    youtrack_config: UserConfig = UserConfig.set_config_file(UserConfig.path)
    # specify the User instance
    user: User = User(youtrack_config)
    # get the full information from thr YouTrack and work with the IssueWorkItem instances
    user.pre_processing()
    # print the new information from the YouTrack
    print("Задачи:")
    issue: Issue
    for issue in user.dict_issue.values():
        print(format(issue, "output"))
    print("----------")
    print("Затраченное время:")
    issue_work_item: IssueWorkItem
    for issue_items in user.dict_issue_work_item.values():
        for issue_work_item in issue_items:
            print(format(issue_work_item, "output"))
    print("----------")

    # get the path to the table
    path = Path(youtrack_config.get_json_attr("path_table")).resolve()
    # generate the file name to save the changes
    name: str = path.name
    path_new_workbook = path.with_name(f"generated_{name}")

    # specify the workbook
    wb: Workbook = openpyxl.load_workbook(path)
    # specify the worksheet
    ws_title: str = wb.sheetnames[1]
    ws: Worksheet = wb[ws_title]

    # specify the style list
    style_list: _StyleWorkItemList = _StyleWorkItemList()
    # get the Normal style since it is built-in
    style_list.get_wb_style(wb, "Normal")
    # specify the ExcelProp instance
    excel_prop: ExcelProp = ExcelProp(ws, style_list)
    # delete the empty rows and join the rows with the same issue name
    excel_prop.pre_processing()

    # specify the JoinUserXL instance
    join_user_xl: JoinUserXL = JoinUserXL(user, excel_prop)

    # add new issues to the table
    join_user_xl.add_all_issues()
    print("Задачи добавлены.")
    # update the TableCell instances since the rows are changed
    excel_prop.update_table_cells()

    # modify the issues in the table
    join_user_xl.modify_all_issues()
    print("Параметры задач (название, родительская задача, дедлайн) обновлены.")
    # update the TableCell instances since the rows are changed
    excel_prop.update_table_cells()

    # update the issue state in the table
    join_user_xl.update_states()
    print("Статусы задач обновлены.")

    # add the IssueWorkItems to the table
    join_user_xl.add_new_work_items()
    print("Затраченное время добавлено.")

    # add cell styles, formulae, and hyperlinks
    excel_prop.post_processing()

    # update the date period:
    # set the new period start to the current period end to get values without day skip
    # set the new period end to the current day
    youtrack_config.update_json_item(youtrack_config.path, "period_start", youtrack_config.get_json_attr("period_end"))
    youtrack_config.update_json_item(youtrack_config.path, "period_end", youtrack_config.today.isoformat())
    print("Даты начала и конца периода в файле youtrack.json обновлены.")

    # save the changes, create the new file, and close it
    wb.save(path_new_workbook)
    wb.close()
    print("Работа завершена. Путь до файла:")
    print(f"{path_new_workbook.resolve()}")
    input("Нажмите любую клавишу, чтобы закрыть окно...")


if __name__ == "__main__":
    main()
