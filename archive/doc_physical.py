import re
from functools import cached_property
from typing import Union, Any, Optional, Iterable
from numpy.core import datetime64
from const_archive import Shelf, CabinetName, CabinetNumber, Cabinet, DocLanguage, Manufacturer, Distributor, \
    DocClassification, DocShipment, DocOrigin, DocProduct
from singleton_type import Singleton


def convert_shelf(shelf: Union[int, Shelf]) -> Shelf:
    return shelf if isinstance(shelf, Shelf) else Shelf(shelf)


def convert_cabinet(cabinet: Union[str, int, CabinetName, CabinetNumber, Cabinet]) -> Cabinet:
    if isinstance(cabinet, int):
        cabinet_number = CabinetNumber(cabinet)
        return Cabinet[cabinet_number.name]
    elif isinstance(cabinet, str):
        cabinet_number = CabinetNumber(int(cabinet))
        return Cabinet[cabinet_number.name]
    elif isinstance(cabinet, CabinetName) or isinstance(cabinet, CabinetNumber):
        return Cabinet[cabinet.name]
    else:
        return cabinet


def _cabinet_valid(cabinet: Union[str, int, CabinetName, CabinetNumber, Cabinet]):
    return convert_cabinet(cabinet) is not None


def _shelf_valid(shelf: Union[int, Shelf]):
    return convert_shelf(shelf) is not None


class DocArchive(Singleton):
    def __init__(self):
        self.name: str = "DocArchive"
        self.dict_doc: dict[int, DocPhysical] = dict()
        self.dict_name_id: dict[str, int] = dict()

    def __repr__(self):
        return f"<DocArchive(name={self.name})>"

    def __str__(self):
        return f"DocArchive: {self.name}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return repr(self)
        if format_spec == "internal":
            return str(self)

    def __len__(self):
        return len(self.dict_doc)

    def __bool__(self):
        return len(self.dict_doc) > 0

    @cached_property
    def shelves(self):
        return [item for item in Shelf]

    @cached_property
    def cabinets(self):
        return [item for item in Cabinet]

    @cached_property
    def cabinet_numbers(self):
        return [item for item in CabinetNumber]

    @cached_property
    def cabinet_names(self):
        return [item for item in CabinetName]

    @cached_property
    def cabinet_number_values(self):
        return [item.value for item in CabinetNumber]

    @cached_property
    def cabinet_name_values(self):
        return [item.value for item in CabinetName]

    def doc_in_shelf(self, cabinet: Union[str, int, CabinetName, CabinetNumber, Cabinet], shelf: Union[int, Shelf]):
        doc_cabinet: Cabinet = convert_cabinet(cabinet)
        doc_shelf: Shelf = convert_shelf(shelf)

        if _cabinet_valid(doc_cabinet) and _shelf_valid(doc_shelf):
            doc: DocPhysical
            return [doc for doc in self.dict_doc.values() if doc.cabinet == doc_cabinet and doc.shelf == doc_shelf]
        else:
            return

    def doc_in_cabinet(self, cabinet: Union[str, int, CabinetName, CabinetNumber, Cabinet]):
        doc_cabinet: Cabinet = convert_cabinet(cabinet)
        if _cabinet_valid(doc_cabinet):
            doc: DocPhysical
            return [doc for doc in self.dict_doc.values() if doc.cabinet == doc_cabinet]
        else:
            return

    @property
    def names(self) -> set[str]:
        return {doc_physical_name for doc_physical_name in self.dict_name_id}


class DocPhysical:
    index = 0

    def __init__(self, name: str, cabinet_attr: Union[str, int, CabinetName, CabinetNumber, Cabinet],
                 shelf_attr: Union[int, Shelf]):
        self.name = name
        self.parent: DocArchive = DocArchive()
        self.cabinet: Cabinet = convert_cabinet(cabinet_attr)
        self.shelf: Shelf = convert_shelf(shelf_attr)
        self.physical_id: int = DocPhysical.index
        self.creation_date: datetime64 = datetime64("today", "s")
        self.modification_date: datetime64 = datetime64("NaT")

        self.parent.dict_doc[self.physical_id] = self
        self.parent.dict_name_id[self.name] = self.physical_id
        DocPhysical.index += 1

    def __repr__(self):
        return f"<DocPhysical(cabinet_attr={self.cabinet}, shelf={self.shelf}, doc_archive={self.parent.name})>, " \
               f"id={self.physical_id}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"DocPhysical: cabinet = {self.cabinet.cabinet_number}, {self.cabinet.cabinet_name}, " \
                   f"shelf = {self.shelf.shelf}, creation date = {self.creation_date.item()}, " \
                   f"modification date = {self.modification_date.item()};"
        elif format_spec == "location":
            return f"DocPhysical<cabinet: {self.cabinet.cabinet_number}, shelf: {self.shelf.shelf}>"

    def __getattribute__(self, key: str):
        if key in ("cabinet_number", "cabinet_name"):
            return object.__getattribute__(self.cabinet, key)
        else:
            return object.__getattribute__(self, key)

    def __setattr__(self, key: str, value: Any):
        if key == "cabinet_number":
            if isinstance(value, int):
                cabinet = convert_cabinet(value)
                self.__dict__["cabinet"] = cabinet
            elif isinstance(value, CabinetNumber):
                cabinet = Cabinet[value.name]
                self.__dict__["cabinet"] = cabinet
            else:
                raise TypeError(f"The key {key} must have a value of the int or CabinetNumber type, "
                                f"not {type(value)}.")
        elif key == "cabinet_name":
            if isinstance(value, str):
                cabinet = convert_cabinet(value)
                self.__dict__["cabinet"] = cabinet
            elif isinstance(value, CabinetName):
                cabinet = Cabinet[value.name]
                self.__dict__["cabinet"] = cabinet
            else:
                raise TypeError(f"The key {key} must have a value of the str or CabinetName type, "
                                f"not {type(value)}.")
        else:
            object.__setattr__(self, key, value)

        self.modification_date = datetime64("now", "s")

    def __hash__(self):
        return hash(self.physical_id)

    def __key(self):
        return self.cabinet, self.shelf

    def __eq__(self, other):
        if isinstance(other, DocPhysical):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, DocPhysical):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @property
    def location(self):
        return self.cabinet.cabinet_number, self.shelf.shelf

    @property
    def _is_valid(self):
        return _cabinet_valid(self.cabinet) and _shelf_valid(self.shelf)


class DocFileArchive(Singleton):
    def __init__(self):
        self.name = "DocFileArchive"
        self.dict_doc = dict()
        self.dict_name_id: dict[str, int] = dict()

    def __repr__(self):
        return f"<DocFileArchive(name={self.name})>"

    def __str__(self):
        return f"DocFileArchive: {self.name}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return repr(self)
        if format_spec == "internal":
            return str(self)

    @property
    def names(self) -> set[str]:
        return {file_doc_name for file_doc_name in self.dict_name_id}

    @property
    def tags(self) -> set[str]:
        item: Document
        return {tag for item in self.dict_doc.values() for tag in item.tags}

    def doc_by_language(self, language_attr: Union[str, DocLanguage]):
        if isinstance(language_attr, str):
            language = DocLanguage[language_attr]
        elif isinstance(language_attr, DocLanguage):
            language = language_attr
        else:
            return
        item: Document
        return tuple(item for item in self.dict_doc.values() if item.language == language)

    def doc_by_tag(self, tag: str):
        if tag in self.tags:
            item: Document
            return tuple(item for item in self.dict_doc.values() if tag in item.tags)
        else:
            print(f"The tag {tag} is not assigned to any document.")
            return

    def doc_by_name(self, name: str):
        if name in self.names:
            item: Document
            for item in self.dict_doc.values():
                if item.name == name:
                    return item

    def doc_by_product(self, attr: str, value: Any):
        if attr in ("product", "modification", "department", "serial", "summary"):
            if isinstance(value, str):
                item: Document
                return [item for item in self.dict_doc.values() if getattr(item, attr) == value]
            else:
                print(f"The {attr} must be str, not {type(value)}.")
                return
        elif attr == "manufacturer":
            if isinstance(value, str):
                item: Document
                return [item for item in self.dict_doc.values() if item.product.manufacturer.value == value]
            elif isinstance(value, Manufacturer):
                item: Document
                return [item for item in self.dict_doc.values() if item.product.manufacturer == value]
            else:
                print(f"The {attr} must be str or Manufacturer, not {type(value)}.")
                return
        else:
            print(f"The attribute {attr} and value {value} are invalid.")
            return

    def doc_by_shipment(self, attr: str, value: Any):
        date_bool = attr == "shipment_date" and isinstance(value, datetime64)
        number_bool = attr == "shipment_number" and isinstance(value, str)
        customer_bool = attr == "shipment_customer" and isinstance(value, str)

        if date_bool or number_bool or customer_bool:
            item: Document
            return [item for item in self.dict_doc.values() if getattr(item.shipment, attr) == value]
        elif attr == "shipment_distributor":
            if isinstance(value, str):
                item: Document
                return [item for item in self.dict_doc.values() if item.shipment.shipment_distributor.value == value]
            elif isinstance(value, Distributor):
                item: Document
                return [item for item in self.dict_doc.values() if item.shipment.shipment_distributor == value]
            else:
                print(f"The {attr} must be str or Distributor, not {type(value)}.")
                return
        else:
            print(f"The attribute {attr} and value {value} are invalid.")
            return

    def doc_path(self, names: Iterable[str]):
        hyperlinks = []
        for name in names:
            document = self.doc_by_name(name)
            if document:
                hyperlinks.append(document.hyperlink)
            else:
                hyperlinks.append(f"The name {name} is invalid.")
        return hyperlinks


class Document:
    index = 0

    def __init__(
            self,
            name: str,
            doc_classification: DocClassification, *,
            summary: str = None,
            language: str = None,
            shipment: DocShipment = DocShipment(),
            product: DocProduct = DocProduct(),
            hyperlink: str = None,
            creation_date: datetime64 = datetime64("today", "s"),
            modification_date: datetime64 = datetime64("NaT")):
        self.doc_id = Document.index

        self.parent = DocFileArchive()
        self.name = name
        self.doc_classification = doc_classification
        self.summary = summary
        self.language = language
        self.product = product
        self.shipment = shipment
        self.hyperlink = hyperlink
        self.creation_date = creation_date
        self.modification_date = modification_date
        self.tags: set[str] = set()

        self.parent.dict_doc[self.doc_id] = self
        self.parent.dict_name_id[self.name] = self.doc_id
        Document.index += 1

    def __str__(self):
        return f"{self.name} ({self.doc_classification}), дата создания: {self.creation_date.item()}"

    def __repr__(self):
        return f"Document({self.name}, {self.doc_classification}, {self.summary}, {self.language}, " \
               f"{self.shipment}, {self.modification_date.item()})"

    def __hash__(self):
        return hash((self.name, self.doc_classification, self.identifier))

    def __key(self):
        return self.name, self.doc_classification

    def __eq__(self, other):
        if isinstance(other, Document):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, Document):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __getattribute__(self, key):
        if key in ("shipment_number", "shipment_date", "shipment_customer", "shipment_product"):
            return object.__getattribute__(self.shipment, key)
        elif key in ("doc_type", "doc_short", "doc_category", "doc_standard"):
            return object.__getattribute__(self.doc_classification, key)
        elif key in ("product", "modification", "department", "serial", "summary", "manufacturer"):
            return object.__getattribute__(self.product, key)
        else:
            return object.__getattribute__(self, key)

    def add_tags(self, tags: Iterable[str]):
        self.tags.update(tags)
        print(f"The tags {', '.join(tags)} are added.")
        self.modification_date = datetime64("today", "s")
        return

    def delete_tags(self, tags: Iterable[str]):
        flag_any_removed: bool = False
        for tag in tags:
            try:
                self.tags.remove(tag)
            except KeyError as e:
                print(f"KeyError, {str(e)}. The tag {tag} is not assigned.")
            else:
                flag_any_removed = True
                print(f"The tag {tag} is removed.")
            finally:
                continue
        if flag_any_removed:
            self.modification_date = datetime64("today", "s")
        return

    def comply_pattern(self, mask: str, attr: str) -> Optional[bool]:
        pattern = re.compile(mask)
        if attr == "name":
            return re.search(pattern, self.name, re.IGNORECASE) is not None
        else:
            return None


class ArchiveFull(Singleton):
    def __init__(self):
        self.doc_archive: DocArchive = DocArchive()
        self.doc_file_archive: DocFileArchive = DocFileArchive()

    def __repr__(self):
        return f"<ArchiveFull(doc_archive={format(self.doc_archive, 'internal')}, " \
               f"doc_file_archive={format(self.doc_file_archive, 'internal')})>"

    def __str__(self):
        return f"ArchiveFull: DocArchive = {self.doc_archive.name}, DocFileArchive = {self.doc_file_archive.name}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return repr(self)
        if format_spec == "internal":
            return str(self)

    @property
    def common_names(self):
        return set.intersection(self.doc_archive.names, self.doc_file_archive.names)

    @property
    def only_physical(self):
        return self.doc_archive.names.intersection(self.doc_file_archive.names)

    @property
    def only_file(self):
        return self.doc_file_archive.names.intersection(self.doc_archive.names)

    @property
    def doc_id(self):
        dict_id: dict[str, tuple[Optional[int], Optional[int]]] = dict()

        for name in self.common_names:
            dict_id[name] = (self.doc_archive.dict_name_id[name], self.doc_file_archive.dict_name_id[name])
        for name_physical in self.only_physical:
            dict_id[name_physical] = (self.doc_archive.dict_name_id[name_physical], None)
        for name_file in self.only_file:
            dict_id[name_file] = (None, self.doc_archive.dict_name_id[name_file])

        return dict_id

    def get_by_id(self, identifier: int, doc_origin: DocOrigin) -> Union[DocPhysical, Document, None]:
        if doc_origin == DocOrigin.PHYSICAL and identifier in self.doc_archive.dict_doc:
            return self.doc_archive.dict_doc[identifier]
        elif doc_origin == DocOrigin.FILE and identifier in self.doc_file_archive.dict_doc:
            return self.doc_file_archive.dict_doc[identifier]
        else:
            print(f"The file with the id {identifier} is not found in the {doc_origin.value} docs.")
            return

    def get_by_name(self, name: str, doc_origin: DocOrigin):
        if doc_origin == DocOrigin.PHYSICAL:
            identifier = self.doc_id[name][0]
            return self.doc_archive.dict_doc[identifier] if identifier else None
        if doc_origin == DocOrigin.FILE and name in self.doc_id:
            identifier = self.doc_id[name][1]
            return self.doc_archive.dict_doc[identifier] if identifier else None

    def get_attribute(self, doc_name: str, doc_origin: DocOrigin, attr: str):
        try:
            doc = self.get_by_name(doc_name, doc_origin)
            attr_value = getattr(doc, attr)
        except TypeError as e:
            print(f"TypeError: {str(e)}")
            return
        except KeyError as e:
            print(f"KeyError: {str(e)}")
            return
        except ValueError as e:
            print(f"ValueError: {str(e)}")
            return
        except OSError as e:
            print(f"OSError: {e.errno}, {e.args}. {e.strerror}")
            return
        else:
            return attr_value

    def doc_in_shelf(self, cabinet: Union[str, int, CabinetName, CabinetNumber, Cabinet], shelf: Union[int, Shelf]):
        return self.doc_archive.doc_in_shelf(cabinet, shelf)


def main():
    print(convert_cabinet("1"))


if __name__ == "__main__":
    main()
