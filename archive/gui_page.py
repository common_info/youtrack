from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter.font import Font
from const_archive import Cabinet, Shelf
from doc_physical import DocArchive, ArchiveFull, DocPhysical


class HomePage(Tk):
    def __init__(self):
        super().__init__()
        self.title = "Main Page"
        self.geometry = "500x1500"
        self.style = ttk.Style(self)

    def configuration(self):
        basic_font = Font(
            root=self,
            exists=False,
            name="basic",
            family="Times New Roman",
            size=28,
            weight="normal",
            slant="italic",
            underline=False,
            overstrike=False)

        self.style.configure(
            "ShelfButton.TButton",
            activebackground="grey",
            activeforeground="black",
            background="grey",
            border=5,
            font=basic_font,
            foreground="black",
            height=150,
            justify="center",
            relief="raised")


class GeneralPage(ttk.Frame):
    def __init__(self, master):
        super().__init__(
            master,
            border=5,
            padding=20,
            relief="groove",
            width=500)
        self.label = ttk.Label(self, text="Page", anchor="n", justify="center", width=50)


class ShelfButton(ttk.Button):
    def __init__(
            self,
            master,
            cabinet: Cabinet,
            shelf: Shelf):
        self.cabinet = cabinet
        self.shelf = shelf
        self.doc_archive = DocArchive()

        super().__init__(
            master,
            command=self.selected,
            name="ShelfButton",
            text=f"Cabinet {cabinet.cabinet_number} Shelf {shelf.shelf}")

    def selected(self):
        text = "\n".join([doc.name for doc in self.doc_archive.doc_in_shelf(self.cabinet, self.shelf)])
        messagebox.showinfo(
            f"Cabinet {self.cabinet.cabinet_number} Shelf {self.shelf.shelf} documents", text)



class ArchiveLabelFrame(ttk.LabelFrame):
    def __init__(
            self,
            master,
            doc_archive: DocArchive):
        super().__init__(master, name="ArchiveLabelFrame")
        self.label = ttk.Label(self, text="Archive", justify="center")
        self.label.pack(padx=20, pady=20)

        self.doc_archive = doc_archive

        cabinet: Cabinet
        shelf: Shelf
        self.shelf_buttons = dict()
        for cabinet in Cabinet.__dict__.keys():
            self.shelf_buttons[cabinet.cabinet_number] = []
            self.cabinet_frame = ttk.LabelFrame(self, name="TLabelFrame")
            for shelf in Shelf.__dict__.keys():
                self.shelf_buttons[cabinet.cabinet_number].append(ShelfButton(self.cabinet_frame, cabinet, shelf))


class SearchEntry(ttk.Entry):
    def __init__(self, master, homepage: Tk):
        super().__init__(
            master,
            background="white",
            foreground="black",
            justify="left",
            name="search_entry",
            width=250)
        self.style = ttk.Style(homepage)
        self.style.configure("SearchEntry.TEntry")


class TypeRadiobutton(ttk.Radiobutton):
    def __init__(self, master, text: str, variable: IntVar, value: int, homepage: Tk):
        super().__init__(
            master,
            name="TypeRadiobutton",
            text=text,
            value=value,
            variable=variable)
        self.style = ttk.Style(homepage)
        self.style.configure("TypeRadiobutton.TRadiobutton")


class PropertyCombobox(ttk.Combobox):
    def __init__(self, master, homepage: Tk):
        super().__init__(
            master,
            background="white",
            foreground="black",
            height=50,
            justify="center",
            name="PropertyCombobox",
            textvariable=StringVar(self, None, "property_value"),
            values=["file name", "shipment name", "product name", "customer", "tag"],
            width=250)
        self.current(0)
        self.style = ttk.Style(homepage)
        self.style.configure("PropertyCombobox")


class SearchButton(ttk.Button):
    def __init__(self, master, homepage: Tk):
        super().__init__(
            master,
            name="Search button",
            padding=20,
            text="Search",
            style="SearchButton.TButton"
        )
        self.style = ttk.Style(homepage)
        self.style.configure("Search.TButton")


class TypeFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)


class SearchFrame(ttk.Frame):
    def __init__(self, master, archive_full: ArchiveFull, homepage: Tk):
        super().__init__(master)
        self.archive_full = archive_full
        self.style = ttk.Style(homepage)
        self.style.configure("SearchFrame.TFrame")

        self.selected = IntVar(self, 0, "selected")

        self.type_frame = TypeFrame(self)
        self.type_frame.pack()

        self.radiobutton_location = TypeRadiobutton(self.type_frame, "Location", self.selected, 1, homepage)
        self.radiobutton_search = TypeRadiobutton(self.type_frame, "Search", self.selected, 2, homepage)
        self.radiobutton_location.grid(row=0, column=0)
        self.radiobutton_search.grid(row=0, column=1)

        self.entry_search = SearchEntry(self, homepage)
        self.entry_search.pack()

        self.property_combobox = PropertyCombobox(self, homepage)
        self.property_combobox.pack()

        self.search_button = SearchButton(self, homepage)
        self.search_button["command"] = self.search
        self.search_button.pack()

    def search(self):
        search_attribute = self.entry_search.get()
        property_attribute = self.property_combobox.get()
        if self.selected == 0:
            return
        elif self.selected == 1:
            # "file name", "shipment name", "product name", "customer", "tag"



def search(search_attribute: str, property_attribute: str, selected: int, archive_full: ArchiveFull):
    if selected == 0:
        return
    if selected == 1:
        if property_attribute == "file name":
            if search_attribute in archive_full.doc_archive.names:
                doc_result: DocPhysical = archive_full.doc_archive.dict_doc[search_attribute]
                return f"Cabinet {doc_result.cabinet.cabinet_number} Shelf {doc_result.shelf.shelf}"
            else:
                return f"The file named {} is not found"
        if property_attribute == "shipment_name":

