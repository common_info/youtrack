import hashlib
import logging
import sqlite3
from functools import cached_property
from sqlite3 import Row
from typing import Optional, Any, NamedTuple
from numpy.core import datetime64, isnat
from const_archive import DocClassification, DocShipment, DocProduct
from full_database import FullDatabase, PATH

sqlite3.enable_callback_tracebacks(True)

filename = "./basic_log.log"
fmt = "%(levelname)s %(asctime)s, %(funcName) --- %(message)s"
level = logging.INFO
logging.basicConfig(filename=filename, format=fmt, level=level)
logger = logging.getLogger(__name__)
handler = logging.FileHandler("log.log")
formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def get_sha256(input_string: str) -> str:
    """
    Gets the SHA-256 hash of the file.

    :param str input_string: the line to encrypt
    :return: the SHA-256 checksum in the hex format.
    :rtype: str
    """
    hash_sha256 = hashlib.sha256()
    hash_sha256.update(input_string.encode("utf-8"))

    return hash_sha256.hexdigest()


class DocArchive(FullDatabase):
    def __init__(self):
        super().__init__(PATH)
        self.dict_doc_physical: dict[str, DocPhysical] = dict()
        self.name: str = "doc_archive"
        self.parent = FullDatabase(PATH)
        print(self.parent)

        self.parent.dict_archive["doc_archive"] = self

    def __repr__(self):
        return f"<DocArchive()>, DocArchive.name = {self.name}, ArchiveFull.name = {self.parent}"

    def __len__(self):
        return len(self.table)

    def __bool__(self):
        return self.dict_doc_physical is not None

    @property
    def values(self):
        return self.get_all_kwargs("SELECT * from doc_archive")

    @cached_property
    def qmarks(self):
        return ", ".join(["?"] * len(self.table))

    @cached_property
    def shelves(self):
        return {item for item in self.get_all_kwargs("SELECT shelf_number FROM shelf")}

    @cached_property
    def cabinets(self):
        return {item for item in self.get_all_kwargs("SELECT cabinet_number FROM cabinet")}

    @property
    def all_doc_id(self):
        return self.get_all_kwargs("SELECT doc_physical_id FROM doc_archive_doc_physical")

    def doc_by_id(self, doc_id: int):
        if doc_id in self.all_doc_id:
            return self.get_one_args("SELECT doc_physical_id FROM doc_archive WHERE id = ?", (doc_id,))

    @property
    def all_doc_proxy(self):
        docs = self.get_all_kwargs("SELECT * FROM doc_physical")
        return [
            _PhysicalProxy(
                index=index,
                name=name,
                shelf_id=shelf_id,
                creation_date=creation_date,
                modification_date=modification_date)
            for index, name, shelf_id, creation_date, modification_date in docs]


class DocFileArchive(FullDatabase):
    def __init__(self):
        super().__init__(PATH)
        self.dict_doc_file: dict[str, DocFile] = dict()
        self.name: str = "doc_file_archive"
        self.table: dict[str, type] = dict(zip(DocFile.slots_attrs, DocFile.slots_types))
        self.parent = FullDatabase(PATH)

        self.parent.dict_archive["doc_file_archive"] = self

    def __repr__(self):
        return f"<DocFileArchive()>, DocFileArchive.name = {self.name}, ArchiveFull.name = {super().name}"

    def __len__(self):
        return len(self.table)

    def __bool__(self):
        return self.dict_doc_file is not None

    @property
    def values(self):
        return self.get_all_kwargs("SELECT * from doc_file_archive")

    @property
    def all_doc_file_id(self):
        return self.get_all_kwargs("SELECT id FROM doc_file")

    @property
    def all_doc_file_name(self):
        return self.get_all_kwargs("SELECT name FROM doc_file")

    def doc_by_file_id(self, file_id: int):
        if file_id in self.all_doc_file_id:
            return self.get_one_args("SELECT * FROM doc_file WHERE id = ?", (file_id,))
        else:
            raise KeyError(f"The doc file id {file_id} is not found.")

    def doc_by_file_name(self, file_name: str):
        if file_name in self.all_doc_file_name:
            return self.get_one_args("SELECT * FROM doc_file WHERE name = ?", (file_name,))
        else:
            raise KeyError(f"The doc file name {file_name} is not found.")

    @property
    def _all_proxy_files(self):
        docs = self.get_all_kwargs("SELECT * FROM doc_file_archive")
        print([doc.keys() for doc in docs])
        return [
            _FileProxy(
                index=index,
                name=name,
                summary=summary,
                classification_id=classification_id,
                product_id=product_id,
                language_id=language_id,
                shipment_id=shipment_id,
                file=file,
                creation_date=creation_date,
                modification_date=modification_date)
            for (index, name, summary, classification_id, product_id, language_id, shipment_id, file, creation_date,
                 modification_date) in docs]

    def __getitem__(self, item):
        if item in self.dict_doc_file:
            return self.dict_doc_file[item]
        else:
            raise KeyError(f"The item {item} is not found.")


class _PhysicalProxy(NamedTuple):
    index: int
    name: str
    shelf_id: int
    creation_date: datetime64
    modification_date: datetime64
    database: FullDatabase = FullDatabase()

    def __repr__(self):
        return f"<_PhysicalProxy(index={self.index}, name={self.name}, shelf_id={self.shelf_id}, " \
               f"creation_date={self.creation_date.item()}, modification_date={self.modification_date.item()})>"

    def convert_to_doc_physical(self):
        shelf: int = self.database.get_one_args("SELECT shelf_number FROM shelf WHERE id = ?", (self.shelf_id,))
        cabinet = self.database.get_one_args("SELECT cabinet_number FROM shelf WHERE id = ?", (self.shelf_id,))

        return DocPhysical(
            index=self.index,
            name=self.name,
            shelf=shelf,
            cabinet=cabinet,
            creation_date=self.creation_date,
            modification_date=self.modification_date)


class _FileProxy(NamedTuple):
    index: int
    name: str
    summary: Optional[str]
    classification_id: int
    product_id: int
    language_id: int
    shipment_id: int
    file: Optional[bytes]
    creation_date: datetime64
    modification_date: datetime64
    database: FullDatabase = FullDatabase()

    def _classification(self):
        classification_sql_request = "SELECT * FROM classification WHERE id = ?"
        classification = self.database.get_one_args(classification_sql_request, self.classification_id)
        return classification

    def convert_to_doc_file(self):
        classification_sql_request = "SELECT * FROM classification WHERE id = ?"
        classification = self.database.get_one_args(classification_sql_request, self.classification_id)
        product_sql_request = "SELECT * FROM product WHERE id = ?"
        product = self.database.get_one_args(product_sql_request, self.product_id)
        language_sql_request = "SELECT name FROM language WHERE id = ?"
        language = self.database.get_one_args(language_sql_request, self.language_id)
        shipment_sql_request = "SELECT name FROM shipment WHERE id = ?"
        shipment = self.database.get_one_args(shipment_sql_request, self.shipment_id)

        return DocFile(
            index=self.index,
            name=self.name,
            classification=classification,
            summary=self.summary,
            language=language,
            shipment=shipment,
            product=product,
            file=self.file,
            creation_date=self.creation_date,
            modification_date=self.modification_date)


class DocPhysical:
    __slots__ = ("index", "name", "shelf", "cabinet", "creation_date", "modification_date", "parent")
    slots_attrs = ("index", "name", "shelf", "cabinet")
    slots_types = (type(int), type(str), type(int), type(int))
    const_attrs = {"index", "creation_date", "parent"}
    var_attrs = {"name", "shelf", "cabinet", "modification_date"}

    if set.intersection(const_attrs, var_attrs):
        raise AttributeError(f"const_attrs={const_attrs}, var_attrs={var_attrs}. Check the attributes.")

    def __init__(
            self,
            index: int,
            name: str,
            shelf: int,
            cabinet: int,
            creation_date: datetime64 = datetime64("NaT"),
            modification_date: datetime64 = datetime64("NaT")):
        if isnat(creation_date):
            creation_date = datetime64("now", "ms")
        if isnat(modification_date):
            modification_date = datetime64("now", "ms")

        self.index = index
        self.name = name
        self.shelf = shelf
        self.cabinet = cabinet
        self.creation_date = creation_date
        self.modification_date = modification_date
        self.parent = DocArchive()
        self.parent.dict_doc_physical[self.name] = self

    def __repr__(self):
        string = ", ".join(f"{key}={getattr(self, key)}" for key in self.__slots__)
        return f"<DocPhysical({string})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"DocPhysical: index = {self.index}, name = {self.name}, shelf = {self.shelf}, " \
                   f"cabinet = {self.cabinet}, created = {self.creation_date.item()}, " \
                   f"modified = {self.modification_date.item()}"

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, DocPhysical):
            return self.name == other.name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, DocPhysical):
            return self.name != other.name
        else:
            return NotImplemented

    def _set_name(self, value: Any):
        if not isinstance(value, str):
            raise TypeError(f"The value {value} must be str, not {type(value)}.")

        if value == self.name:
            print("The name is not changed.")
        elif value in self.parent.dict_doc_physical:
            raise ValueError(f"The name {value} already exists.")
        else:
            object.__setattr__(self, "name", value)

    def _set_cabinet(self, value: Any):
        if not isinstance(value, int):
            raise TypeError(f"The value {value} must be int, not {type(value)}.")

        if value == self.cabinet:
            print("The cabinet is not changed.")
        elif value not in self.parent.cabinets:
            cabinet_string = "".join([str(item) for item in self.parent.cabinets])
            raise ValueError(f"The cabinet {value} is not in the tuple({cabinet_string}).")
        else:
            object.__setattr__(self, "cabinet", value)

    def _set_shelf(self, value: Any):
        if not isinstance(value, int):
            raise TypeError(f"The value {value} must be int, not {type(value)}.")

        if value == self.shelf:
            print("The cabinet is not changed.")
        elif value not in self.parent.shelves:
            shelf_string = "".join([str(item) for item in self.parent.shelves])
            raise ValueError(f"The cabinet {value} is not in the tuple({shelf_string}).")
        else:
            object.__setattr__(self, "shelf", value)

    def _set_modification_date(self):
        object.__setattr__(self, "modification_date", datetime64("now", "ms"))

    def __setattr__(self, key, value):
        if key in self.const_attrs:
            print(f"The attribute {key} cannot be assigned.")
        elif key in self.var_attrs:
            if key == "name":
                self._set_name(value)
            elif key == "cabinet":
                self._set_cabinet(value)
            elif key == "shelf":
                self._set_shelf(value)
            elif key == "modification_time":
                self._set_modification_date()
            else:
                raise KeyError(f"The key {key} and value {value} cannot be assigned.")


class DocFile:
    __slots__ = (
        "index", "name", "classification", "summary", "language", "shipment", "product", "file", "creation_date",
        "modification_date", "parent")
    slots_attrs = ("index", "name", "classification", "summary", "language", "shipment", "product", "file")
    slots_types = (
        type(int), type(str), type(DocClassification), type(str), type(str), type(DocShipment), type(DocProduct),
        type(bytes))
    const_attrs = {"index", "creation_date", "parent"}
    var_attrs = {"name", "classification", "summary", "language", "shipment", "product", "file", "modification_date"}

    if set.intersection(const_attrs, var_attrs):
        raise AttributeError(f"const_attrs={const_attrs}, var_attrs={var_attrs}. Check the attributes.")

    def __init__(
            self,
            index: int,
            name: str,
            classification: DocClassification,
            summary: str,
            language: str,
            shipment: DocShipment,
            product: DocProduct,
            file: bytes,
            creation_date: datetime64 = datetime64("NaT"),
            modification_date: datetime64 = datetime64("NaT")):
        if isnat(creation_date):
            creation_date = datetime64("now", "ms")
        if isnat(modification_date):
            modification_date = datetime64("now", "ms")

        self.index = index
        self.name = name
        self.classification = classification
        self.summary = summary
        self.language = language
        self.shipment = shipment
        self.product = product
        self.file = file
        self.creation_date = creation_date
        self.modification_date = modification_date
        self.parent = DocFileArchive()
        self.parent.dict_doc_file[self.name] = self

    def __repr__(self):
        string = ", ".join(f"{key}={getattr(self, key)}" for key in self.__slots__)
        return f"<DocPhysical({string})>"

    def __hash__(self):
        return hash((self.index, self.name))

    def __key(self):
        return self.index, self.name

    def __eq__(self, other):
        if isinstance(other, DocFile):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, DocFile):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def _set_name(self, value: Any):
        if not isinstance(value, str):
            raise TypeError(f"The value {value} must be str, not {type(value)}.")

        if value == self.name:
            print("The name is not changed.")
        elif value in self.parent.dict_doc_file:
            raise ValueError(f"The name {value} already exists.")
        else:
            object.__setattr__(self, "name", value)

    def _set_summary(self, value: Any):
        if not isinstance(value, str):
            raise TypeError(f"The value {value} must be str, not {type(value)}.")

        if value == self.summary:
            print("The summary is not changed.")
        else:
            object.__setattr__(self, "summary", value)

    def _set_language(self, value: Any):
        if not isinstance(value, str):
            raise TypeError(f"The value {value} must be str, not {type(value)}.")

        if value == self.language:
            print("The language is not changed.")
        else:
            object.__setattr__(self, "language", value)

    def _set_modification_date(self):
        object.__setattr__(self, "modification_date", datetime64("now", "ms"))

    def __setattr__(self, key, value):
        if key in self.const_attrs:
            print(f"The attribute {key} cannot be assigned.")
        elif key in self.var_attrs:
            if key == "name":
                self._set_name(value)
            elif key == "summary":
                self._set_summary(value)
            elif key == "language":
                self._set_language(value)
            elif key == "modification_time":
                self._set_modification_date()
            else:
                raise KeyError(f"The key {key} and value {value} cannot be assigned.")


class ClassificationTable(NamedTuple):
    index: int
    parent: FullDatabase = FullDatabase()

    @property
    def category_id(self) -> int:
        sql_request_category_id: str = "SELECT category_id FROM classification WHERE id = ?"
        return self.parent.get_one_args(sql_request_category_id, self.index)

    @property
    def type_id(self) -> Optional[int]:
        sql_request_type_id: str = "SELECT type_id FROM classification WHERE id = ?"
        return self.parent.get_one_args(sql_request_type_id, self.index)

    @property
    def short_id(self) -> Optional[int]:
        sql_request_short_id: str = "SELECT short_id FROM classification WHERE id = ?"
        return self.parent.get_one_args(sql_request_short_id, self.index)

    @property
    def standard_id(self) -> Optional[list[int]]:
        sql_request_standard_id: str = "SELECT standard_id FROM classification_standard WHERE classification_id = ?"
        return self.parent.get_all_args(sql_request_standard_id, self.index)

    def from_table(self):
        category: str = self.parent.get_one_args("SELECT name FROM category WHERE id = ?", (self.index,))
        type_: str = self.parent.get_one_args("SELECT name FROM type WHERE id = ?", (self.index,))
        short: str = self.parent.get_one_args("SELECT name FROM short WHERE id = ?", (self.index,))

        sql_request_standard: str = "SELECT name FROM standard WHERE id IN ?"
        if self.standard_id:
            standard: Optional[list[str]] = self.parent.get_all_args(sql_request_standard, (*self.standard_id,))
        else:
            standard = None

        return Classification(category, type_, short, standard)


class Classification(NamedTuple):
    category: str
    type_: Optional[str]
    short: Optional[str]
    standard: Optional[list[str]]
    parent: FullDatabase = FullDatabase()

    @property
    def category_id(self) -> int:
        return self.parent.get_one_args("SELECT id FROM category WHERE name = ?", (self.category,))

    @property
    def type_id(self) -> Optional[int]:
        if self.type_:
            return self.parent.get_one_args("SELECT id FROM type WHERE name = ?", (self.type_,))
        return

    @property
    def short_id(self) -> Optional[int]:
        if self.short:
            return self.parent.get_one_args("SELECT id FROM short WHERE name = ?", (self.short,))
        return

    @property
    def standard_id(self) -> Optional[list[int]]:
        if self.standard:
            return self.parent.get_all_args("SELECT id FROM standard WHERE name = ?", (self.standard,))
        return

    def classification_id(self):
        sql_request_index = "SELECT id FROM classification WHERE " \
                            "category_id = {category_id} AND type_id = {type_id} AND short_id = {short_id}"
        classification_id = self.parent.get_one_kwargs(
            sql_request_index, category_id=self.category_id, type_id=self.type_id, short_id=self.short_id)
        return classification_id if classification_id else -1

    def add_value(self):
        if self.classification_id() == -1:
            index = self.parent.table_max_id("classification")
            self.parent.add_value("classification", 4, (index, self.category_id, self.type_id, self.short_id))
            if self.standard_id:
                for standard_id in self.standard_id:
                    self.parent.add_value("classification_standard", 2, (index, standard_id))
        return



def main():
    db = FullDatabase("C:\\Users\\tarasov-a\\Desktop\\archive_.db")
    print(db.connection)
    print(db.table_names)
    doc_archive = DocArchive()
    print(doc_archive.parent.name)
    doc_file_archive = DocFileArchive()
    # print("proxy")
    # print(doc_file_archive._all_proxy_files)
    item: Row
    print([row for item in doc_archive.values for row in item])
    print([item.keys() for item in doc_file_archive.all_doc_file_id])


if __name__ == "__main__":
    db = FullDatabase("C:\\Users\\tarasov-a\\Desktop\\archive_.db")
    db.cursor.execute("PRAGMA foreign_keys = ON;")
    req = db._sql_request_kwargs("SELECT {column} FROM {table_name}", column="id", table_name="classification")
    print(f"req={req}")
    db.add_value("classification", 4, (80, 1, 1, 1))
    # db.cursor.execute("INSERT INTO classification VALUES (1, '1', 1, 1)")
    db.connection.commit()
