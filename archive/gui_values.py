from tkinter import *
from tkinter.font import Font
from gui_page import GUIElement, GUIType, GUIState, GUIAnchor, GUIRelief, GUIJustify, GUIOrient, GUIRadioButton
from doc_physical import Shelf, Cabinet
from tkinter.ttk import Combobox

font_radiobutton = Font(
    name="font_radiobutton",
    exists=False,
    family="Times New Roman",
    size=36,
    weight="normal",
    slant="roman",
    underline=False,
    overstrike=False)

font_shelf = Font(
    name="font_shelf",
    exists=False,
    family="Times New Roman",
    size=20,
    weight="normal",
    slant="roman",
    underline=False,
    overstrike=False)

font_archive = Font(
    name="font_archive",
    exists=False,
    family="Times New Roman",
    size=32,
    weight="bold",
    slant="roman",
    underline=False,
    overstrike=False)


font_search = Font(
    name="font_search",
    exists=False,
    family="Times New Roman",
    size=28,
    weight="normal",
    slant="italic",
    underline=False,
    overstrike=False)


def button_cabinet_shelf(cabinet: Cabinet, shelf: Shelf) -> Button:
    button = GUIElement(
        name=f"button_{cabinet.cabinet_number}_{shelf.shelf}",
        # textvariable=None,
        style_type=GUIType.BUTTON,
        activebackground="grey",
        activeforeground="black",
        anchor=GUIAnchor.NORTH,
        background="grey",
        border=5,
        default=GUIState.NORMAL,
        foreground="black",
        font=Font(),
        height=150,
        justify=GUIJustify.LEFT,
        relief=GUIRelief.RAISED,
        state=GUIState.ACTIVE,
        text=f"Cabinet {cabinet.cabinet_number} Shelf {shelf.shelf}",
        width=400)
    return button.build()


def labelframe_cabinet(cabinet: Cabinet) -> LabelFrame:
    labelframe = GUIElement(
        # name=f"cabinet_{cabinet.cabinet_number}",
        style_type=GUIType.LABELFRAME,
        activebackground="grey",
        activeforeground="black",
        anchor=GUIAnchor.NORTH,
        background="grey",
        border=5,
        default=GUIState.NORMAL,
        foreground="black",
        font=Font(),
        height=150,
        justify=GUIJustify.LEFT,
        relief=GUIRelief.RAISED,
        state=GUIState.ACTIVE,
        text=f"Cabinet {cabinet.cabinet_number}",
        width=400)
    return labelframe.build()


def labelframe_archive() -> LabelFrame:
    labelframe = GUIElement(
        # name="archive",
        style_type=GUIType.LABELFRAME,
        activebackground="white",
        activeforeground="white",
        anchor=GUIAnchor.NORTH,
        background="grey",
        border=5,
        default=GUIState.NORMAL,
        foreground="white",
        font=font_archive,
        height=200,
        justify=GUIJustify.CENTER,
        relief=GUIRelief.SUNKEN,
        state=GUIState.ACTIVE,
        text="Archive",
        width=1500)
    return labelframe.build()


def entry_search() -> Entry:
    entry = GUIElement(
        background="grey",
        border=2,
        foreground="black",
        font=font_search,
        justify=GUIJustify.LEFT,
        name="search",
        relief=GUIRelief.FLAT,
        state=GUIState.ACTIVE,
        width=300,
        textvariable=StringVar(name="search"),
        exportselection=False)
    return entry.build()


def radiobutton_result_type(text: str, value: GUIRadioButton) -> Radiobutton:
    radiobutton = GUIElement(
        activebackground="white",
        activeforeground="red",
        anchor=GUIAnchor.EAST,
        background="white",
        border=2,
        foreground="black",
        font=font_radiobutton,
        height=50,
        justify=GUIJustify.LEFT,
        name="radiobutton",
        relief=GUIRelief.GROOVE,
        state=GUIState.NORMAL,
        text=text,
        textvariable=StringVar(name="radiobutton"),
        value=value,
        width=50)
    return radiobutton.build()


def combobox_choose() -> Combobox:
    combobox = GUIElement(
        background="white",
        foreground="black",
        height=50,
        justify=GUIJustify.LEFT,
        name="choose",
        state=GUIState.ACTIVE,
        textvariable=StringVar(name="choose"),
        values=["file name", "shipment name", "product name", "customer", "tag"],
        width=300)
    return combobox.build()
