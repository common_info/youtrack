import logging
from enum import Enum
from functools import cached_property
from pathlib import Path
from sqlite3 import Connection, connect, Row, Cursor, OperationalError, IntegrityError, DataError, NotSupportedError, \
    InternalError, DatabaseError, ProgrammingError
from typing import Union, Any

from singleton_type import Singleton

PATH = "/Users/user/Desktop/archive_.db"
# PATH = "C:\\Users\\tarasov-a\\Desktop\\archive_.db"

class DocumentType(Enum):
    PHYSICAL = "physical"
    FILE = "file"


class FullDatabase(Singleton):
    logger = logging.getLogger(__name__)
    handler = logging.FileHandler("log.log")
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    def __init__(self, path: Union[str, Path] = PATH):
        self.name: str = "DB Archive"
        self.path = path
        self.connection: Connection = connect(self.path)
        self.connection.row_factory = Row
        self.dict_archive: dict[str, Any] = dict()
        self.unsaved_data: bool = False
        # logging.info(repr(self))

    def __repr__(self):
        return f"<FullDatabase({self.path})>, FullDatabase.name = {self.name}"

    @property
    def cursor(self) -> Cursor:
        return self.connection.cursor()

    def table_columns(self, table: str):
        table_name = self._check_table_name(table)
        column_info = self.get_all_kwargs("PRAGMA table_info({table_name})", table_name=table_name)
        column: Row
        return [column[1] for column in column_info]

    @cached_property
    def table_names(self):
        return [value[1] for value in self.cursor.execute("select * from sqlite_master where type='table'").fetchall()]

    def close(self):
        self.logger.info("The connection is closed.")
        self.connection.close()

    def __getitem__(self, item):
        if item in self.dict_archive:
            return self.dict_archive[item]
        else:
            self.logger.error(KeyError(f"The invalid archive name {item}."))

    def __contains__(self, item):
        return item in self.table_names

    def __len__(self):
        return len(self.dict_tables)

    def get_all_kwargs(self, base_sql_request: str, **kwargs):
        return self.cursor.execute(self._sql_request_kwargs(base_sql_request, **kwargs)).fetchall()

    def get_one_kwargs(self, base_sql_request: str, **kwargs):
        return self.cursor.execute(self._sql_request_kwargs(base_sql_request, **kwargs)).fetchone()

    def get_all_args(self, base_sql_request: str, *args):
        return self.cursor.execute(self._sql_request_args(base_sql_request, *args)).fetchall()

    def get_one_args(self, base_sql_request: str, *args):
        return self.cursor.execute(self._sql_request_args(base_sql_request, *args)).fetchone()

    def insert_args(self, base_sql_request: str, *args):
        return self.cursor.execute(base_sql_request, *args)

    def insert_kwargs(self, base_sql_request: str, **kwargs):
        return self.cursor.execute(base_sql_request, **kwargs)

    def _check_table_name(self, table_name: str):
        if table_name not in self:
            self.logger.error(KeyError(f"The table name {table_name} is invalid."))
        return table_name

    @staticmethod
    def qmarks(length: int):
        return ", ".join(["?"] * length)

    @staticmethod
    def _sql_request_kwargs(base_sql_request: str, **kwargs):
        return base_sql_request.format(**kwargs) if kwargs is not None else base_sql_request

    @staticmethod
    def _sql_request_args(base_sql_request: str, *args):
        return base_sql_request.format(*args)

    def table_attrs_num(self, table: str):
        return len(self.table_columns(table))

    def add_value(self, table: str, num_attrs: int, *args):
        table_name = self._check_table_name(table)
        print(table_name)
        sql_request: str = "INSERT INTO {table_name} VALUES ({qmarks})"
        base_sql_request: str = self._sql_request_kwargs(
            sql_request, table_name=table_name, qmarks=self.qmarks(num_attrs))
        print(f"base_sql_request={base_sql_request}")
        try:
            self.insert_args(base_sql_request, *args)
        except OperationalError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except IntegrityError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except DataError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except NotSupportedError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except ProgrammingError as e:
            # logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
            print(f"{e.args}")
        except InternalError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        except DatabaseError as e:
            self.logger.warning(f"{e.sqlite_errorcode}, {e.sqlite_errorname}, {e.args}")
        else:
            self.unsaved_data = True

    def save_changes(self, user_input: bool = True):
        if super().unsaved_data:
            if user_input:
                self.connection.commit()
                self.logger.info("All changes are saved.")
            else:
                self.connection.rollback()
                self.logger.info("All changes are cancelled.")
        else:
            self.logger.info("No unsaved changes.")

    def get_changes(self):
        return self.connection.total_changes

    @property
    def map_physical_file(self) -> list[zip]:
        values: list[tuple[int, int]] = self.get_all_kwargs("SELECT * FROM doc_archive_doc_physical")
        return [zip(physical_id, file_id) for value in values for physical_id, file_id in value]

    def table_max_id(self, table: str):
        if table in self.table_names:
            return self.get_one_kwargs("SELECT COUNT(id) FROM {table}", table=table)

    def switch_physical_file(self, index: int, doc_type: DocumentType):
        if doc_type == DocumentType.PHYSICAL:
