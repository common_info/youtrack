from enum import Enum
from typing import Any, NamedTuple
from numpy.core import datetime64


class Shelf(Enum):
    FIRST = 1
    SECOND = 2
    THIRD = 3
    FOURTH = 4
    FIFTH = 5
    SIXTH = 6

    def __init__(self, shelf: int):
        self.shelf = shelf

    @classmethod
    def __missing__(cls, key):
        print(f"The key {key} is missing.")
        return


class CabinetNumber(Enum):
    FIRST = 1
    SECOND = 2
    THIRD = 3

    def __init__(self, cabinet_number: int):
        self.cabinet_number = cabinet_number

    @classmethod
    def __missing__(cls, key):
        print(f"The key {key} is missing.")
        return


class CabinetName(Enum):
    FIRST = "ПРОТЕЙ НТЦ"
    SECOND = "ПРОТЕЙ СТ (АРХ.)"
    THIRD = "ПРОТЕЙ (СЕРТ.)"

    def __init__(self, cabinet_name: str):
        self.cabinet_name = cabinet_name

    @classmethod
    def __missing__(cls, key):
        print(f"The key {key} is missing.")
        return


class Cabinet(Enum):
    FIRST = CabinetNumber.FIRST, CabinetName.FIRST
    SECOND = CabinetNumber.SECOND, CabinetName.SECOND
    THIRD = CabinetNumber.THIRD, CabinetName.THIRD

    def __init__(self, cabinet_number, cabinet_name):
        self.cabinet_number = cabinet_number
        self.cabinet_name = cabinet_name

    @classmethod
    def _missing_(cls, key):
        print(f"The key {key} is missing.")
        return None, None


class DocLanguage(Enum):
    RUS = "русский"
    ENG = "английский"

    def __init__(self, language: str):
        self.language = language

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocStandard(Enum):
    """Specify the state standards."""
    GOST_2_102_2013 = "ГОСТ 2.102-2013"
    GOST_2_106_96 = "ГОСТ 2.106-96"
    GOST_2_109_73 = "ГОСТ 2.109-73"
    GOST_2_114_2016 = "ГОСТ 2.114-2016"
    GOST_2_119_2013 = "ГОСТ 2.119-2013"
    GOST_2_120_2013 = "ГОСТ 2.120-2013"
    GOST_2_601_2019 = "ГОСТ 2.601-2019"
    GOST_2_610_2006 = "ГОСТ 2.610-2006"
    GOST_2_701_2008 = "ГОСТ 2.701-2008"

    GOST_3_1105_2011 = "ГОСТ 3.1105-2011"
    GOST_3_1118_82 = "ГОСТ 3.1118-82"
    GOST_3_1122_84 = "ГОСТ 3.1122-84"
    GOST_3_1404_86 = "ГОСТ 3.1404-86"

    GOST_19_202_78 = "ГОСТ 19.202-78"
    GOST_19_301_79 = "ГОСТ 19.301-79"
    GOST_19_401_78 = "ГОСТ 19.401-78"
    GOST_19_402_78 = "ГОСТ 19.402-78"
    GOST_19_403_79 = "ГОСТ 19.403-79"
    GOST_19_404_79 = "ГОСТ 19.404-79"
    GOST_19_502_78 = "ГОСТ 19.502-78"
    GOST_19_503_79 = "ГОСТ 19.503-79"
    GOST_19_504_79 = "ГОСТ 19.504-79"
    GOST_19_505_79 = "ГОСТ 19.505-79"
    GOST_19_507_79 = "ГОСТ 19.507-79"

    GOST_34_201_89 = "ГОСТ 34.201-89"

    GOST_R_2_106_2019 = "ГОСТ Р 2.106-2019"
    GOST_R_2_610_2019 = "ГОСТ Р 2.610-2019"
    GOST_R_2_711_2019 = "ГОСТ Р 2.711-2019"

    GOST_RV_15_211_2002 = "ГОСТ РВ 15.211-2002"

    STO_202_2019 = "СТО 202-2019"

    def __init__(self, standard: str):
        self.standard = standard

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocCategory(Enum):
    """Specify the document categories."""
    BASIC_DESIGN = "Технический проект"
    DESIGN_DOC = "Конструкторская документация"
    DEVELOPMENT_DOC = "Программная документация"
    OPERATION_DOC = "Эксплуатационная документация"
    PRODUCTION_DOC = "Технологическая документация"
    FOREIGN = "Зарубежье"

    def __init__(self, category: str):
        self.category = category

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocShort(Enum):
    """Specify the document type abbreviations."""
    AUTHENTIC_TEXT_HOLDERS_LIST_SHORT = "05"
    SOURCE_CODE_SHORT = "12"
    PROGRAM_DESCRIPTION_SHORT = "13"
    LIST_OPERATION_PAPERS_20_SHORT = "20"
    PRODUCT_STATUS_RECORD_30_SHORT = "30"
    USE_DESCRIPTION_SHORT = "31"
    SYSTEM_PROGRAMMER_GUIDE_SHORT = "32"
    PROGRAMMER_GUIDE_SHORT = "33"
    OPERATOR_GUIDE_SHORT = "34"
    TESTING_PROGRAMME_AND_PROCEDURE_51_SHORT = "51"
    EXPLANATORY_NOTE_81_SHORT = "81"

    OTHERS_SHORT = "90-99"

    DATABASE_CATALOG_SHORT = "В7"
    MESSAGES_OUTPUT_SHORT = "В8"
    LIST_REFERENCE_PAPERS_SHORT = "ВД"
    OPERATION_DOCUMENTATION_TYPES_SHORT = "ВдЭД"
    LIST_BOUGHT_IN_PERMISSION_PAPERS_SHORT = "ВИ"
    GENERAL_ARRANGEMENT_DRAWING_SHORT = "ВО"
    LIST_BOUGHT_IN_PAPERS_SHORT = "ВП"
    LIST_ASSEMBLY_PAPERS_SHORT = "ВП/ВСИ"
    LIST_SPECIFICATIONS_SHORT = "ВС"
    LIST_OPERATION_PAPERS_PROGRAMMING_SHORT = "ВЭ"

    OUTLINE_DRAWING_SHORT = "ГЧ"

    SECURITY_SYSTEM_ADMINISTRATOR_GUIDE_SHORT = "Д3"
    INFORMATION_SECURITY_OFFICER_GUIDE_SHORT = "Д4"
    SECURITY_TOOL_SYSTEM_GUIDE_D5_SHORT = "Д5"
    SECURITY_TOOL_SYSTEM_GUIDE_D6_SHORT = "Д6"
    SECURITY_TOOL_OPERATOR_GUIDE_SHORT = "Д7"
    CONFIGURATION_TEST_CASE_SHORT = "Д8"
    JOINT_RESOLUTION_SHORT = "Д9"
    ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION_SHORT = "Д11"

    PRODUCT_COMPONENT_STRUCTURE_SHORT = "Е1"

    SPTA_SET_LIST_SHORT = "ЗИ"

    INSTRUCTIONS_SHORT = "И"
    SPTA_USE_INSTRUCTION_SHORT = "И1"
    USER_GUIDE_SHORT = "И3"
    DATABASE_MANAGEMENT_INSTRUCTION_SHORT = "И4"
    SECURITY_ADMINISTRATOR_GUIDE_SHORT = "И5"
    MOUNTING_COMMISSIONING_INSTRUCTION_SHORT = "ИМ"
    INSPECTION_AND_MAINTENANCE_GUIDE_SHORT = "ИС"
    OPERATION_INSTRUCTION_SHORT = "ИЭ"

    PROCESS_FLOW_CHART_SHORT = "КТП"

    MOUNTING_DRAWING_SHORT = "МЧ"
    PROCESS_CHART_SHORT = "МК"

    PRODUCT_DESCRIPTION_SHORT = "ОИ"
    DATABASE_DESCRIPTION_SHORT = "ОБД"

    PROCESS_AND_TECHNOLOGY_DESCRIPTION_SHORT = "ПГ"
    GENERAL_SYSTEM_DESCRIPTION_SHORT = "ПД"
    EXPLANATORY_NOTE_PZ_SHORT = "ПЗ"
    TESTING_PROGRAMME_AND_PROCEDURE_PM_SHORT = "ПМ"
    PRODUCT_CERTIFICATE_SHORT = "ПС"

    ADMINISTRATOR_GUIDE_SHORT = "РА"
    INSTALLATION_GUIDE_SHORT = "РИ"
    CONFIGURATION_GUIDE_SHORT = "РН"
    CALCULATION_SHORT = "РР"
    SERVICE_CHARGE_SETTLEMENT_SHORT = "РТО"
    OPERATION_GUIDE_SHORT = "РЭ"

    ASSEMBLY_DRAWING_SHORT = "СБ"
    SECURITY_SYSTEM_GUIDE_SHORT = "СЗИ"

    PROCESSING_INSTRUCTION_SHORT = "ТИ"
    TECHNICAL_DESCRIPTION_SHORT = "ТО"
    BASIC_DESIGN_LIST_SHORT = "ТП"
    TECHNICAL_SPECIFICATION = "ТУ"

    PACKING_DRAWING_SHORT = "УЧ"

    PRODUCT_STATUS_RECORD_FO_SHORT = "ФО"

    ELECTRO_SCHEMATIC_DIAGRAM_SHORT = "Э3"
    ELECTRO_CIRCUIT_DIAGRAM_SHORT = "Э4"
    ELECTRO_CONNECTIONS_DIAGRAM_SHORT = "Э5"
    LIST_OPERATION_PAPERS_ED_SHORT = "ЭД"
    LABEL_SHORT = "ЭТ"

    def __init__(self, short: str):
        self.short = short

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocType(Enum):
    """Specify the document types."""
    AUTHENTIC_TEXT_HOLDERS_LIST = "Ведомость держателей подлинников"
    SPTA_SET_LIST = "Ведомость ЗИП"
    LIST_BOUGHT_IN_PAPERS = "Ведомость покупных изделий"
    LIST_BOUGHT_IN_PERMISSION_PAPERS = "Ведомость разрешения применения покупных изделий"
    LIST_SPECIFICATIONS = "Ведомость спецификаций"
    LIST_REFERENCE_PAPERS = "Ведомость ссылочных документов"
    LIST_ASSEMBLY_PAPERS = "Ведомость сборки изделия"
    BASIC_DESIGN_LIST = "Ведомость технического проекта"
    LIST_OPERATION_PAPERS = "Ведомость эксплуатационных документов"

    OPERATION_DOCUMENTATION_TYPES = "Виды для ЭД"

    OUTLINE_DRAWING = "Габаритный чертеж"

    INSTRUCTIONS = "Инструкции"
    SPTA_USE_INSTRUCTION = "Инструкция по использованию ЗИП-О"
    MOUNTING_LAUNCH_CONFIGURATION_TRIAL_INSTRUCTION = "Инструкция по монтажу, пуску, регулированию и обкатке изделия"
    MOUNTING_COMMISSIONING_INSTRUCTION = "Инструкция по монтажу и вводу в эксплуатацию"
    ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION = "Инструкция по организации антивирусной защиты информации"
    DATABASE_MANAGEMENT_INSTRUCTION = "Инструкция по формированию и ведению базы данных"
    OPERATION_INSTRUCTION = "Инструкция по эксплуатации"

    PROCESS_FLOW_CHART = "Карта технологического процесса"
    DATABASE_CATALOG = "Каталог базы данных"
    CONFIGURATION_TEST_CASE = "Контрольный пример по настройке"

    PROCESS_CHART = "Маршрутная карта"
    MOUNTING_DRAWING = "Монтажный чертеж"

    GENERAL_SYSTEM_DESCRIPTION = "Общее описание системы"
    DATABASE_DESCRIPTION = "Описание базы данных"
    PRODUCT_DESCRIPTION = "Описание изделия"
    PROGRAM_DESCRIPTION = "Описание программы"
    USE_DESCRIPTION = "Описание применения"
    PROCESS_AND_TECHNOLOGY_DESCRIPTION = "Описание технологического процесса"

    PRODUCT_CERTIFICATE = "Паспорт"
    EXPLANATORY_NOTE = "Пояснительная записка"
    TESTING_PROGRAMME_AND_PROCEDURE = "Программа и методика испытаний"
    OTHERS = "Прочие документы"

    CALCULATION = "Расчеты"
    SERVICE_CHARGE_SETTLEMENT = "Расчет затрат на техническое обслуживание"

    INSPECTION_AND_MAINTENANCE_GUIDE = "Регламент технического обслуживания"

    ADMINISTRATOR_GUIDE = "Руководство администратора"
    SECURITY_ADMINISTRATOR_GUIDE = "Руководство администратора безопасности"
    SECURITY_SYSTEM_ADMINISTRATOR_GUIDE = "Руководство администратора СЗИ"
    OPERATOR_GUIDE = "Руководство оператора"
    SECURITY_TOOL_OPERATOR_GUIDE = "Руководство оператора КСЗ"
    USER_GUIDE = "Руководство пользователя"
    SECURITY_TOOL_SYSTEM_GUIDE = "Руководство по КСЗ"
    CONFIGURATION_GUIDE = "Руководство по настройке"
    SECURITY_SYSTEM_GUIDE = "Руководство по СЗИ"
    INSTALLATION_GUIDE = "Руководство по установке"
    OPERATION_GUIDE = "Руководство по эксплуатации"
    INFORMATION_SECURITY_OFFICER_GUIDE = "Руководство пользователя СЗИ"
    PROGRAMMER_GUIDE = "Руководство программиста"
    SYSTEM_PROGRAMMER_GUIDE = "Руководство системного программиста"

    ASSEMBLY_DRAWING = "Сборочный чертеж"
    JOINT_RESOLUTION = "Совместное решение"
    MESSAGES_OUTPUT = "Состав выходных данных (сообщений)"
    SPECIFICATION = "Спецификация"

    PRODUCT_COMPONENT_STRUCTURE = "Схема деления структурная"

    ELECTRO_CONNECTIONS_DIAGRAM = "Схема электрическая подключения"
    ELECTRO_SCHEMATIC_DIAGRAM = "Схема электрическая принципиальная"
    ELECTRO_CIRCUIT_DIAGRAM = "Схема электрическая соединений"

    SOURCE_CODE = "Текст программы"
    TECHNICAL_DESCRIPTION = "Техническое описание"
    TECHNICAL_SPECIFICATION = "Технические условия"
    PROCESSING_INSTRUCTION = "Технологическая инструкция"

    PACKING_DRAWING = "Упаковочный чертеж"
    PRODUCT_STATUS_RECORD = "Формуляр"
    DETAIL_DRAWING = "Чертеж детали"
    GENERAL_ARRANGEMENT_DRAWING = "Чертеж общего вида"
    LABEL = "Этикетка"

    def __init__(self, doc_type: str):
        self.doc_type = doc_type

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class Manufacturer(Enum):
    """Specify the product manufacturer company."""
    PROTEI_RD = "НТЦ ПРОТЕЙ"
    PROTEI_ST = "ПРОТЕЙ СТ"

    def __init__(self, company: str):
        self.company = company

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocClassification(Enum):
    CLASS_0 = DocType.LABEL, DocShort.LABEL_SHORT, DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_R_2_610_2019]
    CLASS_1 = DocType.DETAIL_DRAWING, None, DocCategory.DESIGN_DOC, [DocStandard.GOST_2_102_2013]
    CLASS_2 = DocType.PRODUCT_STATUS_RECORD, DocShort.PRODUCT_STATUS_RECORD_30_SHORT, DocCategory.DEVELOPMENT_DOC, \
              []
    CLASS_3 = DocType.PRODUCT_STATUS_RECORD, DocShort.PRODUCT_STATUS_RECORD_FO_SHORT, DocCategory.OPERATION_DOC, \
              [DocStandard.GOST_R_2_610_2019]
    CLASS_4 = DocType.PACKING_DRAWING, DocShort.PACKING_DRAWING_SHORT, DocCategory.DESIGN_DOC, \
              [DocStandard.GOST_2_102_2013]
    CLASS_5 = DocType.PROCESSING_INSTRUCTION, DocShort.PROCESSING_INSTRUCTION_SHORT, DocCategory.PRODUCTION_DOC, \
              [DocStandard.GOST_3_1105_2011]
    CLASS_6 = DocType.TECHNICAL_DESCRIPTION, DocShort.TECHNICAL_DESCRIPTION_SHORT, DocCategory.DESIGN_DOC, \
              []
    CLASS_7 = DocType.TECHNICAL_SPECIFICATION, DocShort.TECHNICAL_SPECIFICATION, DocCategory.DESIGN_DOC, \
              [DocStandard.GOST_2_114_2016]
    CLASS_8 = DocType.SOURCE_CODE, DocShort.SOURCE_CODE_SHORT, DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_19_401_78]
    CLASS_9 = DocType.ELECTRO_CIRCUIT_DIAGRAM, DocShort.ELECTRO_CIRCUIT_DIAGRAM_SHORT, DocCategory.DESIGN_DOC, \
              [DocStandard.GOST_2_701_2008]
    CLASS_10 = DocType.ELECTRO_SCHEMATIC_DIAGRAM, DocShort.ELECTRO_SCHEMATIC_DIAGRAM_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_2_701_2008]
    CLASS_11 = DocType.ELECTRO_CONNECTIONS_DIAGRAM, DocShort.ELECTRO_CONNECTIONS_DIAGRAM_SHORT, \
               DocCategory.DESIGN_DOC, [DocStandard.GOST_2_701_2008]
    CLASS_12 = DocType.PRODUCT_COMPONENT_STRUCTURE, DocShort.PRODUCT_COMPONENT_STRUCTURE_SHORT, \
               DocCategory.DESIGN_DOC, [DocStandard.GOST_R_2_711_2019]
    CLASS_13 = DocType.SPECIFICATION, None, DocCategory.DESIGN_DOC, [DocStandard.GOST_2_102_2013]
    CLASS_14 = DocType.SPECIFICATION, None, DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_19_202_78]
    CLASS_15 = DocType.MESSAGES_OUTPUT, DocShort.MESSAGES_OUTPUT_SHORT, DocCategory.DESIGN_DOC, []
    CLASS_16 = DocType.JOINT_RESOLUTION, DocShort.JOINT_RESOLUTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.STO_202_2019]
    CLASS_17 = DocType.ASSEMBLY_DRAWING, DocShort.ASSEMBLY_DRAWING_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_2_102_2013]
    CLASS_18 = DocType.SYSTEM_PROGRAMMER_GUIDE, DocShort.SYSTEM_PROGRAMMER_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_503_79]
    CLASS_19 = DocType.PROGRAMMER_GUIDE, DocShort.PROGRAMMER_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_504_79]
    CLASS_20 = DocType.INFORMATION_SECURITY_OFFICER_GUIDE, DocShort.INFORMATION_SECURITY_OFFICER_GUIDE_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.STO_202_2019]
    CLASS_21 = DocType.USER_GUIDE, DocShort.USER_GUIDE_SHORT, DocCategory.DESIGN_DOC, [DocStandard.STO_202_2019]
    CLASS_22 = DocType.USER_GUIDE, DocShort.USER_GUIDE_SHORT, DocCategory.OPERATION_DOC, [DocStandard.GOST_34_201_89]
    CLASS_23 = DocType.OPERATION_GUIDE, DocShort.OPERATION_GUIDE_SHORT, DocCategory.OPERATION_DOC, \
               [DocStandard.GOST_2_610_2006]
    CLASS_24 = DocType.OPERATION_GUIDE, DocShort.OPERATION_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_25 = DocType.SECURITY_SYSTEM_GUIDE, DocShort.SECURITY_SYSTEM_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_26 = DocType.CONFIGURATION_GUIDE, DocShort.CONFIGURATION_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_27 = DocType.SECURITY_TOOL_SYSTEM_GUIDE, DocShort.SECURITY_TOOL_SYSTEM_GUIDE_D6_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.STO_202_2019]
    CLASS_28 = DocType.SECURITY_TOOL_SYSTEM_GUIDE, DocShort.SECURITY_TOOL_SYSTEM_GUIDE_D5_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.STO_202_2019]
    CLASS_29 = DocType.INSTALLATION_GUIDE, DocShort.INSTALLATION_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_30 = DocType.SECURITY_TOOL_OPERATOR_GUIDE, DocShort.SECURITY_TOOL_OPERATOR_GUIDE_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.STO_202_2019]
    CLASS_31 = DocType.OPERATOR_GUIDE, DocShort.OPERATOR_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_505_79]
    CLASS_32 = DocType.SECURITY_SYSTEM_ADMINISTRATOR_GUIDE, DocShort.SECURITY_SYSTEM_ADMINISTRATOR_GUIDE_SHORT, \
               DocCategory.DEVELOPMENT_DOC, []
    CLASS_33 = DocType.SECURITY_ADMINISTRATOR_GUIDE, DocShort.SECURITY_ADMINISTRATOR_GUIDE_SHORT, \
               DocCategory.DEVELOPMENT_DOC, []
    CLASS_34 = DocType.ADMINISTRATOR_GUIDE, DocShort.ADMINISTRATOR_GUIDE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_35 = DocType.INSPECTION_AND_MAINTENANCE_GUIDE, DocShort.INSPECTION_AND_MAINTENANCE_GUIDE_SHORT, \
               DocCategory.DESIGN_DOC, []
    CLASS_36 = DocType.CALCULATION, DocShort.CALCULATION_SHORT, DocCategory.BASIC_DESIGN, \
               [DocStandard.GOST_R_2_106_2019]
    CLASS_37 = DocType.SERVICE_CHARGE_SETTLEMENT, DocShort.SERVICE_CHARGE_SETTLEMENT_SHORT, DocCategory.DESIGN_DOC, []
    CLASS_38 = DocType.OTHERS, DocShort.OTHERS_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_39 = DocType.TESTING_PROGRAMME_AND_PROCEDURE, DocShort.TESTING_PROGRAMME_AND_PROCEDURE_51_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_19_301_79]
    CLASS_40 = DocType.TESTING_PROGRAMME_AND_PROCEDURE, DocShort.TESTING_PROGRAMME_AND_PROCEDURE_PM_SHORT, \
               DocCategory.DESIGN_DOC, [DocStandard.GOST_R_2_610_2019]
    CLASS_41 = DocType.EXPLANATORY_NOTE, DocShort.EXPLANATORY_NOTE_PZ_SHORT, DocCategory.BASIC_DESIGN, \
               [DocStandard.GOST_R_2_106_2019, DocStandard.GOST_2_119_2013, DocStandard.GOST_2_120_2013]
    CLASS_42 = DocType.EXPLANATORY_NOTE, DocShort.EXPLANATORY_NOTE_81_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_404_79]
    CLASS_43 = DocType.PRODUCT_CERTIFICATE, DocShort.PRODUCT_CERTIFICATE_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_44 = DocType.PROCESS_AND_TECHNOLOGY_DESCRIPTION, DocShort.PROCESS_AND_TECHNOLOGY_DESCRIPTION_SHORT, \
               DocCategory.DESIGN_DOC, []
    CLASS_45 = DocType.PROGRAM_DESCRIPTION, DocShort.PROGRAM_DESCRIPTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_402_78]
    CLASS_46 = DocType.USE_DESCRIPTION, DocShort.USE_DESCRIPTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_502_78]
    CLASS_47 = DocType.PRODUCT_DESCRIPTION, DocShort.PRODUCT_DESCRIPTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               []
    CLASS_48 = DocType.DATABASE_DESCRIPTION, DocShort.DATABASE_DESCRIPTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               []
    CLASS_49 = DocType.GENERAL_SYSTEM_DESCRIPTION, DocShort.GENERAL_SYSTEM_DESCRIPTION_SHORT, DocCategory.DESIGN_DOC, \
               []
    CLASS_50 = DocType.MOUNTING_DRAWING, DocShort.MOUNTING_DRAWING_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_2_102_2013]
    CLASS_51 = DocType.PROCESS_CHART, DocShort.PROCESS_CHART_SHORT, DocCategory.PRODUCTION_DOC, \
               [DocStandard.GOST_3_1118_82]
    CLASS_52 = DocType.CONFIGURATION_TEST_CASE, DocShort.CONFIGURATION_TEST_CASE_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.STO_202_2019]
    CLASS_53 = DocType.DATABASE_CATALOG, DocShort.DATABASE_CATALOG_SHORT, DocCategory.DESIGN_DOC, []
    CLASS_54 = DocType.PROCESS_FLOW_CHART, DocShort.PROCESS_FLOW_CHART_SHORT, DocCategory.PRODUCTION_DOC, \
               [DocStandard.GOST_3_1404_86]
    CLASS_55 = DocType.SPTA_USE_INSTRUCTION, DocShort.SPTA_USE_INSTRUCTION_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.STO_202_2019, DocStandard.GOST_2_601_2019]
    CLASS_56 = DocType.OPERATION_INSTRUCTION, DocShort.OPERATION_INSTRUCTION_SHORT, DocCategory.DEVELOPMENT_DOC, []
    CLASS_57 = DocType.DATABASE_MANAGEMENT_INSTRUCTION, DocShort.DATABASE_MANAGEMENT_INSTRUCTION_SHORT, \
               DocCategory.DESIGN_DOC, []
    CLASS_58 = DocType.ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION, \
               DocShort.ANTIVIRUS_PROTECTION_MANAGEMENT_INSTRUCTION_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.STO_202_2019]
    CLASS_59 = DocType.MOUNTING_LAUNCH_CONFIGURATION_TRIAL_INSTRUCTION, \
               DocShort.MOUNTING_COMMISSIONING_INSTRUCTION_SHORT, DocCategory.OPERATION_DOC, \
               [DocStandard.GOST_R_2_610_2019]
    CLASS_60 = DocType.MOUNTING_COMMISSIONING_INSTRUCTION, DocShort.MOUNTING_COMMISSIONING_INSTRUCTION_SHORT, \
               DocCategory.DEVELOPMENT_DOC, []
    CLASS_61 = DocType.INSTRUCTIONS, DocShort.INSTRUCTIONS_SHORT, DocCategory.DESIGN_DOC, [DocStandard.GOST_2_106_96]
    CLASS_62 = DocType.OUTLINE_DRAWING, DocShort.OUTLINE_DRAWING_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_2_109_73]
    CLASS_63 = DocType.OPERATION_DOCUMENTATION_TYPES, DocShort.OPERATION_DOCUMENTATION_TYPES_SHORT, \
               DocCategory.DESIGN_DOC, []
    CLASS_64 = DocType.LIST_OPERATION_PAPERS, DocShort.LIST_OPERATION_PAPERS_ED_SHORT, DocCategory.OPERATION_DOC, \
               [DocStandard.GOST_R_2_610_2019]
    CLASS_65 = DocType.LIST_OPERATION_PAPERS, DocShort.LIST_OPERATION_PAPERS_PROGRAMMING_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_R_2_610_2019]
    CLASS_66 = DocType.LIST_OPERATION_PAPERS, DocShort.LIST_OPERATION_PAPERS_20_SHORT, DocCategory.DEVELOPMENT_DOC, \
               [DocStandard.GOST_19_507_79]
    CLASS_67 = DocType.BASIC_DESIGN_LIST, DocShort.BASIC_DESIGN_LIST_SHORT, DocCategory.BASIC_DESIGN, \
               [DocStandard.GOST_2_106_96, DocStandard.GOST_2_120_2013]
    CLASS_68 = DocType.LIST_REFERENCE_PAPERS, DocShort.LIST_REFERENCE_PAPERS_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_R_2_106_2019]
    CLASS_69 = DocType.LIST_SPECIFICATIONS, DocShort.LIST_SPECIFICATIONS_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_R_2_106_2019]
    CLASS_70 = DocType.LIST_ASSEMBLY_PAPERS, DocShort.LIST_ASSEMBLY_PAPERS_SHORT, DocCategory.PRODUCTION_DOC, \
               [DocStandard.GOST_3_1122_84]
    CLASS_71 = DocType.LIST_BOUGHT_IN_PERMISSION_PAPERS, DocShort.LIST_BOUGHT_IN_PERMISSION_PAPERS_SHORT, \
               DocCategory.DESIGN_DOC, [DocStandard.GOST_R_2_106_2019]
    CLASS_72 = DocType.LIST_BOUGHT_IN_PAPERS, DocShort.LIST_BOUGHT_IN_PAPERS_SHORT, DocCategory.DESIGN_DOC, \
               [DocStandard.GOST_R_2_106_2019]
    CLASS_73 = DocType.SPTA_SET_LIST, DocShort.SPTA_SET_LIST_SHORT, DocCategory.OPERATION_DOC, \
               [DocStandard.GOST_R_2_610_2019]
    CLASS_74 = DocType.AUTHENTIC_TEXT_HOLDERS_LIST, DocShort.AUTHENTIC_TEXT_HOLDERS_LIST_SHORT, \
               DocCategory.DEVELOPMENT_DOC, [DocStandard.GOST_19_403_79]
    CLASS_75 = DocType.GENERAL_ARRANGEMENT_DRAWING, DocShort.GENERAL_ARRANGEMENT_DRAWING_SHORT, \
               DocCategory.DESIGN_DOC, []

    def __init__(
            self,
            doc_type: DocType = None,
            doc_short: DocShort = None,
            doc_category: DocCategory = None,
            doc_standard: list[DocStandard] = None):
        if doc_standard is None:
            doc_standard = []

        self.doc_type = doc_type
        self.doc_short = doc_short
        self.doc_category = doc_category
        self.doc_standard = doc_standard

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class Distributor(Enum):
    PROTEI_RD = "ПРОТЕЙ НТЦ"
    PROTEI_ST = "ПРОТЕЙ СТ"
    PROTEI_TL = "ПРОТЕЙ ТЛ"
    PROTEI_LAB = "ПРОТЕЙ-ЛАБ"
    SIGURD_IT = "СИГУРД-АЙТИ"
    NEOS = "НЕОС"
    PROTEI_SM = "Протей СМ"
    IT_ENGINEERING = "ПРОТЕЙ Ай-Ти - Инжиниринг"

    def __init__(self, company: str):
        self.company = company

    @classmethod
    def _missing_(cls, value: object) -> Any:
        return


class DocOrigin(Enum):
    PHYSICAL = "physical"
    FILE = "file"

    def __init__(self, doc_origin: str):
        self.doc_origin = doc_origin

    @classmethod
    def _missing_(cls, value: object) -> Any:
        print(f"The value {repr(value)} is not allowed.")
        return


class DocProduct(NamedTuple):
    product: str = None
    modification: str = None
    department: str = None
    serial: str = None
    summary: str = None
    manufacturer: Manufacturer = None


class DocShipment(NamedTuple):
    shipment_number: str = None
    shipment_date: datetime64 = datetime64("NaT")
    shipment_customer: str = None
    shipment_distributor: Distributor = None

    def __repr__(self):
        return f"<DocShipment(shipment_number={self.shipment_number}, shipment_date={self.shipment_date.item()}, " \
               f"shipment_customer={self.shipment_customer})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"DocShipment: shipment_number={self.shipment_number}, shipment_date={self.shipment_date.item()}, " \
               f"shipment_customer={self.shipment_customer}"
