import tkinter.messagebox
from tkinter import *
from tkinter import ttk
from const_archive import Cabinet, Shelf
from doc_physical import ArchiveFull, DocPhysical


def main(archive: ArchiveFull):
    range_shelf = len(Shelf) + 1
    range_cabinet = len(Cabinet) + 1

    window = Tk()
    window.wm_attributes("-topmost", 1)

    def exit_pressed():
        response = tkinter.messagebox.askyesno("Выход", "Вы уверены, что хотите выйти?")
        if response:
            window.destroy()

    window.protocol("WM_DELETE_WINDOW", exit_pressed)
    window.title("Archive docs")
    window.geometry("450x250")

    archive_label_frame = ttk.LabelFrame(
        window,
        height=250,
        labelanchor="n",
        padding=5,
        relief="raised",
        text="Архив",
        width=100)
    archive_label_frame.grid(row=0, column=0)

    def shelf_pressed(cabinet_number, shelf_number):
        def pressed():
            doc: DocPhysical
            doc_text = "\n".join([doc.name for doc in archive.doc_in_shelf(cabinet_number, shelf_number)])
            shelf_frame = ttk.Frame(window)
            shelf_frame.tkraise()
            # shelf_label = ttk.Label(
            #     shelf_frame,
            #     background="white",
            #     border=5,
            #     justify="center",
            #     relief="flat",
            #     text=doc_text,
            #     width=50)
            # shelf_label.pack()
            # button = ttk.Button(
            #     shelf_frame,
            #     command=shelf_frame.destroy(),
            #     text="Закрыть",
            #     width=50)
            # button.pack()

            return tkinter.messagebox.showinfo("Содержимое", doc_text)
        return pressed

    def radio_pressed():
        return radio_var.get()

    def search_pressed():
        property_attribute = combobox_var.get()
        value_attribute = entry_var.get()

        print(value_attribute)

    for cabinet in range(1, range_cabinet):
        cabinet_frame = ttk.LabelFrame(
            archive_label_frame,
            height=250,
            labelanchor="s",
            padding=5,
            relief="raised",
            text=f"Шкаф {cabinet}",
            width=30)
        cabinet_frame.grid(row=0, column=cabinet)
        for shelf in range(1, range_shelf):
            ttk.Button(
                cabinet_frame,
                command=shelf_pressed(cabinet, shelf),
                default="normal",
                text=f"Полка {shelf}",
                width=10).grid(row=shelf, column=cabinet)

    search_frame = ttk.LabelFrame(
        window,
        height=250,
        labelanchor="n",
        padding=5,
        relief="raised",
        text="Поиск",
        width=25)
    search_frame.grid(row=0, column=1)

    entry_var = StringVar(value="", name="entry_var")

    entry = ttk.Entry(
        search_frame,
        background="white",
        foreground="black",
        justify="left",
        textvariable=entry_var,
        width=25)
    entry.pack()

    radio_frame = ttk.Frame(
        search_frame,
        border=5,
        height=50,
        name="radio_frame",
        padding=0,
        relief="flat",
        width=25)
    radio_frame.pack()

    radio_var = StringVar(value="", name="radio_var")

    radio_1 = ttk.Radiobutton(
        radio_frame,
        command=radio_pressed,
        padding=5,
        text="location",
        variable=radio_var,
        value="Расположение",
        width=10)
    radio_1.grid(row=0, column=0)
    radio_2 = ttk.Radiobutton(
        radio_frame,
        command=radio_pressed,
        padding=5,
        text="file",
        variable=radio_var,
        value="Файл",
        width=10)
    radio_2.grid(row=0, column=1)

    combobox_var = StringVar(value="", name="combobox_var")

    combobox = ttk.Combobox(
        search_frame,
        background="white",
        foreground="red",
        justify="left",
        state="readonly",
        textvariable=combobox_var,
        values=[
            "Название файла",
            "Номер поставки",
            "Дата поставки",
            "Название продукта",
            "Название заказчика",
            "Тэг"],
        width=25)
    combobox.pack()

    button_frame = ttk.Frame(
        search_frame,
        border=5,
        height=50,
        name="radio_frame",
        padding=0,
        relief="flat",
        width=25)
    button_frame.pack()

    search_button = ttk.Button(
        button_frame,
        command=search_pressed,
        default="normal",
        text="Искать",
        width=10)
    search_button.grid(row=0, column=0)

    exit_button = ttk.Button(button_frame, command=exit_pressed, text="Выход", width=10)
    exit_button.grid(row=0, column=1)

    window.mainloop()


if __name__ == "__main__":
    archive_full = ArchiveFull()
    main(archive_full)


def search(archive: ArchiveFull, property_attribute: str, value_attribute: str):
    if property_attribute == "Название файла":
        return archive.doc_file_archive.doc_by_name(value_attribute)
    elif property_attribute == "Номер поставки":
        return archive.doc_file_archive.doc_by_shipment_number(value_attribute)
    elif property_attribute == "Дата поставки":
        return archive.doc_file_archive.doc_by_shipment_date(value_attribute)
    elif property_attribute == "Название продукта":
        product = archive.doc_file_archive.doc_by_product("product", value_attribute)
        modification = archive.doc_file_archive.doc_by_product("modification", value_attribute)
        if product:
            return product
        elif modification:
            return modification
        else:
            return
    elif property_attribute == "Название заказчика":
        return archive.doc_file_archive.doc_by_shipment_customer(value_attribute)
    elif property_attribute == "Тэг":
        return archive.doc_file_archive.doc_by_tag(value_attribute)
    else:
        print("Something went wrong.")
