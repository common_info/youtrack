#!/usr/bin/python
# -*- coding: cp1251 -*-
from datetime import datetime
from typing import Any, Optional, Union, NamedTuple
from numpy.core import datetime64, divide, isnat
from decimal import Decimal
from pathlib import Path
from enum import Enum
from requests import get as req_get
from openpyxl import load_workbook
from openpyxl.utils.cell import coordinate_to_tuple, get_column_letter
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.cell.cell import Cell
from const_stats import convert_long_date, LOGINS_TW, EMPLOYEES, HEADERS_REQUEST, DEFENSE_ISSUES, SPEC_REPORTERS, \
    COLUMN_ATTRS, PROJECTS_REQUEST, LOGINS, start_period_stat, end_period_stat, PROJECT_STATES, ATTRS, \
    COLUMN_ATTRS_NO_ASSIGNEE


class RequestURL(Enum):
    """
    Contain the URL to send the request.

    Values:
        ISSUE --- the URL to request the issues;\n
        WORK_ITEM --- the URL to request the issue work items;\n
    """
    ISSUE = "https://youtrack.protei.ru/api/issues"
    WORK_ITEM = "https://youtrack.protei.ru/api/workItems"

    def __repr__(self):
        return f"<RequestURL.{self.name} = {self.value}>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"url = {self.value}"


class IssueType(Enum):
    """
    Contain the issue type to specify some issues.

    Values:
        GENERAL --- the main issue group;\n
        DEFENSE --- the issue group with the customer of the Defense Ministery;\n
        SPECIAL --- the issue group with the particular reporters;\n
    """
    GENERAL = "general"
    DEFENSE = "defense"
    SPECIAL = "special"
    NON_ASSIGNED = "not assigned"

    def __repr__(self):
        return f"<IssueType.{self.name} = {self.value}>"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"type = {self.value}"


class IssueStat(NamedTuple):
    """
    Contain the Issue parameters.

    Parameters:
        issue --- the issue name;\n
        state --- the issue state;\n
        summary --- the issue summary;\n
        reporter --- the issue reporter;\n
        parent --- the issue parent name;\n
        assignee --- the issue assignee;\n
        assignees_project --- the aditional issue assignees;\n
        is_parent --- the flag to get the company from the parent issue;\n
        deadline --- the issue deadline;\n
        customer --- the issue customer;\n
        department --- the issue department;\n
        product --- the issue product;\n
        company --- the issue company;\n

    Properties:
        issue_type --- the issue type: DEFENSE/GENERAL/SPECIAL;\n
    """
    issue: str
    state: str
    summary: str
    reporter: str
    parent: Optional[str]
    assignee: Optional[str] = None
    assignees_project: list[str] = None
    is_parent: bool = False
    deadline: datetime64 = datetime64("NaT")
    customer: Optional[str] = None
    department: Optional[str] = None
    product: Optional[str] = None
    company: Optional[str] = None

    def __repr__(self):
        return f"<IssueStat(issue={repr(self.issue)}, state={repr(self.state)}, summary={repr(self.summary)}, " \
               f"reporter={repr(self.reporter)}, parent={repr(self.parent)}, assignee={repr(self.assignee)}, " \
               f"assignees_project={repr(self.assignees_project)}, is_parent={self.is_parent}, " \
               f"deadline={str(self.deadline)}, customer={repr(self.customer)}, department={repr(self.department)}, " \
               f"product={repr(self.product)}, company={repr(self.company)}), issue_type={self.issue_type}>"

    def __str__(self):
        if self.parent:
            parent_string = f", parent = {self.parent}"
        else:
            parent_string = ""

        if not isnat(self.deadline):
            deadline_string = f", deadline = {self.deadline.item().isoformat()}"
        else:
            deadline_string = "deadline not defined"

        if self.customer:
            customer_string = f", customer = {self.customer}"
        else:
            customer_string = ""

        if self.department:
            department_string = f", department = {self.department}"
        else:
            department_string = ""

        if self.product:
            product_string = f", product = {self.product}"
        else:
            product_string = ""

        if self.assignee:
            assignee_string = f", assignee = {self.assignee}"
        else:
            assignee_string = ""

        if self.company:
            company_string = f", company = {self.company}"
        else:
            company_string = ""

        if self.assignees_project:
            assignees_combine = ", ".join(self.assignees_project)
            assignees_string = f", assignees = [{assignees_combine}]"
        else:
            assignees_string = ""

        return f"IssueStat: issue = {self.issue}, state = {self.state}, summary = {self.summary}, , " \
               f"reporter = {self.reporter}{parent_string}{assignee_string}{assignees_string}{deadline_string}" \
               f"{customer_string}{department_string}{product_string}{company_string}, issue_type = {self.issue_type}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"IssueStat: issue = {self.issue}, state = {self.state}, summary = {self.summary},  " \
                   f"reporter = {self.reporter}, parent = {self.parent}, assignee = {self.assignee}, " \
                   f"assignees_project = {self.assignees_project}, deadline = {self.deadline.item()}, " \
                   f"customer = {self.customer}, department = {self.department}, product = {self.product}, " \
                   f"company = {self.company}, issue_type = {self.issue_type}"

    @property
    def issue_type(self) -> str:
        """
        Get the issue type due to issue parameters.

        :return: the issue type.
        :rtype: str
        """
        if self.customer in DEFENSE_ISSUES:
            return IssueType.DEFENSE.value
        elif self.reporter in SPEC_REPORTERS:
            return IssueType.SPECIAL.value
        elif not (self.assignee or self.assignees_project):
            return IssueType.NON_ASSIGNED.value
        else:
            return IssueType.GENERAL.value


class IssueWorkItemStat(NamedTuple):
    """
    Contain the IssueWorkItem parameters.

    Parameters:
         issue --- the issue name;\n
         author --- the issue work item author;\n
         spent_time --- the issue work item spent time;\n
    """
    issue: str
    author: str
    spent_time: Decimal

    def __repr__(self):
        return f"<IssueWorkItemStat(issue={repr(self.issue)}, author={repr(self.author)}, " \
               f"spent_time={str(self.spent_time.quantize(Decimal('1.00')))})>"

    def __str__(self):
        return f"IssueWorkItemStat: issue = {self.issue}, author = {self.author}, spent_time = {str(self.spent_time)}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"IssueWorkItemStat: issue = {self.issue}, author = {self.author}, " \
                   f"spent_time = {str(self.spent_time)}"


class RequestAttributes(NamedTuple):
    """
    Contain the request attributes.

    Parameters:
        url --- the URL to send the request;\n
        fields --- the 'fields' part of the request;\n
        query --- the 'query' part of the request;\n
        query_or --- the 'query' part of the request to implement with the OR operator;\n
        custom_fields --- the 'customFields' part of the request;\n
        custom_fields_named --- the 'customFields' part of the request with the specific name;\n

    Functions:
        process_attributes() --- handle the attributes to get params;\n
    """
    url: str
    fields: list[str]
    query: list[tuple[str, str]]
    query_or: Optional[list[tuple[str, str]]]
    custom_fields: Optional[list[str]]
    custom_fields_named: list[tuple[str, str]]

    def __str__(self):
        return f"RequestAttributes: url = {self.url}, headers = {HEADERS_REQUEST}, params = {self.process_attributes()}"

    def __repr__(self):
        return f"RequestAttributes(url={self.url}, query={self.query}, query_or={self.query_or}, " \
               f"custom_fields={self.custom_fields})"

    def process_attributes(self) -> tuple[tuple[str, str], ...]:
        """
        Handle the attributes to get params.

        :return: the params value in the requests_get.
        :rtype: tuple[tuple[str, str], ...]
        """
        fields_string = ",".join(self.fields)
        query_or_string = " OR ".join([f"{item[0]}: {item[1]}" for item in self.query_or])
        query_string = " ".join([f"{item[0]}: {item[1]}" for item in self.query])
        merged_query_string = " ".join((query_string, query_or_string))

        fields_params = ("fields", fields_string)
        query_params = ("query", merged_query_string)
        custom_fields_params = [("customField", item) for item in self.custom_fields]
        params = [fields_params, query_params, *custom_fields_params, *self.custom_fields_named]
        return *params,


def request_issues(login: str) -> RequestAttributes:
    """
    Get the request attributes for Issue.

    :param str login: the user login
    :return: the request attributes.
    :rtype: RequestAttributes
    """
    return RequestAttributes(
        url="https://youtrack.protei.ru/api/issues",
        fields=[
            "idReadable",
            "summary",
            "parent(issues(idReadable,reporter(fullName)))",
            "reporter(fullName)",
            "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"
        ],
        query=[
            ("State", PROJECT_STATES),
        ],
        query_or=[
            ("Assignee", f"{login}"),
            ("Исполнители_Doc", f"{login}"),
            ("Исполнители_Doc_ST", f"{login}"),
            ("Исполнители_Archive", f"{login}"),
            ("Исполнители_Archive_ST", f"{login}"),
        ],
        custom_fields=[
            "Assignee",
            "State",
            "Дедлайн",
            "Продукт",
            "Направление",
            "Заказчик",
            "Компания ПРОТЕЙ",
            "Исполнители_Doc",
            "Исполнители_Doc_ST",
            "Исполнители_Archive",
            "Исполнители_Archive_ST"
        ],
        custom_fields_named=[]
    )


def issues_json() -> tuple[dict[str, Any], ...]:
    """
    Get the response to the issue request.

    :return: the response items.
    :rtype: tuple[dict[str, Any], ...]
    """
    issues_unparsed: list = []
    for login in LOGINS:
        request_attributes: RequestAttributes = request_issues(login)
        url = request_attributes.url
        params = request_attributes.process_attributes()
        response_unparsed = req_get(url=url, headers=HEADERS_REQUEST, params=params).json()
        for issue in response_unparsed:
            issues_unparsed.append(issue)
    return *issues_unparsed,


def parse_response_issue(response_item: dict[str, Any]):
    """
    Parse the response in the dict format to get Issue.

    :param response_item: the response to parse
    :type response_item: dict[str, Union[str, dict]]
    :return: IssueStat.
    """
    # specify the issue name
    issue: str = response_item["idReadable"]
    # specify the parent issue name
    parent_issues = response_item["parent"]["issues"]
    parent: Optional[str]
    # check if the parent issue exists
    if len(parent_issues):
        parent = parent_issues[0]["idReadable"]
    else:
        parent = None
    # specify the issue summary
    summary: str = response_item["summary"]
    # specify the issue reporter
    reporter: str = response_item["reporter"]["fullName"]
    is_parent: bool
    if reporter in LOGINS_TW and parent:
        is_parent = True
    else:
        is_parent = False
    state: Optional[str] = None
    deadline: Optional[datetime64] = datetime64("NaT")
    customer: Optional[str] = None
    department: Optional[str] = None
    product: Optional[str] = None
    company: Optional[str] = None
    assignee: Optional[str] = None
    assignees: list[str] = []

    for item in response_item["customFields"]:
        # specify the issue state
        if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField", "StateProjectCustomField"):
            state = item["value"]["name"]

        # specify the issue deadline
        elif item["$type"] == "DateIssueCustomField":
            deadline = convert_long_date(item["value"])

        # specify the assignee
        elif item["$type"] == "SingleUserIssueCustomField" and item["value"]:
            assignee = item["value"]["name"]

        elif item["$type"] == "SingleEnumIssueCustomField":
            # specify the customer
            if item["projectCustomField"]["field"]["name"] == "Заказчик" and item["value"]:
                customer = item["value"]["name"]
            # specify the company
            elif item["projectCustomField"]["field"]["name"] == "Компания ПРОТЕЙ":
                if item["value"]:
                    company = item["value"]["name"]
                else:
                    company = EMPLOYEES[reporter]
            else:
                continue

        elif item["$type"] == "SingleOwnedIssueCustomField" and item["value"]:
            # specify the department
            if item["projectCustomField"]["field"]["name"] == "Направление":
                department = item["value"]["name"]
            # specify the product
            elif item["projectCustomField"]["field"]["name"] == "Продукт":
                product = item["value"]["name"]
            else:
                continue

        # specify the assignees
        elif item["$type"] == "MultiUserIssueCustomField" and item["value"]:
            assignees = [assignee_user["name"] for assignee_user in item["value"]]

        else:
            continue

    return IssueStat(issue, state, summary, reporter, parent, assignee, assignees, is_parent, deadline, customer,
                     department, product, company)


def request_work_items(login: str, issues: list[str]) -> RequestAttributes:
    """
    Get the request attributes for IssueWorkItem.

    :param str login: the user login
    :param issues: the issue names
    :type issues: list[str]
    :return: the request attributes.
    :rtype: RequestAttributes
    """
    return RequestAttributes(
        url="https://youtrack.protei.ru/api/workItems",
        fields=[
            "author(name)",
            "duration(minutes)",
            "issue(idReadable)"
        ],
        query=[
            ("work author", f"{login}"),
            ("work date", f"{start_period_stat()} .. {end_period_stat()}"),
            ("issue ID", ",".join(issues)),
        ],
        query_or=[],
        custom_fields=[],
        custom_fields_named=[
            ("startDate", f"{start_period_stat()}"),
            ('endDate', f"{end_period_stat()}"),
            ('author', f"{login}"),
        ]
    )


def work_items_json(issues: list[str]) -> tuple[dict[str, Any], ...]:
    """
    Get the response to the issue work item request.

    :param issues: the issue names to get the work items
    :type issues: list[str]
    :return: the response items.
    :rtype: tuple[dict[str, Any], ...]
    """
    work_items_unparsed: list = []
    for login in LOGINS:
        request_attributes: RequestAttributes = request_work_items(login, issues)
        url = request_attributes.url
        params = request_attributes.process_attributes()
        response_unparsed = req_get(url=url, headers=HEADERS_REQUEST, params=params).json()
        for work_item in response_unparsed:
            work_items_unparsed.append(work_item)
    return *work_items_unparsed,


def parse_response_work_item(response_item: dict[str, Any]):
    """
    Get the parameters from the response item.

    :param dict response_item: the item from the response
    :return: the issue name and modified spent time.
    :rtype: IssueWorkItemStat
    """
    # specify the issue name of the work item
    issue: str = response_item['issue']['idReadable']
    # specify the author name
    author: str = response_item['author']['name']
    # specify the spent time of the work item
    spent_time: Decimal = Decimal(divide(response_item['duration']['minutes'], 60)).quantize(Decimal("1.00"))
    # convert to the hours
    return IssueWorkItemStat(issue, author, spent_time)


def request_issues_parent(issue_names: list[str]):
    issues = ",".join(issue_names)
    return RequestAttributes(
        url="https://youtrack.protei.ru/api/issues",
        fields=[
            "idReadable",
            "reporter(fullName)",
            "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"
        ],
        query=[
            ("issue ID", issues),
        ],
        query_or=[],
        custom_fields=[
            "Продукт",
            "Направление",
            "Заказчик",
            "Компания ПРОТЕЙ"
        ],
        custom_fields_named=[]
    )


def issues_parent_json(issue_names: list[str]):
    request_attributes: RequestAttributes = request_issues_parent(issue_names)
    url = request_attributes.url
    params = request_attributes.process_attributes()
    response_unparsed = req_get(url=url, headers=HEADERS_REQUEST, params=params).json()
    issues_unparsed = [issue for issue in response_unparsed]
    return *issues_unparsed,


def parse_response_issue_parent(response_item: dict[str, Any]):
    print(response_item)

    issue: str = response_item["idReadable"]
    reporter: str = response_item["reporter"]["fullName"]
    customer: Optional[str] = None
    department: Optional[str] = None
    product: Optional[str] = None
    company: Optional[str] = None

    for item in response_item["customFields"]:
        if item["$type"] == "SingleOwnedIssueCustomField" and item["value"]:
            # specify the department
            if item["projectCustomField"]["field"]["name"] == "Направление":
                department = item["value"]["name"]
            # specify the product
            elif item["projectCustomField"]["field"]["name"] == "Продукт":
                product = item["value"]["name"]
            else:
                continue

        elif item["$type"] == "SingleEnumIssueCustomField":
            # specify the customer
            if item["projectCustomField"]["field"]["name"] == "Заказчик" and item["value"]:
                customer = item["value"]["name"]
            # specify the company
            elif item["projectCustomField"]["field"]["name"] == "Компания ПРОТЕЙ":
                if item["value"]:
                    company = item["value"]["name"]
                else:
                    company = EMPLOYEES[reporter]

    return issue, reporter, customer, department, product, company


def request_issues_no_assignee() -> RequestAttributes:
    """
    Get the request attributes for Issue.

    :return: the request attributes.
    :rtype: RequestAttributes
    """
    return RequestAttributes(
        url="https://youtrack.protei.ru/api/issues",
        fields=[
            "idReadable",
            "summary",
            "parent(issues(idReadable,reporter(fullName)))",
            "reporter(fullName)",
            "customFields(value,value(name),value(name,login),projectCustomField(field(name)))"
        ],
        query=[
            ("State", PROJECT_STATES),
            ("project", PROJECTS_REQUEST),
            ("has", "-Assignee"),
            ("has", "-Исполнители_Doc"),
            ("has", "-Исполнители_Doc_ST"),
            ("has", "-Исполнители_Archive"),
            ("has", "-Исполнители_Archive_ST"),
        ],
        query_or=[],
        custom_fields=[
            "State",
            "Дедлайн",
            "Продукт",
            "Направление",
            "Заказчик",
            "Компания ПРОТЕЙ",
        ],
        custom_fields_named=[]
    )


def issues_no_assignee_json() -> tuple[dict[str, Any], ...]:
    """
    Get the response to the issue request.

    :return: the response items.
    :rtype: tuple[dict[str, Any], ...]
    """
    issues_unparsed: list = []
    request_attributes: RequestAttributes = request_issues_no_assignee()
    url = request_attributes.url
    params = request_attributes.process_attributes()
    response_unparsed = req_get(url=url, headers=HEADERS_REQUEST, params=params).json()
    for issue in response_unparsed:
        issues_unparsed.append(issue)
    return *issues_unparsed,


def parse_response_issue_no_assignee(response_item: dict[str, Any]):
    """
    Parse the response in the dict format to get Issue.

    :param response_item: the response to parse
    :type response_item: dict[str, Union[str, dict]]
    :return: IssueStat.
    """
    # specify the issue name
    issue: str = response_item["idReadable"]
    parent = None
    # specify the issue summary
    summary: str = response_item["summary"]
    # specify the issue reporter
    reporter: str = response_item["reporter"]["fullName"]
    is_parent: bool = False
    state: Optional[str] = None
    deadline: Optional[datetime64] = datetime64("NaT")
    customer: Optional[str] = None
    department: Optional[str] = None
    product: Optional[str] = None
    company: Optional[str] = None
    assignee: Optional[str] = None
    assignees: list[str] = []

    for item in response_item["customFields"]:
        # specify the issue state
        if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField", "StateProjectCustomField"):
            state = item["value"]["name"]

        # specify the issue deadline
        elif item["$type"] == "DateIssueCustomField":
            deadline = convert_long_date(item["value"])

        elif item["$type"] == "SingleEnumIssueCustomField":
            # specify the customer
            if item["projectCustomField"]["field"]["name"] == "Заказчик" and item["value"]:
                customer = item["value"]["name"]
            # specify the company
            elif item["projectCustomField"]["field"]["name"] == "Компания ПРОТЕЙ":
                if item["value"]:
                    company = item["value"]["name"]
                else:
                    company = EMPLOYEES[reporter]
            else:
                continue

        elif item["$type"] == "SingleOwnedIssueCustomField" and item["value"]:
            # specify the department
            if item["projectCustomField"]["field"]["name"] == "Направление":
                department = item["value"]["name"]
            # specify the product
            elif item["projectCustomField"]["field"]["name"] == "Продукт":
                product = item["value"]["name"]
            else:
                continue

        else:
            continue

    return IssueStat(issue, state, summary, reporter, parent, assignee, assignees, is_parent, deadline, customer,
                     department, product, company)


class JoinStat:
    """
    Contain the instance to join Issues and IssueWorkItems.

    Parameters:
        wb --- the workbook to add the values;\n
        issue_stat --- the issue parameters;\n
        work_items --- the issue work item parameters;\n
        row --- the row;\n

    Properties:
        ws --- the worksheet to add the values;\n

    """

    def __init__(
            self,
            wb: Workbook,
            issue_stat: IssueStat,
            work_items: Optional[list[IssueWorkItemStat]] = None,
            row: int = None
    ):
        if work_items is None:
            work_items = []

        self.wb: Workbook = wb
        self.issue_stat: IssueStat = issue_stat
        self.work_items: Optional[list[IssueWorkItemStat]] = work_items
        self.row: Optional[int] = row

    def __str__(self):
        return f"JoinStat: wb = {str(self.wb)}, issue_stat = {str(self.issue_stat)}"

    def __repr__(self):
        work_items_repr = ", ".join([repr(work_item) for work_item in self.work_items]) if self.work_items else None
        return f"<JoinStat(wb={repr(self.wb)}, issue_stat={repr(self.issue_stat)}, work_items={work_items_repr}, " \
               f"row={self.row})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            work_items_format = ", ".join([format(work_item, "output") for work_item in self.work_items]) \
                if self.work_items else None
            return f"JoinStat: wb = {self.wb.sheetnames}, issue_stat = {format(self.issue_stat, 'output')}, " \
                   f"work_items = {work_items_format}, row = {self.row}"

    def __hash__(self):
        return hash((self.issue, self.state))

    def __key(self):
        return self.issue, self.state

    def __eq__(self, other):
        if isinstance(other, JoinStat):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, JoinStat):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __len__(self):
        return len(self.work_items)

    def __getitem__(self, item):
        if item in self.work_items:
            return self.work_items[item]
        else:
            raise KeyError(f"The key {item} is out of bounds.")

    def __bool__(self):
        return len(self) > 0

    def __lt__(self, other):
        if isinstance(other, JoinStat):
            return self.issue < other.issue
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, JoinStat):
            return self.issue > other.issue
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, JoinStat):
            return self.issue <= other.issue
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, JoinStat):
            return self.issue >= other.issue
        else:
            return NotImplemented

    def __getattribute__(self, attr):
        if attr == "deadline":
            deadline: datetime64 = object.__getattribute__(self.issue_stat, "deadline")
            if not isnat(deadline):
                return deadline.item()
            else:
                return None
        elif attr in ATTRS:
            return object.__getattribute__(self.issue_stat, attr)
        else:
            return object.__getattribute__(self, attr)

    @property
    def ws(self) -> Worksheet:
        """
        Get the worksheet.

        :return: the worksheet.
        :rtype: Worksheet
        """
        issue_type = getattr(self, "issue_type")
        if not issue_type:
            print(f"Invalid issue type {issue_type}, issue: {self.issue_stat.issue}.")
            return self.wb.active
        elif issue_type not in self.wb.sheetnames:
            print(f"The worksheet {issue_type} does not exist, so it has been created.")
            self.wb.create_sheet(f"{issue_type}")
        return self.wb[f"{issue_type}"]

    @property
    def users(self) -> set[str]:
        """
        Get the users from the issue work items.

        :return: the users.
        :rtype: list[str]
        """
        return set([work_item.author for work_item in self.work_items])

    @property
    def users_spent_time(self) -> tuple[tuple[str, Decimal], ...]:
        """
        Get the spent time values from the issue work items.

        :return: the spent time values.
        :rtype: list[Decimal]
        """
        dict_author_spent_time: dict[str, Decimal] = dict()
        for author in self.users:
            dict_author_spent_time[author] = Decimal("0")
        for work_item in self.work_items:
            author_work_item = work_item.author
            dict_author_spent_time[author_work_item] += work_item.spent_time
        user_spent_time = [(key, value) for key, value in dict_author_spent_time.items()]
        return *user_spent_time,

    @property
    def total_spent_time(self) -> Decimal:
        """
        Get the total spent time.

        :return: the total spent time.
        :rtype: Decimal
        """
        return sum(work_item.spent_time for work_item in self.work_items)

    def join_work_items(self):
        self.work_items = [
            IssueWorkItemStat(self.issue_stat.issue, user, spent_time) for user, spent_time in self.users_spent_time
        ]

    @property
    def hyperlink(self) -> str:
        """
        Specify the issue hyperlink.

        :return: the hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    def shift_cell(self, base: str, shift: int = 0) -> Optional[Cell]:
        """
        Get the cell shifted to some columns.

        :param str base: the base cell coordinate
        :param int shift: the number of columns, default = 0
        :return: the shifted cell.
        :rtype: Cell or None
        """
        base_row, base_col_idx = coordinate_to_tuple(base)
        if base_col_idx + shift > 0:
            shift_column = get_column_letter(base_col_idx + shift)
            return self.ws[f"{shift_column}{base_row}"]
        else:
            print("Incorrect coordinates.")
            return

    def parse_table(self):
        """Parse the instance to the table."""
        if self.row:
            for column, attr in COLUMN_ATTRS:
                self.ws[f"{column}{self.row}"].value = getattr(self, attr)
            self.ws[f"K{self.row}"].value = self.total_spent_time
            self.ws[f"B{self.row}"].hyperlink = self.hyperlink
            cell_base = f"K{self.row}"

            for user, spent_time in self.users_spent_time:
                # cell_shift = self.shift_cell(cell_base, 1)
                self.shift_cell(cell_base, 1).value = user
                self.shift_cell(cell_base, 2).value = spent_time
                cell_base = self.shift_cell(cell_base, 2).coordinate
            return
        else:
            print(f"The row is not specified, issue {self.issue}.")
            return

    def parse_table_no_assignee(self):
        if self.row:
            for column, attr in COLUMN_ATTRS_NO_ASSIGNEE:
                self.ws[f"{column}{self.row}"].value = getattr(self, attr)
            self.ws[f"B{self.row}"].hyperlink = self.hyperlink
            return
        else:
            print(f"The row is not specified, issue {self.issue}.")
            return


class Stats:
    issue_attrs: tuple[str, ...] = (
        "issue", "state", "summary", "reporter", "deadline", "customer", "department", "product", "company"
    )

    def __init__(self, path: Union[Path, str]):
        if not Path(path).resolve().exists():
            self.wb = Workbook()
        else:
            self.wb: Workbook = load_workbook(path)

        if IssueType.GENERAL.value not in self.wb.sheetnames:
            self.wb.create_sheet(IssueType.GENERAL.value)
        if IssueType.DEFENSE.value not in self.wb.sheetnames:
            self.wb.create_sheet(IssueType.DEFENSE.value)
        if IssueType.SPECIAL.value not in self.wb.sheetnames:
            self.wb.create_sheet(IssueType.SPECIAL.value)
        if IssueType.NON_ASSIGNED.value not in self.wb.sheetnames:
            self.wb.create_sheet(IssueType.NON_ASSIGNED.value)

        if "Sheet" in self.wb.sheetnames:
            del self.wb["Sheet"]

        self.dict_issues: dict[str, IssueStat] = dict()
        self.dict_work_items: dict[str, list[IssueWorkItemStat]] = dict()
        self.dict_join_stats: dict[str, JoinStat] = dict()

    def __str__(self):
        return f"Stats: join_stats: {self.dict_join_stats.values()}"

    def __repr__(self):
        return f"<Stats(dict_join_stats={self.dict_join_stats.values()})>"

    def __format__(self, format_spec):
        if format_spec == "output_work_items":
            lines: list[str] = [
                f"{key} = {format(value, 'output')}"
                for key, values in self.dict_work_items.items() for value in values
            ]
            return "\n".join(lines)
        if format_spec == "output_issues":
            lines: list[str] = [format(issue, "output") for issue in self.dict_issues.values()]
            return "\n".join(lines)
        if format_spec == "output":
            lines_issues: list[str] = [format(issue, "output") for issue in self.dict_issues.values()]
            lines_work_items: list[str] = [
                f"{key} = {format(value, 'output')}"
                for key, values in self.dict_work_items.items() for value in values
            ]
            lines: list[str] = [*lines_issues, *lines_work_items]
            return "\n".join(lines)

    def __len__(self):
        return len(self.dict_join_stats)

    def __bool__(self):
        return len(self) > 0

    def ws(self, title: str) -> Optional[Worksheet]:
        """
        Get the worksheet.

        :param str title: the worksheet title.
        :return: the worksheet.
        :rtype: Worksheet or None
        """
        if title in self.wb.sheetnames:
            return self.wb[title]
        else:
            print(f"The worksheet {title} does not exist.")
            return

    def issues_stat(self):
        """Specify the IssueStat instances."""
        for issue_unparsed in issues_json():
            issue = parse_response_issue(issue_unparsed)
            self.dict_issues[issue.issue] = issue
        for issue_not_assigned_unparsed in issues_no_assignee_json():
            issue_not_assigned = parse_response_issue_no_assignee(issue_not_assigned_unparsed)
            self.dict_issues[issue_not_assigned.issue] = issue_not_assigned

    @property
    def _issue_name_parent(self):
        return {
            issue_stat.parent: issue_stat_name for issue_stat_name, issue_stat in self.dict_issues.items()
            if issue_stat.is_parent
        }

    def issues_stat_parent(self):
        issues_unparsed = issues_parent_json(list(self._issue_name_parent))
        for issue_unparsed in issues_unparsed:
            item = parse_response_issue_parent(issue_unparsed)
            p_issue, p_reporter, p_customer, p_department, p_product, p_company = item
            issue_stat_name = self._issue_name_parent[p_issue]
            issue_stat: IssueStat = self.dict_issues[issue_stat_name]

            final_customer = issue_stat.customer if issue_stat.customer else p_customer
            final_department = issue_stat.department if issue_stat.department else p_department
            final_product = issue_stat.product if issue_stat.product else p_product
            final_company = issue_stat.company if issue_stat.company else p_company

            new_issue_stat = IssueStat(
                issue_stat.issue, issue_stat.state, issue_stat.summary, p_reporter, p_issue, issue_stat.assignee,
                issue_stat.assignees_project, False, issue_stat.deadline, final_customer, final_department,
                final_product, final_company)

            del issue_stat
            self.dict_issues[new_issue_stat.issue] = new_issue_stat

    def work_items_stat(self):
        """Specify the IssueWorkItemStat instances."""
        issue_names = [issue_stat for issue_stat in self.dict_issues]
        for work_item_unparsed in work_items_json(issue_names):
            work_item = parse_response_work_item(work_item_unparsed)
            if work_item.issue not in self.dict_work_items:
                self.dict_work_items[work_item.issue] = []
            self.dict_work_items[work_item.issue].append(work_item)

    def join_stats(self):
        """Get the JoinStat instances."""
        for issue_name, issue in self.dict_issues.items():
            self.dict_join_stats[issue_name] = JoinStat(self.wb, issue)
            if issue_name in self.dict_work_items:
                self.dict_join_stats[issue_name].work_items = self.dict_work_items[issue_name]

    @property
    def defense_stat(self):
        return [
            join_stat for join_stat in self.dict_join_stats.values()
            if join_stat.issue_type == IssueType.DEFENSE.value
        ]

    @property
    def spec_stat(self):
        return [
            join_stat for join_stat in self.dict_join_stats.values()
            if join_stat.issue_type == IssueType.SPECIAL.value
        ]

    @property
    def general_stat(self):
        return [
            join_stat for join_stat in self.dict_join_stats.values()
            if join_stat.issue_stat.issue_type == IssueType.GENERAL.value
        ]

    @property
    def not_assigned_stat(self):
        return [
            join_stat for join_stat in self.dict_join_stats.values()
            if join_stat.issue_stat.issue_type == IssueType.NON_ASSIGNED.value
        ]

    def parse_defense(self):
        row = 3
        for join_stat in self.defense_stat:
            join_stat.row = row
            join_stat.parse_table()
            row += 1

    def parse_spec(self):
        row = 3
        for join_stat in self.spec_stat:
            join_stat.row = row
            join_stat.parse_table()
            row += 1

    def parse_general(self):
        row = 3
        for join_stat in self.general_stat:
            join_stat.row = row
            join_stat.parse_table()
            row += 1

    def parse_not_assigned(self):
        row = 3
        for join_stat in self.not_assigned_stat:
            join_stat.row = row
            join_stat.parse_table()
            row += 1

    def make_sort(self):
        for sheet in self.wb.sheetnames:
            ws: Worksheet = self.wb[f"{sheet}"]
            for col_idx in range(1, 12):
                column = get_column_letter(col_idx)
                cell: Cell = ws[f"{column}2"]
                if cell.value:
                    ws.auto_filter.add_filter_column(col_idx - 1, blank=True)


def main():
    datetime_start = datetime.now()
    path = "C:\\Users\\tarasov-a\\Desktop\\stats.xlsx"
    stats = Stats(path)
    stats.issues_stat()
    stats.work_items_stat()
    # stats.issues_stat_parent()
    stats.join_stats()
    for join_stat in stats.dict_join_stats.values():
        join_stat.join_work_items()
    print("----------")
    print("Issue:")
    for issue in stats.dict_issues.values():
        print(format(issue, "output"))
    print("----------")
    print("WorkItem:")
    for work_items in stats.dict_work_items.values():
        for work_item in work_items:
            print(format(work_item, "output"))
    print("----------")
    print("JoinStat:")
    for join_stat in stats.dict_join_stats.values():
        print(format(join_stat, "output"))
    print("----------")
    # print("Format stats issues:")
    # print(format(stats, "output"))
    # print("----------")
    # print("Format stats work_items:")
    # print(format(stats, "output_work_items"))
    stats.parse_general()
    stats.parse_defense()
    stats.parse_spec()
    stats.parse_not_assigned()
    stats.wb.save(path)
    stats.wb.close()
    datetime_end = datetime.now()
    work_time = datetime_end - datetime_start
    print(f"Время работы: {str(work_time)}")


def new_main():
    fields: tuple[str, str] = ("fields", ",".join(
        ("idReadable", "summary", "parent(issues(idReadable,reporter(fullName)))", "reporter(fullName)",
         "customFields(value,value(name),value(name,login),projectCustomField(field(name)))")
    ))
    query: tuple[str, str] = ("query", ": ".join(
        ("issue ID", "DOC_ST-1745,DOC-685,DOC_ST-1509,DOC-1286,ARCH-2,DOC_ST-1375,ARCH-5,DOC-173")
    ))
    custom_fields: tuple[tuple[str, str], ...] = (
        ("customFields", "Assignee"),
        ("customFields", "State"),
        ("customFields", "Дедлайн"),
        ("customFields", "Продукт"),
        ("customFields", "Направление"),
        ("customFields", "Заказчик"),
        ("customFields", "Компания ПРОТЕЙ"),
        ("customFields", "Исполнители_Doc"),
        ("customFields", "Исполнители_Doc_ST"),
        ("customFields", "Исполнители_Archive"),
        ("customFields", "Исполнители_Archive_ST"),
    )
    params = (fields, query, *custom_fields,)
    response = req_get(url=RequestURL.ISSUE.value, headers=HEADERS_REQUEST, params=params).json()
    print(response)


if __name__ == "__main__":
    main()
    # new_main()
