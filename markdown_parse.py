import logging
import re
from logging import Logger
from re import Pattern, Match
from typing import Union, Any


logger: Logger = logging.getLogger(f"root.{__name__}")


class InvalidTextLineNo(BaseException):
    """Invalid number of the line in the markdown text"""


class MarkdownItemFilter:
    user_pattern: Pattern = re.compile(r".*\|\s?@([a-z\-]+)\s?\|")
    delivery_pattern: Pattern = re.compile(r".*(\d{3}\.\d{3}).*")

    def __init__(
            self,
            is_table: bool = False,
            is_text: bool = False):
        self._is_table: bool = is_table
        self._is_text: bool = is_text

    @staticmethod
    def string_to_lines(text: str, separator: str = "\n") -> list[str]:
        return text.split(separator)

    def get_table_lines(self, text: str):
        if self._is_table is False:
            logging.warning(f"Таблица не извлечена, поскольку не задана настройка извлечения таблицы")
            return
        else:
            lines: list[str] = self.string_to_lines(text)
            bounds: list[int] = [index for index, line in enumerate(lines) if not line]
            bound_to: int
            bound_from: int
            bound_from, bound_to = min(bounds) + 1, max(bounds)
            return lines[bound_from:bound_to]

    def get_user(self, text: str, line_no: int = -1):
        if line_no < 0 or line_no is None:
            logging.error(f"Некорректно задан номер строки, {line_no}")
            raise InvalidTextLineNo
        text_line: str = self.string_to_lines(text)[line_no]
        user_match: Match = re.match(self.user_pattern, text_line)
        if not user_match:
            logging.warning(f"В строке {line_no} не указан ни один пользователь")
        else:
            return user_match.group(1)

    def get_delivery(self, text: str):
        delivery_match: Match = re.match(self.delivery_pattern, text)
        if not delivery_match:
            logging.warning(f"В строке не указана ни одна поставка")
        else:
            return delivery_match.group(1)


class MarkdownText:
    def __init__(self, content: Union[str, list[str]], md_filter: MarkdownItemFilter):
        self.lines: list[str]
        if isinstance(content, str):
            self.lines = content.split("\n")
        else:
            self.lines = content
        self.md_filter: MarkdownItemFilter = md_filter


class DeliveryUnit:
    def __init__(
            self,
            name: str,
            documents: dict[str, str] = None,
            state: str = "Active"):
        if documents is None:
            documents = dict()
        self.name: str = name
        self.documents: dict[str, str] = documents
        self.state: str = state

    def assignees(self):
        return self.documents.values()


class MarkdownTableParser:
    def __init__(self, lines: list[str]):
        self.lines: list[str] = lines[2:]
        self._heading: str = lines[0]
        self.items: dict[str, Any] = dict()

    def get_items(self):
        pass
